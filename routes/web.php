<?php 

/*
CACHE:

1. register route via service for caching -> method, url, [slug=>table.column] (no more than 1 dynamic slug)
2. add userCacher middleware for route

*/

Route::get('/get-image/{src}/{w}/{h}', function ($img, $w, $h) {

    $img = str_replace('--', '/', $img);
    $tmpImage = str_replace('/storage/', '', $img);
    if (Storage::disk('public')->exists($tmpImage)) {
        $img = Storage::disk('public')->url($tmpImage);
        $newImage = \Image::cache(function ($image) use ($img, $w, $h) {
            if ($h !== 'null') {
                return $image->make($img)->resize($w, $h);
            } else {
                return $image->make($img)->resize($w, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }
        }, 120, false);
        return Response::make($newImage, 200, ['Content-Type' => 'image/jpeg']);
    }
    return Response::make($img, 200, ['Content-Type' => 'image/jpeg']);
});

/*
        |--------------------------------------------------------------------------
        | Web Routes
        |--------------------------------------------------------------------------
        |
        | Here is where you can register web routes for your application. These
        | routes are loaded by the RouteServiceProvider within a group which
        | contains the "web" middleware group. Now create something great!
        |
        */
/*************** SWITCH LANGUAGES *********************************************************/

Route::get('setlocale/{lang}', 'Front\SwitchLanguageController@switchLanguage')->name('setlocale');

/*************** FRONT ROUTS **************************************************************/

Route::get('module/pickupBicycle', 'Front\Catalog\HomeController@chooseBicycle')->name('module.pickupBicycle');

//\ResponseCacher::regiserCacheableRoute('GET', \Request::root() . '/choose-bicycle/{products_id}', ['products_id' => 'products.id']);
Route::get('/choose-bicycle/{products_id}', 'Front\Catalog\HomeController@chooseBicycleView')->middleware('userCacher');

//\ResponseCacher::regiserCacheableRoute('GET', \Request::root() . '/module/marketplace', []);
Route::get('module/marketplace', 'Front\Catalog\HomeController@marketplace')->name('module.marketplace')->middleware('userCacher');

Route::get('module/hotline', 'Front\Catalog\HomeController@hotline')->name('module.hotline');

Route::get('module/hotline-sales', 'Front\Catalog\HomeController@hotlineSales')->name('module.hotline-sales');


/*************** RESET PASSWORD ***********************************************************/

Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
Route::get('password/reset/{token} ', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

/*************** FRONT ROUTS **************************************************************/

//socialize
Route::get('/redirect/{service}', 'SocialAuthController@redirect')->name('socialize.redirect');
Route::get('/callback/{service}', 'SocialAuthController@callback')->name('socialize.callback');

//\ResponseCacher::regiserCacheableRoute('GET', \Request::root() . '/city/{slug}', ['slug' => 'landing_pages.url']);
Route::get('/city/{slug}', 'Front\Information\LandingPageController@index')->name('city.index')->middleware('userCacher');
Route::get('/landing/{slug}', function ($slug) {
    return Redirect::to('/city/' . $slug, 301);
});

Route::group(['namespace' => 'Front'], function () {
    //new post
    Route::get('newpost/city', 'Checkout\Shipping\NewPostController@getCity')->name('newpost.city');
    Route::get('newpost/stores', 'Checkout\Shipping\NewPostController@getStores')->name('newpost.stores');

    //product actions
    Route::get('getProductPrice', 'Catalog\ProductController@calculateProductPrices')->name('product.calculatePrices');
    Route::post('product/addReview', 'Catalog\ProductController@addReview')->name('product.addReview');

    //popups
    Route::post('headerCallback', 'Catalog\PopupProcessorController@headerCallback')
        ->name('popupProcessor.headerCallback');
    Route::post('oneClickBuy', 'Catalog\PopupProcessorController@oneClickBuy')->name('popupProcessor.oneClickBuy');
    Route::post('preorder', 'Catalog\PopupProcessorController@preorder')->name('popupProcessor.preorder');

    //wishlist
    Route::put('wishlist', 'Catalog\WishlistController@add')->name('wishlist.add');
    Route::delete('wishlist', 'Catalog\WishlistController@delete')->name('wishlist.delete');

    //compare
    Route::put('compare', 'Catalog\CompareController@add')->name('compare.add');
    Route::delete('compare', 'Catalog\CompareController@delete')->name('compare.delete');

    //cart
    Route::group(['prefix' => 'cart', 'namespace' => 'Cart'], function () {
        Route::post('add', 'CartController@add')->name('cart.add');
        Route::get('add-credit/', 'CartController@addCredit')->name('cart.add-credit');
        Route::post('delete', 'CartController@delete')->name('cart.delete');
        Route::post('change', 'CartController@changeItemQuantity')->name('cart.changeQuantity');
    });
});

Route::group(['prefix' => \App\Http\Middleware\BeforeLanguagePrefix::getPrefix()], function () {
    Route::group(['prefix' => 'cabinet', 'namespace' => 'Front\Cabinet', 'middleware' => ['auth:web']], function () {
        Route::get('/', 'CabinetController@index')->name('cabinet.index');
        Route::post('/', 'CabinetController@update');
        Route::get('/orderHistory', 'CabinetController@orderHistory')->name('cabinet.orderHistory');
  //      \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/cabinet' . '/changePassword', []);
        Route::get('/changePassword', 'CabinetController@password')->name('cabinet.changePassword')->middleware('userCacher');
        Route::post('/changePassword', 'CabinetController@updatePassword');
        Route::get('/wishlist', 'CabinetController@wishlist')->name('cabinet.wishlist');
        Route::get('/purchasedGoods', 'CabinetController@purchasedGoods')->name('cabinet.purchasedGoods');
    });

    //auth and register
    Route::group(['namespace' => 'Auth'], function () {
        Route::post('/login', 'LoginController@login')->name('login');
        Route::get('/logout', 'LoginController@logout')->name('logout');
        Route::get('/checkAuth', 'LoginController@checkAuth')->name('checkAuth');
        Route::get('/register', 'RegisterController@showRegistrationForm')->name('register');
        Route::post('/register', 'RegisterController@register');
    });


    //permanent pages
    Route::group(['namespace' => 'Front'], function () {
        //checkout
        Route::group(['namespace' => 'Checkout'], function () {
            Route::match(['POST', 'GET'], 'checkout', 'CheckoutController@index')->name('checkout');
            Route::match(['POST', 'GET'], 'checkout/shippingInfo', 'CheckoutController@getShippingInfo')
                ->name('checkout.shippingInfo');
            Route::match(['POST', 'GET'], 'checkout/paymentInfo', 'CheckoutController@getPaymentInfo')
                ->name('checkout.paymentInfo');
            Route::post('checkout/confirm', 'CheckoutController@confirm')->name('checkout.confirm');
            Route::get('success', 'SuccessController@index')->name('success');

    //        \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/failure', []);
            Route::get('failure', 'FailureController@index')->name('failure')->middleware('userCacher');
            Route::match(['GET', 'POST'], 'liqpay/renderstatus', 'CheckoutController@liqPayRenderStatus')
                ->name('liqpay.renderStatus');
            Route::post('liqpay/callback', 'CheckoutController@liqPayCallback')->name('liqpay.callback');
            Route::match(['GET', 'POST'], 'payparts/renderstatus', 'CheckoutController@paypartsRenderStatus')->name('payparts.renderstatus');
            Route::match(['GET', 'POST'], 'payparts/confirm', 'CheckoutController@paypartsConfirm')->name('payparts.confirm');
            Route::match(['GET', 'POST'], 'payparts/cancel', 'CheckoutController@paypartsCancel')->name('payparts.cancel');
        });

        Route::group(['namespace' => 'Information'], function () {
            Route::get('about_us', 'AboutUsController@index')->name('about_us');

      //      \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/guarantee', []);
            Route::get('guarantee', 'Guarantee@index')->name('guarantee')->middleware('userCacher');

        //    \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/credit', []);
            Route::get('credit', 'Credit@index')->name('credit')->middleware('userCacher');

          //  \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/delivery', []);
            Route::get('delivery', 'DeliveryController@index')->name('delivery')->middleware('userCacher');

            //\ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/contacts', []);
            Route::get('contacts', 'ContactsController@index')->name('contacts')->middleware('userCacher');
            Route::post('contacts', 'ContactsController@question')->name('contacts.add');

           // \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/faq', []);
            Route::get('faq', 'FaqController@index')->name('faq')->middleware('userCacher');

           // \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/offer', []);
            Route::get('offer', 'OfferController@index')->name('offer')->middleware('userCacher');

           // \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/privacy-policy', []);
            Route::get('privacy-policy', 'PrivacyPoliceController@index')->name('privacy-policy')->middleware('userCacher');

           // \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/testimonials', []);
            Route::get('testimonials', 'TestimonialController@index')->name('testimonials')->middleware('userCacher');
            Route::post('testimonials', 'TestimonialController@add')->name('testimonials.add');
        });

        Route::group(['namespace' => 'Catalog'], function () {
            
           // \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . '/', []);
            Route::get('/', 'HomeController@index')->name('home')->middleware('userCacher');
            Route::post('subscribe', 'PopupProcessorController@subscribe')->name('subscribe');

            Route::get('new', 'CategoryController@newProducts')->name('new');
            Route::get('top', 'CategoryController@top')->name('top');


           // \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/specials', []);
            Route::get('specials', 'SpecialController@list')->name('special.list')->middleware('userCacher');

            /* \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/special/{slug}', ['slug' => 
'specials.slug']);*/
            Route::get('special/{slug}', 'SpecialController@show')->name('special.show')->middleware('userCacher');
            Route::post('special', 'SpecialController@question')->name('special.question');

            /*\ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/shares/{slug}', ['slug' => 
'shares.slug']);*/
            Route::get('shares/{slug}', 'ShareController@show')->name('share.show')->middleware('userCacher');

            /*\ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/shares-products/{slug}', ['slug' 
=> 
'shares.slug']);*/
            Route::get('shares-products/{slug}', 'ShareController@productList')->name('share.product-list')->middleware('userCacher');

           // \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/news', []);
            Route::get('news', 'NewsController@list')->name('news.list')->middleware('userCacher');

           /* \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/news/{slug}', ['slug' => 
'newses.slug']);*/
            Route::get('news/{slug}', 'NewsController@show')->name('news.show')->middleware('userCacher');
            Route::post('news/addReview', 'NewsController@addReview')->name('news.addReview');

            //\ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/manufacturers', []);
            Route::get('manufacturers', 'ManufacturerController@list')->name('manufacturer.list')->middleware('userCacher');
            Route::get('manufacturer/{slug}/{filter?}', 'ManufacturerController@show')->name('manufacturer.show');

            Route::get('search/{filter?}', 'SearchController@index')->name('search');
            Route::get('ajaxSearch', 'SearchController@ajaxSearch')->name('search.ajax');

           // \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/compare/categories', []);
            Route::get('compare/categories', 'CompareController@categoryList')->name('compare.categoryList')->middleware('userCacher');

           /* \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/compare/category/{id}', ['id' => 
'categories.id']);*/
            Route::get('compare/category/{id}', 'CompareController@productList')->name('compare.productList')->middleware('userCacher');

           /* \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/product/{slug}', ['slug' => 
'products.slug']);
            \ResponseCacher::regiserCacheableRoute('GET', \Request::root() . \App\Http\Middleware\BeforeLanguagePrefix::getPrefix() . '/{category_path}', ['category_path' => 
'categories.slug']);*/
            //dynamic page (category or product)
            Route::get('product/{slug}', 'ProductController@index')->name('product_path')->middleware('userCacher');
            Route::get('{category_path}', 'CategoryController@index')->name('category_path')->where('category_path', '.+')->middleware('userCacher');
        });
    });
});


Route::middleware(['pagespeed'])->group(function () {

});


