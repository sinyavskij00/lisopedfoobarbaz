<?php
return [
    'title' => 'Личный кабинет',
    'sidebar' => [
        'personal_info' => 'Персональные данные',
        'change_password' => 'Сменить пароль',
        'order_history' => 'История заказов',
        'wishlist' => 'Список желаний',
        'purchased_goods' => 'Оставить отзыв о товаре',
        'logout' => 'Выход'
    ],
    'orders_history' => [
        'order_number' => 'Номер заказа',
        'order_number_2' => 'Заказ № :number',
        'order_sum' => 'Сумма',
        'order_status' => 'Статус',
        'order_date' => 'Дата заказа',
        'product_name' => 'Наименование',
        'product_ttn' => '№ТТН Новой Почты',
        'product_price' => 'Цена',
        'product_quantity' => 'Количество',
        'product_sum' => 'Сумма',
    ],
    'password' => [
        'enter_password' => 'Старый пароль',
        'enter_new_password' => 'Новый пароль',
        'confirm_password' => 'Еще раз новый пароль',
        'submit' => 'Сохранить',
        'success' => 'Пароль изменен'
    ],
    'wishlist' => [
        'image' => 'Фото',
        'product' => 'Товар',
        'price' => 'Цена',
        'text_available' => 'Есть в наличии',
        'text_not_available' => 'Нет в наличии',
    ],
    'personal_info' => [
        'fields' => [
            'first_name' => 'Имя',
            'last_name' => 'Фимилия',
            'birthday' => 'День рожденья',
            'address' => 'Адрес',
            'city' => 'Город',
            'email' => 'Email',
        ],
        'descriptions' => [
            'name' => 'Имя и фамилия для оформления и доставки заказов.',
            'email' => 'Почта нужна для отправки пароля, информации о заказах.',
            'telephone' => 'Телефон для связи по поводу заказов.',
            'birthday' => 'День рождения, нужен что бы мы тебя поздравили.',
            'address' => 'Домашний адрес, нужен для доставки заказа курьером.',
            'new_post' => 'Номер отделения Новой Почты для получения посылок.',
        ],
        'choose_store' => 'Выберите отделение',
        'subscribe' => [
            'title' => 'Хочу узнавать об акциях и спецпредложениях',
            'yes' => 'Да',
            'no' => 'Нет',
        ],
        'submit' => 'Сохранить'
    ],
    'purchased_goods' => [
        'title' => 'Оставить отзыв о купленном товаре',
        'photo' => 'Фото',
        'product' => 'Товар',
        'btn_feedback' => 'Оставить отзыв'
    ]
];
