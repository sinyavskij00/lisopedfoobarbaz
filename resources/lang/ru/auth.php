<?php
return [
    'forgot_password' => [
        'link_was_send' => 'Ссылка для восстановления пароля была отправлена на твой Email.',
        'link_wasnt_send' => 'Произошла ошибка, ссылка не была отправлена.'
    ],
    'login' => [
        'wrong_credentials' => 'Логин и/или пароль неправильный'
    ],
    'register' => [
        'title' => 'Регистрация',
        'form' => [
            'title' => 'Основные данные',
            'placeholder_first_name' => 'Имя*',
            'placeholder_last_name' => 'Фимилия*',
            'placeholder_email' => 'Email*',
            'placeholder_telephone' => 'Телефон*',
            'password_title' => 'Пароль',
            'placeholder_password' => 'Пароль*',
            'placeholder_confirm_password' => 'Повторите пароль*',
            'btn_submit' => 'Зарегистрироваться'
        ]
    ]
];