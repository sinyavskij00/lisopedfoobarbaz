<?php
return [
    'title' => 'Личный кабинет',
    'sidebar' => [
        'personal_info' => 'Персональные данные',
        'change_password' => 'Сменить пароль',
        'order_history' => 'История заказов',
        'wishlist' => 'Список желаний',
        'logout' => 'Выход'
    ],
    'orders_history' => [
        'order_number' => 'Номер заказа',
        'order_sum' => 'Сумма',
        'order_status' => 'Статус',
        'order_date' => 'Дата заказа',
        'product_name' => 'Наименование',
        'product_price' => 'Цена',
        'product_quantity' => 'Количество',
        'product_sum' => 'Сумма',
    ],
    'password' => [
        'enter_password' => 'Введи старый пароль',
        'enter_new_password' => 'Введи новый пароль',
        'confirm_password' => 'Повтори новый пароль',
        'submit' => 'Сохранить',
        'success' => 'Пароль изменен'
    ],
    'wishlist' => [
        'image' => 'Фото',
        'product' => 'Товар',
        'price' => 'Цена',
        'text_available' => 'Есть в наличии',
        'text_not_available' => 'Нет в наличии',
    ],
    'personal_info' => [
        'fields' => [
            'first_name' => 'Имя',
            'last_name' => 'Фимилия',
            'birthday' => 'День рожденья',
            'address' => 'Адрес',
            'city' => 'Город',
            'email' => 'email',
        ],
        'descriptions' => [
            'name' => 'Имя и фамилия для оформления и доставки заказов.',
            'email' => 'Почта нужна для отправки пароля, информации о заказах.',
            'telephone' => 'Телефон для связи по поводу заказов.',
            'birthday' => 'День рождения, нужен что бы мы тебя поздравили.',
            'address' => 'Домашний адрес, нужен для доставки заказа курьером.',
            'new_post' => 'Номер отделения Новой Почты для получения посылок.',
        ],
        'choose_store' => 'Выберите отделение',
        'subscribe' => [
            'title' => 'Хочу узнавать об акциях и спецпредложениях',
            'yes' => '',
            'no' => '',
        ],
        'submit' => 'Сохранить'
    ]
];