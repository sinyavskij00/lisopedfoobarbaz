@extends('mail.layout')
@section('content')
    <table class="table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tbody style="box-sizing: border-box;">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;"><b style="box-sizing: border-box;">{{__('mails.orders.status_changed.title')}}</b></td>
        </tr>
        </tbody>
    </table>

    <table class="main table order-description" style="box-sizing: border-box; font-size: .9em; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tbody style="box-sizing: border-box;">
        @if(!empty($publicOrderId))
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.orders.status_changed.sub_title')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$publicOrderId}}</td>
            </tr>
        @endif
        @if(isset($orderHistory->status) && isset($orderHistory->status->localDescription))
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.orders.status_changed.status')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{ $orderHistory->status->localDescription->name }}</td>
            </tr>
        @endif
        @if(!empty($orderHistory->public_comment))
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.orders.status_changed.comment')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{!! $orderHistory->public_comment !!}</td>
            </tr>
        @endif
        </tbody>
    </table>

    @if(isset($orderHistory->order) && isset($orderHistory->order->skus))
        @include('mail.order_skus_table', ['orderSkus' => $orderHistory->order->skus])
    @endif

    @if(isset($billingAccount))
        <table class="table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
            <tbody style="box-sizing: border-box;">
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box;"><b style="box-sizing: border-box;">{{__('mails.orders.status_changed.billing_account.title')}}</b></td>
            </tr>
            </tbody>
        </table>

        <table class="main table order-description" style="box-sizing: border-box; font-size: .9em; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
            <tbody style="box-sizing: border-box;">
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.orders.status_changed.billing_account.name')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$billingAccount->getAttributeValue('name')}}</td>
            </tr>
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.orders.status_changed.billing_account.employer_number')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$billingAccount->getAttributeValue('employer_number')}}</td>
            </tr>
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.orders.status_changed.billing_account.bank_number')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$billingAccount->getAttributeValue('bank_number')}}</td>
            </tr>
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.orders.status_changed.billing_account.checking_account')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$billingAccount->getAttributeValue('checking_account')}}</td>
            </tr>
            </tbody>
        </table>
    @endif
    @if(isset($billingCart))
        <table class="table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
            <tbody style="box-sizing: border-box;">
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box;"><b style="box-sizing: border-box;">{{__('mails.orders.status_changed.billing_cart.title')}}</b></td>
            </tr>
            </tbody>
        </table>

        <table class="main table order-description" style="box-sizing: border-box; font-size: .9em; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
            <tbody style="box-sizing: border-box;">
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.orders.status_changed.billing_cart.name')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$billingCart->getAttributeValue('name')}}</td>
            </tr>
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.orders.status_changed.billing_cart.number')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$billingCart->getAttributeValue('number')}}</td>
            </tr>
            </tbody>
        </table>
    @endif
@endsection
