@extends('mail.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px 0; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box; padding: 20px 0 8px;"><h6 style="font-size: 1em; margin: 0 0 1.3em; box-sizing: border-box; margin-bottom: 0; font-weight: 600;">Здравствуй, <span class="accent" style="box-sizing: border-box; color: #11ad62;">{{$order->getFullName()}}!</span></h6></td>
        </tr>
    </table>

    <table class="main table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tbody style="box-sizing: border-box;">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;">
                <p class="m-0" style="box-sizing: border-box; margin: 0;">Заказ <b class="accent" style="box-sizing: border-box; color: #11ad62;">№{{$order->public_id}}</b> оформлен.</p>
                <p class="m-b-xs" style="box-sizing: border-box; margin: 0 0 .9em;">Спасибо за заказ. Мы его уже обрабатываем и скоро тебе позвоним.</p>
                <br style="box-sizing: border-box;">
                <p class="m-0" style="box-sizing: border-box; margin: 0;">В твоем заказе:</p>
            </td>
        </tr>
        </tbody>
    </table>

    @include('mail.order_skus_table', ['orderSkus' => $order->skus])

    <table class="table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;">
                <div class="order__description" style="box-sizing: border-box; padding-top: 10px;">
                    <p style="box-sizing: border-box; margin: 0 0 5px;">Получатель: <b style="box-sizing: border-box;">{{$order->getFullName()}}</b></p>
                    <p style="box-sizing: border-box; margin: 0 0 5px;">Адрес доставки: <b style="box-sizing: border-box;">{{$order->address}}</b></p>
                    <p style="box-sizing: border-box; margin: 0 0 5px; margin-bottom: 2.5em;">Способ оплаты: <b style="box-sizing: border-box;">{{$order->payment_method}}</b></p>
                </div>
                <p style="box-sizing: border-box; margin: 0 0 1.3em;">Отслеживай статус заказа в <a href="{{route('cabinet.orderHistory')}}" rel="nofollow" target="_blank" style="box-sizing: border-box; color: #333;">личном кабинете</a></p>
            </td>
        </tr>
    </table>
    <!-- Main == end ==============================================-->
@endsection

