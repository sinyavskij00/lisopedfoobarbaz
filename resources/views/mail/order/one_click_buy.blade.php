@extends('mail.layout')
@section('content')
    <table class="table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tbody style="box-sizing: border-box;">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;"><b style="box-sizing: border-box;">{{__('popup.one_click_buy.letter_title')}}</b></td>
        </tr>
        </tbody>
    </table>

    <table class="main table order-description" style="box-sizing: border-box; font-size: .9em; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tbody style="box-sizing: border-box;">
        @if(!empty($phone))
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('popup.one_click_buy.text_phone')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$phone}}</td>
            </tr>
        @endif
        @if(!empty($name))
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">Имя:</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$name}}</td>
            </tr>
        @endif
        @if(isset($sku))
            @if(isset($sku->product) && isset($sku->product->localDescription) && !empty($sku->product->localDescription->name))
                <tr style="box-sizing: border-box;">
                    <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('popup.one_click_buy.text_product_name')}}</td>
                    <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$sku->product->localDescription->name}}</td>
                </tr>
            @endif
            @if(!empty($sku->model))
                <tr style="box-sizing: border-box;">
                    <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('popup.one_click_buy.text_product_model')}}</td>
                    <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{isset($sku->model) ? $sku->model : ''}}</td>
                </tr>
            @endif
            @if(!empty($sku->sku))
                <tr style="box-sizing: border-box;">
                    <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('popup.one_click_buy.text_product_sku')}}</td>
                    <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$sku->sku}}</td>
                </tr>
            @endif
        @endif
        </tbody>
    </table>
@endsection