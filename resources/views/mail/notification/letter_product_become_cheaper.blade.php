@extends('mail.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px 0; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box; padding: 20px 0 8px;"><h6 style="font-size: 1em; margin: 0 0 1.3em; box-sizing: border-box; margin-bottom: 0; font-weight: 600;">Здравствуй, <span class="accent" style="box-sizing: border-box; color: #11ad62;">{{$user->first_name . ' ' . $user->last_name}}!</span></h6></td>
        </tr>
    </table>

    <table class="main table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tbody style="box-sizing: border-box;">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;">
                <p class="m-0" style="box-sizing: border-box; margin: 0;">Товары из списка желания стали дешевле:</p>
            </td>
        </tr>
        </tbody>
    </table>

    <table class="main table table__revies-products" style="box-sizing: border-box; border: 1px solid #ddd; font-size: .9em; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tbody style="box-sizing: border-box;">
        @if(!empty($sku))
            <tr class="t-a-c" style="box-sizing: border-box; text-align: center;" align="center">
                <td style="box-sizing: border-box; padding: 10px 7px; background-color: #fafafa;" bgcolor="#fafafa">
                    <a href="{{route('product_path', $sku->product->slug)}}" rel="nofollow" target="_blank" style="box-sizing: border-box; color: #333;">
                        <img class="m-auto" src="{{$catalogService->resize($sku->images->first(), 90, 60)}}" alt="{{isset($sku->product) && isset($sku->product->localDescription)
                             ? $sku->product->localDescription->name
                             : ''}}" style="box-sizing: border-box; display: block; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;">
                    </a>
                </td>
                <td class="price" style="box-sizing: border-box; padding: 10px 7px; background-color: #fafafa; white-space: nowrap;" bgcolor="#fafafa"><span style="box-sizing: border-box;">{{$catalogService->format($catalogService->calculate($sku->price))}}</span></td>
                <td style="box-sizing: border-box; padding: 10px 7px; background-color: #fafafa;" bgcolor="#fafafa">
                    <a href="{{route('product_path', $sku->product->slug)}}" rel="nofollow" target="_blank" class="name" style="box-sizing: border-box; color: #333;">{{isset($sku->product) && isset($sku->product->localDescription)
                             ? $sku->product->localDescription->name
                             : ''}}</a>
                </td>
                <td style="box-sizing: border-box; padding: 10px 7px; background-color: #fafafa;" bgcolor="#fafafa"><a href="{{route('product_path', $sku->product->slug)}}" rel="nofollow" target="_blank" class="btn" style="box-sizing: border-box; background-color: #11ad62; display: inline-block; text-decoration: none; line-height: 1; color: #fff; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; -webkit-transition: all .3s ease; -moz-transition: all .3s ease; -ms-transition: all .3s ease; -o-transition: all .3s ease; transition: all .3s ease; padding: 9px 20px;"><span style="box-sizing: border-box;">Купить</span></a>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
    <br style="box-sizing: border-box;">
    <br style="box-sizing: border-box;">
    <!-- Main == end ==============================================-->
@endsection

