@extends('mail.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px 0; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;"><td style="box-sizing: border-box; padding: 20px 0 8px;"><h6 style="font-size: 1em; margin: 0 0 1.3em; box-sizing: border-box; margin-bottom: 0; font-weight: 600;">Здравствуй, <span class="accent" style="box-sizing: border-box; color: #11ad62;">{{$name}}!</span></h6></td></tr>
    </table>

    <table class="main table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;">
                <p class="m-0" style="box-sizing: border-box; margin: 0;">Мы получили  твой запрос на смену пароля.</p>
                <p style="box-sizing: border-box; margin: 0 0 1.3em;">Для этого нажми на кнопку.</p>
                <p class="t-a-c" style="box-sizing: border-box; text-align: center; margin: 0 0 1.3em;"><a href="{{$url}}" class="btn" style="box-sizing: border-box; padding: 10px 30px; background-color: #11ad62; display: inline-block; text-decoration: none; line-height: 1; color: #fff; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; -webkit-transition: all .3s ease; -moz-transition: all .3s ease; -ms-transition: all .3s ease; -o-transition: all .3s ease; transition: all .3s ease;"><span style="box-sizing: border-box;">Изменить пароль</span></a></p>
                <p style="box-sizing: border-box; margin: 0 0 1.3em;">Больше не забывай :)</p>
            </td>
        </tr>
    </table>
    <!-- Main == end ==============================================-->
@endsection
