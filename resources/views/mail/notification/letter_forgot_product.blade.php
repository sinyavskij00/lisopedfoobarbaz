@extends('mail.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px 0; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box; padding: 20px 0 8px;"><h6 style="font-size: 1em; margin: 0 0 1.3em; box-sizing: border-box; margin-bottom: 0; font-weight: 600;">Здравствуй, <span class="accent" style="box-sizing: border-box; color: #11ad62;">{{$user->first_name . ' ' . $user->last_name}}!</span></h6></td>
        </tr>
    </table>

    <table class="main table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tbody style="box-sizing: border-box;">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;">
                <p style="box-sizing: border-box; margin: 0 0 1.3em;">В корзине есть забытый товар. Если тебе нужна консультация, позвони нам или закажи обратный звонок на
                    <a href="https://lisoped.ua/" rel="nofollow" target="_blank" style="box-sizing: border-box; color: #333;">сайте</a>.</p>
            </td>
        </tr>
        </tbody>
    </table>

    @if(!empty($skus))
        <table class="main table table__bordered" style="box-sizing: border-box; font-size: .9em; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
            <tbody style="box-sizing: border-box;">
            @foreach($skus as $sku)
                @if(!empty($sku->product))
                    <tr class="t-a-c" style="box-sizing: border-box; text-align: center;" align="center">
                        <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">
                            <a href="{{route('product_path', $sku->product->slug)}}" rel="nofollow" target="_blank" style="box-sizing: border-box; color: #333;">
                                <img class="m-auto" src="{{$catalogService->resize($sku->images->first(), 90, 60)}}" alt="{{$sku->product->localDescription->name}}" style="box-sizing: border-box; display: block; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;">
                            </a>
                        </td>
                        <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">
                            <a href="{{route('product_path', $sku->product->slug)}}" rel="nofollow" target="_blank" style="box-sizing: border-box; color: #333;">{{isset($sku->product) && isset($sku->product->localDescription) ? $sku->product->localDescription->name : ''}}</a>
                        </td>
                        <td class="price t-a-c" style="box-sizing: border-box; text-align: center; border: 1px solid #ccc; padding: 5px;" align="center"><span style="box-sizing: border-box;">{{$catalogService->format($catalogService->calculate($sku->price))}}</span></td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    @endif
    <br style="box-sizing: border-box;">
    <!-- Main == end ==============================================-->
@endsection
