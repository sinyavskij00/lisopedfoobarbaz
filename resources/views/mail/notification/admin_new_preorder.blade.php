@extends('mail.layout')
@section('content')
    <table class="table">
        <tbody>
        <tr>
            <td><b>{{__('mails.preorder.admin.text', ['number' => (isset($number) ? $number : '')])}}</b></td>
        </tr>
        </tbody>
    </table>
@endsection
