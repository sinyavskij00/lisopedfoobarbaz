@extends('mail.layout')
@section('content')
    <table class="main table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;">
                <p style="box-sizing: border-box; margin: 0 0 1.3em;">На сайте опубликован новый отзыв. <a href="{{url('/admin/testimonials')}}/{{$id}}">Перейти к просмотру</a>.</p>
                <div class="review" style="box-sizing: border-box; padding: 15px 20px; border: 1px solid #ccc; border-radius: 10px; margin-bottom: 1.3em; font-style: italic; background: #fafafa; text-align: center;">
                    <span class="line" style="box-sizing: border-box;"></span>
                    <p class="m-0" style="box-sizing: border-box; margin: 0;">"{{$text}}"</p>
                </div>
            </td>
        </tr>
    </table>
    <!-- Main == end ==============================================-->
@endsection
