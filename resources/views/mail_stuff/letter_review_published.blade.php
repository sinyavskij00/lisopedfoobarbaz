@extends('mail_stuff.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table">
        <tr><td><h6>Здравствуй, <span class="accent">Артур Титов!</span></h6></td></tr>
    </table>

    <table class="main table">
        <tr>
            <td>
                <p>Твой <a href="https://lisoped.ua/testimonials" rel="nofollow" target="_blank">отзыв</a> опубликован.</p>
                <div class="review">
                    <span class="line"></span>
                    <p class="m-0">"Заказывал велосипед ребенку, были проблемы на Новой Почте, менеджер Сергей помог решить ситуацию, огромное ему спасибо!"</p>
                </div>
                <p>Спасибо за обратную связь!</p>
            </td>
        </tr>
    </table>
    <!-- Main == end ==============================================-->
@endsection