@extends('mail_stuff.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table">
        <tr>
            <td><h6>Здравствуй, <span class="accent">Артур Титов!</span></h6></td>
        </tr>
    </table>

    <table class="main table">
        <tbody>
        <tr>
            <td>
                <p class="m-0">Заказ <b class="accent">№12345678901234</b> оформлен.</p>
                <p class="m-b-xs">Спасибо за заказ. Мы его уже обрабатываем и скоро тебе позвоним.</p>
                <br>
                <p class="m-0">В твоем заказе:</p>
            </td>
        </tr>
        </tbody>
    </table>

    <table class="main table order-description">
        <thead>
        <tr class="t-a-l">
            <th>Фото</th>
            <th>Товар</th>
            <th>Характеристики</th>
            <th>Цена</th>
            <th>К-ство</th>
            <th>Сумма</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <a href="https://lisoped.ua/" rel="nofollow" target="_blank">
                    <img src="https://lisoped.ua/storage/cache/shop_images/import/26-Formula-HUMMER-DD-cherno-zelenyiy-2018-1361-1980x1360-90-60.jpg"
                         alt="Romet Orkan 3.0M">
                </a>
            </td>
            <td>
                <div class="product-name"><a href="https://lisoped.ua/" rel="nofollow" target="_blank">Romet Orkan
                        3.0M</a></div>
                <div class="ttn">Номер ТТН Новой Почты: 12345678901234</div>
            </td>
            <td>
                <table class="attributes">
                    <tr class="attribute">
                        <td class="attribute-name">Размер:</td>
                        <td class="attribute-value">S</td>
                    </tr>
                    <tr class="attribute">
                        <td class="attribute-name">Цвет:</td>
                        <td class="attribute-value">черный</td>
                    </tr>
                    <tr class="attribute">
                        <td class="attribute-name">Год:</td>
                        <td class="attribute-value">2018</td>
                    </tr>
                </table>
            </td>
            <td class="price t-a-c"><span>11 000 грн</span></td>
            <td class="t-a-c qnty"><span>1</span></td>
            <td class="sum t-a-c"><span>11 000 грн</span></td>
        </tr>
        <tr>
            <td>
                <a href="https://lisoped.ua/" rel="nofollow" target="_blank">
                    <img src="https://lisoped.ua/storage/cache/shop_images/import/26-Formula-HUMMER-DD-cherno-zelenyiy-2018-1361-1980x1360-90-60.jpg"
                         alt="Romet Orkan 3.0M">
                </a>
            </td>
            <td>
                <div class="product-name"><a href="https://lisoped.ua/" rel="nofollow" target="_blank">Romet Orkan
                        3.0M</a></div>
                <div class="ttn">Номер ТТН Новой Почты: 12345678901234</div>
            </td>
            <td>
                <table class="attributes">
                    <tr class="attribute">
                        <td class="attribute-name">Размер:</td>
                        <td class="attribute-value">S</td>
                    </tr>
                    <tr class="attribute">
                        <td class="attribute-name">Цвет:</td>
                        <td class="attribute-value">черный</td>
                    </tr>
                    <tr class="attribute">
                        <td class="attribute-name">Год:</td>
                        <td class="attribute-value">2018</td>
                    </tr>
                </table>
            </td>
            <td class="price t-a-c"><span>11 000 грн</span></td>
            <td class="t-a-c qnty"><span>1</span></td>
            <td class="sum t-a-c"><span>11 000 грн</span></td>
        </tr>
        <tr>
            <td>
                <a href="https://lisoped.ua/" rel="nofollow" target="_blank">
                    <img src="https://lisoped.ua/storage/cache/shop_images/import/26-Formula-HUMMER-DD-cherno-zelenyiy-2018-1361-1980x1360-90-60.jpg"
                         alt="Romet Orkan 3.0M">
                </a>
            </td>
            <td>
                <div class="product-name"><a href="https://lisoped.ua/" rel="nofollow" target="_blank">Romet Orkan
                        3.0M</a></div>
                <div class="ttn">Номер ТТН Новой Почты: 12345678901234</div>
            </td>
            <td>
                <table class="attributes">
                    <tr class="attribute">
                        <td class="attribute-name">Размер:</td>
                        <td class="attribute-value">S</td>
                    </tr>
                    <tr class="attribute">
                        <td class="attribute-name">Цвет:</td>
                        <td class="attribute-value">черный</td>
                    </tr>
                    <tr class="attribute">
                        <td class="attribute-name">Год:</td>
                        <td class="attribute-value">2018</td>
                    </tr>
                </table>
            </td>
            <td class="price t-a-c"><span>11 000 грн</span></td>
            <td class="qnty t-a-c"><span>1</span></td>
            <td class="sum t-a-c"><span>11 000 грн</span></td>
        </tr>
        </tbody>
    </table>

    <table class="table">
        <tr>
            <td>
                <div class="order__description">
                    <p>Получатель: <b>Артур Титов</b></p>
                    <p>Адрес доставки: <b>г. Харьков, ул. Короленко, 6</b></p>
                    <p>Способ оплаты: <b>наличный</b></p>
                </div>
                <p>Отслеживай статус заказа в <a href="https://lisoped.ua/" rel="nofollow" target="_blank">личном
                        кабинете</a></p>
            </td>
        </tr>
    </table>
    <!-- Main == end ==============================================-->
@endsection
