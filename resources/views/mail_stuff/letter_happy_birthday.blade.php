@extends('mail_stuff.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="main character table">
        <tr>
            <td>
                <br>
                <div class="row row--2">
                    <div class="col text">
                        <p>Здравствуй, <span class="accent">Артур Титов!</span></p>
                        <p class="m-0"><b>С Днем Рождения!</b></p>
                        <p>Желаем дорог без кочек и верных спутников рядом. Покорения новых вершин и прекрасных горизонтов впереди.</p>
                        <p>Мы подготовили тебе подарок :)</p>
                        <p>В следующем заказе любой добавленный товар до 100 грн будет твоим подарком*. Просто укажи выбранный подарок в комментарии к заказу или менеджеру при звонке.</p>
                        <p class="f-z-small">*Под подарком имеется ввиду покупка за 0.01 грн. Подарок можно получить при общей сумме заказа от 300 грн.</p>
                    </div>
                    <div class="col img">
                        <img src="http://lisoped.ua/assets/img/letters/character_HappyBirthday.png" alt="Илья">
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <!-- Main == end ==============================================-->
@endsection