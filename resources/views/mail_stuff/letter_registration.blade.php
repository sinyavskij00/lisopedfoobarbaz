@extends('mail_stuff.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table">
        <tr><td class="pdd-b-0 "><h6>Здравствуй, <span class="accent">Артур Титов!</span></h6></td></tr>
    </table>

    <table class="main registration table">
        <tr>
            <td>
                <div class="row row--2">
                    <div class="col text">
                        <h3>Спасибо за регистрацию!</h3>
                        <p>Теперь у тебя есть личный кабинет в магазине Lisoped. Это очень удобно: отслеживай статус заказа, сохраняй товары в список желаний, оставляй отзывы.</p>
                        <p><b>Твои данные для входа:</b></p>
                        <table class="log-pass__table">
                            <tbody>
                            <tr>
                                <td class="accent">Логин:</td>
                                <td>arturtitov@gmail.com</td>
                            </tr>
                            <tr>
                                <td class="accent">Пароль:</td>
                                <td>F546n814</td>
                            </tr>
                            </tbody>
                        </table>
                        <p>В нашем <a href="https://lisoped.ua/news" target="_blank" rel="nofollow">блоге</a> много полезных статей.</p>
                        <p>Подписывайся на наши соцсети к сообществу велолюбителей, там иногда разыгрывают велосипеды :)</p>
                        <br>
                        <br>
                    </div>
                    <div class="col img">
                        <img src="http://lisoped.ua/assets/img/letters/character_hello.png" alt="Илья">
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <!-- Main == end ==============================================-->
@endsection