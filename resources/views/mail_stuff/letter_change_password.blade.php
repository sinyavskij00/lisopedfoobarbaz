@extends('mail_stuff.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table">
        <tr><td><h6>Здравствуй, <span class="accent">Артур Титов!</span></h6></td></tr>
    </table>

    <table class="main table">
        <tr>
            <td>
                <p class="m-0">Мы получили  твой запрос на смену пароля.</p>
                <p>Для этого нажми на кнопку.</p>
                <p class="t-a-c"><a href="https://lisoped.ua/" class="btn"><span>Изменить пароль</span></a></p>
                <p>Больше не забывай :)</p>
            </td>
        </tr>
    </table>
    <!-- Main == end ==============================================-->
@endsection