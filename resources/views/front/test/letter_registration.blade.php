<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{config('app.name')}}</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&amp;subset=cyrillic" rel="stylesheet">
    <style type="text/css">
        .content,.content__wrapper,.letter table{border-collapse:collapse}.letter{background:#d6e3ec;color:#333333;font:600 13px/18px 'Open Sans',Helvetica,Arial,Verdana,sans-serif;padding:50px 15px;min-height:100vh}.socials,.socials li{list-style-type:none;line-height:1}.letter img{display:block;max-width:100%;height:auto}.categories a,.categories li,.socials a,.socials li{display:inline-block}.letter table{width:100%}.letter .table{margin:11px 35px;width:calc(100% - 70px)}.letter .table.registration{margin:11px 35px 0}.letter .header{background:#001421;color:#11ad62;width:100%}.letter .header__content td{padding:11px 35px 10px}.letter .welcome{margin:11px 35px;width:calc(100% - 70px)}.letter .footer__content{margin:35px 35px 25px;width:calc(100% - 70px);font-size:.93em}.content,.footer table{width:100%}.letter a{color:#333333}.letter p{margin:0 0 1.4em}.letter .m-0{margin:0}.letter .log-pass__table{margin:0 0 25px}.m-b{margin:0 0 1.4em}.content__wrapper{max-width:650px;margin:0 auto}.content{background:#ffffff}.v-a-t{vertical-align:top}.categories a,.v-a-m{vertical-align:middle}.t-a-c{text-align:center}.t-a-l{text-align:left}.categories,.phones,.t-a-r,.thank{text-align:right}.t-a-r img{margin:0 0 0 auto}.pdd-0{padding:0}.f-w-n{font-weight:400}.accent{color:#11ad62}.attribute,.ttn{color:#666}address{margin:0;font-style:normal}.categories{list-style-type:none;padding:0;margin:0}.categories li{list-style-type:none;margin:0 10px}.categories a{padding:3px 10px}h1,h2,h3{font-size:1.2em;margin:0 0 1.4em}h4,h5,h6{font-size:1em;margin:0 0 1.4em}.attribute,.order-description,.order-description th,.ttn{font-size:.9em}.welcome td{padding:20px 0}.welcome h6{margin-bottom:0;font-weight:600}.footer{background:#f6f6f6}.footer__content td,.footer__content>div{padding:0 15px 10px}.socials{margin:12px 0 0;padding:0}.socials a{width:16px;height:16px;margin:0 2px}.socials img{display:block;max-width:100%;height:auto}.row--2 .img,.row--2 .text{display:inline-block;vertical-align:middle}.phones p{margin:0}.row--2 .text{width:60%}.row--2 .img{width:35%}.registration .row--2 .text{width:60%;vertical-align:top}.registration .row--2 .img{width:calc(39% - 4px);vertical-align:bottom;margin:0 0 0 auto;text-align:right}.registration .row--2 .img img{margin:0 0 0 auto}.thank{max-width:190px}.thank span{display:block}.order-description td,.order-description th{border:1px solid #cccccc;padding:5px}.order-description .attribute td{border:none;padding:0}.product-name,.ttn{max-width:150px}.ttn{margin-top:5px}.attribute .attribute-name{color:#333333}@media (max-width:575.98px){.letter .footer__content,.letter .table,.letter .table.registration,.letter .welcome{width:calc(100% - 40px)!important}.letter .header__content td{padding:11px 10px 10px!important}.categories li{margin:0 3px!important}.categories a{padding:3px 0!important}.letter-logo img{max-width:85px!important}.letter .table{margin:22px 20px!important}.letter .table.registration{margin:10px 20px!important}.letter .welcome{margin:22px 20px 0!important}.letter .footer__content{margin:30px 20px 15px!important}.address{max-width:140px!important}.footer__content td,.footer__content>div{padding:0 0 10px!important}.registration .row--2 .text,.row--2 .text{width:100%!important}.registration .row--2 .img,.row--2 .img{width:100%!important;text-align:center!important;margin:25px auto 0!important}.registration .row--2 .img img{margin:0 auto!important}.thank{max-width:150px!important;text-align:left!important}.thank span{display:inline!important}}
    </style>
</head>
<body>

<div class="letter" style="background-color:#d6e3ec;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;color:#333333;font-style:normal;font-variant:normal;font-weight:600;font-size:13px;font-family:'Open Sans',Helvetica,Arial,Verdana,sans-serif;line-height:18px;padding-top:50px;padding-bottom:50px;padding-right:15px;padding-left:15px;min-height:100vh;" >
    <table class="content__wrapper" style="max-width:650px;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;width:100%;border-collapse:collapse;" >
        <tbody>
        <tr>
            <td class="pdd-0" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                <table class="content" style="background-color:#ffffff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;width:100%;border-collapse:collapse;" >
                    <tbody>
                    <tr>
                        <td class="pdd-0" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >

                            <table class="header" style="border-collapse:collapse;background-color:#001421;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;color:#11ad62;width:100%;" >
                                <tr>
                                    <td>
                                        <table class="header__content" style="width:100%;border-collapse:collapse;" >
                                            <tr>
                                                <td style="padding-top:11px;padding-bottom:10px;padding-right:35px;padding-left:35px;" >
                                                    <a href="https://lisoped.ua/" rel="nofollow" target="_blank" class="letter-logo" style="color:#333333;" >
                                                        <img src="E:openserverOSPaneldomainslisoped_projectpublicassetsimgletterslisoped_logo.png" alt="Интернет-магазин Lisoped" title="Интернет-магазин Lisoped" style="display:block;max-width:100%;height:auto;" >
                                                    </a>
                                                </td>
                                                <td class="t-a-r" style="text-align:right;padding-top:11px;padding-bottom:10px;padding-right:35px;padding-left:35px;" >
                                                    <ul class="categories" style="list-style-type:none;text-align:right;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;" >
                                                        <li style="display:inline-block;list-style-type:none;margin-top:0;margin-bottom:0;margin-right:10px;margin-left:10px;" >
                                                            <a href="https://lisoped.ua/" rel="nofollow" target="_blank" style="color:#333333;display:inline-block;padding-top:3px;padding-bottom:3px;padding-right:10px;padding-left:10px;vertical-align:middle;" >
                                                                <img src="E:openserverOSPaneldomainslisoped_projectpublicassetsimglettersicon_velik.png" alt="Велосипеды" title="Велосипеды" style="display:block;max-width:100%;height:auto;margin-top:0;margin-bottom:0;margin-right:0;margin-left:auto;" >
                                                            </a>
                                                        </li>
                                                        <li style="display:inline-block;list-style-type:none;margin-top:0;margin-bottom:0;margin-right:10px;margin-left:10px;" >
                                                            <a href="https://lisoped.ua/" rel="nofollow" target="_blank" style="color:#333333;display:inline-block;padding-top:3px;padding-bottom:3px;padding-right:10px;padding-left:10px;vertical-align:middle;" >
                                                                <img src="E:openserverOSPaneldomainslisoped_projectpublicassetsimglettersicon_accessories.png" alt="Аксуссуары" title="Аксуссуары" style="display:block;max-width:100%;height:auto;margin-top:0;margin-bottom:0;margin-right:0;margin-left:auto;" >
                                                            </a>
                                                        </li>
                                                        <li style="display:inline-block;list-style-type:none;margin-top:0;margin-bottom:0;margin-right:10px;margin-left:10px;" >
                                                            <a href="https://lisoped.ua/" rel="nofollow" target="_blank" style="color:#333333;display:inline-block;padding-top:3px;padding-bottom:3px;padding-right:10px;padding-left:10px;vertical-align:middle;" >
                                                                <img src="E:openserverOSPaneldomainslisoped_projectpublicassetsimglettersicon_spares.png" alt="Запчасти" title="Запчасти" style="display:block;max-width:100%;height:auto;margin-top:0;margin-bottom:0;margin-right:0;margin-left:auto;" >
                                                            </a>
                                                        </li>
                                                        <li style="display:inline-block;list-style-type:none;margin-top:0;margin-bottom:0;margin-right:10px;margin-left:10px;" >
                                                            <a href="https://lisoped.ua/" rel="nofollow" target="_blank" style="color:#333333;display:inline-block;padding-top:3px;padding-bottom:3px;padding-right:10px;padding-left:10px;vertical-align:middle;" >
                                                                <img src="E:openserverOSPaneldomainslisoped_projectpublicassetsimglettersicon_instruments.png" alt="Инструменты" title="Инструменты" style="display:block;max-width:100%;height:auto;margin-top:0;margin-bottom:0;margin-right:0;margin-left:auto;" >
                                                            </a>
                                                        </li>
                                                        <li style="display:inline-block;list-style-type:none;margin-top:0;margin-bottom:0;margin-right:10px;margin-left:10px;" >
                                                            <a href="https://lisoped.ua/" rel="nofollow" target="_blank" style="color:#333333;display:inline-block;padding-top:3px;padding-bottom:3px;padding-right:10px;padding-left:10px;vertical-align:middle;" >
                                                                <img src="E:openserverOSPaneldomainslisoped_projectpublicassetsimglettersicon_equipment.png" alt="Экипировка" title="Экипировка" style="display:block;max-width:100%;height:auto;margin-top:0;margin-bottom:0;margin-right:0;margin-left:auto;" >
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                            <table class="welcome table" style="border-collapse:collapse;margin-top:11px;margin-bottom:11px;margin-right:35px;margin-left:35px;width:calc(100% - 70px);" >
                                <tr><td style="padding-top:20px;padding-bottom:20px;padding-right:0;padding-left:0;" ><h6 style="font-size:1em;margin-top:0;margin-right:0;margin-left:0;margin-bottom:0;font-weight:600;" >Здравствуйте, <span class="accent" style="color:#11ad62;" >Артур Титов!</span></h6></td></tr>
                            </table>



                            <!-- Main == start ==============================================-->
                            <table class="main registration table" style="border-collapse:collapse;margin-top:11px;margin-bottom:11px;margin-right:35px;margin-left:35px;width:calc(100% - 70px);" >
                                <tr>
                                    <td>
                                        <div class="row row--2">
                                            <div class="col text" style="display:inline-block;width:60%;vertical-align:top;" >
                                                <h3 style="font-size:1.2em;margin-top:0;margin-bottom:1.4em;margin-right:0;margin-left:0;" >Спасибо за регистрацию!</h3>
                                                <p style="margin-top:0;margin-bottom:1.4em;margin-right:0;margin-left:0;" >Теперь у тебя есть личный кабинет в магазине Lisoped. Это очень удобно: отслеживай статус заказа, сохраняй товары в список желаний, оставляй отзывы.</p>
                                                <p style="margin-top:0;margin-bottom:1.4em;margin-right:0;margin-left:0;" ><b>Твои данные для входа:</b></p>
                                                <table class="log-pass__table" style="width:100%;border-collapse:collapse;margin-top:0;margin-bottom:25px;margin-right:0;margin-left:0;" >
                                                    <tbody>
                                                    <tr>
                                                        <td class="accent" style="color:#11ad62;" >Логин:</td>
                                                        <td>arturtitov@gmail.com</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="accent" style="color:#11ad62;" >Пароль:</td>
                                                        <td>F546n814</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <p style="margin-top:0;margin-bottom:1.4em;margin-right:0;margin-left:0;" >В нашем <a href="https://lisoped.ua/news" target="_blank" rel="nofollow" style="color:#333333;" >блоге</a> много полезных статей.</p>
                                                <p style="margin-top:0;margin-bottom:1.4em;margin-right:0;margin-left:0;" >Подписывайся на наши соцсети к сообществу велолюбителей, там иногда разыгрывают велосипеды :)</p>
                                            </div>
                                            <div class="col img" style="display:inline-block;width:calc(39% - 4px);vertical-align:bottom;margin-top:0;margin-bottom:0;margin-right:0;margin-left:auto;text-align:right;" >
                                                <img src="E:openserverOSPaneldomainslisoped_projectpublicassetsimgletterscharacter_hello.png" alt="Илья" style="display:block;max-width:100%;height:auto;margin-top:0;margin-bottom:0;margin-right:0;margin-left:auto;" >
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <!-- Main == end ==============================================-->



                            <table class="footer" style="background-color:#f6f6f6;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;width:100%;border-collapse:collapse;" >
                                <tr>
                                    <td>
                                        <table class="footer__content" style="border-collapse:collapse;margin-top:35px;margin-bottom:25px;margin-right:35px;margin-left:35px;width:calc(100% - 70px);font-size:0.93em;" >
                                            <tr class="v-a-t" style="vertical-align:top;" >
                                                <td style="padding-top:0;padding-bottom:10px;padding-right:15px;padding-left:15px;" >
                                                    <div class="address">
                                                        <address style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;font-style:normal;" >Киев, ул.Казимира Малевич 89</address>
                                                        <address style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;font-style:normal;" >Харьков, пр.Науки 30</address>
                                                    </div>
                                                    <ul class="socials" style="margin-top:12px;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:1;list-style-type:none;" >
                                                        <li style="display:inline-block;list-style-type:none;line-height:1;" ><a href="https://instagram/" rel="nofollow" title="instagram" target="_blank" class="instagram" style="color:#333333;display:inline-block;width:16px;height:16px;margin-top:0;margin-bottom:0;margin-right:2px;margin-left:2px;" ><img src="E:openserverOSPaneldomainslisoped_projectpublicassetsimglettersicon_instagram.png" alt="instagram" style="display:block;max-width:100%;height:auto;" ></a></li>
                                                        <li style="display:inline-block;list-style-type:none;line-height:1;" ><a href="https://fasebook/" rel="nofollow" title="fasebook" target="_blank" class="facebook" style="color:#333333;display:inline-block;width:16px;height:16px;margin-top:0;margin-bottom:0;margin-right:2px;margin-left:2px;" ><img src="E:openserverOSPaneldomainslisoped_projectpublicassetsimglettersicon_facebook.png" alt="facebook" style="display:block;max-width:100%;height:auto;" ></a></li>

                                                        <li style="display:inline-block;list-style-type:none;line-height:1;" ><a href="https://viber/" rel="nofollow" title="viber" target="_blank" class="viber" style="color:#333333;display:inline-block;width:16px;height:16px;margin-top:0;margin-bottom:0;margin-right:2px;margin-left:2px;" ><img src="E:openserverOSPaneldomainslisoped_projectpublicassetsimglettersicon_viber.png" alt="viber" style="display:block;max-width:100%;height:auto;" ></a></li>

                                                        <li style="display:inline-block;list-style-type:none;line-height:1;" ><a href="https://whatsapp/" rel="nofollow" title="whatsapp" target="_blank" class="whatsapp" style="color:#333333;display:inline-block;width:16px;height:16px;margin-top:0;margin-bottom:0;margin-right:2px;margin-left:2px;" ><img src="E:openserverOSPaneldomainslisoped_projectpublicassetsimglettersicon_whatsapp.png" alt="whatsapp" style="display:block;max-width:100%;height:auto;" ></a></li>

                                                        <li style="display:inline-block;list-style-type:none;line-height:1;" ><a href="https://telegram/" rel="nofollow" title="telegram" target="_blank" class="telegram" style="color:#333333;display:inline-block;width:16px;height:16px;margin-top:0;margin-bottom:0;margin-right:2px;margin-left:2px;" ><img src="E:openserverOSPaneldomainslisoped_projectpublicassetsimglettersicon_telegram.png" alt="telegram" style="display:block;max-width:100%;height:auto;" ></a></li>
                                                    </ul>
                                                </td>

                                                <td style="padding-top:0;padding-bottom:10px;padding-right:15px;padding-left:15px;" >
                                                    <div class="phones" style="text-align:right;" >
                                                        <p style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;" >(067) 620-66-11</p>
                                                        <p style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;" >(095) 620-66-11</p>
                                                        <p style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;" >(073) 620-66-11</p>
                                                        <p style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;" >(044) 465-66-11</p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>

