@extends('front.layout')
@section('meta-title')
    <title>Блог интернет-магазина Lisoped ᐈ Полезные статьи о велосипедах</title>
@stop
@section('meta-description')
    <meta name="description" content="Блог интернет-магазина Lisoped ᐈ Полезные статьи и новости нашего магазина. Следите за новостями о велосипедах в нашем блоге">
@stop
@section('meta-keywords')

@stop
@section('content')
    <main id="my-content" class="page-content p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
                <div class="col-12">
                    <h1 class="p-title">{{__('news.list.title')}}</h1>
                </div>
            </div>
        </div>


        <div class="blog-items">
            <div class="container">
                <div class="row">
                    @foreach($newses as $news)
                    <div class="col-lg-3 col-sm-6">
                        <div class="blog-item">
                            <a href="{{route('news.show', $news->slug)}}" class="blog-item__img">
                                <img class="lazy" data-src="{{$catalogService->resize($news->image, 270, 305, false)}}" alt="blog-item__title">
                            </a>
                            <p class="blog-item__title">
                                <a href="{{route('news.show', $news->slug)}}">{{$news->localDescription->title}}</a>
                            </p>
                            <a href="{{route('news.show', $news->slug)}}" class="more">
                                <span>{{__('news.list.read_more')}}</span>
                                <i class="i-next"></i>
                            </a>
                        </div>
                    </div>
                    @endforeach

                    <div class="col-12">
                        {{$newses->links('front.paginate')}}
                    </div>

                </div>
            </div>
        </div>

        <div class="character-wrapper character-blog character-left" data-time="0">
            <div class="character">
                <div class="character-text">
                    <div class="holder">
                        <div class="character-text__inner">{!! __('character.article.phrase', ['email' => config('app.admin_email')] ) !!}</div>
                        <button class="close"><i class="icon-close"></i></button>
                    </div>
                </div>
                <div class="character-img__holder">
                    <img data-src="images/charackter/Lisoped_man_2.svg" alt="character" class="character-img lazy">
                </div>
            </div>
        </div>

        <div class="page-overlay"></div>

    </main>
@endsection
