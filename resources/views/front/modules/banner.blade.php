@if(isset($banners) && $banners->count() > 0)
    <section class="s-banner">
        <div class="main-banner">
            @foreach($banners->sortBy('sort') as $banner)
                @if(isset($banner->main_image))
                    <div class="banner-item__wrapper">
                        {{--<div class="banner-item banner-item__with-img"
                             style="background-image: url({{Storage::disk('public')->url($banner->background_image)}});">--}}

                        {{-- фоновое изображение для разных экранов, вместо предыдущего блока --}}
                        <div class="banner-item banner-item__with-img">

                            <img class="background-img" src="@if(empty($banner->background_image)) {{Storage::disk('public')->url($banner->mobile_image)}} @else {{Storage::disk('public')->url($banner->background_image)}} @endif" alt="Lisoped" data-mobile="@if(empty($banner->mobile_image)) {{Storage::disk('public')->url($banner->background_image)}} @else {{Storage::disk('public')->url($banner->mobile_image)}} @endif" data-pc="@if($banner->background_image) {{Storage::disk('public')->url($banner->background_image)}} @endif">
                            {{--<img class="background-img" src="@if(empty($banner->background_image)) {{Storage::disk('public')->url($banner->mobile_image)}} @else {{Storage::disk('public')->url($banner->background_image)}} @endif" alt="Lisoped" srcset="@if(empty($banner->mobile_image)) {{Storage::disk('public')->url($banner->background_image)}} @else {{Storage::disk('public')->url($banner->mobile_image)}} @endif 576w, {{Storage::disk('public')->url($banner->background_image)}} 1920w" >--}}


                            <a href="{{$banner->link}}" class="full-slide-link"></a>
                            <div class="d-flex">
                                <div class="container">
                                    <div class="row align-items-center">
                                        <div class="col-lg-7 col-sm-6 order-sm-last banner-p__img-wrap">
                                            <a href="{{$banner->localDescription->link}}" class="banner-p__img">
                                                <img class="lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{parse_url(url('/get-image/' .str_replace('/', '--', $banner->main_image) .'/'. '663' . '/' .'null'), PHP_URL_PATH)}}" alt="bicycle">
                                            </a>
                                        </div>
                                        <div class="col-lg-5 col-sm-6 banner-item_descr-wrap">
                                            <div class="banner-item_descr">
                                                @if($banner->localDescription->text)
                                                    <p class="h2">{{$banner->localDescription->text}}</p>
                                                @endif
                                                <div class="banner-item_links">
                                                    @if($banner->localDescription->link_title)
                                                        <a href="{{$banner->localDescription->link}}" class="banner-c__link btn_accent">
                                                            <span class="text">{{$banner->localDescription->link_title}}</span>
                                                            <span class="i i-next"></span>
                                                        </a>
                                                    @endif
                                                    @if($banner->localDescription->link_subtitle)
                                                        <a href="{{$banner->localDescription->link}}" class="banner-p__link">
                                                            {{$banner->localDescription->link_subtitle}}
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="banner-item__wrapper">
                        {{--<div class="banner-item" style="background-image: url({{Storage::disk('public')->url($banner->background_image)}});">--}}

                        {{-- фоновое изображение для разных экранов, вместо предыдущего блока --}}
                        <div class="banner-item">
                            <img class="background-img" src="@if(empty($banner->background_image)) {{Storage::disk('public')->url($banner->mobile_image)}} @else {{Storage::disk('public')->url($banner->background_image)}} @endif" alt="Lisoped" data-mobile="@if(empty($banner->mobile_image)) {{Storage::disk('public')->url($banner->background_image)}} @else {{Storage::disk('public')->url($banner->mobile_image)}} @endif" data-pc="@if($banner->background_image) {{Storage::disk('public')->url($banner->background_image)}} @endif">
                            {{--<img class="background-img" src="@if(empty($banner->background_image)) {{Storage::disk('public')->url($banner->mobile_image)}} @else {{Storage::disk('public')->url($banner->background_image)}} @endif" alt="Lisoped" srcset="@if(empty($banner->mobile_image)) {{Storage::disk('public')->url($banner->background_image)}} @else {{Storage::disk('public')->url($banner->mobile_image)}} @endif 576w, {{Storage::disk('public')->url($banner->background_image)}} 1920w" >--}}

                            <a href="{{$banner->link}}" class="full-slide-link"></a>
                            <div class="d-flex">
                                <div class="container">
                                    <div class="row align-items-center">
                                        <div class="col-lg-5 col-md-6 banner-item_descr-wrap">
                                            <div class="banner-item_descr">
                                                @if($banner->localDescription->text)
                                                    <p class="h2">{{$banner->localDescription->text}}</p>
                                                @endif
                                                <div class="banner-item_links">
                                                    @if($banner->localDescription->link_title)
                                                        <a href="{{$banner->localDescription->link}}" class="banner-c__link btn_accent">
                                                            <span class="text">{{$banner->localDescription->link_title}}</span>
                                                            <span class="i i-next"></span>
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </section>
@endif
