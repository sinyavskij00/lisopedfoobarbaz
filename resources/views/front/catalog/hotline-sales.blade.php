<sales>
    <sale>
        <title>Дарим подарок к велосипеду + бесплатную курьерскую (до двери) доставку Новой почтой по Украине!</title>
        <url>{{url('/shares')}}/{{$sales->slug}}</url>
        <date_start>{{\Carbon\Carbon::parse($sales->date_start)->format('Y-m-d')}}</date_start>
        <date_end>{{\Carbon\Carbon::parse($sales->date_end)->format('Y-m-d')}}</date_end>
        <type>Подарок к покупке</type>
        <products>
            @foreach($sales->shareTakeProducts as $shareProduct)
                @if(isset($shareProduct->product->manufacturer) && $shareProduct->product->manufacturer->slug !== 'bergamont')
                    <product id="{{$shareProduct->product->id}}">{{url('/product') . '/' . $shareProduct->product->slug}}</product>
                @endif
            @endforeach
        </products>
        <custom_present>
            <present_title>
                Аксессуар на выбор + бесплатная курьерская доставка до двери по Украине!
            </present_title>
        </custom_present>
    </sale>
    <sale>
        <title>Бесплатная курьерская (до двери) доставка Новой почтой по Украине!</title>
        <url>{{url('/delivery')}}</url>
        <date_start>{{\Carbon\Carbon::parse($sales->date_start)->format('Y-m-d')}}</date_start>
        <date_end>{{\Carbon\Carbon::parse($sales->date_end)->format('Y-m-d')}}</date_end>
        <type>Подарок к покупке</type>
        <products>
            @if(!empty($bergamontProducts))
                @foreach($bergamontProducts as $shareProduct)
                    <product id="{{$shareProduct->id}}">{{url('/product') . '/' . $shareProduct->slug}}</product>
                @endforeach
            @endif

        </products>
        <custom_present>
            <present_title>
                Бесплатная курьерская (до двери) доставка Новой почтой по Украине!
            </present_title>
        </custom_present>
    </sale>
</sales>