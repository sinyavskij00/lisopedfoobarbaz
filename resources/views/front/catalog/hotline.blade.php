<price>
    <date>{{\Illuminate\Support\Carbon::now()}}</date>
    <firmName>Lisoped.ua</firmName>
    <firmId>34708</firmId>
    <delivery id="1" type="warehouse" cost="100" freeFrom="" time="1" carrier="NP" region="01*-94*"/>
    <delivery id="2" type="address" cost="null" freeFrom="2000" time="1" carrier="NP" region="01*-94*"/>
    <categories>
        @foreach($categories as $category)
            <category>
                <id>{{$category->id}}</id>
                @if(isset($category->parent_id))
                    <parentId>{{$category->parent_id}}</parentId>
                @endif
                <name>{{$category->name}}</name>
            </category>
        @endforeach
    </categories>

    <items>
        @foreach ($products as $product)
            <item>
                <id>{{$product->sku}}</id>
                <categoryId>{{$product->category_id}}</categoryId>
                <code>{{$product->sku}}</code>
                <barcode>{{$product->vendor_sku}}</barcode>
                <vendor>{{$product->manufacturer}}</vendor>
                <name>{{$product->title}} {{$product->sizes}} {{$product->color}} ({{$product->vendor_sku}})</name>
                @if(iconv_strlen(strip_tags(htmlspecialchars($product->description, ENT_XML1))) <= 0)
                    <description>{{$product->title}}</description>
                @else
                    <description>{{strip_tags(htmlspecialchars($product->description, ENT_XML1))}}</description>
                @endif
                <url>{{url('/product') . '/' . $product->slug}}</url>
                <image>{{Storage::disk('public')->url($product->image)}}</image>
                @if(\App\Entities\SkuSpecial::where('unit_id', '=', $product->sku_id)->first())
                    <priceRUAH>{{\App\Entities\SkuSpecial::where('unit_id', '=', $product->sku_id)->first()->price}} UAH</priceRUAH>
                    @else
                    <priceRUAH>{{$product->price}} UAH</priceRUAH>
                @endif

                <stock>В наличии</stock>
                @if($product->manufacturer === 'Winner' || $product->manufacturer === 'Cyclone')
                    <guarantee type="shop">12</guarantee>
                @endif
                @if($product->manufacturer === 'Pride' || $product->manufacturer === 'Bergamont')
                    <guarantee type="shop">36</guarantee>
                @endif
                @if($product->manufacturer === 'Orbea' || $product->manufacturer === 'Ghost' || $product->manufacturer === 'Schwinn')
                    <guarantee type="shop">Пожизненная гарантия на велосипед</guarantee>
                @endif
                @if($product->manufacturer === 'Apollo')
                    <guarantee type="shop">24</guarantee>
                @endif
                @if($product->manufacturer === 'Leon')
                    <guarantee type="shop">18</guarantee>
                @endif

                <param name="Размер рамы">{{$product->sizes}}</param>
                <param name="Цвет">{{$product->color}}</param>
            </item>
        @endforeach
    </items>
</price>