<div class="product-item">
    <div class="product-item__stickers">
        @if($product->getAttributeValue('special_price'))
            <div class="product-item__sticker sticker_sale">
                <span class="sticker_text">{{__('catalog.labels.sale')}}</span>
                <span class="sticker_percent">-{{ (int) round((1 - ($product->getAttribute('special_price') / $product->getAttributeValue('price'))) * 100)}}%</span>
            </div>
        @endif

        @if($product->getAttributeValue('is_our_choice'))
            <div class="product-item__sticker sticker_our-choice">
                <span class="sticker_text">{{__('catalog.labels.our_choice')}}</span>
            </div>
        @endif
        @if($product->getAttributeValue('is_new'))
            <div class="product-item__sticker sticker_new">
                <span class="sticker_text">{{__('catalog.labels.new')}}</span>
            </div>
        @endif
        @if($product->getAttributeValue('is_hit'))
            <div class="product-item__sticker sticker_hit">
                <span class="sticker_text">{{__('catalog.labels.hit')}}</span>
            </div>
        @endif
    </div>
    <div class="product-item__btns">
        @if(isset($product->shares) && count($product->shares) > 0)
            @if($product->shares->first()->shares->status == 1)
                <span class="product-item__btn" title="Подарок при покупке">
                <i class="i icon-gift"></i>
            </span>
            @endif

        @endif
        @if($compare->isCompared($product))
        <button type="button" onclick="compare.delete({{$product->getAttributeValue('id')}}, this)" class="product-item__btn" title="{{__('catalog.product_item.text_compare')}}">
            <i class="i i-lawyer accent"></i>
        </button>
        @else
            <button type="button" onclick="compare.add({{$product->getAttributeValue('id')}}, this)" class="product-item__btn" title="{{__('catalog.product_item.text_compare')}}">
                <i class="i i-lawyer"></i>
            </button>
        @endif

        @if($wishListService->isWished($product))
            <button type="button" onclick="wishlist.delete({{$product->getAttributeValue('id')}}, this)" class="product-item__btn" title="{{__('catalog.product_item.text_wishlist')}}">
                    <i class="i icon-heart"></i>
            </button>
        @else
            <button type="button" onclick="wishlist.add({{$product->getAttributeValue('id')}}, this)" class="product-item__btn" title="{{__('catalog.product_item.text_wishlist')}}">
                <i class="i i-heart"></i>
            </button>
        @endif
    </div>
    @if($product->colors)
        <ul class="product-item__color-list d-flex flex-column justify-content-center">
            @foreach($product->colors as $color)
                @if(Storage::disk('public')->exists($color->getAttributeValue('image')))
                <li class="active">
                    <img class="lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{Storage::disk('public')->url($color->getAttributeValue('image'))}}">
                </li>
                @endif
            @endforeach
        </ul>
    @endif
    <div class="product-item__img-wrap">
        <a href="{{route('product_path', $product->slug)}}"
           class="product-item__img" >
            <img class="lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{$catalogService->resize($product->image, 260, 150)}}"
                 alt="{{$product->getAttributeValue('name')}}">
        </a>
    </div>
    <div class="product-item__name-wrap">
        <div class="product__part-payment">
            @if($product->is_payparts === 1 && ($product->getAttributeValue('price') < 50000 && $product->getAttributeValue('price') > 1000))
                <a href="#privat" class="product__part-payment--item js-part-payment">
                    <img src="images/credit/svg/privat.svg" alt="Приват банк" class="product__part-payment--icon" title="Приват банк">
                </a>
            @endif

            {{--<a href="#mono" class="product__part-payment--item js-part-payment">
                <img src="images/credit/svg/mono.svg" alt="Моно банк" class="product__part-payment--icon" title="Моно банк">
            </a> --}}
            @if($product->getAttributeValue('price') < 70000 && $product->getAttributeValue('price') > 1000)
                <a href="#alfa" class="product__part-payment--item js-part-payment">
                    <img src="images/credit/svg/alfabank-logo-red-on-white.svg" alt="Альфа банк" class="product__part-payment--icon" title="Альфа банк">
                </a>
            @endif

        </div>


        @if($product->getAttributeValue('quantity') > 0)
            <div class="product-item__availability availability available">{{__('catalog.product_item.available')}}</div>
        @else

            @if($product->product_expected === 1)
                <div class="product-item__availability availability product_expected">{{__('catalog.product_item.product_expected')}}</div>
            @else
                {{--<div class="product-item__availability availability not-available">{{__('catalog.product_item.not_available')}}</div>--}}
            @endif

        @endif
        <p class="product-item__name">
            <a href="{{route('product_path', $product->slug)}}">{{$product->getAttributeValue('name')}}</a>
        </p>
    </div>
    <div class="product-item__descr">
        <div class="product-item__code">{{__('catalog.product_item.sizes')}} {{implode(', ', $product->sizes_range)}}</div>
        <ul class="product-options">
            @foreach($product->productItemAttributes as $productAttribute)
                <li class="product-options__item">
                    <span class="product-options__item-name">
                        <span>{{$productAttribute->attribute->localDescription->name}}</span>
                    </span>
                    <span class="product-options__item-value">{{ $productAttribute->text }}</span>
                </li>
            @endforeach
        </ul>
    </div>

    <div class="product-item__bottom">
        <div class="rating-wrap">
            <div class="rating">
                @for($i = 1; $i <= 5; $i++)
                    @if($product->rating >= $i)
                        <i class="icon-star-full i_stack-2x"></i>
                    @else
                        <i class="icon-star-full i_stack-1x"></i>
                    @endif
                @endfor
            </div>
            <a href="{{route('product_path', $product->slug)}}"
               class="reviews-number">({{$product->activeReviews->count()}} {{__('catalog.product_item.reviews')}})</a>
        </div>

        <div class="product-item__price-wrap d-flex justify-content-between align-items-center flex-wrap  @if($product->product_expected === 1 && $product->getAttributeValue('quantity') <= 0) product_expected @elseif($product->getAttributeValue('quantity') <= 0) product-unavailable @endif">
            {{-- Если товара нету выводим следующий блок, а цену скрываем --}}
            {{--<div class="not-available">нет в наличии</div>--}}
            @if($product->product_expected !== 1 && $product->getAttributeValue('quantity') <= 0)
                <div id="not-available-product-block" class="not-available">нет в наличии</div>
                @else
                @if($product->getAttributeValue('special_price'))
                    <div class="product-item__price d-flex flex-wrap align-items-center">
                        <span class="product-titem__price-old">{{$catalogService->format($catalogService->calculate($product->getAttributeValue('price')))}}</span>
                        <span class="product-titem__price-current">{{$catalogService->format($catalogService->calculate($product->getAttributeValue('special_price')))}}</span>
                    </div>
                @else
                    <div class="product-item__price d-flex flex-wrap align-items-center">
                        <span class="product-titem__price-current">{{$catalogService->format($catalogService->calculate($product->getAttributeValue('price')))}}</span>
                    </div>
                @endif
            @endif


            <a href="{{route('product_path', $product->slug)}}" class="btn">
                <span>{{__('catalog.product_item.btn_buy')}}</span>
            </a>
        </div>
    </div>

</div>
<!-- /.product-item -->
