@extends('front.layout')
@section('robots')
    <meta name="robots" content="noindex, follow" />
@stop
@section('content')
    <main id="my-content" class="page-content p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="p-title">{{__('catalog.compare.product.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container">

            <div class="compare__block">

                <div class="page-compare__left">
                    <div class="one-goods__first table-compare__item"><span class="title">{{__('catalog.compare.product.products')}}</span></div>
                    <div class="table-compare">
                        <div class="table-compare__item"><span class="title">{{__('catalog.compare.product.price')}}</span></div>
                        @foreach($attributes as $attribute)
                        <div class="table-compare__item"><span class="title">{{$attribute->localDescription->name}}</span></div>
                        @endforeach
                    </div>
                </div>

                <div class="page-compare__right">
                    <div class="compare-page__slider">

                        @foreach($products as $product)
                        <div class="item compare-product-{{$product->id}}">
                            <div class="one-goods one-goods__first">
                                <button class="action delete" onclick="compare.delete({{$product->id}})" title="{{__('catalog.compare.product.btn_delete')}}"><i class="icon-close2"></i></button>
                                <a class="one-goods__image" href="{{route('product_path', $product->slug)}}" title="{{$product->name}}">
                                    <img class="product-image-photo lazy" data-src="{{$catalogService->resize($product->image, 260, 150)}}" title="{{$product->name}}" alt="{{$product->name}}">
                                </a>
                                <a href="{{route('product_path', $product->slug)}}" class="one-goods__name" title="{{$product->name}}">{{$product->name}}</a>
                                <span class="one-goods__code">{{__('catalog.compare.product.sku')}} {{$product->sku}}</span>
                            </div>
                            <div class="table-compare">
                                <div class="table-compare__item">

                                    @if($product->getAttributeValue('special_price'))
                                        <span class="comparison-product__prices align-items-center product-item__price">
                                            <span class="product-titem__price-old">{{$catalogService->format($catalogService->calculate($product->getAttributeValue('price')))}}</span>
                                            <span class="comparison-product__price comparison-product__price">{{$catalogService->format($catalogService->calculate($product->getAttributeValue('special_price')))}}</span>
                                        </span>
                                    @else
                                        <span class="comparison-product__prices">
                                            <span class="comparison-product__price comparison-product__price">{{$catalogService->format($catalogService->calculate($product->price))}}</span>
                                        </span>
                                    @endif
                                    <span class="comparison-product__btn-wrapper">
										<a href="{{route('product_path', $product->slug)}}" class="btn"><span>{{__('catalog.compare.product.btn_more')}}</span></a>
                                    </span>
                                </div>
                                @foreach($attributes as $attribute)
                                    @if(!empty($product->productAttributes->where('attribute_id', '=', $attribute->id)->first()))
                                        <div class="table-compare__item">{{$product->productAttributes->where('attribute_id', '=', $attribute->id)->first()->text}}</div>
                                    @else
                                        <div class="table-compare__item">-</div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="page-overlay"></div>

    </main>
@endsection
