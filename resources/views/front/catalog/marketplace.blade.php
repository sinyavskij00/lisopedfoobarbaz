<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
    <channel>
        <title>{{$main_info['name']}}</title>
        <link>{{$main_info['url']}}</link>
        <description>{{$main_info['description']}}</description>
        @foreach ($products as $product)
            <item>
                <link>{{url('/product') . '/' . $product->slug}}</link>
                <g:id>{{$product->id}}</g:id>
                <g:price>{{$product->price}} UAH</g:price>
                <g:condition>new</g:condition>
                <g:availability>in stock</g:availability>
                <g:product_type>Велосипеды</g:product_type>
                <g:image_link>{{asset($product->image)}}</g:image_link>
                <g:brand>{{$product->manufacturer}}</g:brand>
                <title>{{$product->title}}</title>
                @if(iconv_strlen(strip_tags(htmlspecialchars($product->description, ENT_XML1))) <= 0)
                    <description>{{$product->title}}</description>
                    @else
                    <description>{{strip_tags(htmlspecialchars($product->description, ENT_XML1))}}</description>
                @endif

            </item>
        @endforeach
    </channel>
</rss>