@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content page-product__content">
        <div class="container">

            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
                <div class="col-12">
                    <h1 class="p-title">{{ $product->getAttributeValue('name') }}</h1>
                </div>
            </div>

            @include('front.catalog.current_sku_content')

            @include('front.sizes_popup', ['tables' => isset($sizeTables) ? $sizeTables : []])

            <div class="row">
                <div class="col-12">
                    <div id="product-info__tabs" class="tab-block product-info__tabs">

                        <ul class="tab-nav d-flex align-items-center flex-wrap">
                            <li class="active">{{__('catalog.product_page.description')}}</li>
                            <li>{{__('catalog.product_page.specifications')}}</li>
                            <li>{{__('catalog.product_page.reviews')}} ({{$product->activeReviews->where('parent_id', '=', null)->count()}})</li>
                            @if($product->videos->count() > 0)
                                <li>{{__('catalog.product_page.videos')}}</li>
                            @endif
                        </ul>

                        <div class="tab-cont">

                            <div class="tab-pane">
                                {!! $product->getAttributeValue('description') !!}
                            </div>

                            <div class="tab-pane">
                                <div class="properties d-flex justify-content-between flex-wrap">
                                    @foreach($attributeGroups->sortBy('sort') as $attributeGroup)
                                        <div class="propertie">
                                            <div class="propertie-head">
                                                @if(Storage::disk('public')->exists($attributeGroup->getAttributeValue('image')))
                                                    <img src="{{Storage::disk('public')->url($attributeGroup->getAttributeValue('image'))}}" alt="list">
                                                @endif
                                                @if(isset($attributeGroup->localDescription))
                                                <div class="propertie-head__text">{{$attributeGroup->localDescription->getAttributeValue('name')}}</div>
                                                @endif
                                            </div>
                                            <ul class="propertie-list">
                                                @foreach($attributeGroup->attributes->sortBy('sort') as $attribute)
                                                    @foreach($attribute->productAttributes->sortBy('sort') as $productAttribute)
                                                        <li class="propertie-list__item">
                                                            @if(isset($attribute->localDescription))
                                                                <span class="propertie-list__item-name">
                                                                    <span>{{$attribute->localDescription->getAttributeValue('name')}}</span>
                                                                </span>
                                                            @endif
                                                            <span class="propertie-list__item-value">{!! $productAttribute->getAttributeValue('text') !!}</span>
                                                        </li>
                                                    @endforeach
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="tab-pane">
                                <div class="reviews-list">
                                    @foreach($product->activeReviews->where('parent_id', '=', null) as $review)
                                        <div class="reviews-item">
                                            <div class="reviews-item__reviewer">{{$review->getAttributeValue('name')}}</div>
                                            <div class="reviews-item__date">{{Carbon\Carbon::parse($review->created_at)->format(config('app.date_format'))}}</div>
                                            <div class="rating reviews-item__rating">
                                                @for($i = 1; $i <= 5; $i++)
                                                    @if($review->getAttributeValue('rating') >= $i)
                                                        <i class="icon-star-full i_stack-2x"></i>
                                                    @else
                                                        <i class="icon-star-full i_stack-1x"></i>
                                                    @endif
                                                @endfor
                                            </div>
                                            <div class="reviews-item__descr">
                                                <p>{{$review->text}}</p>
                                                @if($review->images->count() > 0)
                                                    @foreach($review->images as $image)
                                                        <img src="{{$catalogService->resize($image->getAttributeValue('image'), 100, 100, false)}}">
                                                    @endforeach
                                                @endif
                                                <div class="answer">
                                                    <span class="to-answer" data-form="answer"
                                                          data-parent-id="{{$review->id}}">Ответить</span>
                                                </div>
                                            </div>
                                        </div>
                                        @if($product->activeReviews->where('parent_id', '=', $review->id)->where('status', '=', 1)->count() > 0)
                                            @foreach($product->activeReviews->where('parent_id', '=', $review->id)->where('status', '=', 1) as $child)
                                                <div class="reviews-item level1 hidden" style="display: block">
                                                    <div class="reviews-item__reviewer">{{$child->name}}
                                                        <span>ответил(а)</span> {{$review->name}}</div>
                                                    <div class="reviews-item__date">{{Carbon\Carbon::parse($child->created_at)->format(config('app.date_format'))}}</div>
                                                    <div class="reviews-item__descr">
                                                        <p>{{$child->text}}</p>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    @endforeach
                                </div>
                                <form action="" class="reviews-form" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="product_id" value="{{$product->getAttributeValue('id')}}">
                                    <div class="formhead">{{__('catalog.product_page.leave_comment')}}</div>
                                    <div class="d-flex justify-content-sm-between flex-wrap">
                                        <div class="reviews-form__content">
                                            <textarea name="text" cols="30" rows="6" required maxlength="3000"
                                                      placeholder="{{__('catalog.product_page.text_comment')}}"></textarea>
                                            <label class="reviews-form__file">
                                                <input type="file" name="images[]" multiple>
                                                <span class="reviews-form__file-name"></span>
                                                <span class="reviews-form__file-icon">
                                                    <i class="icon-attach_file"></i>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="reviews-form__info">
                                            <div class="rating-wrapper clearfix">
                                                <input type="radio" class="rating-input" id="rating-input-1-5"
                                                       name="rating" value="5"/>
                                                <label for="rating-input-1-5" class="rating-star"></label>

                                                <input type="radio" class="rating-input" id="rating-input-1-4"
                                                       name="rating" value="4"/>
                                                <label for="rating-input-1-4" class="rating-star"></label>

                                                <input type="radio" class="rating-input" id="rating-input-1-3"
                                                       name="rating" value="3"/>
                                                <label for="rating-input-1-3" class="rating-star"></label>

                                                <input type="radio" class="rating-input" id="rating-input-1-2"
                                                       name="rating" value="2"/>
                                                <label for="rating-input-1-2" class="rating-star"></label>

                                                <input type="radio" class="rating-input" id="rating-input-1-1"
                                                       name="rating" value="1"/>
                                                <label for="rating-input-1-1" class="rating-star"></label>
                                            </div>
                                            <input type="text" name="name" required placeholder="{{__('catalog.product_page.text_name')}}">
                                            <input type="email" name="email" required placeholder="{{__('catalog.product_page.text_email')}}">
                                            <button type="submit" class="btn"><span>{{__('catalog.product_page.submit')}}</span></button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                            @if($product->videos->count() > 0)
                                <div class="tab-pane tab-video">
                                    @foreach($product->videos as $video)
                                        <div class="w-iframe">
                                            <iframe width="100%" height="100%"
                                                    src="{{$video->getAttributeValue('video')}}"
                                                    frameborder="0" allow="autoplay; encrypted-media"
                                                    allowfullscreen></iframe>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>

                    </div>
                </div>
            </div>

            @if(!empty($jsonLd))
                <script type="application/ld+json">
                    @json($jsonLd)
                </script>
            @endif

            @if($relatedProducts->count() > 0)
            <div class="slider-section">

                <h2 class="slider-title">{{__('catalog.product_page.related_products')}}</h2>
                <div class="slider-products">
                    @foreach($relatedProducts as $related)
                        <div class="slider-products__item">
                            @include('front.catalog.product_item', ['product' => $related])
                        </div>
                    @endforeach
                </div>
            </div>
            @endif

            @include('front.modules.viewed')

        </div>
        <div class="character-wrapper character-help" data-time="60000">
            <div class="character">
                <div class="character-text">
                    <div class="holder">
                        <div class="character-text__inner">{!!__('catalog.product_page.character_text')!!}</div>
                        <button class="close"><i class="icon-close"></i></button>
                    </div>
                </div>
                <div class="character-img__holder">
                    <img data-src="images/charackter/Lisoped_man_8.svg" alt="character" class="character-img lazy">
                </div>
            </div>
        </div>

        <div class="page-overlay"></div>
    </main>

    <script type="text/javascript">
        $(document).on('change', '.product-info__options input', function (e) {
            $.ajax({
                url: '{{url()->current()}}' + '?' + $('.product-info__options input:checked').serialize(),
                success: function(result) {
                    if(result['status'] && result['status'] === 1){
                        $('#current-sku-content').replaceWith(result['template']);
                        if ($(window).innerWidth() >= 767) {
                            $('#glasscase').glassCase({
                                zoomPosition: 'inner',
                                thumbsPosition: 'bottom',
                                widthDisplay: 654,
                                heightDisplay: 370,
                                autoInnerZoom: true,
                                nrThumbsPerRow: 4,
                                isZoomDiffWH: true,
                                isShowAlwaysIcons: true,
                                isDownloadEnabled: false,
                                zoomMargin: 10,
                                colorActiveThumb: '#c6c6c6',
                                thumbsMargin: 25
                            });
                        } else {
                            $('#glasscase').glassCase({
                                isOverlayEnabled: false,
                                zoomPosition: 'inner',
                                thumbsPosition: 'bottom',
                                widthDisplay: 654,
                                heightDisplay: 370,
                                autoInnerZoom: true,
                                nrThumbsPerRow: 4,
                                isZoomDiffWH: true,
                                isShowAlwaysIcons: true,
                                isDownloadEnabled: false,
                                zoomMargin: 10,
                                colorActiveThumb: '#c6c6c6',
                                thumbsMargin: 25
                            });
                        }
                    }
                }
            });
        });
        $('[data-form="answer"]').on('click', function () {

            var toAnswerBtn = $(this);
            var div = $(this).parent().parent();
            var reviewerName = toAnswerBtn.closest('.reviews-item').find('.reviews-item__reviewer').text();
            var parentCommentId = $(this).attr('data-parent-id');

            var html = "<form class='answer-form' enctype='multipart/form-data'>";
            html += "<div class='answer-form__head'>{{__('information.testimonials.answer')}} <strong>" + reviewerName + "</strong></div>";
            html += "<div class='d-flex justify-content-sm-between flex-wrap'>";
            html += "<div class='answer-form__content'>";
            html += "<textarea maxlength=\"3000\" name='text' cols='30' rows='2' required placeholder='{{__('information.testimonials.placeholder_answer')}}'></textarea>";
            html += "<label class='reviews-form__file'>";
            html += "<input type='file' name='images[]' multiple><span class='reviews-form__file-name'></span><span class='reviews-form__file-icon'><i class='icon-attach_file'></i></span>";
            html += "<input type='hidden' name='_token' value='{{csrf_token()}}'>";
            if (parentCommentId) {
                html += "<input type='hidden' name='parent_id' value='" + parentCommentId + "'>";
            }
            html += "<input type='hidden' name='product_id' value='{{$product->getAttributeValue('id')}}'>";
            html += "</label>";
            html += "</div>";
            html += "<div class='answer-form__info'>";
            html += "<input type='text' name='name' required placeholder='Ваше имя'><button type='submit' class='btn butt1'><span>{{__('information.testimonials.answer')}}</span></button><button class='butt2'><span>Отмена</span></button>";
            html += "</div>";
            html += "</div>";
            html += "</form>";

            var block = $(html);

            block.find(".butt2").click(function () {
                toAnswerBtn.css("display", "block");
                block.remove();
            });

            block.on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                    url: `{{route('product.addReview')}}`,
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        if (result['status'] && result['status'] === 1) {
                            $('.answer-form').append('<div class="success active"><div class="s-inner"><span>' + result['message'] + '</span></div></div>');
                            setTimeout(function () {
                                $('.answer-form .success').remove();
                                $('.answer-form')[0].reset();
                            }, 4000);
                        }
                    }
                });
            });

            div.after(block);
            toAnswerBtn.css("display", "none");
        });
    </script>
    <script type="text/javascript">
        $(document).on('click', '#addProductToCart', function (e) {
            var skuId = $(this).data('sku-id');
            $.ajax({
                url: `{{route('cart.add')}}`,
                data: {
                    sku_id: skuId,
                    quantity: $('#quantity').val(),
                    _token: `{{csrf_token()}}`
                },
                type: 'POST',
                success: function (result) {
                    if (result['status'] && result['status'] === 1) {
                        if (result['cart_quantity']) {
                            var html = '<span class="num-total">' + result['cart_quantity'] + '</span>';
                            $('.hub-i__cart .num-total').remove();
                            $('.hub-i__cart .i-shopping-cart').after(html);
                        }
                        if (result['cart_popup']) {
                            $('#cart-popup').html(result['cart_popup']);
                        }
                    }
                }
            });
        });
        $(document).on('click', '.credit-action', function (e) {
            e.preventDefault();
            var parts = $($(this).data('parts-input')).val();
            var url = $(this).attr('href') + '&parts=' + parts;
            console.log(url);
            $.ajax({
                url: url,
                type: 'GET',
                success: function (result) {
                    console.log(result);
                    if (result == 1) {
                        window.location = '/checkout';
                    }
                }
            });
        });
    </script>

    <script type="text/javascript">
        $('.reviews-form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: `{{route('product.addReview')}}`,
                data: new FormData(this),
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (result) {
                    if (result['status'] && result['status'] === 1) {
                        $('.reviews-form').append('<div class="success active"><div class="s-inner"><span>' + result['message'] + '</span></div></div>');
                        setTimeout(function () {
                            $('.reviews-form .success').remove();
                            $('.reviews-form')[0].reset();
                        }, 4000);
                    }
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).on('submit', '#one-click-buy__popup form', function (e) {
            e.preventDefault();
            var quantity = parseInt($('#quantity').val());
            quantity = (quantity && Number.isInteger(quantity) && quantity > 0) ? quantity : 1;
            $.ajax({
                url: `{{route('popupProcessor.oneClickBuy')}}`,
                data: $(this).serialize() + '&quantity=' + quantity,
                type: 'POST',
                success: function (result) {
                    if (result['status'] && result['status'] === 1) {
                      $('#one-click-buy__popup').append('<div class="success active"><div class="s-inner"><span class="success-title">' + result['title'] + '</span><span>' + result['message'] + '</span></div></div>');
											$('#one-click-buy__popup form').addClass('hidden');
											setTimeout(function () {
													$('#one-click-buy__popup .success').remove();
													$.magnificPopup.close();
													$('#one-click-buy__popup form')[0].reset();
                                                    $('#one-click-buy__popup form').removeClass('hidden');
											}, 4000);
                    }
                }
            });
        });
    </script>
    <script type="text/javascript">
        function changeProductQuantity(value) {
            var quantityInput = $('#quantity');
            var currentValue = parseInt(quantityInput.val());
            var resultValue = currentValue + value;
            var skuId = parseInt({{$currentSku->id}});

            if (resultValue > 0) {
                quantityInput.val(resultValue);
                updatePrice(skuId, resultValue);
            }
        }

        function updatePrice(skuId, quantity) {
            $.ajax({
                url: `{{route('product.calculatePrices')}}`,
                data: {
                    sku_id: skuId,
                    quantity: quantity
                },
                success: function (result) {
                    var priceDiv = $('.product-info__price');
                    var html;
                    if (result['discount'] || result['special']) {
                        if (result['discount']) {
                            html = '<div class="main-price">' + result['discount'] + '</div>'
                                + '<div class="old-price">' + result['price'] + '</div>';
                        } else if (result['special']) {
                            html = '<div class="main-price">' + result['special'] + '</div>'
                                + '<div class="old-price">' + result['price'] + '</div>';
                        }
                    } else {
                        html = '<div class="main-price">' + result['price'] + '</div>';
                    }
                    priceDiv.html(html);
                }
            });
        }
    </script>
@endsection
