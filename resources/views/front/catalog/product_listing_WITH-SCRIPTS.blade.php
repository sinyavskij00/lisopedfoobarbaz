@extends('front.layout')
@section('robots')
    @if(empty(Request::query()) && strpos(Request::fullUrl(), 'filter') === false)

    @else
        <meta name="robots" content="noindex, follow"/>
    @endif

@stop
@section('content')
    <script>// #product_block_for_dom_list_shuffling
/*        $(document).ready(function () {
            if ((!document.location.href.includes('query=')) && (!document.location.href.includes('filter='))) {

                document.getElementById('products-row-list-block').style.cssText = "opacity: 0"
                document.getElementById('pg-prod-block').style.cssText = "display:none"

                var parent = $("#parent_product_block_for_dom_list_shuffling")
                var divs = $("#parent_product_block_for_dom_list_shuffling #product_block_for_dom_list_shuffling")

                var counter = 0
                while (divs.length) {
                    counter++
                   // if (divs[counter].children().attr('id') != 'not-available-product-block') {
                        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
                  //  }
                }
                $.when(counter == divs.length).then(function () {
                    setTimeout(function () {
                        document.getElementById('products-row-list-block').style.cssText = 'position: relative; opacity:1';
                        var v = document.getElementById('pg-prod-block')
                        $("pg-prod-block").remove()
                        document.getElementById('parent_product_block_for_dom_list_shuffling').appendChild(v)
                        document.getElementById('pg-prod-block').style.display = 'block';
                    }, 100)
                })
            } else {
// document.getElementById('products-row-list-block').style.cssText = 'position: relative; opacity:1';
//                     var v = document.getElementById('pg-prod-block')
//                     $("pg-prod-block").remove()
//                     document.getElementById('parent_product_block_for_dom_list_shuffling').appendChild(v)
//                     document.getElementById('pg-prod-block').style.display = 'block';
            }
        })*/
    </script>


    <div id="my-content" class="page-content p-padding">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
            </div>
        </div>

        <div class="catalog-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col filter__wrapper">
                        @include('front.catalog.filter')
                    </div>

                    <div class="col-lg-8 col-12">
                        <main class="catalog__main-content">

                            <div class="filter-trigger" data-toggle="filter">
                                <i class="icon-filter"></i>
                                <span>{{__('catalog.product_listing.filters')}}</span>
                            </div>

                            <h1 class="p-title">{{$meta_h1}}</h1>

                            <div class="products-row">
                                @if(count($pages) > 0)
                                    <div class="row">
                                        <div class="pages_box">
                                            @if(count($pages) > 5)
                                                <div class="more_and_less">
                                                    <button id="button_more" class="visible-button btn btn-primary">
                                                        Больше >
                                                    </button>
                                                    <button id="button_less" class="hidden-button btn btn-primary"> <
                                                        Меньше
                                                    </button>
                                                </div>
                                            @endif
                                            <ul id="some_list" class="links"
                                                style="display: inline-grid; grid-template-columns: repeat(auto-fit, 100px); max-width: 40vw; list-style-type: none">
                                                @foreach($pages as $key)
                                                    <li class="hidden badge badge-primary pageseo">
                                                        <a href="{{ $key['slug'] }}">{{ $key['name'] }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="product-sort__group d-sm-flex flex-sm-row align-items-sm-center justify-content-sm-between">

                                <div class="product-sort__item d-sm-flex align-items-center has-dropdown__list">
                                    <span class="product-sort__item-name">{{__('catalog.product_listing.sort_title')}}</span>
                                    @foreach($sorts as $sort)
                                        @if($sort['is_active'])
                                            <span class="product-sort__item-selected">{{$sort['text']}}</span>
                                        @endif
                                    @endforeach
                                    <ul class="product-sort__item-select dropdown-list">
                                        @foreach($sorts as $sort)
                                            @if(!$sort['is_active'])
                                                <li><a href="{{$sort['link']}}">{{$sort['text']}}</a></li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>

                                <div class="product-sort__item d-sm-flex align-items-center has-dropdown__list">
                                    <span class="product-sort__item-name">{{__('catalog.product_listing.limit_title')}}</span>
                                    @foreach($limits as $limit)
                                        @if($limit['is_active'])
                                            <span class="product-sort__item-selected">{{$limit['value']}}</span>
                                        @endif
                                    @endforeach
                                    <ul class="product-sort__item-select dropdown-list">
                                        @foreach($limits as $limit)
                                            @if(!$limit['is_active'])
                                                <li><a href="{{$limit['link']}}">{{$limit['value']}}</a></li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                            <div class="product-count-wrap"><p>Найдено {{$searchCountProduct}} товаров</p></div>
                            <div id="products-row-list-block" class="products-row">
                                <div id="parent_product_block_for_dom_list_shuffling" class="row">
{{--                                    <div id="products-listing-item-wrapper">--}}
{{--                                        @foreach($shuffle === true ? $products->shuffle() : $products as $product)--}}
                                    @foreach($products as $product)
                                            <div id="product_block_for_dom_list_shuffling" class="col-sm-6">
                                            @include('front.catalog.product_item', ['product' => $product])
                                            <!-- /.product-item -->
                                            </div>
                                        @endforeach
{{--                                    </div>--}}
                                    <div class="col-12" id="pg-prod-block">
                                        @if($products->hasMorePages())
                                            <a id="load-content" class="btn-more">
                                                <span>Показать еще</span>
                                            </a>
                                            <script type="text/javascript">
                                                var currentPage = parseInt({{$products->currentPage()}});
                                                var lastPage = parseInt({{$products->lastPage()}});

                                                $('#load-content').on('click', function (e) {
                                                    e.preventDefault();
                                                    $.ajax({
                                                        url: `{!! Request::fullUrl() !!}`,
                                                        data: {
                                                            page: ++currentPage,
                                                            action: 'show_more'
                                                        },
                                                        success: function (result) {
                                                            if (result['status'] && result['status'] === 1) {
                                                                var html = result['template'];
                                                                $('#load-content').parent().before(html);
                                                                if (currentPage === lastPage) {
                                                                    $('#load-content').parent().remove();
                                                                }
                                                            }
                                                        }
                                                    });
                                                });
                                            </script>
                                        @endif
                                        {{$products->links('front.paginate')}}
                                    </div>
                                </div>
                            </div>

                            @if(!empty($cities))
                                <div class="links">
                                    <div class="links__list">
                                        @forelse($cities as $city)
                                            <div class="links__list-item">
                                                <a href="{{url('/city')}}/{{$city->url}}">{{$city->city}}</a>
                                            </div>
                                        @empty

                                        @endforelse
                                    </div>
                                </div>
                            @endif

                        </main>
                    </div>

                </div>
            </div>
        </div>

        @if(!empty($description) && empty($page_number))
            <section class="description">
                <div class="container">

                    <div class="text-description show-text">
                        <div>
                            <div class="text-wrap">
                                <div class="text">
                                    {!! $description !!}
                                </div>
                            </div>
                            <div class="btn-wrap hidden">
                                <span class="btn__text-more">{{__('catalog.product_listing.read_more')}}</span>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            @if(!empty($jsonLd))
                <script type="application/ld+json">
                    @json($jsonLd)
                </script>
            @endif
        @endif
        <div class="character-wrapper character-filter--help character-left" data-time="15000">
            <div class="character">
                <div class="character-text">
                    <div class="holder">
                        <div class="character-text__inner">{{__('catalog.product_listing.character_text')}}</div>
                        <button class="close"><i class="icon-close"></i></button>
                    </div>
                </div>
                <div class="character-img__holder">
                    <img data-src="images/charackter/Lisoped_man_11.svg" alt="character" class="character-img lazy">
                </div>
            </div>
        </div>

        <div class="page-overlay">
        </div>
    </div>
    <script>
/*        window.onload = function () {
            var elements = document.getElementById('some_list').childNodes
            var len = (elements.length + 5) - elements.length
            var button_more = document.getElementById('button_more')
            var button_less = document.getElementById('button_less')

            if (elements.length > 5) {

                for (var i = 0; i <= 10; i++) {
                    if (elements[i] instanceof HTMLElement) {
                        elements[i].classList.remove('hidden')
                        elements[i].classList.add('visible')
                    }
                }

                button_less.addEventListener('click', function () {
                    for (var i = 10; i < elements.length; i++) {
                        if (elements[i] instanceof HTMLElement) {
                            elements[i].classList.remove('visible')
                            elements[i].classList.add('hidden')
                        }
                    }
                    button_less.classList.remove('visible-button')
                    button_less.classList.add('hidden-button')

                    button_more.classList.remove('hidden-button')
                    button_more.classList.add('visible-button')
                })
                button_more.addEventListener('click', function () {
                    for (var i = 0; i < elements.length; i++) {
                        if (elements[i] instanceof HTMLElement) {
                            elements[i].classList.remove('hidden')
                            elements[i].classList.add('visible')
                        }
                    }
                    button_more.classList.remove('visible-button')
                    button_more.classList.add('hidden-button')

                    button_less.classList.remove('hidden-button')
                    button_less.classList.add('visible-button')
                })
            } else {
                for (var i = 0; i < elements.length; i++) {
                    if (elements[i] instanceof HTMLElement) {
                        elements[i].classList.remove('hidden')
                        elements[i].classList.add('visible')
                    }
                }
            }
        }*/
    </script>
@endsection

