<div id="current-sku-content">
    <div class="row align-items-md-center align-items-lg-start">
        <div class="col-md-6 col-lg-7">
            <div class="product-images">

                <div class="product-item__stickers">
                    @if($currentSku->getAttributeValue('special_price'))
                        <div class="product-item__sticker sticker_sale">
                            <span class="sticker_text">{{__('catalog.labels.sale')}}</span>
                            <span class="sticker_percent">-{{ (int) round((1 - ($currentSku->getAttributeValue('special_price') / $currentSku->getAttributeValue('price'))) * 100)}} %</span>
                        </div>
                    @endif
                    @if($product->getAttributeValue('is_our_choice'))
                        <div class="product-item__sticker sticker_our-choice">
                            <span class="sticker_text">{{__('catalog.labels.our_choice')}}</span>
                        </div>
                    @endif
                    @if($product->getAttributeValue('is_new'))
                        <div class="product-item__sticker sticker_new">
                            <span class="sticker_text">{{__('catalog.labels.new')}}</span>
                        </div>
                    @endif
                    @if($product->getAttributeValue('is_hit'))
                        <div class="product-item__sticker sticker_hit">
                            <span class="sticker_text">{{__('catalog.labels.hit')}}</span>
                        </div>
                    @endif
                </div>
                <ul id="glasscase" class="gc-start">
                    @foreach($currentSku->images as $image)
                        <li><img src="{{$catalogService->resize($image->getAttributeValue('image'), 1500, 900)}}"
                                 title="{{$product->getAttributeValue('name')}}"
                                 alt="{{$product->getAttributeValue('name')}}">
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-md-6 col-lg-5">
            <div class="product-info">
                <div class="product-info__top d-flex align-items-center justify-content-between">
                    <div class="rating-wrap">
                        <div class="product-info__rating rating">
                            @for ($i = 1; $i <= 5; $i++)
                                @if($product->rating >= $i)
                                    <i class="icon-star-full i_stack-2x"></i>
                                @else
                                    <i class="icon-star-full i_stack-1x"></i>
                                @endif
                            @endfor
                        </div>
                        <a href="#product-info__tabs" class="reviews-number">
                            ({{ $product->activeReviews->count() }} {{ __('catalog.current_sku.reviews') }})
                        </a>
                    </div>
                    <div class="product-info__btns">
                        @if( $compare->isCompared($product))
                            <button type="button" onclick="compare.delete({{ $product->getAttributeValue('id')}}, this)"
                                    class="product-item__btn" title="В сравнение">
                                <i class="i i-lawyer accent"></i>
                            </button>
                        @else
                            <button type="button" onclick="compare.add({{ $product->getAttributeValue('id')}}, this)"
                                    class="product-item__btn" title="В сравнение">
                                <i class="i i-lawyer"></i>
                            </button>
                        @endif

                        @if($wishListService->isWished($product))
                            <button type="button" onclick="wishlist.delete({{ $product->getAttributeValue('id')}}, this)"
                                    class="product-item__btn" title="В закладки">
                                <i class="i icon-heart"></i>
                            </button>
                        @else
                            <button type="button" onclick="wishlist.add({{ $product->getAttributeValue('id')}}, this)"
                                    class="product-item__btn" title="В закладки">
                                <i class="i i-heart"></i>
                            </button>
                        @endif
                    </div>
                </div>

                <div class="product-info__d-list">
                    <div class="product-info__d-item">
                        <span class="product-info__d-item--name">{{__('catalog.current_sku.sku')}}</span>
                        <span class="product-info__d-item--value">{{$currentSku->getAttributeValue('sku')}}</span>
                    </div>
                    @if($product->getAttributeValue('manufacturer_name'))
                        <div class="product-info__d-item">
                            <span class="product-info__d-item--name">{{__('catalog.current_sku.manufacturer')}}</span>
                            <span class="product-info__d-item--value">{{$product->getAttributeValue('manufacturer_name')}}</span>
                        </div>
                    @endif
                    @if(!empty($sizeTables))
                        <a href="#sizes-popup" class="product-info__sizes-link to-popup">{{__('catalog.current_sku.size_table')}}</a>
                    @endif
                </div>

                @if(isset($product->shares) && count($product->shares) > 0)
                    @if($product->shares->first()->shares->status == 1)
                        <div class="product-info__gift">
                            <i class="icon-gift"></i>
                            <span class="gift-descr">
                            <b>{{$product->shares->first()->shares->title}}</b>
                            <a href="{{url('/shares/' . $product->shares->first()->shares->slug )}}" class="gift-descr__link">Условия акции</a>
                        </span>
                        </div>
                    @endif

                @endif
                <div class="product-info__options">
                    <div class="product-info__options-row">
                        @if($years->count() > 0 && !is_null($years[0]))
                            <div class="product-info__option product-info__option--size">
                                <div class="product-info__option-name">{{__('catalog.current_sku.year')}}</div>
                                <div class="product-info__option--flex">
                                    <div class="product-info__option-values">
                                        @foreach($years as $year)
                                            @if(isset($year->value))
                                                <label class="product-info__option-value">
                                                    <input type="radio" name="year"
                                                           value="{{$year->value}}"
                                                           {{$year->checked ? 'checked' : ''}}
                                                           class="product-info__option-value--input">
                                                    <span class="product-info__option-value--size">{{$year->value}}</span>
                                                </label>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if($sizes->count() > 0 && !is_null($sizes[0]))
                            <div class="product-info__option product-info__option--size">
                                <div class="product-info__option-name">{{__('catalog.current_sku.size')}}</div>
                                <div class="product-info__option-values">
                                    @foreach($sizes as $size)
                                        @if(isset($size->value))
                                            <label class="product-info__option-value">
                                                <input type="radio" name="size"
                                                       value="{{$size->value}}"
                                                       {{$size->checked ? 'checked' : ''}}
                                                       class="product-info__option-value--input">
                                                <span class="product-info__option-value--size">{{$size->value}} </span>
                                                @if($size->comment)
                                                    <span class="product-info__option-value--tooltip">
                                                <span class="accent">{{$size->value}}</span>
                                                ({{$size->comment}})
                                            </span>
                                                @endif
                                            </label>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                    @if($colors->count() > 0 && !is_null($colors[0]))
                        <div class="product-info__option product-info__option--color">
                            <div class="product-info__option-name">{{__('catalog.current_sku.color')}}</div>
                            <div class="product-info__option-values">
                                @foreach($colors as $color)
                                    @if(Storage::disk('public')->exists($color->image))
                                        @if(isset($color->anchor))
                                        <label class="product-info__option-value">
                                            <input type="radio" name="color"
                                                   value="{{$color->anchor}}"
                                                   {{$color->checked ? 'checked' : ''}}
                                                   class="product-info__option-value--input">
                                            <img class="lazy product-info__option-value--color"
                                                 data-src="{{Storage::disk('public')->url($color->image)}}"
                                                 title="{{$color->localDescription->name}}">
                                        </label>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>

                <div class="product-info__price  @if($product->getAttributeValue('product_expected') === 1 && $currentSku->getAttributeValue('quantity') <= 0) product_expected @elseif($currentSku->getAttributeValue('quantity') <= 0) product-unavailable @endif">
                    @if($currentSku->getAttributeValue('special_price'))
                        <div class="main-price">{{$catalogService->format($catalogService->calculate($currentSku->getAttributeValue('special_price')))}}</div>
                        <div class="old-price">{{$catalogService->format($catalogService->calculate($currentSku->getAttributeValue('price')))}}</div>
                    @else
                        <div class="main-price">{{$catalogService->format($catalogService->calculate($currentSku->getAttributeValue('price')))}}</div>
                    @endif
                </div>

                <div class="product-info__main-controls">
                    @if($currentSku->getAttributeValue('quantity') > 0)
                        <div class="product-info__main-controls--col">
                            <div class="product-info__count">
                                <div class="product-info__button-decrease" onclick="changeProductQuantity(-1)">-</div>
                                <input class="product-info__count-value" type="text" readonly="" id="quantity"
                                       name="quantity"
                                       value="1">
                                <div class="product-info__button-increase" onclick="changeProductQuantity(1)">+</div>
                            </div>

                            {{-- Если нужно вывести кнопки рассрочки, то следующую кнопку id="addProductToCart" расскомментируем--}}
                            {{--<a href="#cart-popup" id="addProductToCart"--}}
                               {{--data-sku-id="{{$currentSku->id}}"--}}
                               {{--class="btn to-popup"><span>{{__('catalog.current_sku.btn_buy')}}</span></a>--}}
                            <a href="#cart-popup" id="addProductToCart"
                               data-sku-id="{{$currentSku->id}}"
                               class="btn to-popup"><span>{{__('catalog.current_sku.btn_buy')}}</span></a>

                            <span class="product-info__availability product-info__availability--available">{{__('catalog.current_sku.available')}}</span>
                        </div>
                    @endif
                    <div class="product-info__main-controls--col">
                        @if($currentSku->getAttributeValue('quantity') > 0)

                            {{-- Если нужно вывести кнопки рассрочки, то следующую кнопку id="addProductToCart" комментируем, а блоки ниже: class="product__part-payment" и кнопку id="addProductToCredit" - расскоментируем --}}


                            <div class="product__part-payment">
                                @if($product->is_payparts === 1 && ($currentSku->getAttributeValue('price') < 50000 && $currentSku->getAttributeValue('price') > 1000))
                                    <a href="#privat" class="product__part-payment--item js-part-payment">
                                        <img src="images/credit/svg/privat.svg" alt="Приват банк" class="product__part-payment--icon" title="Приват банк">
                                    </a>
                                @endif

                                    {{--<a href="#mono" class="product__part-payment--item js-part-payment">
                     <img src="images/credit/svg/mono.svg" alt="Моно банк" class="product__part-payment--icon" title="Моно банк">
                 </a> --}}
                                @if($currentSku->getAttributeValue('price') < 70000 && $currentSku->getAttributeValue('price') > 1000)
                                    <a href="#alfa" class="product__part-payment--item js-part-payment">
                                        <img src="images/credit/svg/alfabank-logo-red-on-white.svg" alt="Альфа банк" class="product__part-payment--icon" title="Альфа банк">
                                    </a>
                                @endif

                            </div>

                            @if(($product->is_payparts === 1 && ($currentSku->getAttributeValue('price') < 50000 && $currentSku->getAttributeValue('price') > 1000)) || ($currentSku->getAttributeValue('price') < 70000 && $currentSku->getAttributeValue('price') > 1000))
                                <a href="#credit__popup" id="addProductToCredit"
                                   data-sku-id="{{$currentSku->id}}"
                                   class="btn-continue to-popup"><span>Купить в рассрочку</span></a>
                            @endif


                            <a href="#one-click-buy__popup" class="buy-one-click-btn to-popup">
                                <span>{{__('catalog.current_sku.buy_one_click')}}</span>
                            </a>
                        @else
                            @if($product->getAttributeValue('product_expected') === 1)
                                <a id="notAvailability" href="#preorder" class="btn btn__accent btn__notavailable to-popup">
                                    <span>{{__('catalog.current_sku.not_available')}}</span>
                                </a>

                                <div class="product-item__availability availability product_expected">
                                    {{__('catalog.product_item.product_expected')}}
                                </div>
                                @else
                                <div class="product-info__not-available">
                                    <div class="availability">
                                        {{__('catalog.product_item.not_available')}}
                                    </div>

                                    <a id="notAvailability" href="#preorder" class="btn btn__accent btn__notavailable to-popup">
                                        <span>{{__('catalog.current_sku.not_available')}}</span>
                                    </a>
                                </div>

                                {{--<div class="product-item__availability availability not-available">--}}
                                    {{--{{__('catalog.product_item.not_available')}}--}}
                                {{--</div>--}}
                            @endif

                        @endif
                    </div>
                </div>
                @if(!empty($productAdvantages) && $productAdvantages->count() > 0)
                <div class="product-info__bottom">
                    @foreach($productAdvantages as $productAdvantage)
                    <a href="#product-advantage-{{$productAdvantage->id}}" class="product-info__bottom-link to-popup">
                        <span class="{{$productAdvantage->icon}}"></span>
                        <span class="text">{{$productAdvantage->localDescription->title}}</span>
                    </a>
                    @endforeach
                </div>

                <div class="hidden">
                    @foreach($productAdvantages as $productAdvantage)
                    <div id="product-advantage-{{$productAdvantage->id}}" class="product-info__popup">
                        {!! $productAdvantage->localDescription->text !!}
                        <div class="t-a-right">
                            <a style="border-bottom: 1px dashed #656565;" href="{{$productAdvantage->link}}">{{__('catalog.current_sku.product_advantages.btn_more')}}</a>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="hidden">

        <div id="credit__popup" class="credit__popup popup">
            <form action="" class="credit__form">
                <div class="formhead">Купить в рассрочку</div>
                <input type="hidden" name="sku_id" value="{{$currentSku->getAttributeValue('id')}}">
                <div class="credit__variants">
                    <div class="credit__variants-head">
                       <div class="flex ai-c jc-sb">
                           <div class="col__offers">Предложения</div>
                           <div class="col__restrictions">Ограничения</div>
                           <div class="col__term">Срок кредита, мес</div>
                           <div class="col__action"></div>
                       </div>
                    </div>
                    <div class="credit__variants-body">
                        @if($product->is_payparts === 1  && ($currentSku->getAttributeValue('price') < 50000 && $currentSku->getAttributeValue('price') > 1000))
                            <div class="flex ai-c jc-sb">
                                <div class="col__offers">
                                    <span>Оплата частями <br>ПриватБанк</span>
                                    <img src="images/credit/logo/private-payparts.png" alt="ПриватБанк">
                                </div>
                                <div class="col__restrictions">до 50 000 грн</div>
                                <div class="col__term">
                                    <div class="term-holder">
                                        <select name="term" id="method-private">
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                        </select>
                                        <span></span>
                                    </div>
                                </div>
                                <div class="col__action">
                                    <a href="{{url('/cart/add-credit')}}?method=private&sku={{$currentSku->id}}" class="credit-action btn" data-parts-input="#method-private"><span>Оформить</span></a>
                                </div>
                            </div>
                        @endif


                       {{--
                        <div class="flex ai-c jc-sb">
                            <div class="col__offers">
                                <span>Покупка частями <br>ПАТ "Унiверсал Банк"</span>
                                <img src="images/credit/logo/mono-logo-check.png" alt="Monobank">
                            </div>
                            <div class="col__restrictions">до 100 000 грн</div>
                            <div class="col__term">
                                <div class="flex ai-c">
                                    <select name="term" id="method-mono">
                                        <option value="04">04</option>
                                    </select>
                                    <span></span>
                                </div>
                            </div>
                            <div class="col__action">
                                <a href="{{url('/cart/add-credit')}}?method=monobank&sku={{$currentSku->id}}" class="credit-action btn" data-parts-input="#method-mono" ><span>Оформить</span></a>
                            </div>
                        </div>
                        --}}
                            @if($currentSku->getAttributeValue('price') < 70000 && $currentSku->getAttributeValue('price') > 1000)
                                <div class="flex ai-c jc-sb">
                                    <div class="col__offers">
                                        <span>Супер рассрочка от Альфа-Банк</span>
                                        <img src="images/credit/logo/alfabank.png" alt="Альфа-Банк">
                                    </div>
                                    <div class="col__restrictions">до 70 000 грн</div>
                                    <div class="col__term">
                                        <div class="flex ai-c">
                                            <select name="term" id="method-alfa">
                                                <option value="02">02</option>
                                                <option value="03">03</option>
                                                <option value="04">04</option>
                                                <option value="05">05</option>
                                                <option value="06">06</option>
                                                <option value="07">07</option>
                                                <option value="08">08</option>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                            </select>
                                            <span></span>
                                        </div>
                                    </div>
                                    <div class="col__action">
                                        <a href="{{url('/cart/add-credit')}}?method=alfa&sku={{$currentSku->id}}" class="credit-action btn" data-parts-input="#method-alfa" ><span>Оформить</span></a>
                                    </div>
                                </div>
                            @endif


                    </div>
                </div>
            </form>
            <button title="Close (Esc)" type="button" class="mfp-close">×</button>
        </div>

        <div id="one-click-buy__popup" class="one-click__popup popup">
            <form action="" class="popup-form">
                @csrf
                <div class="success">
                    <div class="s-inner">
                        {!! __('catalog.current_sku.one_click_buy_form.success') !!}
                    </div>
                </div>
                <div class="formhead">{{__('catalog.current_sku.one_click_buy_form.title')}}</div>
                <input type="hidden" name="sku_id" value="{{$currentSku->getAttributeValue('id')}}">
                <label>
                    <span>{{__('catalog.current_sku.one_click_buy_form.name')}}</span>
                    <input type="text" name="name" required
                           value="{{Auth::check() ? Auth::user()->first_name . ' ' . Auth::user()->last_name : ''}}">
                </label>
                <label>
                    <span>{{__('catalog.current_sku.one_click_buy_form.telephone')}}</span>
                    <input type="tel" name="phone" required value="{{ Auth::check() ? Auth::user()->telephone : '' }}">
                </label>
                <label>
                    <span>{{__('catalog.current_sku.one_click_buy_form.comment')}}</span>
                    <textarea name="text" rows="1" maxlength="300"></textarea>
                </label>
                <input type="hidden" name="product_id" value="{{$product->getAttributeValue('name')}}">
                <div class="btn-submit__wrap">
                    <button type="submit" class="btn">
                        <span>{{__('catalog.current_sku.one_click_buy_form.submit')}}</span>
                    </button>
                </div>
            </form>
        </div>

        <div id="preorder" class="one-click__popup popup">
            <form class="popup-form">
                @csrf
                <input type="hidden" name="sku_id" value="{{$currentSku->getAttributeValue('id')}}">
                <label>
                    <span>{{__('catalog.current_sku.preorder.name')}}</span>
                    <input type="text" name="name" required
                           value="{{Auth::check() ? Auth::user()->first_name . ' ' . Auth::user()->last_name : ''}}">
                </label>
                <label>
                    <span>{{__('catalog.current_sku.preorder.telephone')}}</span>
                    <input type="tel" name="telephone" required value="{{ Auth::check() ? Auth::user()->telephone : '' }}">
                </label>
                <label>
                    <span>{{__('catalog.current_sku.preorder.email')}}</span>
                    <input type="email" name="email" required value="{{ Auth::check() ? Auth::user()->email : '' }}">
                </label>
                <label>
                    <span>{{__('catalog.current_sku.preorder.comment')}}</span>
                    <textarea name="comment" maxlength="300"></textarea>
                </label>
                <div class="btn-submit__wrap">
                    <button type="submit" class="btn">
                        <span>{{__('catalog.current_sku.preorder.submit')}}</span>
                    </button>
                </div>
            </form>
            <script>
                $('#preorder form').on('submit', function (e) {
                    e.preventDefault();
                    $.ajax({
                        url: '{{route('popupProcessor.preorder')}}',
                        type: 'POST',
                        data: $(this).serialize(),
                        success: function (result) {
                            if (result['status'] && result['status'] === 1) {
                                $('#preorder').append('<div class="success active"><div class="s-inner"><span>' + result['message'] + '</span></div></div>');
                                $('#preorder form').addClass('hidden');
                                setTimeout(function () {
                                    $('#preorder .success').remove();
                                    $.magnificPopup.close();
                                    $('#preorder form')[0].reset().removeClass('hidden');
                                }, 4000);
                            }
                        }
                    });
                });
            </script>
        </div>
    </div>
</div>
