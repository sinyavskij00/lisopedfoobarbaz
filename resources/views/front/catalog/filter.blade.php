<aside id="filter" class="filter">

    @if(!empty(get_category_tree()))
        @foreach(get_category_tree() as $firstLevelCategory)

            @if($firstLevelCategory->status === 1 && $firstLevelCategory->slug === explode('/', Request::path())[0])
                <ul class="filter-categories filter-main-categories">
                    <li>
                        <a href="{{route('category_path', category_path($firstLevelCategory->getAttributeValue('id')))}}"
                           @if(Request::url() === route('category_path', category_path($firstLevelCategory->getAttributeValue('id')))) class="current-url"
                                @endif>{{$firstLevelCategory->localDescription->getAttributeValue('name')}}</a>

                        @if($firstLevelCategory->children)
                            <ul class="filter-categories filter-sub-categories">
                                @foreach($firstLevelCategory->children as $secondLevelCategory)
                                    @if($secondLevelCategory->status === 1)
                                        <li class="filter-categories__item filter-categories__item--closed">
                                            <div class="filter-categories__item-head">
                                                <a href="{{route('category_path', category_path($secondLevelCategory->getAttributeValue('id')))}}"
                                                   @if(Request::url() === route('category_path', category_path($secondLevelCategory->getAttributeValue('id'))))
                                                   class="current-url" @endif>{{$secondLevelCategory->localDescription->getAttributeValue('name')}}</a>
                                                <div class="filter-categories__item-btn"><i class="icon-caret-down"></i>
                                                </div>
                                            </div>
                                            @if($secondLevelCategory->children)
                                                <ul class="filter-categories__item-content">
                                                    @foreach($secondLevelCategory->children as $thirdLevelCategory)
                                                        @if($thirdLevelCategory->status === 1)
                                                            <li>
                                                                <a href="{{route('category_path', category_path($thirdLevelCategory->getAttributeValue('id')))}}"
                                                                   @if(Request::url() === route('category_path',
category_path($thirdLevelCategory->getAttributeValue('id'))))
                                                                   class="current-url" @endif>{{$thirdLevelCategory->localDescription->getAttributeValue('name')}}</a>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        @endif
                    </li>
                </ul>
            @endif
        @endforeach
    @endif

    <div class="filter-head filter-tab__toggle hidden " id="filters_clear_block">
        <div class="chosen-filter__clear __active all-filter-clear">
            <a href="{{$filter['reset']}}" class="justify-content-between d-flex w-100">
                <span>{{__('catalog.filter.reset_filter')}}</span>
                <i class="icon-close2"></i>
            </a>
        </div>
    </div>

    <div class="filter-tab">

        <div class="filter-tab__head">
            <span>{{__('catalog.filter.price')}}</span>
        </div>

        <div class="filter-tab__content open">
            <div class="filter-price__slider">
                <div id="keypress"></div>
                <div class="d-flex align-items-center justify-content-between">
                    <span class="filter-price__currency-text">{{__('catalog.filter.from')}}</span>
                    <div class="filter-price__input-wrap">
                        <input type="number" id="input-with-keypress-0"
                               data-min="{{$filter['price']['min']}}"
                               data-start="{{config('app.filter_data.price.min') !== null ? config('app.filter_data.price.min') : $filter['price']['min']}}"
                               data-max="{{$filter['price']['max']}}"
                               name="price-min" value="{{config('app.filter_data.price.min')}}">
                        <span class="filter-price__currency">грн</span>
                    </div>
                    <span class="filter-price__currency-text">{{__('catalog.filter.to')}}</span>
                    <div class="filter-price__input-wrap">
                        <input type="number" id="input-with-keypress-1"
                               data-start="{{config('app.filter_data.price.max') !== null ? config('app.filter_data.price.max') : $filter['price']['max']}}"
                               data-min="{{$filter['price']['min']}}" data-max="{{$filter['price']['max']}}"
                               name="price-max" value="{{config('app.filter_data.price.max')}}">
                        <span class="filter-price__currency">грн</span>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @if($filter['show_manufacturers'] && isset($filter['manufacturers']))
        <div class="filter-tab filter-tab__closed manufacturer-filter-tab" data-id="tab-manufacturers">

            <div class="filter-tab__head filter-tab__toggle">
                <span>{{__('catalog.filter.manufacturers')}}</span>
            </div>

            <div class="filter-tab__content">
                <ul class="filter-tab__options-list">

                    @foreach($filter['manufacturers'] as $manufacturer)

                        @if($manufacturer->getAttributeValue('products_count') > 0)

                            <li data-id="{{$manufacturer->id}}" {!! in_array($manufacturer->id, config('app.filter_data.manufacturers', [])) ? "class='filter-option__label
checked'"  : 'class="filter-option__label"' !!} >
                                <a class="filter-option"
                                   {!! config('app.is_nofollow_noindex', false) ? 'rel="nofollow, noindex"' : ''!!}
                                   href="{{$manufacturer->filter_link}}">
                                    <span class="filter-option__checkbox input-checkbox"></span>
                                    <span class="filter-option__name">{{$manufacturer->getAttributeValue('name')}}</span>
                                    <span class="filter-option__qnty">({{$manufacturer->getAttributeValue('products_count')}})</span>
                                </a>
                            </li>
                            {{--                            {!! in_array($manufacturer->id, config('app.filter_data.manufacturers', [])) ?
"<script>addChosenFilterCleaner();</script>"  : '' !!}--}}
                        @else

                            <li data-id="{{$manufacturer->id}}" {!! in_array($manufacturer->id, config('app.filter_data.manufacturers', [])) ? 'class="filter-option__label
checked"' : 'class="filter-option__label filter-disabled d-none"' !!} >
                                <a class="filter-option filter-disabled"
                                   {!! config('app.is_nofollow_noindex', false) ? 'rel="nofollow, noindex"' : ''!!}
                                   href="#">
                                    <span class="filter-option__checkbox input-checkbox"></span>
                                    <span class="filter-option__name">{{$manufacturer->getAttributeValue('name')}}</span>
                                    <span class="filter-option__qnty">({{$manufacturer->getAttributeValue('products_count')}})</span>
                                </a>
                            </li>
                        @endif


                    @endforeach
                </ul>
            </div>
        </div>
    @endif

    @if($filter['show_years'] === 1 && isset($filter['years']))
        <div class="filter-tab filter-tab__closed years-filter-tab" data-id="tab-years">

            <div class="filter-tab__head filter-tab__toggle">
                <span>{{__('catalog.filter.years')}}</span>
            </div>

            <div class="filter-tab__content">
                <ul class="filter-tab__options-list">
                    @foreach($filter['years'] as $year)
                        @if($year->getAttributeValue('products_count') > 0)
                            <li data-id="{{$year->id}}" {!! in_array($year->id, config('app.filter_data.year', [])) ? 'class="filter-option__label checked"' :
'class="filter-option__label"' !!} >
                                <a class="filter-option"
                                   {!! config('app.is_nofollow_noindex', false) ? 'rel="nofollow, noindex"' : ''!!}
                                   href="{{$year->filter_link}}">
                                    <span class="filter-option__checkbox input-checkbox"></span>
                                    <span class="filter-option__name">{{$year->getAttributeValue('value')}}</span>
                                    <span class="filter-option__qnty">({{$year->getAttributeValue('products_count')}})</span>
                                </a>

                            </li>
                        @else
                            <li data-id="{{$year->id}}" {!! in_array($year->id, config('app.filter_data.year', [])) ? 'class="filter-option__label checked filter-disabled"'
:
'class="filter-option__label d-none"' !!} >
                                <a class="filter-option filter-disabled"
                                   {!! config('app.is_nofollow_noindex', false) ? 'rel="nofollow, noindex"' : ''!!}
                                   href="#">
                                    <span class="filter-option__checkbox input-checkbox"></span>
                                    <span class="filter-option__name">{{$year->getAttributeValue('value')}}</span>
                                    <span class="filter-option__qnty">({{$year->getAttributeValue('products_count')}})</span>
                                </a>

                            </li>
                        @endif

                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    @if($filter['show_sizes'] === 1 && isset($filter['sizes']))
        <div class="filter-tab filter-tab__closed sizes-filter-tab" data-id="tab-sizes">

            <div class="filter-tab__head filter-tab__toggle">
                <span>{{__('catalog.filter.sizes')}}</span>
            </div>

            <div class="filter-tab__content">
                <ul class="filter-tab__options-list">
                    @foreach($filter['sizes'] as $size)

                        @if($size->products_count > 0)
                            <li data-id="{{$size->id}}" {!! in_array($size->id, config('app.filter_data.size', [])) ? 'class="filter-option__label checked"' :
'class="filter-option__label "' !!} >
                                <a class="filter-option"
                                   {!! config('app.is_nofollow_noindex', false) ? 'rel="nofollow, noindex"' : ''!!}
                                   href="{{$size->filter_link}}">
                                    <span class="filter-option__checkbox input-checkbox"></span>
                                    <span class="filter-option__name">{{$size->value}}</span>
                                    <span class="filter-option__qnty">({{$size->products_count}})</span>
                                </a>
                            </li>
                        @else
                            <li data-id="{{$size->id}}" {!! in_array($size->id, config('app.filter_data.size', [])) ? 'class="filter-option__label checked"' :
'class="filter-option__label filter-disabled d-none"' !!} >
                                <a class="filter-option filter-disabled"
                                   {!! config('app.is_nofollow_noindex', false) ? 'rel="nofollow, noindex"' : ''!!}
                                   href="#">
                                    <span class="filter-option__checkbox input-checkbox"></span>
                                    <span class="filter-option__name">{{$size->value}}</span>
                                    <span class="filter-option__qnty">({{$size->products_count}})</span>
                                </a>
                            </li>
                        @endif

                    @endforeach
                </ul>
            </div>
        </div>
    @endif

    @if(isset($filter['filters_cat']))
        @foreach($filter['filters_cat'] as $filterEntity)
            @if(count($filterEntity->valuesWithoutCount) > 0)
                <div class="filter-tab filter-tab__closed filters-filter-cat-tab"
                     data-id="filters_cat-{{$filterEntity->id}}">

                    <div class="filter-tab__head filter-tab__toggle">
                        <span>{{$filterEntity->name}}</span>
                    </div>

                    <div class="filter-tab__content">
                        <ul class="filter-tab__options-list">
                            @foreach($filterEntity->valuesWithoutCount->unique('slug')->sortBy('text', SORT_NUMERIC, false) as $filterValue)
                                @if($filterValue->products_count > 0)
                                    <li data-id="{{$filterValue->id}}" {!! in_array($filterValue->slug, config('app.filter_data.filter_attributes', [])) ?
'class="filter-option__label checked"' : 'class="filter-option__label"' !!} >
                                        <a class="filter-option"
                                           {!! config('app.is_nofollow_noindex', false) ? 'rel="nofollow, noindex"' : ''!!}
                                           href="{{$filterValue->filter_link}}">
                                            <span class="filter-option__checkbox input-checkbox"></span>
                                            <span class="filter-option__name">{{$filterValue->text}}</span>
                                            <span class="filter-option__qnty">({{$filterValue->products_count}})</span>

                                        </a>
                                    </li>
                                @else
                                    <li data-id="{{$filterValue->id}}" {!! in_array($filterValue->slug, config('app.filter_data.filter_attributes', [])) ?
'class="filter-option__label checked"' : 'class="filter-option__label filter-disabled d-none"' !!} >
                                        <a class="filter-option filter-disabled"
                                           {!! config('app.is_nofollow_noindex', false) ? 'rel="nofollow, noindex"' : ''!!}
                                           href="#">
                                            <span class="filter-option__checkbox input-checkbox"></span>
                                            <span class="filter-option__name">{{$filterValue->text}}</span>
                                            <span class="filter-option__qnty">({{$filterValue->products_count}})</span>

                                        </a>
                                    </li>
                                @endif

                            @endforeach
                        </ul>
                    </div>

                </div>
            @endif

        @endforeach
    @endif

    @if(isset($filter['filters']))
        @foreach($filter['filters'] as $filterEntity)
            <div class="filter-tab filter-tab__closed filters-filter-tab">

                <div class="filter-tab__head filter-tab__toggle">
                    <span>{{$filterEntity->name}}</span>
                </div>

                <div class="filter-tab__content">
                    <ul class="filter-tab__options-list">
                        @foreach($filterEntity->values as $filterValue)
                            <li data-id="{{$filterValue->id}}" {!! in_array($filterValue->id, config('app.filter_data.filter_values', [])) ? 'class="filter-option__label
checked"' : 'class="filter-option__label"' !!} >
                                <a class="filter-option"
                                   {!! config('app.is_nofollow_noindex', false) ? 'rel="nofollow, noindex"' : ''!!}
                                   href="{{$filterValue->filter_link}}">
                                    <span class="filter-option__checkbox input-checkbox"></span>
                                    <span class="filter-option__name">{{$filterValue->getAttributeValue('name')}}</span>
                                    <span class="filter-option__qnty">({{$filterValue->getAttributeValue('products_count')}})</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>

            </div>
        @endforeach
    @endif

</aside>
<script type="text/javascript">
    $('.chosen-filter__clear').on('click', function (e) {
        $('.filter input').each(function () {
            this.checked = false;
        });
    });
    $('.filter-tab .filter-tab__head').on('click', function () {
        $(this).closest('.filter-tab').toggleClass('filter-tab__closed');
        $(this).next('.filter-tab__content').slideToggle(300);
    });

    var checkedOptions = '.filter-tab .filter-option__label.checked';
    $(checkedOptions)
        .closest('.filter-tab__content').addClass('open')
        .closest('.filter-tab').removeClass('filter-tab__closed');

    if ($(checkedOptions).length) {
        $('#filter .filter-head').removeClass('hidden');
    }

    var checkedElems = $('.filter-option__label.checked').children('a');

    if(checkedElems) {
        checkedElems.each(function () {
            var parent_cat = $(this).parents('.filter-tab');
            var cat_name = parent_cat.children('.filter-tab__head').children('span').text();

            var href = $(this).attr('href');
            var name = $(this).children('.filter-option__name').text();
            var data_id = $(this).parent().attr('data-id');
            
var catAndName = cat_name + ' - ' + name;

var html = `
<div class='chosen-filter__clear  clear_list_element remove-filter-single-block' data-id-to-remove='${data_id}'>
<a class='filter-option drop-filter-link d-flex justify-content-between w-100' href='${href}'>
 <span> ${catAndName}</span>
 <span><i class='icon-close2'></i></span>
</a>
</div>
`

$('#filters_clear_block').prepend(html);
        })
    }




</script>
<script type="text/javascript">
    filterObj = {
        manufacturers: {},
        years: {},
        sizes: {},
        filters: {},
        filters_cat: {},
        priceMin: null,
        priceMax: null,
        lastKlickedElement: null,
        updateFilter: function (e) {
            e.preventDefault();
            e.stopPropagation();
            var href = $(this).find('a').attr('href');

            if (!$(this).find('a').hasClass('filter-disabled')) {
                $(this).toggleClass('checked').promise().done(function () {
                    var data_id_toggle = $(this).attr('data-id');
                    $('[data-id-to-remove = '+ data_id_toggle+ ']').css('display', 'none');
                });

                filterObj.lastKlickedElement = $(this);
                window.history.pushState({state: 1}, '', href);
                filterObj.makeRequest(href, $(this).closest('.filter-tab').attr('data-id'), $(this).attr('data-id'));
            }

        },
        changePrice: function (e) {
            var priceMin = $(filterObj.priceMin).val();
            var priceMax = $(filterObj.priceMax).val();

            filterObj.lastKlickedElement = $(this).closest('.filter-tab');

            var url = document.createElement('a');
            url.href = window.location;

            var targetUrl = '';

            var path = url.pathname;
            var filterStartIndex = path.indexOf('filter=');
            if (filterStartIndex === -1) {
                path += '/filter=price:' + priceMin + ',' + priceMax;
            } else {
                var priceStartIndex = path.indexOf('price:', filterStartIndex);
                if (priceStartIndex === -1) {
                    path += ';price:' + priceMin + ',' + priceMax;
                } else {
                    var priceEndIndex = path.indexOf(';', priceStartIndex);
                    var path1 = path.substring(0, priceStartIndex);
                    var path2 = priceEndIndex === -1 ? '' : path.substring(priceEndIndex);
                    path = path1 + 'price:' + priceMin + ',' + priceMax + path2;
                }
            }
            targetUrl += (path + url.search);
            filterObj.makeRequest(targetUrl, null, null);
        },
        makeRequest: function (href, filterTabID, linkID) {
            window.history.pushState({state: 1}, '', href);
            preloader.view.delete();


            if ($('#filter-ask-layer')){
                $('#filter-ask-layer').css('display','none');
            }

            $.ajax({
                url: href,
                beforeSend:() => {
                preloader.view.create();
        },
            success: (response) => {

                preloader.view.delete();
                insertResponseDataInDOM(response);
            },
            // error: (err)=> console.log(err),
        });


// callback function for insert response in DOM
            function insertResponseDataInDOM(response) {

                if (response && response['status'] === 1) {
                    var productsCount = response['products_count'];

                    var filter = response['filter'];
                    if (filter.manufacturers && filterTabID === 'tab-manufacturers' && $("div[data-id='" + filterTabID + "']").find('li.checked').length > 0) {
                        $("div[data-id='" + filterTabID + "']").find('li').each(function (i, elem) {
                            var elementText = $(elem).find('.filter-option__qnty').text();
                            var elemCount = elementText.replace(/[^-0-9]/gi, '');

                            if ($(elem).attr('data-id') !== linkID) {
                                $(elem).find('.filter-option__qnty').empty().append('(+' + elemCount + ')');
                            }
                        });
                        for (var manufacturer of filter.manufacturers) {
                            if (manufacturer.id && filterObj.manufacturers[manufacturer.id]) {
                                $(filterObj.manufacturers[manufacturer.id])
                                    .find('a')
                                    .attr('href', manufacturer.filter_link);
                            }
                        }
                    } else {
                        if (filter.manufacturers) {
                            for (var manufacturer of filter.manufacturers) {
                                if (manufacturer.id && filterObj.manufacturers[manufacturer.id]) {
                                    $(filterObj.manufacturers[manufacturer.id])
                                        .find('a')
                                        .attr('href', manufacturer.filter_link);

                                    $(filterObj.manufacturers[manufacturer.id]).find('.filter-option__qnty').empty().append('(' + manufacturer.count + ')');

                                    if (manufacturer.count === 0) {
                                        $(filterObj.manufacturers[manufacturer.id]).addClass('filter-disabled d-none');
                                        $(filterObj.manufacturers[manufacturer.id]).find('a').addClass('filter-disabled');
                                    } else {
                                        $(filterObj.manufacturers[manufacturer.id]).removeClass('filter-disabled d-none');
                                        $(filterObj.manufacturers[manufacturer.id]).find('a').removeClass('filter-disabled');
                                    }
                                }
                            }
                        }
                    }

                    if (filter.years && filterTabID === 'tab-years' && $("div[data-id='" + filterTabID + "']").find('li.checked').length > 0) {
                        $("div[data-id='" + filterTabID + "']").find('li').each(function (i, elem) {
                            var elementText = $(elem).find('.filter-option__qnty').text();
                            var elemCount = elementText.replace(/[^-0-9]/gi, '');
                            if ($(elem).attr('data-id') !== linkID) {
                                $(elem).find('.filter-option__qnty').empty().append('(+' + elemCount + ')');
                            }
                        });
                        for (var year of filter.years) {
                            if (year.id && filterObj.years[year.id]) {
                                $(filterObj.years[year.id])
                                    .find('a')
                                    .attr('href', year.filter_link);
                            }
                        }
                    } else {
                        if (filter.years) {
                            for (var year of filter.years) {
                                if (year.id && filterObj.years[year.id]) {
                                    $(filterObj.years[year.id])
                                        .find('a')
                                        .attr('href', year.filter_link);
                                    $(filterObj.years[year.id]).find('.filter-option__qnty').empty().append('(' + year.count + ')');
                                    if (year.count === 0) {
                                        $(filterObj.years[year.id]).addClass('filter-disabled d-none');
                                        $(filterObj.years[year.id]).find('a').addClass('filter-disabled ');
                                    } else {
                                        $(filterObj.years[year.id]).removeClass('filter-disabled d-none');
                                        $(filterObj.years[year.id]).find('a').removeClass('filter-disabled');
                                    }
                                }
                            }
                        }
                    }

                    if (filter.filters) {
                        for (var filterElement of filter.filters) {
                            if (filterElement.id && filterObj.filters[filterElement.id]) {
                                $(filterObj.filters[filterElement.id])
                                    .find('a')
                                    .attr('href', filterElement.filter_link);
                            }
                        }
                    }

                    if (filter.sizes && filterTabID === 'tab-sizes' && $("div[data-id='" + filterTabID + "']").find('li.checked').length > 0) {
                        $("div[data-id='" + filterTabID + "']").find('li').each(function (i, elem) {
                            var elementText = $(elem).find('.filter-option__qnty').text();
                            var elemCount = elementText.replace(/[^-0-9]/gi, '');
                            if ($(elem).attr('data-id') !== linkID) {
                                $(elem).find('.filter-option__qnty').empty().append('(+' + elemCount + ')');
                            }
                        });
                        for (var size of filter.sizes) {
                            if (size.id && filterObj.sizes[size.id]) {
                                $(filterObj.sizes[size.id])
                                    .find('a')
                                    .attr('href', size.filter_link);
                            }
                        }
                    } else {
                        if (filter.sizes) {
                            for (var size of filter.sizes) {
                                if (size.id && filterObj.sizes[size.id]) {
                                    $(filterObj.sizes[size.id])
                                        .find('a')
                                        .attr('href', size.filter_link);
                                    $(filterObj.sizes[size.id]).find('.filter-option__qnty').empty().append('(' + size.count + ')');
                                    if (size.count === 0) {
                                        $(filterObj.sizes[size.id]).addClass('filter-disabled d-none');
                                        $(filterObj.sizes[size.id]).find('a').addClass('filter-disabled');
                                    } else {
                                        $(filterObj.sizes[size.id]).removeClass('filter-disabled d-none');
                                        $(filterObj.sizes[size.id]).find('a').removeClass('filter-disabled');
                                    }
                                }
                            }
                        }
                    }

                    //FILTERS CAT
                    if (filter.filters_cat) {
                        for (var filterElementCat of filter.filters_cat) {
                            if (filterTabID === $(filterObj.filters_cat[filterElementCat.id]).closest('.filter-tab').attr('data-id') && $("div[data-id='" + filterTabID +
"']").find('li.checked').length > 0) {

                                $("div[data-id='" + filterTabID + "']").find('li').each(function (i, elem) {
                                    var elementText = $(elem).find('.filter-option__qnty').text();
                                    var elemCount = elementText.replace(/[^-0-9]/gi, '');

                                    // if ($(elem).attr('data-id') !== linkID) {
                                    $(elem).find('.filter-option__qnty').empty().append('(+' + elemCount + ')');
                                    // }
                                });

                                if (filterElementCat.id && filterObj.filters_cat[filterElementCat.id]) {

                                    var filterCheckboxName = $("li[data-id='" + filterElementCat.id + "']");

                                    if (filterCheckboxName.length > 1) {
                                        filterCheckboxName.each(function () {

                                            var checkbox_a = $(this).find('a');
                                            var item_name = checkbox_a.find('span.filter-option__name').text();
                                            var href = filterElementCat.filter_link;

                                            if (filterElementCat.name === item_name) {
                                                checkbox_a.attr('href', href);
                                            }
                                        });
                                    } else {

                                        $(filterObj.filters_cat[filterElementCat.id])
                                            .find('a')
                                            .attr('href', filterElementCat.filter_link);
                                    }
                                }

                            } else {

                                if (filterElementCat.id && filterObj.filters_cat[filterElementCat.id]) {

                                    var filterCheckboxName = $("li[data-id='" + filterElementCat.id + "']");

                                    if (filterCheckboxName.length > 1) {
                                        filterCheckboxName.each(function () {

                                            var checkbox_a = $(this).find('a');
                                            var item_name = checkbox_a.find('span.filter-option__name').text();
                                            var href = filterElementCat.filter_link;

                                            if (filterElementCat.name === item_name) {
                                                checkbox_a.attr('href', href);

                                                $(checkbox_a).find('span.filter-option__qnty').empty().append('(' + filterElementCat.count + ')');
                                                if (filterElementCat.count === 0) {

                                                    $(this).addClass('filter-disabled d-none');
                                                    $(this).find('a').addClass('filter-disabled');
                                                } else {
                                                    $(this).removeClass('filter-disabled d-none');
                                                    $(this).find('a').removeClass('filter-disabled');
                                                }
                                            }
                                        });
                                    } else {

                                        $(filterObj.filters_cat[filterElementCat.id])
                                            .find('a')
                                            .attr('href', filterElementCat.filter_link);

                                        $(filterObj.filters_cat[filterElementCat.id]).find('.filter-option__qnty').empty().append('(' + filterElementCat.count + ')');
                                        if (filterElementCat.count === 0) {
                                            $(filterObj.filters_cat[filterElementCat.id]).addClass('filter-disabled d-none');
                                            $(filterObj.filters_cat[filterElementCat.id]).find('a').addClass('filter-disabled');
                                        } else {
                                            $(filterObj.filters_cat[filterElementCat.id]).removeClass('filter-disabled d-none');
                                            $(filterObj.filters_cat[filterElementCat.id]).find('a').removeClass('filter-disabled');
                                        }
                                    }

                                }
                            }

                        }
                    }

                    filterObj.makeButton(productsCount, window.location);
                }
            }
        },
        makeButton: function (productsCount, href) {

            var span = filterObj.lastKlickedElement;
            var windowWidth = jQuery(window).width();
            var layer = $("#filter-ask-layer");

            if (layer.length == 0) {
                layer = $("<div id='filter-ask-layer' style='display: none; '><a class='btn' id='link-filter-ask-layer'><b>Показать</b></a><span></span></div>");
                if (windowWidth < 992) {
                    $("#filter").prepend(layer);
                } else {
                    $(".catalog-content").append(layer);
                }
            }

            layer.css("display", "none");
            layer.find("a").attr("data-href", href).css('cursor', 'pointer');

            var pos = span.offset();
            var name = span.find('.filter-option__name');
            var size = name.outerWidth();
            var priceSliderWidth = $('.filter-price__slider').outerWidth();

            if (productsCount == 0) {
                if (windowWidth < 992) {
                    var text = "0 товаров"
                } else {
                    var text = "Выбрано 0 товаров"
                }
                layer.find("a").css("display", "none")
            } else {
                if (windowWidth < 992) {
                    var text = productsCount + " товар" + NumberEnd(productsCount, ["", "а", "ов"]);
                } else {
                    var text = "Выбран" + NumberEnd(productsCount, ["", "о", "о"]) + " " + productsCount + " товар" + NumberEnd(productsCount, ["", "а",
"ов"]);
                }
                layer.find("a").css("display", "inline-block");
            }

            layer.find("span").text(text);

            if (windowWidth >= 992) {
                if (size) {
                    layer.css("left", pos.left + size + 60 + "px").css("top", pos.top + 3 + "px");
                } else {
                    layer.css("left", pos.left + priceSliderWidth + "px").css("top", pos.top + 57 + "px");
                }
            }

            layer.css("display", "block");
        }
    };

    var preloader = {
        html: {
            loader: () => {
            const html = `<div class="loader" id="loaderForFilter"></div>`;
    return $(html);
    },
    },
    view: {
        getPos: () => {
            return filterObj.lastKlickedElement.offset();
        },
        delete: () => {
            var loader = $('#loaderForFilter');
            if (loader) {
                loader.remove();
            }
        },
        create: (position = null) => {

            var loader = preloader.html.loader();
            var span = null;
            if (position){
                span = position;
            } else {
                span = filterObj.lastKlickedElement;
            }

            var windowWidth = jQuery(window).width();
            var pos = span.offset();
            var name = span.find('.filter-option__name');
            var size = name.outerWidth();
            var priceSliderWidth = $('.filter-price__slider').outerWidth();

            if (windowWidth >= 992) {
                if (size) {
                    loader.css("left", pos.left + size + 120 + "px").css("top", pos.top + "px");

                } else {
                    loader.css("left", pos.left + priceSliderWidth + 60 + "px").css("top", pos.top + 60 + "px");
                }
                $(".catalog-content").append(loader);
            } else {
                $("#filter").prepend(loader);
            }
        }
    },
    };


    function NumberEnd(mNumber, mEnds) {
        var cases = [2, 0, 1, 1, 1, 2];
        return mEnds[(mNumber % 100 > 4 && mNumber % 100 < 20) ? 2 : cases[Math.min(mNumber % 10, 5)]];
    }

    filterObj.priceMin = $('.filter-price__input-wrap [name=price-min]');
    filterObj.priceMax = $('.filter-price__input-wrap [name=price-max]');
    $(filterObj.priceMin).on('change', filterObj.changePrice);
    $(filterObj.priceMax).on('change', filterObj.changePrice);

    $('.filters-filter-tab li').each(function (key, link) {
        $(link).on('click', filterObj.updateFilter);
        var id = $(link).attr('data-id');
        filterObj.filters[id] = link;
    });

    $('.filters-filter-cat-tab li').each(function (key, link) {
        $(link).on('click', filterObj.updateFilter);
        var id = $(link).attr('data-id');
        filterObj.filters_cat[id] = link;
    });

    $('.manufacturer-filter-tab li').each(function (key, link) {
        $(link).on('click', filterObj.updateFilter);
        var id = $(link).attr('data-id');
        filterObj.manufacturers[id] = link;
    });

    $('.years-filter-tab li').each(function (key, link) {
        $(link).on('click', filterObj.updateFilter);
        var id = $(link).attr('data-id');
        filterObj.years[id] = link;
    });

    $('.sizes-filter-tab li').each(function (key, link) {
        $(link).on('click', filterObj.updateFilter);
        var id = $(link).attr('data-id');
        filterObj.sizes[id] = link;
    });

    $('#filters_clear_block').on('click', function () {
        var loader = preloader.html.loader();
        var windowWidth = jQuery(window).width();
        loader.css('position', 'relative');

        if(windowWidth >= 992) {
            $(this).empty().prepend("<div id='lod' class='w-100 justify-content-center d-flex'></div>");
        } else {
            $(this).empty().prepend("<div id='lod' class='w-100 justify-content-center'></div>");
        }

        $('#lod').prepend(loader);

    });

    $('body').on('click', '#link-filter-ask-layer', function () {
        var link = $(this).attr('data-href');
        $('#filter-ask-layer').css('display', 'none');
        preloader.view.create();
        window.location.href = link;
    });

</script>


