@extends('front.layout')
@section('meta-title')
    <title>{{$page->meta_title}}</title>
@stop
@section('meta-description')
    <meta name="description" content="{!! $page->meta_description !!}">
@stop
@section('meta-keywords')
    {!! $page->meta_keywords !!}
@stop
@section('content')
    <div id="my-content" class="page-content p-padding">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
            </div>
        </div>

        <div class="catalog-content landing-page-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col filter__wrapper">
                        @if(!empty(get_category_tree()))

                            @foreach(get_category_tree() as $firstLevelCategory)
                                @if($firstLevelCategory->status === 1 && $firstLevelCategory->slug === 'velosipedy')
                                    <ul class="filter-categories filter-main-categories">
                                        <li>
                                            <a href="{{route('category_path', category_path($firstLevelCategory->getAttributeValue('id')))}}" @if(Request::url() === route('category_path', category_path($firstLevelCategory->getAttributeValue('id')))) class="current-url" @endif>{{$firstLevelCategory->localDescription->getAttributeValue('name')}}</a>

                                            @if($firstLevelCategory->children)
                                                <ul class="filter-categories filter-sub-categories">
                                                    @foreach($firstLevelCategory->children as $secondLevelCategory)
                                                        @if($secondLevelCategory->status === 1)
                                                            <li class="filter-categories__item filter-categories__item--closed">
                                                                <div class="filter-categories__item-head">
                                                                    <a href="{{route('category_path', category_path($secondLevelCategory->getAttributeValue('id')))}}" @if(Request::url() === route('category_path', category_path($secondLevelCategory->getAttributeValue('id')))) class="current-url" @endif>{{$secondLevelCategory->localDescription->getAttributeValue('name')}}</a>
                                                                    <div class="filter-categories__item-btn"><i class="icon-caret-down"></i></div>
                                                                </div>
                                                                @if($secondLevelCategory->children)
                                                                    <ul class="filter-categories__item-content">
                                                                        @foreach($secondLevelCategory->children as $thirdLevelCategory)
                                                                            @if($thirdLevelCategory->status === 1)
                                                                                <li>
                                                                                    <a href="{{route('category_path', category_path($thirdLevelCategory->getAttributeValue('id')))}}" @if(Request::url() === route('category_path', category_path($thirdLevelCategory->getAttributeValue('id')))) class="current-url" @endif>{{$thirdLevelCategory->localDescription->getAttributeValue('name')}}</a>
                                                                                </li>
                                                                            @endif
                                                                        @endforeach
                                                                    </ul>
                                                                @endif
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    </ul>
                                @endif
                            @endforeach
                        @endif
                    </div>
                    <div class="col-lg-8 col-12">
                        <main class="catalog__main-content">
                            <h1 class="p-title">{{$page->meta_h1}}</h1>
                            @if(!empty($page->description))
                                <section class="description">
                                    <div class="container">

                                        <div class="text-description show-text">
                                            <div>
                                                <div class="text-wrap landing-description">
                                                    <div class="text">
                                                        {!! $page->description !!}
                                                    </div>
                                                </div>
                                                <div class="btn-wrap hidden">
                                                    <span class="btn__text-more">{{__('catalog.product_listing.read_more')}}</span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </section>
                                @if(!empty($jsonLd))
                                    <script type="application/ld+json">
                                        @json($jsonLd)
                                    </script>
                                @endif
                            @endif
                        </main>
                    </div>

                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="products-row">
                            <div class="row">
                                @foreach($products as $product)
                                    <div class="col-sm-6">
                                        @include('front.catalog.product_item', ['product' => $product])
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">

                        <div class="links">
                            <h4 class="links__title">Купить велосипед можно в следующих городах:</h4>
                            <div class="links__list">
                                @forelse($otherCities as $city)
                                    <div class="links__list-item">
                                        <a href="{{url('/city')}}/{{$city->url}}">{{$city->city}}</a>
                                    </div>
                                @empty

                                @endforelse
                            </div>
                        </div>

                        <h3>Велосипед в город {{$page->city}} доставляем на грузовые отделения Новой почты:</h3>
                        <section class="description new-post-description">
                            <div class="container">

                                <div class="text-description show-text">
                                    <div>
                                        <div class="text-wrap landing-description">
                                            <div class="text">
                                                <ul>
                                                    @if(!empty($newPost))
                                                        @foreach($newPost as $post)
                                                            <li>{{$page->city}}, {{$post}}</li>
                                                        @endforeach
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>


        <div class="character-wrapper character-filter--help character-left" data-time="15000">
            <div class="character">
                <div class="character-text">
                    <div class="holder">
                        <div class="character-text__inner">{{__('catalog.product_listing.landing_text')}}</div>
                        <button class="close"><i class="icon-close"></i></button>
                    </div>
                </div>
                <div class="character-img__holder">
                    <img data-src="images/charackter/Lisoped_man_11.svg" alt="character" class="character-img lazy">
                </div>
            </div>
        </div>

        <div class="page-overlay">
        </div>
    </div>
@endsection