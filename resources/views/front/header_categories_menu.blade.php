@if(isset($categoryTree))
<nav class="site-nav d-lg-flex align-items-center remove-md">
    @foreach($categoryTree as $firstLevelCategory)
        @if($firstLevelCategory->status === 1)
        <div class="nav-item">
            <a href="{{route('category_path', category_path($firstLevelCategory->getAttributeValue('id')))}}"
               class="nav-item__t">{{$firstLevelCategory->localDescription->getAttributeValue('name')}}</a>
            <div class="nav-item__dropdown align-items-end justify-content-between flex-wrap">
                @if($firstLevelCategory->children)
                    <div class="nav-item__dropdown-descr">
                        @foreach($firstLevelCategory->children as $secondLevelCategory)
                            @if($secondLevelCategory->status === 1)
                            <div class="nav-item__links">
                                <a class="nav-item__links-title"
                                   href="{{route('category_path', category_path($secondLevelCategory->getAttributeValue('id')))}}">{{$secondLevelCategory->localDescription->getAttributeValue('name')}}</a>
                                @if($secondLevelCategory->children)
                                    <ul class="nav-item__links-list">
                                        @foreach($secondLevelCategory->children as $thirdLevelCategory)
                                            @if($thirdLevelCategory->status === 1)
                                            <li>
                                                <a href="{{route('category_path', category_path($thirdLevelCategory->getAttributeValue('id')))}}">{{$thirdLevelCategory->localDescription->getAttributeValue('name')}}</a>
                                            </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                            @endif
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        @endif
    @endforeach
</nav>
@endif
