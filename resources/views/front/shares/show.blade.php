@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content p-padding share-page">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @if(!empty($share->banner_image))
                        <img class="lazy" data-src="{{Storage::disk('public')->url($share->banner_image)}}"
                             alt="{{$share->title}}"
                             style="margin: 8px 40px 15px 0; float: left;">
                    @endif
                </div>
            </div>

        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-xl-3">
                    <div class="share-views">

                    </div>
                    <div class="timer-title">
                        @if($share->status == 1)
                            Спеши! До конца акции осталось:
                        @else
                           Акция завершена
                        @endif

                    </div>
                    <div class="share-time-end">
                        <div class="days share-time-end-item">
                            <div class="value">0</div>
                            <div class="key">ДНЕЙ</div>
                        </div>
                        <div class="hours share-time-end-item">
                            <div class="value">0</div>
                            <div class="key">ЧАСОВ</div>
                        </div>
                        <div class="minutes share-time-end-item">
                            <div class="value">0</div>
                            <div class="key">МИНУТ</div>
                        </div>
                        <div class="seconds share-time-end-item">
                            <div class="value">0</div>
                            <div class="key">СЕКУНД</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-xl-9">
                    <div class="col-12">
                        @if($share->status == 1)<span class="share-label share-label-availability">Действует</span> @else <span class="share-label share-label-unavailability">Не действует </span>@endif
                        <span class="share-date">({{\Illuminate\Support\Carbon::parse($share->date_start)->format('m.d.Y')}} - {{\Illuminate\Support\Carbon::parse($share->date_end)->format('m.d.Y')}})</span>
                    </div>
                    <div class="col-12">
                        <h1 class="p-title">{{$share->title}}</h1>
                    </div>
                    <div class="col-12">
                        <div class="content-text">
                            {!! $share->description !!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
        @if($share->status == 1)
            <section class="s-products__tabs s-padding">
                <div class="container">
                    <div class="tab-block products-tabs">

                        <div class="row">
                            <div class="col-12">
                                <ul class="tab-nav d-flex align-items-center flex-wrap">
                                    <li class="active">Товары, участвующие в акции</li>
                                </ul>
                            </div>
                        </div>

                        <div class="tab-cont">
                            <div class="tab-pane">
                                <div class="tab-products">
                                    @foreach($products as $product)
                                        <div class="slider-products__item">
                                            @include('front.catalog.product_item')
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container shares-product-more">
                    <div class="row">
                        <div class="col-12">
                            <a href="{{url('shares-products')}}/{{$share->slug}}" class="btn">Показать ещё</a>
                        </div>
                    </div>
                </div>
            </section>
        @endif

        <div class="page-overlay"></div>

    </main>
    @if($share->status == 1)
        <script type="text/javascript">
            var end = new Date('{{$share->date_end}}');

            var _second = 1000;
            var _minute = _second * 60;
            var _hour = _minute * 60;
            var _day = _hour * 24;
            var timer;

            function showRemaining() {
                var now = new Date();
                var distance = end - now;
                if (distance < 0) {

                    clearInterval(timer);
                    $('.share-time-end').empty().append('<div class="share-expired">Акция закончена</div>');

                    return;
                }
                var days = Math.floor(distance / _day);
                var hours = Math.floor((distance % _day) / _hour);
                var minutes = Math.floor((distance % _hour) / _minute);
                var seconds = Math.floor((distance % _minute) / _second);

                $('.share-time-end .days .value').empty().append(days);
                $('.share-time-end .hours .value').empty().append(hours);
                $('.share-time-end .minutes .value').empty().append(minutes);
                $('.share-time-end .seconds .value').empty().append(seconds);
            }

            timer = setInterval(showRemaining, 1000);
        </script>
    @endif

@endsection
