@if(isset($categoryTree))
<div id="mobile-catalog" class="hidden">
    <ul>
        @foreach($categoryTree as $firstLevelCategory)
            <li>
                <a href="{{route('category_path', category_path($firstLevelCategory->id))}}">{{$firstLevelCategory->localDescription->getAttributeValue('name')}}</a>
                @if(count($firstLevelCategory->children) > 0)
                    <ul>
                        @foreach($firstLevelCategory->children as $secondLevelCategory)
                            <li>
                                <a href="{{route('category_path', category_path($secondLevelCategory->id))}}">{{$secondLevelCategory->localDescription->getAttributeValue('name')}}</a>
                                @if(count($secondLevelCategory->children) > 0)
                                    <ul>
                                        @foreach($secondLevelCategory->children as $thirdLevelCategory)
                                            <li>
                                                <a href="{{route('category_path', category_path($thirdLevelCategory->id))}}">{{$thirdLevelCategory->localDescription->getAttributeValue('name')}}</a>

                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
        <li><a href="{{route('news.list')}}">{{__('common.header.blog')}}</a></li>
        <li><a href="{{route('contacts')}}">{{__('common.header.contacts')}}</a></li>
        <li><a href="{{route('special.list')}}">{{__('common.header.specials')}}</a></li>
    </ul>
</div>
@endif