<div class="checkout-head">
    @if(isset($subTotal))
        <div class="checkout-head__row checkout-head__row--total">
            <span class="text">{{__('checkout.checkout_head.subtotal')}}</span>
            <span class="sum">{{$catalogService->format($subTotal)}}</span>
        </div>
    @endif
    @if(isset($shippingPayment) && $shippingPayment > 0)
        <div class="checkout-head__row checkout-head__row--total">
            <span class="text">{{__('checkout.checkout_head.delivery')}}</span>
            <span class="sum">{{$catalogService->format($shippingPayment)}}</span>
        </div>
    @endif
    @if(isset($total))
        <div class="checkout-head__row checkout-head__row--total">
            <span class="text">{{__('checkout.checkout_head.total')}}</span>
            <span class="sum">{{$catalogService->format($total)}}</span>
        </div>
    @endif
    <div class="checkout-head__row checkout-head__row--qnty">
        <span class="text">{{__('checkout.checkout_head.products_quantity')}}</span>
        <span class="sum">{{$productQuantity}}</span>
    </div>
    @if(isset($subTotal) && $subTotal < 2000)
        <div class="checkout-head__row checkout-head__row--descr accent">{{__('checkout.checkout_head.text_free_delivery', ['sum' => '2 000 грн'])}}</div>
    @endif
</div>