@extends('front.layout')
@section('success-order-script')
    @if(isset($orderHistory) && !empty($orderHistory))
        <script>
            dataLayer = [{
                'transactionId': "{{$orderHistory['transactionId']}}",
                'transactionAffiliation': "{{$orderHistory['transactionAffiliation']}}",
                'transactionTotal': "{{$orderHistory['transactionTotal']}}",
                'transactionTax': "{{$orderHistory['transactionTax']}}",
                'transactionShipping': "{{$orderHistory['transactionShipping']}}",
                'transactionProducts': [
                    @foreach($orderHistory['transactionProducts'] as $item)
                    {
                    'sku': "{{$item['sku']}}",
                    'name': "{{$item['name']}}",
                    'category': "{{$item['category']}}",
                    'price': "{{$item['price']}}",
                    'quantity': "{{$item['quantity']}}"
                    },
                    @endforeach
                ]
            }];
        </script>
    @endif
@stop
@section('content')
		<main id="my-content" class="page-content page-success p-padding">

        <div class="container">
            <div class="success-content">
                <div class="text-holder">
                    <div class="success-content__title">{{__('checkout.success.title')}}</div>
                    <p class="success-content__text">{{__('checkout.success.sub_title')}} {{$public_id}}</p>
                    <div class="success-content__text">{{__('checkout.success.text')}}</div>
                </div>
                <img  class="success-content__img lazy" data-src="images/charackter/Lisoped_man_ok.svg" alt="{{__('checkout.success.title')}}">
            </div>
        </div>
        <div class="page-overlay"></div>
    </main>
@endsection