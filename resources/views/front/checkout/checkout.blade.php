@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content p-padding page-checkout">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="p-title">{{__('checkout.checkout.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">

                @include('front.checkout.checkout_product_list', ['cart' => $checkout->cart])


                <div class="col-md-5 col-lg-4">
                    <div class="checkout-title">{{__('checkout.checkout.making_order')}}</div>

                    <div class="checkout">
                        @include('front.checkout.checkout_head', [
                        'subTotal' => $checkout->subTotal,
                        'shippingPayment' => $checkout->shippingPayment,
                        'total' => $checkout->total,
                        'productQuantity' => $checkout->cart->items()->count()])

                        <form class="checkout-form">
                            @csrf
                            @if(!Auth::check())
                                <div class="checkout-form__head">
                                    <span>{{__('checkout.checkout.customer')}}</span>
                                    <a href="#log-in-popup" class="to-popup">{{__('checkout.checkout.text_account')}}</a>
                                </div>
                            @endif
                            <div class="checkout-row">
                                <input type="text" name="first_name" required="" class="input"
                                       value="{{$checkout->userInfo['first_name']}}"
                                       placeholder="{{__('checkout.checkout.placeholder_first_name')}}">
                            </div>
                            <div class="checkout-row">
                                <input type="text" name="last_name" required="" class="input"
                                       value="{{$checkout->userInfo['last_name']}}" placeholder="{{__('checkout.checkout.placeholder_last_name')}}">
                            </div>
                            <div class="checkout-row">
                                <input type="tel" name="telephone" required="" class="input"
                                       value="{{$checkout->userInfo['telephone']}}" placeholder="{{__('checkout.checkout.placeholder_telephone')}}">
                            </div>
                            <div class="checkout-row">
                                <input type="email" id="field-email" name="email" required="" class="input"
                                       value="{{$checkout->userInfo['email']}}" placeholder="{{__('checkout.checkout.placeholder_email')}}">
                            </div>

                            <div class="shipping-block">
                                <div class="checkout-row">
                                    <select name="shipping_code" id="order-shipping">
                                        <option value="">{{__('checkout.checkout.choose_payment')}}</option>
                                        @foreach($checkout->shippingMethods as $shippingMethod)
                                            <option {!! $shippingMethod->getCode() === $checkout->selectedShippingMethod->getCode() ? 'selected' : '' !!}
                                                    value="{{$shippingMethod->getCode()}}">{{$shippingMethod->getName()}} ({{$shippingMethod->getCostTitle($checkout->subTotal)}})</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div id="current-shipping-info">
                                    @if($checkout->selectedShippingMethod)
                                        {!! $checkout->selectedShippingMethod->show() !!}
                                    @endif
                                </div>
                            </div>
                            <div class="payment-block">
                                <select name="payment_code" id="order-payment" class="select" required>
                                    <option selected value="">Выберите способ оплаты</option>
                                    @foreach($checkout->paymentMethods as $paymentMethod)
                                        <option value="{{$paymentMethod->getCode()}}">{{$paymentMethod->getName()}}</option>
                                    @endforeach
                                    @if(\App\Library\Checkout\Checkout::checkPayParts($checkout) === true)
                                        <option value="payparts" @if(session()->get('payparts_method') === 'private') selected @endif>Оплата частями ПриватБанк</option>
                                    @endif

                                    @if(\App\Library\Checkout\Checkout::checkPayPartsAlfa($checkout) === true)
                                        <option value="payparts-alfa" @if(session()->get('payparts_method') === 'alfa') selected @endif>Оплата частями АльфаБанк</option>
                                    @endif
                                </select>
                            </div>
                           {{-- <div class="payparts-parts">
                                <select name="payparts-parts" id="alfa-payparts-parts" class="select" required>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                                <select name="payparts-parts" id="alfa-payparts-parts" class="select" required>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                </select>
                            </div> --}}
                            <div class="payparts-inn" @if(\App\Library\Checkout\Checkout::checkPayPartsAlfa($checkout) === false || session()->get('payparts_method') !== 'alfa') style="display: none" @endif>
                                <input type="text" name="inn" required="" class="input"
                                       placeholder="Идентификационный номер">
                            </div>
                            <div class="checkout__row">
                                <textarea name="order_comment" rows="1" class="input"
                                          placeholder="{{__('checkout.checkout.placeholder_comment')}}">{{$checkout->comment}}</textarea>
                            </div>

                            <div class="btn-wrap">
                                <button type="submit" class="btn"><span>{{__('checkout.checkout.make_order')}}</span></button>
                                <a href="{{route('home')}}" class="btn-continue">{{__('checkout.checkout.text_continue')}}</a>
                            </div>

                        </form>

                    </div>

                </div>

            </div>
        </div>

        <div class="page-overlay"></div>
    </main>

    <script type="text/javascript">
        $(function () {
            var shippingSelect = $('#order-shipping').selectize();

            shippingSelect[0].selectize.on('change', function (code) {
                getShippingInfo(code)
            });

            function getShippingInfo(code) {
                $.ajax({
                    url: `{{route('checkout.shippingInfo')}}`,
                    data: {
                        shipping_code: code
                    },
                    success: function (result) {
                        if(result['status'] && result['status'] === 1){
                            $('#current-shipping-info').html(result['template']);
                            $('.checkout .checkout-head').replaceWith(result['checkout_head']);
                        }
                    }
                });
            }
        });

        $(document).ready(function () {
           if($('#order-payment').val() === 'payparts-alfa'){
               $('.payparts-inn').find('input').prop('required', true);
           }else{
               $('.payparts-inn').find('input').prop('required', false);
           }
            if($('#order-payment').val() === 'payparts-alfa' || $('#order-payment').val() === 'payparts'){
                $('#field-email').prop('required', true);
            }else{
                $('#field-email').prop('required', false);
            }
        });

        $(document).on('change', '#order-payment', function () {
            if($(this).val() === 'payparts-alfa'){
                $('.payparts-inn').show();
                $('.payparts-inn').find('input').prop('required', true);
            }else{
                $('.payparts-inn').hide();
                $('.payparts-inn').find('input').prop('required', false);
            }

            if($('#order-payment').val() === 'payparts-alfa' || $('#order-payment').val() === 'payparts'){
                $('#field-email').prop('required', true);
            }else{
                $('#field-email').prop('required', false);
            }
        });


        $('.checkout-form').on('submit', function (e) {
            if($('#order-payment').val() !== 'payparts'){
                e.preventDefault();
                $.ajax({
                    url: `{{route('checkout.confirm')}}`,
                    data: $(this).serialize(),
                    type: 'POST',
                    success: function (result) {
                        if(result['status'] && result['status'] === 1){
                            var html = '<div class="hidden" id="confirm-form">' + result['template'] + '</div>';
                            $('body').append(html);
                            $('#confirm-form form').submit();
                        }else{
                            $('.cart-user-message').empty().append(result['template']);
                        }
                    },
                    error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        console.log(err.Message);
                        console.log(status);
                        console.log(error);
                    }
                });
            }else{
                $('.checkout-form').attr('action', '{{route('checkout.confirm')}}');
                $('.checkout-form').attr('method', 'post');
            }
        });
    </script>

@endsection