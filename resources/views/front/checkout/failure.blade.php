@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content page-failure page-success p-padding">
        <div class="container">
            <div class="success-content">
                <div class="text-holder">
                    <h1 class="failure__title">Ты не завершил свою покупку</h1>
                    <p>Возможно у тебя остались вопросы?</p>
                    <p>Позвони нам</p>
                </div>
                <img  class="success-content__img lazy" data-src="images/charackter/Lisoped_man_8.jpg" alt="Позвони нам">
            </div>
        </div>
        <div class="page-overlay"></div>
    </main>
@endsection