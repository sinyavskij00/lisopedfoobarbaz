@extends('front.layout')
@section('meta-title')
    <title>ᐈ LISOPED — Интернет магазине велосипедов | Продажа велосипедов ⋙ Веломагазин: Одесса, Харьков, Запорожье, Киев, Днепр</title>
@stop
@section('meta-description')
   
    {{--<meta name="description" content="Lisoped это: ✅Большой ассортимент велосипедов ✅Запчасти и аксессуары ✈Бесплатная доставка по Украине от2000грн ❗Рассрочка ✅Гарантия">--}}
    {{--<meta name="description" content="👉 LISOPED — это: Широкий ассортимент велосипедов и 
аксессуаров. 
Профессиональная консультация ≫ Бесплатная доставка от 1000 грн ≫ Рассрочка 0% 💰💰">
   



--}}
<meta name="description" content="👉 LISOPED — это: Широкий ассортимент велосипедов и аксессуаров. 
Профессиональная консультация ≫ Бесплатная доставка от 1000 грн ≫ Рассрочка 0% 
💰">
 <meta name="title" content="ᐈ LISOPED — Интернет магазине велосипедов | Продажа велосипедов ⋙ Веломагазин: Одесса, Харьков, Запорожье, Киев, Днепр">
@stop
@section('meta-keywords')

@stop
@section('content')


    <main id="my-content" class="page-content home-content">

        {!! $content !!}
        <div class="character-wrapper character-welcome" data-time="25000">
            <div class="character">
                <div class="character-text">
                    <div class="holder">
                        <div class="character-text__inner">{!!__('common.home.character_text')!!}</div>
                        <button class="close"><i class="icon-close"></i></button>
                    </div>
                </div>
                <div class="character-img__holder">
                    <img data-src="images/charackter/Lisoped_man_7.svg" alt="character" class="character-img lazy">
                </div>
            </div>
        </div>

        <div class="page-overlay"></div>

    </main>
@endsection
