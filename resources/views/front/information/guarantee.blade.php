@extends('front.layout')
@section('meta-title')
    <title>Информация о гарантии и возврате товара ᐈ интернет-магазин Lisoped</title>
@stop
@section('meta-description')
    <meta name="description" content="Покупатель имеет право обменять или вернуть товар, купленный в нашем магазине, в течении 14 дней со дня получения товара.">
@stop
@section('meta-keywords')

@stop
@section('content')
    <main id="my-content" class="page-content p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
                <div class="col-12">
                    <h1 class="p-title">{{__('information.guarantee.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="content-text">
                {!! __('information.guarantee.text') !!}
            </div>
            @include('front.information.any_questions_form')
        </div>
        <div class="page-overlay"></div>
    </main>
@endsection