@extends('front.layout')
@section('meta-title')
    <title>Частые вопросы наших клиентов ᐈ интернет-магазин Lisoped</title>
@stop
@section('meta-description')
    <meta name="title" content="Частые вопросы наших клиентов ᐈ интернет-магазин Lisoped">
    <meta name="description" content="Рубрика вопрос - ответ. Самые популярные вопросы наших клиентов и ответы на них. Интернет-магазин ⭐LISOPED.UA⭐">
@stop
@section('meta-keywords')

@stop
@section('content')
    <main id="my-content" class="page-content p-padding__sm">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
                <div class="col-12">
                    <h1 class="p-title">{{__('information.faq.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container faq-list">
            <div class="row">
                @foreach($questionGroups as $questionGroup)
                    <div class="col-md-6">
                        <div class="faq-item">
                            <div class="faq-item__title">
                                {{isset($questionGroup->localDescription) ? $questionGroup->localDescription->name : ''}}
                            </div>

                            <div class="accordion">
                                @foreach($questionGroup->questions as $question)
                                    <div class="accordion-item">
                                        <div class="accordion-item__title">
                                            <span>{{$question->localDescription->question_text}}</span>
                                        </div>
                                        <div class="accordion-item__content">
                                            {!! $question->localDescription->answer !!}
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="character-wrapper character-help" data-time="60000">
            <div class="character">
                <div class="character-text">
                    <div class="holder">
                        <div class="character-text__inner">{!! __('character.faq.phrase') !!}</div>
                        <button class="close"><i class="icon-close"></i></button>
                    </div>
                </div>
                <div class="character-img__holder">
                    <img data-src="images/charackter/Lisoped_man_8.svg" alt="character"
                         class="character-img lazy">
                </div>
            </div>
        </div>
        <div class="page-overlay"></div>
    </main>
@endsection
