@extends('front.layout')
@section('meta-title')
    <title>О компании ⋙ интернет-магазин Lisoped</title>
@stop
@section('meta-description')
    {{--<meta name="description" content="Перед открытием мы представили, каким должен быть идеальный магазин. И решили, что это то место, где ставят высокую планку уровню сервиса">--}}
    <meta name="description" content="ᐈ Мы можем много и интересно рассказывать о велосипедах, комплектующих и обо всем, что с ними связано. Поэтому звони нам, всегда будем рады!">
    <meta name="title" content="О компании ⋙ интернет-магазин Lisoped">
@stop
@section('meta-keywords')

@stop
@section('content')
    <main id="my-content" class="page-content p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
                <div class="col-12">
                    <h1 class="p-title">{{__('information.about_us.title')}}</h1>
                </div>
            </div>
        </div>
        <style>
            .about-us__content strong {
                color: #42cf9e;
            }
        </style>
        <div class="container">
            @if(isset($text))
            <div class="about-us__content">
                {!! $text !!}
            </div>
            @endif
        </div>

        <div class="page-overlay"></div>

    </main>
@endsection
