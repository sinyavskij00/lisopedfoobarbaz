@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content page-error">

        {{--<img src="assets/img/404_bg.jpg" alt="404" class="page-error__img">--}}

        <div class="container">
            <div class="row align-items-sm-center">
                <div class="col-sm-6">
                    <div class="error-content">
                        <div class="error-content__title">{{__('errors.404.title')}}</div>
                        <p class="error-content__descr">{{__('errors.404.text')}}</p>
                        <ul class="ul error-content__links d-flex flex-lg-row flex-column justify-content-lg-between flex-wrap">
                            <li><a href="{{route('home')}}" class="btn">{{__('errors.404.btn_home')}}</a></li>
                            <li><a href="#callback-popup" class="btn to-popup">{{__('errors.404.btn_help')}}</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 order-sm-first">
                    <div class="error-img">
                        <img src="images/charackter/Lisoped_man_404.svg" alt="404">
                    </div>
                </div>
            </div>
        </div>
        <!-- /.error-content -->

        <div class="page-overlay"></div>

    </main>
@endsection