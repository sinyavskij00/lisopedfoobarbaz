<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Check</title>
    <style>

        body *, table , p, tr, td, b, a{
            font-family: "anonymouspro" !important;
        }

        .wrapper{
            border: 15px solid #001926;
            padding: 10px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        .t-a-r {
            text-align: right;
        }

        .check thead, .check thead td {
            text-align: left;
            font-weight: 600;
        }

        .check td {
            padding: 15px 5px;
        }

        .wrapper {
            max-width: 780px;
            margin: 0 auto;
        }

        .check-header {
            text-align: center;
            margin-bottom: 30px;
        }

        .stamp-wrapper {
            text-align: right;
            margin-top: 20px;
        }

        .stamp-image {
            margin-left: auto;
            max-width: 200px;
        }

        .header-image{
            width: 200px;
            max-width: 200px;
        }

        .header table p{
            text-align: right;
        }

        .header table .header-info{
            text-align: right;
        }

        .header table p b{
            font-weight: bold;
        }

        .header table{
            vertical-align: top;
        }

        .header table tr{
            vertical-align: top;
        }

        .header table tr td{
            vertical-align: top;
        }

        .footer-info{
            vertical-align: top;
        }

        .footer-info tr{
            vertical-align: top;
        }

        .footer-info tr td{
            vertical-align: top;
        }

        .check table, .check table tr, .check table tr td{
            border: 0;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="header">
        <table>
            <tr>
                <td>
                    <img class="header-image" src="{{Storage::disk('public')->url('/shop_images/pdf-logo.png')}}" alt="">
                </td>
                <td class="header-info">
                    <p><b>Киев ул.Казимира Малевича 89<br>
                            Харьков пр. Науки 30</b></p>
                    <br>
                    <br>
                    <p>+38 067 620 66 11</p>
                    <p>+38 095 620 66 11</p>
                    <p>+38 073 620 66 11</p>
                    <p>lisoped.ua</p>
                    <p>info@lisoped.ua</p>
                </td>
            </tr>
        </table>
    </div>
    <div class="check-main-info">
        <p class="check-main-info title" style="font-size: 18px; color: #42cf9e; margin-bottom: 0"><b>ТОВАРНЫЙ ЧЕК #00{{$order_id}}</b> </p>
        <p class="date">{{$orderDate}}</p>
    </div>

    <table class="check">
        <thead>
        <tr>
            <td><b>{{__('mails.check.content.product')}}</b></td>
            <td><b>{{__('mails.check.content.quantity')}}</b></td>
            <td><b>{{__('mails.check.content.price')}}</b></td>
            <td><b>{{__('mails.check.content.total')}}</b></td>
        </tr>
        </thead>
        <tbody>
        @if(isset($orderSkus) && $orderSkus->count() > 0)
            @foreach($orderSkus as $orderSku)
                <tr>
                    <td>{{$orderSku->name}}</td>
                    <td>{{$orderSku->quantity}}</td>
                    <td>{{$orderSku->price}} грн.</td>
                    <td>{{$orderSku->total}} грн.</td>
                </tr>
            @endforeach
        @endif
        <tr>
            <td></td>
            <td></td>
            <td><b>Всего</b></td>
            <td>{{$price}} грн.</td>
        </tr>
        </tbody>
    </table>
    <br><br>
    <table class="footer-info">
        <tbody>
        <tr>
            <td>
               <p class="footer-title"><b>ДЕТАЛИ ЗАКАЗА</b></p>
                <p class="detail-order"><b>Номер заказа #{{$orderNumber}}</b></p>
            </td>
            <td class="t-a-r">
                @if($stamp)
                    <div class="stamp-wrapper"><img class="stamp-image" src="{{Storage::disk('public')->url($stamp)}}">
                    </div>
                @endif
            </td>
        </tr>
        </tbody>
    </table>
    <br>
    <p style="text-align: right; font-size: 18px; color: #42cf9e">СПАСИБО!</p>
</div>
</body>
</html>