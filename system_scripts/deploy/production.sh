#!/usr/bin/expect -f

host=''
sshUsername='ubuntu'
sshPemFile=''
pathToProject='/var/www/'

. $PWD/system_scripts/config.sh

sudo ssh -i ${sshPemFile} ${sshUsername}@${host} 'bash -s' << EOF
  cd ${pathToProject}$LARADOCKDIRNAME && docker-compose exec -T workspace bash ./system_scripts/install/deploy.sh
EOF
