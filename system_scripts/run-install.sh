#!/bin/bash

#Init config
. system_scripts/common/config.sh

#Create env files from examples
. $conf_systemScriptsPath/install/env.sh

#Add hooks for git
. $conf_systemScriptsPath/install/git.sh

echo "cd $conf_basePath$conf_laradock_dirName"
cd $conf_basePath$conf_laradock_dirName

echo "run containers "$conf_docker_containers_forAll_nginx
docker-compose up -d $conf_docker_containers_forAll_nginx
echo "Ok"

echo "run workspase"
#Script for create directories
docker-compose exec workspace bash -e ./system_scripts/install/mkdir.sh
#Script for set permissions project
docker-compose exec workspace bash -e ./system_scripts/install/chmod-install.sh
#Script for install php dependencies
docker-compose exec workspace bash -e ./system_scripts/install/php-install.sh
#Script for install npm
docker-compose exec --user laradock workspace bash -e ./system_scripts/install/npm.sh
#Script for chmod on system directories
docker-compose exec workspace bash -e ./system_scripts/install/keychmod-install.sh
#Script for chmod on bash files
docker-compose exec workspace bash -e ./system_scripts/install/chmod-bash.sh
echo "Ok"

echo "Rebuild workspace container"
docker-compose build workspace
echo "Ok"
