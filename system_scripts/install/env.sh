#!/bin/bash

echo 'Create .env file'
cp $conf_basePath/.env.example $conf_basePath/.env

echo 'Create .env.testing file'
cp $conf_basePath/.env.testing.example $conf_basePath/.env.testing

echo "cp .env for laradock"
cp $conf_basePath/$conf_laradock_dirName/env-example $conf_basePath/$conf_laradock_dirName/.env
