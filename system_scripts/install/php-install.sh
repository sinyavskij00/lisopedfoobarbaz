#!/bin/bash

echo 'Run composer install'

composer require "squizlabs/php_codesniffer=*" --dev --no-update
composer install

echo 'Run migrate'
php artisan migrate

echo 'Run Seeds'
php artisan db:seed

echo 'Artisan key:generate'
php artisan key:generate

#echo 'Create keys for oauth'
#php artisan passport:install
#echo 'Clear cache'

echo 'Clear cache'
php artisan route:clear
php artisan cache:clear
php artisan config:clear

exit
