<?php

namespace App\Http\Route;

use App\Entities\Category;
use Illuminate\Contracts\Routing\UrlRoutable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Request;

class CategoryPath implements UrlRoutable
{
    public $id;

    protected $slugs;

    public function __construct()
    {
        $this->slugs = Cache::remember('slug_table', 100, function () {
            $slugs = [];
            foreach (Category::get(['slug', 'id']) as $category) {
                $slugs[] = [
                    'chunk' => 'category=' . $category->id,
                    'slug' => $category->slug
                ];
            }

            return collect($slugs);
        });
    }

    public function withId($id)
    {
        $clone = clone $this;
        $clone->id = $id;
        return $clone;
    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        $segments = [];

        $segments[] = Cache::rememberForever('category_path_' . $this->id, function () {
            return Category::findOrFail($this->id)->getPath();
        });

        return implode('/', $segments);
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'category_path';
    }

    /**
     * Retrieve the model for a bound value.
     *
     * @param $current_url
     * @return CategoryPath
     */
    public function resolveRouteBinding($current_url)
    {
        $parts = explode('/', $current_url);
        $categories = [];
        $tableSlugs = $this->slugs->pluck('chunk', 'slug');
        $filterSlug = '';

        do {
            $slug = count($parts) > 0 ? array_shift($parts) : null;
            if (isset($slug) && str_contains($slug, 'filter=')) {
                $filterSlug = $slug;
            } elseif (isset($slug) && $next = isset($tableSlugs[$slug]) ? $tableSlugs[$slug] : null) {
                $categories[] = $next;
            }
        } while (!empty($slug) && !empty($next));

        $targetCategory = end($categories);
        if (!$targetCategory) {
            abort(404);
        }

        $tmp = explode('=', $targetCategory);

        if (!isset($tmp[1])) {
            abort(404);
        }
        $this->id = $tmp[1];
        $seoPath = route('category_path', category_path($this->id), false);

        if (trim(str_replace($filterSlug, '', Request::path()), '/') !== trim($seoPath, '/')) {
            redirect($seoPath, 301)->send();
        }

        return $this;
    }
}
