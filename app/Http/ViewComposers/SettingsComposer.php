<?php

namespace App\Http\ViewComposers;

use App\Entities\Language;
use App\Library\Settings;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\View\View;
use Storage;

class SettingsComposer
{
    public function compose(View $view)
    {
        $settings = app()->make(Settings::class);

        $categoryTree = get_category_tree();

        $view->with('categoryTree', $categoryTree);

        $headerCategoryMenu = Cache::remember('headerCategoryMenu', 30, function () use ($categoryTree) {
            return view('front.header_categories_menu', ['categoryTree' => $categoryTree])->render();
        });
        $view->with('headerCategoryMenu', $headerCategoryMenu);

        $headerCategoryMobile = Cache::remember('headerCategoryMobile', 30, function () use ($categoryTree) {
            return view('front.header_categories_mobile', ['categoryTree' => $categoryTree])->render();
        });
        $view->with('headerCategoryMobile', $headerCategoryMobile);

        $languages = Cache::remember('all_languages', 120, function () {
            return Language::all();
        });
        $languagesSwitcher = [];
        foreach ($languages as $language) {
            $languagesSwitcher[] = [
                'text' => $language->slug,
                'link' => route('setlocale', $language->slug),
                'is_active' => Config::get('current_language_id') == $language->id
            ];
        }
        $view->with('languagesSwitcher', $languagesSwitcher);

        $logo = $settings->getByKey('logo');
        if (Storage::disk('public')->exists($logo)) {
            $view->with('logo', Storage::disk('public')->url($logo));
        }
    }
}
