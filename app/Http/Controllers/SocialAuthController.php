<?php

namespace App\Http\Controllers;

use App\Entities\User;
use App\Entities\WishProduct;
use App\Library\Checkout\Cart;
use App\Mail\Auth\UserRegistred;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Laravel\Socialite\Facades\Socialite;
use App\Entities\CustomerGroup;

class SocialAuthController extends Controller
{
    protected $cart;

    protected $adminEmail;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
        $this->adminEmail = config('app.admin_email');
    }

    public function redirect($service)
    {
        return Socialite::driver($service)->redirect();
    }

    public function callback($service)
    {
        $userData = Socialite::with($service)->user();
        $lastName = isset($userData->user)
        && isset($userData->user['name'])
        && isset($userData->user['name']['familyName'])
            ? $userData->user['name']['familyName']
            : '';

        $firstName = isset($userData->user)
        && isset($userData->user['name'])
        && isset($userData->user['name']['givenName'])
            ? $userData->user['name']['givenName']
            : '';


        $customer = User::where('email', '=', $userData->email)->first();

        if (!isset($customer)) {
            $password = str_random(8);
            $customer = User::create([
                'email' => $userData->email,
                'first_name' => isset($firstName) ? $firstName : 'social registration',
                'last_name' => isset($lastName) ? $lastName : 'social registration',
                'password' => Hash::make($password),
                'customer_group_id' => CustomerGroup::where('is_default', '=', 1)->first()->id
            ]);

            $action = __('mails.user_registred.actions.registration');
            $title = __('mails.user_registred.title', ['action' => $action]);

            $subTitle = __('mails.user_registred.sub_title', [
                'name' => $customer->first_name . ' ' . $customer->last_name,
                'action' => $action
            ]);

            $mail = new UserRegistred($title, $subTitle, $customer->email, $password);
            $mail->from($this->adminEmail, config('app.name'));
            $mail->subject($title);
            Mail::to($customer->email)->send($mail);
        }
        $oldSessionId = session()->getId();
        Auth::login($customer);
        $newSessionId = session()->getId();

        //wishlist from session to db
        $wishlist = session('wishList', []);
        foreach ($wishlist as $productId) {
            if (isset($productId)) {
                WishProduct::firstOrCreate([
                    'product_id' => $productId,
                    'customer_id' => Auth::id()
                ]);
            }
        }
        session()->forget('wishList');

        $this->cart->updateItems($oldSessionId, $newSessionId, Auth::id());
        return redirect()->route('cabinet.index');
    }
}
