<?php

namespace App\Http\Controllers\Front\Catalog;

use App\Entities\LandingPage;
use App\Entities\StockKeepingUnit;
use App\Services\CatalogService;
use App\Services\FilterService;
use App\Services\NewFilterService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Entities\Filter;

class ProductListingController extends Controller
{
    protected $catalogService;
    protected $filterService;
    protected $request;

    protected $listingData;
    protected $query;

    public function __construct(CatalogService $catalogService, NewFilterService $filterService, Request $request)
    {
        $this->catalogService = $catalogService;
        $this->filterService = $filterService;

        $this->request = $request;
        $this->query = $request->input('query', '');

        $inputLimit = $request->input('limit', CatalogService::DEFAULT_LIMIT);
        $inputSort = $request->input('sort', CatalogService::DEFAULT_SORT);
        $inputOrder = $request->input('order', CatalogService::DEFAULT_ORDER);

        $limit = in_array($inputLimit, CatalogService::ALLOWED_LIMITS) ? $inputLimit : CatalogService::DEFAULT_LIMIT;
        $sort = in_array($inputSort, CatalogService::ALLOWED_SORTS) ? $inputSort : CatalogService::DEFAULT_SORT;
        $order = in_array($inputOrder, CatalogService::ALLOWED_ORDERS) ? $inputOrder : CatalogService::DEFAULT_ORDER;

        $this->listingData = [
            'limit' => $limit,
            'sort' => $sort,
            'order' => $order
        ];
    }

    public function index(Builder $products, $metaData = [], $filterCacheKey = null, $searchCountProduct = null)
    {
        $products = $products->where('is_render', 1);       
        if($this->request->page && $this->request->page === '1'){
            return redirect($this->request->url(), 301);
        }

        $catalog = CatalogService::PRODUCT_ITEM_SELECTED_FIELDS;
        array_push(
            $catalog,
            \DB::raw("(CASE WHEN sks.price IS NOT NULL or sks.price = '' THEN sks.price ELSE sku.price END) AS price")
        );

        $products->select($catalog)->where([
            ['status', '=', 1],
        ]);

        $products = $this->catalogService->addProductInfo($products);

        $products->with(CatalogService::PRODUCT_ITEM_RELATIONS);

//        if (!empty($filterCacheKey)) {
//            $filter = Cache::remember($filterCacheKey, 120, function () use ($products) {
//                return $this->filterService->getFilterByProducts($products);
//            });
//        } else {
//            $filter = $this->filterService->getFilterByProducts($products);
//        }

        $category = explode('_', $filterCacheKey);
        $filter = null;

        $getFilterByProducts = $this->filterService->getFilterByProducts($products, end($category));
        $filter = $getFilterByProducts[0];
        $products = $getFilterByProducts[1];

//        $filter = $this->filterService->getFilterByProducts($products, end($category));
//        $products = $this->filterService->getFilteredProducts($products);

        $filter = $this->filterService->addLinksToFilter($filter);

        $productMinValue = StockKeepingUnit::max('price');
        $shuffle = false;

            $getProducts = $products->get();

        $allProductsCount = count($getProducts);

        if($allProductsCount === 0){
            $productMinValue = 0.00;
        }else{
            foreach ($getProducts as $product_item){
                $product_sku = $product_item->skus->first();
                if($product_sku->price < $productMinValue && $product_sku->price !== '0.00'){
                    $productMinValue = $product_sku->price;
                }
            }
        }

        $sorts = $this->catalogService->getProductListingsSorts($this->listingData);

        $noCorrectSorts = false;
        foreach ($sorts as $sort) {
            $param = $this->filterService->getSortParamFromString($sort['link'], 'sort=', '&');
            if ($param == \Request::get('sort')) {
                $noCorrectSorts = true;
            }
        }

        if((\Request::get('sort') == '') || (!$noCorrectSorts)){
//            dump('shuffle');
//            dump(sprintf('order by : %s %s', $this->listingData['sort'], $this->listingData['order']));
            $products = $this->getPagination(
                $products->orderBy('quantity', 'DESC')
//                    ->orderBy($this->listingData['sort'], $this->listingData['order'])
//                    ->orderBy('price', 'ASC')
                    ->inRandomOrder()
            );
            $shuffle = true;
        }else{
//            dump('order');
//            dump(sprintf('order by : %s %s', $this->listingData['sort'], $this->listingData['order']));
            $products = $this->getPagination(
                $products->orderBy('quantity', 'DESC')
                    ->orderBy($this->listingData['sort'], $this->listingData['order'])
                    ->inRandomOrder()
            );
        }

        if ($this->request->ajax()) {
            $ajaxAction = $this->request->input('action', '');
            if ($ajaxAction === 'show_more') {
                $view = view('front.catalog.show_more', [
                    'products' => $products,
                ]);
                return response()->json([
                    'status' => 1,
                    'template' => $view->render()
                ]);
            }
            return response()->json([
                'status' => 1,
                'filter' => $this->getFilterData($filter),
                'products_count' => $products->total(),
                'products_ids' => $products->pluck('id'),
            ]);
        }

        $cities = '';
        $veloCategories = explode('/', parse_url(\Request::url())['path'])[1];
        if($veloCategories === 'velosipedy'){
            $cities = LandingPage::inRandomOrder()->take(25)->get();
        }

        $request = \Request::all();
        if(isset($filter['price']['min']) && $filter['price']['min'] !== '0.00'){
            $productMinValue = $filter['price']['min'];
        }

        $pages = Filter::select('slug', 'name')->orderBy('sort', 'ASC')->limit(15)->get()->toArray();

        return view('front.catalog.product_listing', [
            'products' => $products,
            'shuffle' => $shuffle,
            'filter' => $filter,
            'limits' => $this->catalogService->getProductListingLimits($this->listingData),
            'sorts' => $sorts,
            'description' => isset($metaData['description']) ? $metaData['description'] : '',
            'meta_title' => isset($metaData['meta_title']) ? $metaData['meta_title'] : '',
            'meta_description' => isset($metaData['meta_description']) ? $metaData['meta_description'] : '',
            'meta_keywords' => isset($metaData['meta_keywords']) ? $metaData['meta_keywords'] : '',
            'meta_h1' => isset($metaData['meta_h1']) ? $metaData['meta_h1'] : '',
            'page_number' => isset($request['page']) ? 'Страница №' . $request['page'] . ' ' : '',
            'cities' => $cities,
            'searchCountProduct' => $allProductsCount,
            'metaLinks' => [
                'prev' => $products->previousPageUrl(),
                'next' => $products->nextPageUrl(),
                'canonical' => url()->current()
            ],
            'jsonLd' => $this->getJsonLd([
                'name' => isset($metaData['meta_h1']) ? $metaData['meta_h1'] : '',
                'description' => isset($metaData['description']) ? $metaData['description'] : '',
                'quantity' => $products->total(),
                'high_price' => isset($filter['price']['max']) ? $filter['price']['max'] : 0.00,
                'low_price' => $productMinValue
                //'low_price' => isset($filter['price']['min']) ? $filter['price']['min'] : ''
            ]),
            'pages' => $pages,
        ]);
    }

    private function getPagination($products)
    {
        $result = $products->paginate($this->listingData['limit']);

        $this->listingData['limit'] !== CatalogService::DEFAULT_LIMIT
            ? $result->appends('limit', $this->listingData['limit'])
            : null;

        if (!($this->listingData['sort'] === CatalogService::DEFAULT_SORT
            && $this->listingData['order'] === CatalogService::DEFAULT_ORDER)
        ) {
            $result->appends('sort', $this->listingData['sort']);
            $result->appends('order', $this->listingData['order']);
        }
        $result->appends('filter', $this->request->input('filter'));
        if (!empty($this->query)) {
            $result->appends('query', $this->query);
        }
        return $result;
    }

    private function getJsonLd($data)
    {
        $jsonLd = [
            '@context' => 'http://schema.org',
            '@type' => 'Product',
            'url' => $this->request->fullUrl(),
            'name' => isset($data['name']) ? $data['name'] : '',
            'description' => isset($data['description']) ? $data['description'] : '',
            'offers' => [
                [
                    '@type' => 'AggregateOffer',
                    'offerCount' => isset($data['quantity']) ? $data['quantity'] : '',
                    'highPrice' => isset($data['high_price']) ? $data['high_price'] : '',
                    'lowPrice' => isset($data['low_price']) ? $data['low_price'] : '',
                    'priceCurrency' => 'UAH'
                ]
            ]
        ];
        return $jsonLd;
    }

    private function getFilterData($filter)
    {
        $filterData = [
            'manufacturers' => [],
            'years' => [],
            'filters' => [],
            'price' => [
                'min' => $filter['price']['min'],
                'max' => $filter['price']['max'],
            ]
        ];

        foreach ($filter['filters'] as $filterElem) {
            foreach ($filterElem->values as $filterValue) {
                $filterData['filters'][] = [
                    'id' => $filterValue->id,
                    'filter_link' => $filterValue->filter_link,
                    'name' => $filterValue->name
                ];
            }
        }

        foreach ($filter['filters_cat'] as $filterElem) {
            foreach ($filterElem->valuesWithoutCount as $filterValue) {
                $filterData['filters_cat'][] = [
                    'id' => $filterValue->id,
                    'filter_link' => $filterValue->filter_link,
                    'name' => $filterValue->text,
                    'count' => $filterValue->products_count
                ];
            }
//            dd( $filterData['filters_cat']);
        }

        foreach ($filter['manufacturers'] as $manufacturer) {
            $filterData['manufacturers'][] = [
                'id' => $manufacturer->id,
                'filter_link' => $manufacturer->filter_link,
                'name' => $manufacturer->name,
                'count' => $manufacturer->products_count
            ];
        }
        foreach ($filter['years'] as $year) {
            $filterData['years'][] = [
                'id' => $year->id,
                'filter_link' => $year->filter_link,
                'name' => $year->value,
                'count' => $year->products_count
            ];
        }
        foreach ($filter['sizes'] as $size) {
            $filterData['sizes'][] = [
                'id' => $size->id,
                'filter_link' => $size->filter_link,
                'name' => $size->value,
                'count' => $size->products_count
            ];
        }

        return $filterData;
    }
}
