<?php

namespace App\Http\Controllers\Front\Catalog;

use App\Entities\Category;
use App\Entities\Product;
use App\Entities\Shares;
use App\Entities\SkuSpecial;
use App\Library\Modules\Front\AdvantagesModule;
use App\Library\Modules\Front\BannerModule;
use App\Library\Modules\Front\BlogModule;
use App\Library\Modules\Front\CategoryModule;
use App\Library\Modules\Front\HtmlContentModule;
use App\Library\Modules\Front\ManufacturerModule;
use App\Library\Modules\Front\PickupBicycleModule;
use App\Library\Modules\Front\ProductTabs;
use App\Services\CatalogService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Validator;
use Spatie\ArrayToXml\ArrayToXml;
use Illuminate\Database\Eloquent\Builder;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class HomeController extends Controller
{
    protected $catalogService;

    public function __construct(CatalogService $catalogService)
    {
        $this->catalogService = $catalogService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	//$cache_content = false;//Cache::store('file')->get('home_page');
        $cache_content = Cache::store('file')->get('home_page');
		if ($cache_content) {
			$content = $cache_content;

		} else {

		$modules = [
            BannerModule::class,
            CategoryModule::class,
            PickupBicycleModule::class,
            ProductTabs::class,
            AdvantagesModule::class,
            BlogModule::class,
            ManufacturerModule::class,
            HtmlContentModule::class
        ];

        $content = '';

			foreach ($modules as $module) {
				$languageId = config('current_language_id');
				$customerGroup = config('current_customer_group_id');
				$res = Cache::remember(
					'home_' . $module . '_' . $languageId . '_' . $customerGroup,
					6000,
					function () use ($module) {
						return app($module)->show();
					}
				);
				$content .= $res;
			}
		}

		Cache::store('file')->add('home_page', $content, 120 );

        $result = view('front.home', [
			'content' => $content,
			'products' => []
		]);

        return $result;
    }

    public function chooseBicycle(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'price_range' => 'required',
            'trace_type' => 'required',
            'weight' => 'required',
            'height' => 'required',
            'gender' => 'required',
        ]);
        if ($validator->passes()) {
            $pickupModule = app()->make(PickupBicycleModule::class);
            $query = $pickupModule->filter($request);
            $product = $query->get();
            if (isset($product) && count($product)>0) {
                $product = implode('-', $query->get()->pluck('id')->toArray());
                return response()->json(['status' => 1, 'result' => [
                    'quantity' => $query->count(),
                    'link' => url('/choose-bicycle/' . $product)
                ]]);
            } else {
                return response()->json(['status' => 1, 'result' => [
                    'quantity' => 0,
                    'link' => '#',
                    'message' => __('messages.pickup_bicycle.not_found')
                ]]);
            }
        }
        return response()->json([
            'status' => 0,
            'errors' => $validator->errors(),
            'message' => __('messages.pickup_bicycle.not_found')
        ]);
    }

    public function chooseBicycleView(Request $request){
        $productsIDs = explode('-', $request->products_id);
        $selectedFields = [
            'product_descriptions.name',
            'product_descriptions.description',
            'product_descriptions.meta_title',
            'product_descriptions.meta_description',
            'product_descriptions.meta_keywords',
            'sku.price',
            'sku.quantity',
            'sku.sku',
            'active_specials.price as special_price',
            'ui.image',
            'products.*',

        ];
        $products = Product::whereIn('products.id', $productsIDs)
            ->with(CatalogService::PRODUCT_ITEM_RELATIONS)
            ->leftJoin('product_descriptions', function ($join) {
                $languageId = config('current_language_id');
                $join->on('products.id', '=', 'product_descriptions.product_id')->where('language_id', '=', $languageId);
            })
                ->leftJoin('stock_keeping_units as sku',
                    'sku.id',
                    '=',
                    \DB::raw(
                        '(SELECT skusub.id
                    FROM stock_keeping_units as skusub 
                    WHERE skusub.product_id = products.id
                    ORDER BY skusub.quantity DESC LIMIT 1)'
                    )
                )
                ->leftJoin('unit_images as ui', 'ui.unit_id', '=', 'sku.id')
                ->leftJoin('sku_specials AS sks', 'sks.unit_id' ,'=' ,'sku.id');
        $activeSpecials = SkuSpecial::getActiveSpecials();

        $products->leftJoinSub($activeSpecials, 'active_specials', function ($join) {
            $join->on('sku.id', '=', 'active_specials.unit_id');
        })
            ->addSelect($selectedFields);
        return view('front.catalog.pickup_bicycle', [
            'products' => $products->get()
        ]);
    }

    public function marketplace(){
        $xml = [];
        $xml['name'] = config('app.name');
        $xml['description'] = 'Сталкиваясь, каждый день с тем, что сегодня существует в велосипедных магазинах (обслуживание, товарное предложение и пр.), мы видели, что можно сделать процесс покупки и дальнейшего сервиса во много раз лучше, проще, полезнее для покупателя.  Мы понимали это, потому что сами являемся  клиентами, а также могли посмотреть на организацию работы магазина и всех его процессов обслуживания изнутри как продавцы одного из существующих интернет-магазинов. Мы считаем, что создать хороший магазин велосипедов без любви к продукту очень сложно. Необходимо целиком и полностью гореть своей мечтой и задумкой. В этом нам помогает любовь к велосипедам и велоспорту, ведь мы сами катаемся и живем велотемой. Это привито нам еще с детства нашими родителями, которые даже сейчас в пожилом возрасте дадут фору молодым. ';
        $xml['url'] = url('/');
        $categories = [
            'category' => [
                '_attributes' => ['id' => '1'],
                'title' => 'Велосипеды'
            ]
        ];
        $xml['categories'] = $categories;
        $products = \DB::table('products')
            ->join('product_to_category', 'products.id', '=', 'product_to_category.product_id')
            ->join('product_descriptions', 'products.id', '=', 'product_descriptions.product_id')
            ->join('stock_keeping_units', 'products.id', '=', 'stock_keeping_units.product_id')
            ->join('unit_images', 'stock_keeping_units.id', '=', 'unit_images.unit_id')
            ->join('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
            ->join('manufacturer_descriptions', 'manufacturers.id', '=', 'manufacturer_descriptions.manufacturer_id')
            ->distinct('stock_keeping_units.product_id')
            ->distinct('unit_images.unit_id')
            ->where('product_to_category.category_id','=',1)
            ->where('stock_keeping_units.quantity', '>', 0)
            ->where('stock_keeping_units.price', '>', 0)
            ->select([
                'products.id as id',
                'product_descriptions.name as title',
                'product_descriptions.description as description',
                'products.slug as slug',
                'stock_keeping_units.quantity as quantity',
                'stock_keeping_units.price as price',
                'unit_images.image as image',
                'manufacturer_descriptions.name as manufacturer',
            ])
            ->get();
        $content = \View::make('front.catalog.marketplace', ['products' => $products, 'main_info' => $xml]);
        $content = "<?xml version=\"1.0\" ?>\n" . $content;
        return \Response::make($content, '200')->header('Content-Type', 'text/xml');
    }

    public function hotline(){
        /*$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load(public_path('hotline.xlsx'));
        $spreadsheet = array_map('array_filter', $spreadsheet->getActiveSheet()->toArray());
        $productsID = [];
        foreach ($spreadsheet as $key => $item){
            $productsID[] = (int)$item[0];
        }*/


        $xml = [];
        $xml['name'] = config('app.name');
        $xml['description'] = 'Сталкиваясь, каждый день с тем, что сегодня существует в велосипедных магазинах (обслуживание, товарное предложение и пр.), мы видели, что можно сделать процесс покупки и дальнейшего сервиса во много раз лучше, проще, полезнее для покупателя.  Мы понимали это, потому что сами являемся  клиентами, а также могли посмотреть на организацию работы магазина и всех его процессов обслуживания изнутри как продавцы одного из существующих интернет-магазинов. Мы считаем, что создать хороший магазин велосипедов без любви к продукту очень сложно. Необходимо целиком и полностью гореть своей мечтой и задумкой. В этом нам помогает любовь к велосипедам и велоспорту, ведь мы сами катаемся и живем велотемой. Это привито нам еще с детства нашими родителями, которые даже сейчас в пожилом возрасте дадут фору молодым. ';
        $xml['url'] = url('/');

        $products = \DB::table('products')
            ->join('product_to_category', 'products.id', '=', 'product_to_category.product_id')
            ->join('product_descriptions', 'products.id', '=', 'product_descriptions.product_id')
            ->join('stock_keeping_units', 'products.id', '=', 'stock_keeping_units.product_id')
            ->join('unit_images', 'stock_keeping_units.id', '=', 'unit_images.unit_id')
            ->join('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
            ->join('manufacturer_descriptions', 'manufacturers.id', '=', 'manufacturer_descriptions.manufacturer_id')
            ->join('colors', 'colors.id', '=', 'stock_keeping_units.color_id')
            ->join('color_descriptions', 'colors.id', '=', 'color_descriptions.color_id')
            ->join('sizes', 'sizes.id', '=', 'stock_keeping_units.size_id')
            ->distinct('stock_keeping_units.product_id')
            ->distinct('unit_images.unit_id')
//            ->where('product_to_category.category_id','=',1)
			->whereIn('product_to_category.category_id',[1,215,216,221])
            ->where('stock_keeping_units.quantity', '>', 0)
            ->where('stock_keeping_units.price', '>', 0)
            ->where('color_descriptions.language_id', '=', 1)
            ->where('products.status', '=', 1)
            ->select([
                'products.id as id',
                'product_descriptions.name as title',
                'product_descriptions.description as description',
                'products.slug as slug',
                'stock_keeping_units.id as sku_id',
                'stock_keeping_units.quantity as quantity',
                'stock_keeping_units.price as price',
                'stock_keeping_units.sku as sku',
                'stock_keeping_units.vendor_sku as vendor_sku',
                'unit_images.image as image',
                'manufacturer_descriptions.name as manufacturer',
                'products.category_id',
                'color_descriptions.name as color',
                'sizes.value as sizes',
            ])
//            ->groupBy('products.id')
            ->get();

        $categoriesAll = \DB::table('categories')
			->join('product_to_category', function ($join) use ($products){
				$join->on('categories.id', '=', 'product_to_category.category_id')
					->whereIn('product_to_category.product_id', $products->pluck('id'));
			})
			->join('category_descriptions', 'categories.id', '=', 'category_descriptions.category_id')
			->select(['categories.id', 'categories.parent_id', 'category_descriptions.name'])
			->groupBy('categories.id')
			->get();

//        $categories = [];
//        $categories[1]['id'] = 1;
//        $categories[1]['name'] = 'Велосипеды';
//        $xml['categories'] = $categories;
//
//        foreach ($products as $product){
//
//            if(!isset($categories[$product->category_id] )){
//                $categories[$product->category_id]['id'] = $product->category_id;
//
//                $categoryTemp = $categoriesAll
//					->where('category_id', '=', $product->category_id)
//					->first();
//
//                if($categoryTemp->parent_id !== null){
//                    $categories[$product->category_id]['parent_id'] = $categoryTemp->parent_id;
//                }
//
//                $categories[$product->category_id]['name'] = $categoryTemp->name;
//
//            }
//        }

		$cacheProducts = Cache::remember(
            'hotline_products',
            720,
            function () use ($products) {
                return $products;
            }
        );
        $content = \View::make('front.catalog.hotline', ['products' => $cacheProducts, 'main_info' => $xml, 'categories' => $categoriesAll]);
        $content = "<?xml version=\"1.0\" ?>\n" . $content;
        return \Response::make($content, '200')->header('Content-Type', 'text/xml');
    }

    public function hotlineSales(){
        $xml = [];
        $xml['name'] = config('app.name');
        $xml['description'] = 'Сталкиваясь, каждый день с тем, что сегодня существует в велосипедных магазинах (обслуживание, товарное предложение и пр.), мы видели, что можно сделать процесс покупки и дальнейшего сервиса во много раз лучше, проще, полезнее для покупателя.  Мы понимали это, потому что сами являемся  клиентами, а также могли посмотреть на организацию работы магазина и всех его процессов обслуживания изнутри как продавцы одного из существующих интернет-магазинов. Мы считаем, что создать хороший магазин велосипедов без любви к продукту очень сложно. Необходимо целиком и полностью гореть своей мечтой и задумкой. В этом нам помогает любовь к велосипедам и велоспорту, ведь мы сами катаемся и живем велотемой. Это привито нам еще с детства нашими родителями, которые даже сейчас в пожилом возрасте дадут фору молодым. ';
        $xml['url'] = url('/');

        $sales = Shares::where('id', '=', 1)
            ->with(['shareTakeProducts' => function($shareProducts){
                $shareProducts->with(['product' => function($product){
                    $product
                        ->where('status', '=', 1)
                        ->whereHas('skus', function ($skus){
                            $skus->where('quantity', '>', 0);
                        })
                        ->with(['manufacturer']);
                }]);
            }])
            ->first();
        $bergamontProducts = \DB::table('products')
            ->join('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
            ->join('stock_keeping_units', 'products.id', '=', 'stock_keeping_units.product_id')
            ->where('products.status', '=', 1)
            ->where('stock_keeping_units.quantity', '>', 0)
            ->where('manufacturers.slug', 'LIKE', 'bergamont')
            ->groupBy('products.id')
            ->select([
                'products.id as id',
                'products.slug as slug'
            ])
            ->get();
        $categories = [];
        $categories[1]['id'] = 1;
        $categories[1]['name'] = 'Велосипеды';
        $xml['categories'] = $categories;
        $cacheSales = Cache::remember(
            'hotline_sales_products',
            720,
            function () use ($sales) {
                return $sales;
            }
        );
        $content = \View::make('front.catalog.hotline-sales', ['main_info' => $xml, 'categories' => $categories, 'sales' => $cacheSales, 'bergamontProducts' => $bergamontProducts]);
        $content = "<?xml version=\"1.0\" ?>\n" . $content;
        return \Response::make($content, '200')->header('Content-Type', 'text/xml');
    }
}
