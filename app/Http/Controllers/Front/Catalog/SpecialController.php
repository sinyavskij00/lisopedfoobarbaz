<?php

namespace App\Http\Controllers\Front\Catalog;

use App\Entities\Special;
use App\Http\Controllers\Controller;
use App\Mail\Info\SpecialQuestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;

class SpecialController extends Controller
{
    protected $adminEmail;

    public function __construct()
    {
        $this->adminEmail = config('app.admin_email');
    }

    public function list()
    {
        $specials = Special::where('status', '=', 1)->with([
            'localDescription'
        ])->get();

        return view('front.specials.list', [
            'specials' => $specials
        ]);
    }

    public function show($slug)
    {
        $special = Special::where('slug', '=', $slug)
            ->where('status', '=', 1)
            ->with(['localDescription'])
            ->first();

        if (!isset($special)) {
            abort(404);
        }

        return view('front.specials.show', [
            'special' => $special
        ]);
    }

    public function question(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'text' => 'required'
        ]);
        if ($validator->passes()) {
            $name = $request->input('name');
            $email = $request->input('email');
            $text = $request->input('text');

            $mail = new SpecialQuestion($name, $email, $text);
            $mail->subject(__('mails.specials.subject'));

            Mail::to($this->adminEmail)->send($mail);
            return response()->json(['status' => 1, 'message' => __('messages.specials.success')]);
        }
        return response()->json(['status' => 0, 'errors' => $validator->errors()]);
    }
}
