<?php

namespace App\Http\Controllers\Front\Catalog;

use App\Entities\Product;
use App\Entities\Shares;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\CatalogService;

class ShareController extends Controller
{
    protected $catalogService;

    public function __construct(CatalogService $catalogService)
    {
        $this->catalogService = $catalogService;
    }

    public function show($slug){
        $share = Shares::where('slug', '=', $slug)
            ->with(['shareProducts', 'shareTakeProducts'])
            ->first();
        if (!isset($share)) {
            abort(404);
        }
        $share->views += 1;
        $share->save();
        $shareProducts = Product::where('status', '=', 1)
            ->whereIn('products.id', $share->shareTakeProducts->pluck('product_id')->toArray())
            ->with(CatalogService::PRODUCT_ITEM_RELATIONS)
            ->select(CatalogService::PRODUCT_ITEM_SELECTED_FIELDS);
        $shareProducts = $this->catalogService->addProductInfo($shareProducts);
        return view('front.shares.show', [
            'share' => $share,
            'products' => $shareProducts->inRandomOrder()->take(16)->get()
        ]);
    }

    public function productList($slug){
        $share = Shares::where('slug', '=', $slug)
            ->with(['shareProducts', 'shareTakeProducts'])
            ->first();

        if (!isset($share)) {
            abort(404);
        }
        $products = Product::whereIn('products.id', $share->shareTakeProducts->pluck('product_id')->toArray());
//        foreach ($share->shareTakeProducts->pluck('product_id')->toArray() as $id){
//
//        }
        $localDescription = $share;
        $metaData = [
            'description' => isset($localDescription) ? $localDescription->description : '',
            'meta_title' => isset($localDescription) && !empty($localDescription->meta_title)
                ? $localDescription->meta_title
                : $localDescription->title,
            'meta_description' => isset($localDescription) ? $localDescription->meta_description : '',
            'meta_keywords' => isset($localDescription) ? $localDescription->meta_keywords : '',
            'meta_h1' => isset($localDescription) && !empty($localDescription->meta_h1)
                ? $localDescription->meta_h1 : $localDescription->name,
        ];

        $languageId = config('current_language_id');
        return $productListingController = app(ProductListingController::class)->callAction(
            'index',
            [
                'products' => $products,
                'metaData' => $metaData,
                'filterCacheKey' => 'filter_for_category_1_1'
            ]
        );
    }
}
