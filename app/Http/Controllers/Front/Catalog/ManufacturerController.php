<?php

namespace App\Http\Controllers\Front\Catalog;

use App\Entities\Manufacturer;
use App\Entities\Product;
use App\Http\Controllers\Controller;

class ManufacturerController extends Controller
{
    public function list()
    {
        $manufacturers = Manufacturer::where('status', '=', 1)->with(['localDescription'])->get();

        return view('front.catalog.manufacturer_listing', [
            'manufacturers' => $manufacturers
        ]);
    }

    public function show($slug)
    {
        $manufacturer = Manufacturer::where('slug', '=', $slug)
            ->where('status', '=', 1)
            ->with(['localDescription'])
            ->first();

        if (!isset($manufacturer)) {
            abort(404);
        }

        $products = Product::where('manufacturer_id', '=', $manufacturer->id);

        $localDescription = $manufacturer->localDescription;
        $metaData = [
            'description' => isset($localDescription) ? $localDescription->description : '',
            'meta_title' => isset($localDescription) && !empty($localDescription->meta_title)
                ? $localDescription->meta_title
                : $localDescription->title,
            'meta_description' => isset($localDescription) ? $localDescription->meta_description : '',
            'meta_keywords' => isset($localDescription) ? $localDescription->meta_keywords : '',
            'meta_h1' => isset($localDescription) && !empty($localDescription->meta_h1)
                ? $localDescription->meta_h1 : $localDescription->name,
        ];

        $languageId = config('current_language_id');
        return $productListingController = app(ProductListingController::class)->callAction(
            'index',
            [
                'products' => $products,
                'metaData' => $metaData,
                'filterCacheKey' => 'filter_for_manufacturer_' . $languageId . '_' . $manufacturer->id
            ]
        );
    }
}
