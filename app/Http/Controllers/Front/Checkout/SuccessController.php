<?php

namespace App\Http\Controllers\Front\Checkout;

use App\Entities\Category;
use App\Entities\CategoryDescription;
use App\Entities\Order;
use App\Entities\StockKeepingUnit;
use App\Library\Checkout\Cart;
use App\Http\Controllers\Controller;

class SuccessController extends Controller
{
    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function index(Cart $cart)
    {
        $public_id = session()->get('public_id');
        $orderInfo = session()->get('order_data');
        $orderHistory = null;
        if(!empty($orderInfo)){
            $orderHistory = [
                'transactionId' => $orderInfo->public_id,
                'transactionAffiliation' => config('app.url'),
                'transactionTotal' => $orderInfo->total,
                'transactionTax' => '0',
                'transactionShipping' => $orderInfo->shipping_payment,
            ];
            $transactionProducts = [];
            foreach ($orderInfo->skus()->get() as $item){
                $transactionProducts[] = [
                    'sku' => $item->sku,
                    'name' => $item->name,
                    'category' => CategoryDescription::where('category_id', StockKeepingUnit::where('sku', $item->sku)->first()->product->category_id)->first()->name,
                    'price' => $item->price,
                    'quantity' => $item->quantity,
                ];
            }
            $orderHistory['transactionProducts'] = $transactionProducts;
        }
        if(session()->get('payparts_method') === 'alfa'){
            $order = Order::find(session()->get('order_data')->id);
            $order->order_status_id = 36;
            $order->save();
        }

        session()->forget('orderId');
        session()->forget('order_data');
        session()->forget('payparts_method');
        session()->forget('payparts_parts');
        $cart->deleteAll();
        return view('front.checkout.success', compact('public_id', 'orderHistory'));
    }
}
