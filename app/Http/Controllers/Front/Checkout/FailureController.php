<?php

namespace App\Http\Controllers\Front\Checkout;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FailureController extends Controller
{
    public function index()
    {
        return view('front.checkout.failure');
    }
}
