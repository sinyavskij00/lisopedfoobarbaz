<?php

namespace App\Http\Controllers\Front\Checkout;

use App\Entities\CategoryDescription;
use App\Entities\Order;
use App\Entities\OrderSku;
use App\Entities\StockKeepingUnit;
use App\Library\Checkout\Cart;
use App\Library\Checkout\Checkout;
use App\Library\Checkout\Payment\LiqPayPayment;
use App\Services\OrderService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class CheckoutController extends Controller
{
    public function index(Checkout $checkout)
    {
        if ($checkout->cart->items()->count() === 0) {
            return view('front.checkout.empty_checkout');
        }

        return view('front.checkout.checkout', [
            'checkout' => $checkout,
        ]);
    }

    public function confirm(Request $request, Checkout $checkout, OrderService $orderService)
    {
        if(count($checkout->cart->items()) <= 0 || empty($checkout->cart->items())){
            $result = [
                'status' => 0,
                'template' => view('front.checkout.errors.empty_cart')->render()
            ];
            return response()->json($result);
        }
        if($request->payment_code === 'payparts' && \App\Library\Checkout\Checkout::checkPayParts($checkout) !== true){
            $result = [
                'status' => 0,
                'template' => view('front.checkout.errors.private')->render()
            ];
            return response()->json($result);
        }
        if($request->payment_code === 'payparts-alfa' && \App\Library\Checkout\Checkout::checkPayPartsAlfa($checkout) !== true){
            $result = [
                'status' => 0,
                'template' => view('front.checkout.errors.alfabank')->render()
            ];
            return response()->json($result);
        }
        $customer = Auth::user();
        $currency = Config::get('current_currency');
        $customerId = isset($customer) ? $customer->id : 0;
        $customerGroup = isset($customer) && isset($customer->customerGroup)
            ? $customer->customerGroup : Config::get('current_customer_group');
        $userInfo = $checkout->userInfo;
        $payment_method = '';
        $payment_code = '';
        switch ($request->payment_code){
            case 'payparts':
                $payment_method = 'Оплата частями (Приват)';
                $payment_code = 'payparts';
                break;
            case 'payparts-alfa':
                $payment_method = 'Оплата частями (Альфа-Банк)';
                $payment_code = 'payparts-alfa';
                break;
            default:
                $payment_method = $checkout->selectedPaymentMethod->getName();
                $payment_code = $checkout->selectedPaymentMethod->getCode();
        }

        $data = [
            'order_invoice' => '',
            'customer_id' => $customerId,
            'customer_group_id' => $customerGroup->id,
            'first_name' => isset($userInfo['first_name']) ? $userInfo['first_name'] : '',
            'last_name' => isset($userInfo['last_name']) ? $userInfo['last_name'] : '',
            'email' => isset($userInfo['email']) ? $userInfo['email'] : '',
            'telephone' => isset($userInfo['telephone']) ? $userInfo['telephone'] : '',
            'address' => $checkout->selectedShippingMethod->getAddress(),
            'comment' => isset($checkout->comment) ? $checkout->comment : '',
            'payment_method' => $payment_method,
            'payment_code' => $payment_code,
            'inn' => $request->inn,
            'payparts_parts' => session()->get('payparts_parts'),
            'shipping_method' => $checkout->selectedShippingMethod->getName(),
            'shipping_code' => $checkout->selectedShippingMethod->getCode(),
            'sub_total' => $checkout->subTotal,
            'shipping_payment' => $checkout->shippingPayment,
            'total' => $checkout->total,
            'language_id' => Config::get('current_language_id'),
            'currency_id' => $currency->id,
            'currency_code' => $currency->code,
            'currency_value' => $currency->value
        ];

        $orderSkus = [];

        foreach ($checkout->cart->items() as $cartItem) {
            $sku = $cartItem->sku;
            $quantity = $cartItem->getAttributeValue('quantity');
            $name = isset($sku->product) && isset($sku->product->localDescription)
                ? $sku->product->localDescription->getAttributeValue('name')
                : '';
            $colorName = isset($sku->color) && isset($sku->color->localDescription)
                ? $sku->color->localDescription->getAttributeValue('name')
                : '';
            $sizeValue = isset($sku->size) ? $sku->size->getAttributeValue('value') : '';
            $yearValue = isset($sku->year) ? $sku->year->getAttributeValue('value') : '';

            $orderSkus[] = [
                'sku_id' => $sku->getAttributeValue('id'),
                'sku' => $sku->getAttributeValue('sku'),
                'vendor_sku' => $sku->getAttributeValue('vendor_sku'),
                'name' => $name,
                'model' => $sku->getAttributeValue('model'),
                'color' => $colorName,
                'size' => $sizeValue,
                'year' => $yearValue,
                'quantity' => $quantity,
                'price' => $checkout->catalogService->getSkuPrice($sku, $quantity, $customerGroup),
                'vendor_price' => $sku->getAttributeValue('vendor_price'),
                'total' => $checkout->catalogService->getSkuPrice($sku, $quantity, $customerGroup, true),
                'vendor_total' => $sku->getAttributeValue('vendor_price') * $quantity,
                'reward' => 0
            ];
        }
        $data['order_skus'] = $orderSkus;

        $order = $orderService->createOrder($data);
        session()->put('order_data', $order);
        switch ($request->payment_code){
            case 'payparts' :
                $token = $this->getPayPartsForms($order);
                return \Redirect::to('https://payparts2.privatbank.ua/ipp/v2/payment?token=' . $token);
                break;
            case 'payparts-alfa':
                $result = [
                    'status' => 1,
                    'template' => view('front.checkout.payment.receipt')->render()
                ];
                break;
            default:
                $result = [
                    'status' => 1,
                    'template' => $checkout->selectedPaymentMethod->confirm($order)
                ];
        }
        session()->put('public_id', $order->public_id);
        return response()->json($result);
    }

    public function liqPayCallback(LiqPayPayment $liqPayPayment, Request $request)
    {
        $liqPayPayment->callback($request);
    }

    public function liqPayRenderStatus(LiqPayPayment $liqPayPayment, Request $request)
    {
        $action = $liqPayPayment->renderStatus($request);
        return redirect()->route($action);
    }

    public function getShippingInfo(Request $request, Checkout $checkout)
    {
        $shippingMethods = collect(app()->tagged('shipping_methods'));
        $shippingCode = $request->input('shipping_code');
        $shippingMethod = $shippingMethods->first(function ($item) use ($shippingCode) {
            return $item->getCode() === $shippingCode;
        });
        $result = [
            'status' => 1,
            'template' => $shippingMethod->show()->render(),
            'checkout_head' => view('front.checkout.checkout_head', [
                'subTotal' => $checkout->subTotal,
                'shippingPayment' => $checkout->shippingPayment,
                'total' => $checkout->total,
                'productQuantity' => $checkout->cart->items()->count()
            ])->render()
        ];
        return response()->json($result);
    }

    public function getPaymentInfo(Request $request)
    {
        $paymentMethods = collect(app()->tagged('payment_methods'));
        $paymentCode = $request->input('payment_code');
        $paymentMethod = $paymentMethods->first(function ($item) use ($paymentCode) {
            return $item->getCode() === $paymentCode;
        });
        $result = [
            'status' => 1,
            'template' => $paymentMethod->show()->render()
        ];
        return response()->json($result);
    }

    public function getPayPartsForms($order){
        $sessionPayparts = session()->get('payparts_parts');
        if($sessionPayparts[0] === '0'){
            $sessionPayparts = str_replace('0', '', $sessionPayparts);
        }
        $password = config('payparts.private.password');
        $storeId = config('payparts.private.storeId');
        $orderId = $order->public_id;
        $amount = $order->total;
        $amountInt =  (string)($amount * 100);
        $partsCount = $sessionPayparts;
        $merchantType = 'PP';
        $responseUrl = route('payparts.confirm');
        $redirectUrl = route('payparts.renderstatus');


        $orderSku = OrderSku::where('order_id', '=', $order->id)->get();
        $products_string = '';
        $products = [];
        foreach ($orderSku as $key => $item){
            $products[$key]['name'] = $item->name;
            $products[$key]['count'] = $item->quantity;
            $products[$key]['price'] = $item->price;
            $products_string .= $item->name . $item->quantity . $item->price * 100;
        }

        $signature = base64_encode(sha1($password . $storeId . $orderId . $amountInt . $partsCount . $merchantType . $responseUrl . $redirectUrl . $products_string . $password, true));

        $formData = [
            'storeId' => $storeId,
            'orderId' => $orderId,
            'amount' => $amount,
            'partsCount' => $partsCount,
            'merchantType' => $merchantType,
            'products' => $products,
            'responseUrl' => $responseUrl,
            'redirectUrl' => $redirectUrl,
            'signature' => $signature,
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://payparts2.privatbank.ua/ipp/v2/payment/create');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Accept: application/json; charset=utf-8'
        ]);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($formData));
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        $paypartsResult = json_decode(curl_exec($ch), true);
        curl_close($ch);
        $token = $paypartsResult['token'];
        return $token;
    }

    public function paypartsCancel(Cart $cart){
        $public_id = session()->get('public_id');
        $orderInfo = session()->get('order_data');
        $orderHistory = null;
        $order = Order::where('public_id', '=',$orderInfo->public_id)->first();
        $order->order_status_id = 38;
        $order->save();
        if(!empty($orderInfo)){
            $orderHistory = [
                'transactionId' => $orderInfo->public_id,
                'transactionAffiliation' => config('app.url'),
                'transactionTotal' => $orderInfo->total,
                'transactionTax' => '0',
                'transactionShipping' => $orderInfo->shipping_payment,
            ];
            $transactionProducts = [];
            foreach ($orderInfo->skus()->get() as $item){
                $transactionProducts[] = [
                    'sku' => $item->sku,
                    'name' => $item->name,
                    'category' => CategoryDescription::where('category_id', StockKeepingUnit::where('sku', $item->sku)->first()->product->category_id)->first()->name,
                    'price' => $item->price,
                    'quantity' => $item->quantity,
                ];
            }
            $orderHistory['transactionProducts'] = $transactionProducts;
        }


        session()->forget('orderId');
        session()->forget('order_data');
        $cart->deleteAll();
        return redirect()->route('failure');
    }

    public function paypartsRenderStatus(Request $request){
        $orderInfo = session()->get('order_data');
        $orderHistory = null;
        $order = Order::where('public_id', '=',$orderInfo->public_id)->first();
        $order->order_status_id = 37;
        $order->save();
        return redirect()->route('success');
    }

    public function paypartsConfirm(Request $request){
        \Log::info('--------------------------------------------------------------------------------------------------------\n\n\n\n\n');
        \Log::info($request->all());
        \Log::info('\n\n\n\n\n--------------------------------------------------------------------------------------------------------');
    }
}
