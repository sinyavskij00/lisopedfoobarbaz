<?php

namespace App\Http\Controllers\Front\Information;

use App\Library\Settings;
use App\Http\Controllers\Controller;

class DeliveryController extends Controller
{
    protected $settings;

    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    public function index()
    {

        $delivery = $this->settings->getByKey('page_delyvery');
        $pay = $this->settings->getByKey('page_buy');
        $languageId = config('current_language_id');
        $text_d = isset($delivery) && !empty($delivery[$languageId]) ? $delivery[$languageId] : '';
        $text_p = isset($pay) && !empty($pay[$languageId]) ? $pay[$languageId] : '';
        return view('front.information.delivery', [
            'delivery' => $text_d,
            'pay' => $text_p
        ]);
    }
}
