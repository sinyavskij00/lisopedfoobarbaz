<?php

namespace App\Http\Controllers\Front\Information;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Guarantee extends Controller
{
    public function index()
    {
        return view('front.information.guarantee');
    }
}
