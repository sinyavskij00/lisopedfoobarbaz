<?php

namespace App\Http\Controllers\Front\Information;

use App\Library\Settings;
use App\Http\Controllers\Controller;

class Credit extends Controller
{
    protected $settings;

    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    public function index()
    {
        $texts = $this->settings->getByKey('credit');
        $languageId = config('current_language_id');
        $text = isset($texts) && !empty($texts[$languageId]) ? $texts[$languageId] : '';
        return view('front.information.credit', [
            'text' => $text
        ]);
    }
}
