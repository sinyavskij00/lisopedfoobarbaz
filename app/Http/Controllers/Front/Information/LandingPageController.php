<?php

namespace App\Http\Controllers\Front\Information;

use App\Entities\LandingPage;
use App\Entities\Product;
use App\Library\Checkout\NewPost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\CatalogService;

class LandingPageController extends Controller
{

    protected $catalogService;

    public function __construct(CatalogService $catalogService)
    {
        $this->catalogService = $catalogService;
    }

    public function index($slug){
        $page = LandingPage::where('url', '=', $slug)->first();
        if (!isset($page)) {
            abort(404);
        }
        $pageProducts = Product::where('status', '=', 1)
            ->whereHas('showCategories', function($category){
                $category->where('category_id', '=', 1);
            })
            ->whereHas('skus', function ($sku){
                $sku->where('quantity', '>', 0);
            })
            ->with(CatalogService::PRODUCT_ITEM_RELATIONS)
            ->select(CatalogService::PRODUCT_ITEM_SELECTED_FIELDS);
        $pageProducts = $this->catalogService->addProductInfo($pageProducts);
        $otherCities = LandingPage::where('city', 'NOT LIKE', $page->city)->inRandomOrder()->take(25)->get();
        $newPost = \DB::table('new_post_warehouses')->where('CityDescriptionRu', 'LIKE', $page->city)->where('TotalMaxWeightAllowed','=', 0)->where('Description', 'NOT LIKE', '%до 30 кг%')->get();
        return view('front.landing.show', [
            'page' => $page,
            'otherCities' => $otherCities,
            'newPost' => $newPost->pluck('DescriptionRu')->toArray(),
            'products' => $pageProducts->inRandomOrder()->take(36)->get()
        ]);
    }
}
