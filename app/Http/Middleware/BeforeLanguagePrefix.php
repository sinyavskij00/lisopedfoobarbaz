<?php

namespace App\Http\Middleware;

use App\Entities\Language;
use Closure;
use Illuminate\Support\Facades\App;
use Request;
use Illuminate\Support\Facades\Config;
use Schema;

class BeforeLanguagePrefix
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Schema::hasTable('languages')) {
            return $next($request);
        }
        $languages = Language::all();
        $langPrefixes = $languages->pluck('slug')->toArray();
        $segment = $request->segment(1);
        $default_language = $languages->where('is_default', '=', 1)->first();
        Config::set('default_language_id', $default_language->id);
        $lang_id = $default_language->id;
        $locale = $default_language->slug;

        if (isset($segment) && in_array($segment, $langPrefixes)) {
            if ($segment === $default_language->slug) {
                return redirect(mb_substr($request->server->get('REQUEST_URI'), 3), 301);
            }
            $current_language = $languages->where('slug', '=', $segment)->first();
            $lang_id = $current_language->id;
            $locale = $current_language->slug;
        }
        Config::set('current_language_id', $lang_id);
        App::setLocale($locale);

        return $next($request);
    }

    public static function getPrefix()
    {
        if (!Schema::hasTable('languages')) {
            return null;
        }
        $languages = Language::all();
        $langPrefixes = $languages->pluck('slug')->toArray();
        $segment = Request::segment(1);
        if (isset($segment) && in_array($segment, $langPrefixes)) {
            return $segment;
        }
        return null;
    }

    public static function getPrefixes()
    {
        $languages = Language::all();
        $langPrefixes = $languages->pluck('slug')->toArray();
        return $langPrefixes;
    }
}
