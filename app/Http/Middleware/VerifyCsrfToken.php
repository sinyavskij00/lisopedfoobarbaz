<?php

namespace App\Http\Middleware;

use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;


class VerifyCsrfToken extends Middleware
{

    /**
     * VerifyCsrfToken constructor.
     *
     * @param Application $app
     * @param Encrypter $encrypter
     */
    public function __construct(Application $app, Encrypter $encrypter)
    {
        parent::__construct($app, $encrypter);
//        $this->except = array_merge(\ResponseCacher::getCacheableRoutes(), $this->except);
    }


    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'liqpay/callback',
        'liqpay/renderstatus',
        'success',
        'password/email'
    ];
}
