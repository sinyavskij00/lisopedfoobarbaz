<?php

namespace App\Http\Middleware;

use App\Entities\Attribute;
use App\Entities\Category;
use App\Entities\FilterValue;
use App\Entities\Manufacturer;
use App\Entities\Filter as FilterEntity;
use App\Entities\Size;
use App\Entities\Year;
use Closure;
use Illuminate\Support\Facades\Request;

class Filter
{
    protected $languageId;

    protected $checkedFilterParams = 0;

    public function __construct()
    {
        $this->languageId = config('current_language_id');
    }

    protected $filterVariable = 'filter=';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentUrl = Request::path();
        $urlsSet = [];
        $seo_url = FilterEntity::select('filter_url', 'slug')->get()->toArray();

        foreach ($seo_url as $pair) {
            $urlsSet[$pair['filter_url']] = $pair['slug'];
        }

        if(array_key_exists(\Request::url(), $urlsSet))
        {
            return redirect($urlsSet[\Request::url()], 301);
        }

        if (str_contains($currentUrl, $this->filterVariable)) {
            $parts = explode('/', $currentUrl);
            foreach ($parts as $slug) {
                if (str_contains($slug, $this->filterVariable)) {
                    $slug = $this->clearSlug($slug);
                    $filterParts = $this->getFilterParts($slug);
                    config()->set('app.filter_parts', $filterParts);

                    $resultData = $this->getFilterValues($filterParts);

                    config()->set('app.filter_data', $resultData);
                }
            }
        }
        config()->set('app.is_nofollow_noindex', $this->checkedFilterParams >= 2);

        return $next($request);
    }

    private function getManufacturer($slug)
    {
        if (!empty($slug)) {
            $manufacturer = Manufacturer::where('slug', '=', $slug)->first();
            if (isset($manufacturer)) {
                return $manufacturer->getAttributeValue('id');
            }
        }
    }

    private function getYear($slug)
    {
        if (!empty($slug)) {
            $year = Year::where('value', '=', $slug)->first();
            if (isset($year)) {
                return $year->getAttributeValue('id');
            }
        }
    }

    private function getSize($slug)
    {
        if (!empty($slug)) {
            $attrUrlParam = explode('/', \Request::path());
            $categoriesIDs = [];


            if ($attrUrlParam[0] === 'manufacturer') {

                $categoriesIDs = \DB::table('products')
                    ->join('product_to_category', 'products.id', '=', 'product_to_category.product_id')
                    ->join('categories', 'categories.id', '=', 'product_to_category.category_id')
                    ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
                    ->where('manufacturers.slug', '=', $attrUrlParam[1])
                    ->select('categories.id')
                    ->get()
                    ->pluck('id')
                    ->unique()
                    ->toArray();
            } else {
                $categoriesIDs[] = Category::where('slug', 'LIKE', explode('/', \Request::path())[0])->first()->id;
            }
            if (is_numeric($slug)) {
                $slug .= '"';
            }
            $size = \DB::table('products')
                ->join('product_to_category', 'products.id', '=', 'product_to_category.product_id')
                ->join('stock_keeping_units', 'stock_keeping_units.product_id', '=', 'products.id')
                ->join('sizes', 'sizes.id', '=', 'stock_keeping_units.size_id')
                ->whereIn('product_to_category.category_id', $categoriesIDs)
                ->where('sizes.value', 'LIKE', $slug)
                ->groupBy('stock_keeping_units.size_id')
                ->select('stock_keeping_units.size_id')
                ->get();
            $sizeIDs = [];
            foreach ($size as $s) {
                $sizeIDs[] = $s->size_id;
            }
            if (isset($sizeIDs)) {
                return $sizeIDs;
            }
        }
    }

    private function getFilterValue($slug)
    {
        if (!empty($slug)) {
            $filterValue = FilterValue::where('slug', '=', $slug)->first();
            if (isset($filterValue)) {
                return $filterValue->getAttributeValue('id');
            }
        }
    }

    private function clearSlug($slug)
    {
        $slug = str_replace($this->filterVariable, '', $slug);
        $slug = str_replace(';;', ';', $slug);
        $slug = str_replace('::', ':', $slug);
        $slug = str_replace(',,', ',', $slug);
        return $slug;
    }

    private function getFilterParts($slug)
    {
        $filterParameters = explode(';', $slug);
        $filterParts = [];
        foreach ($filterParameters as $filterParameter) {
            $filterParameter = explode(':', $filterParameter);
            $filterParameterName = $filterParameter[0];
            $filterParts[$filterParameterName] = [];
            $filterParameterValues = explode(',', $filterParameter[1]);

            foreach ($filterParameterValues as $filterParameterValue) {
                $filterParts[$filterParameterName][] = $filterParameterValue;
            }
        }
        return $filterParts;
    }

    private function getFilterValues($filterParts)
    {
        $attributes = Attribute::get()->pluck('id', 'slug');

        $resultData = [];
        foreach ($filterParts as $key => $value) {
            if ($key === 'price') {
                $resultData['price']['min'] = $value[0] ?? null;
                $resultData['price']['max'] = $value[1] ?? null;
            } elseif ($key === 'brand') {
                if (!isset($resultData['manufacturers'])) {
                    $resultData['manufacturers'] = [];
                }
                foreach ($value as $manufacturerSlug) {
                    if (!empty($manufacturerSlug)) {
                        $resultData['manufacturers'][] = $this->getManufacturer($manufacturerSlug);
                        $this->checkedFilterParams++;
                    }
                }
            } elseif ($key === 'year') {
                if (!isset($resultData['year'])) {
                    $resultData['year'] = [];
                }
                foreach ($value as $yearsSlug) {
                    if (!empty($yearsSlug)) {
                        $resultData['year'][] = $this->getYear($yearsSlug);
                        $this->checkedFilterParams++;
                    }
                }
            } elseif ($key === 'size') {
                if (!isset($resultData['size'])) {
                    $resultData['size'] = [];
                }
                $tempSize = [];
                foreach ($value as $sizeSlug) {
                    if (!empty($sizeSlug)) {
                        //$resultData['size'][] = $this->getSize($sizeSlug);
                        $tempSize[] = $this->getSize($sizeSlug);
                        $this->checkedFilterParams++;
                    }
                }
                foreach ($tempSize as $size) {
                    foreach ($size as $id) {
                        array_push($resultData['size'], $id);
                    }
                }
            } elseif (isset($attributes[$key])) {
                foreach ($value as $filterValueSlug) {
                    if (!empty($filterValueSlug)) {
                        if (!isset($resultData['filter_attributes'])) {
                            $resultData['filter_attributes'] = [];
                        }
                        $resultData['filter_attributes_grouped'][$key][] = $filterValueSlug;
                        $resultData['filter_attributes'][] = $filterValueSlug;
                        $this->checkedFilterParams++;
                    }
                }
            } else {
                foreach ($value as $filterValueSlug) {
                    if (!empty($filterValueSlug)) {
                        if (!isset($resultData['filter_values'])) {
                            $resultData['filter_values'] = [];
                        }

                        if ($this->getFilterValue($filterValueSlug)) {
                            $resultData['filter_values'][] = $this->getFilterValue($filterValueSlug);
                            $this->checkedFilterParams++;
                        }
                    }
                }
            }
        }

        return $resultData;
    }
}
