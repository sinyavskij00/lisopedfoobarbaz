<?php

namespace App\Http\Middleware;

use App\Services\ResponseCacher;
use Illuminate\Support\Str;
use Closure;

/**
 * Class CustomCacheAdminMiddleware
 *
 * Caching works only for that routes and c\u\d operations, only if you bind that middleware to these routes
 * Recomendation: For routes, where action does not change any state.
 */
class CustomCacheAdminMiddleware
{

    /**
     * Tables to listen onchange for keeping cache up to date
     *
     * @var array $tables
     */
    private $tables;

    public function __construct()
    {
        $tables = \DB::select('SHOW TABLES');
        $res = [];
        foreach ($tables as $table) {
            foreach ($table as $key => $value)
                $res[] = $value;
        }
        $this->tables = $res;
    }

    /** 
     * @param $request
     * @param Closure $next
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|mixed
     */
    public function handle($request, Closure $next)
    {
//        $tables = $this->tables;
//        \DB::listen(function ($query) use ($tables) {
//            foreach ($tables as $table) {
//                if (Str::contains($query->sql, $table)) {
//                    if ((Str::contains($query->sql, 'insert')) || (Str::contains($query->sql, 'update')) || (Str::contains($query->sql, 'delete'))) {
//                            ResponseCacher::recacheAllResponses();
//                    }
//                }
//            }
//        });
        return $next($request);
    }
}
