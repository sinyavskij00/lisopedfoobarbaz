<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class CustomerGroup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::check()){
            $currentCustomerGroupId = \Auth::user()->customer_group_id;
            $currentCustomerGroup = \Auth::user()->customerGroup;
        } else {
            $customersGroups = Cache::remember('customerGroups', 200, function (){
                return \App\Entities\CustomerGroup::all();
            });
            $defaultCustomerGroup = $customersGroups->where('is_default', '=', 1)->first();

            $currentCustomerGroup = $defaultCustomerGroup;
            $currentCustomerGroupId = $currentCustomerGroup->id;
        }
        Config::set('current_customer_group_id', $currentCustomerGroupId);
        Config::set('current_customer_group', $currentCustomerGroup);
        return $next($request);
    }
}
