<?php

namespace App\Http\Middleware;

use Encore\Admin\Facades\Admin as AdminFacade;
use App\Services\ResponseCacher;
use Illuminate\Support\Str;
use Closure;

/**
 * Class CustomCacheUserMiddleware
 *
 * Caching works only for that routes and c\u\d operations, only if you bind that middleware to these routes
 * Recomendation: For routes, where action does not change any state.
 */
class CustomCacheUserMiddleware
{

    /**
     * @param $request
     * @param Closure $next
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|mixed
     */
    public function handle($request, Closure $next)
    {
//        $resp = null;
//        $resp = ResponseCacher::getResponseIfExists();
//        if (null != $resp) {
//          if ((gettype($resp) == "string") && (strpos($resp, '<!DOCTYPE html>') !== false)) {
//             return response($resp);
//          }
//         $resp = \Response::make($resp->response, 200);
//            return $resp;
//         // return response()->json(json_decode($resp->response, true));
//
//        } else {
//            $response = $next($request);
//            $admin_user = AdminFacade::user();
//            if ($admin_user) {
//               return $response;
//            } else {
//               ResponseCacher::cacheResponse(method_exists($response, 'render') ? $response->render() : $response->getContent());
//            }
//            return $response;
//        }
        return $next($request);
    }
}
