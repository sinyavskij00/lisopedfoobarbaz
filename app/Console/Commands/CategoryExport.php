<?php

namespace App\Console\Commands;

use App\Entities\Category;
use App\Entities\Language;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use SimpleXMLElement;

class CategoryExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:category';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export categories';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $language = Language::where('is_default', '=', 1)->first();
        $languageId = $language->id;
        config()->set('current_language_id', $languageId);

        $categories = get_category_tree();

        $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><categories></categories>");

        foreach ($categories as $category) {
            $this->addCategoryNode($category, $xml);
        }

        $xml = $xml->asXML();
        Storage::disk('1C')->put('categories/lisoped_categories_export.xml', $xml);
    }

    public function addCategoryNode(Category $category, SimpleXMLElement $parentNode)
    {
        $categoryNode = $parentNode->addChild('category');
        $categoryNode->addChild('name', $category->localDescription->name);
        $childrenNode = $categoryNode->addChild('children');
        if ($category->children->count() > 0) {
            foreach ($category->children as $child) {
                $childrenNode->addChild('category', $this->addCategoryNode($child, $childrenNode));
            }
        }
        return $categoryNode;
    }
}
