<?php

namespace App\Console\Commands;

use App\Entities\User;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class HappyBirthday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:happy-birthday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send letters happy birthday to customers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $customers = User::whereDate('birthday', '=', Carbon::today()->toDateString())->get();
        if ($customers->count() === 0) {
            return;
        }
        foreach ($customers as $customer) {
            if (!empty($customer->email)) {
                $customer->notify(new \App\Notifications\HappyBirthday());
            }
        }
    }
}
