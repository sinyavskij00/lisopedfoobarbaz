<?php

namespace App\Console\Commands;

use App\Entities\Order;
use App\Entities\OrderStatus;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class ProductReview extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:review-product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send letters review product to customers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $oneMonthAgo = Carbon::now()->subMonth(1)->format('Y-m-d');
        $customers = Order::select('orders.id', \DB::raw('IF(u.email, u.email, orders.email) as email'))
            ->leftJoin('users as u', 'u.id', '=', 'orders.customer_id')
            ->where('orders.order_status_id', '=', OrderStatus::getClosedStatusId())
            ->whereDate('orders.updated_at', '=', $oneMonthAgo)
            ->with(['skus'])
            ->get();

        if ($customers->count() === 0) {
            return;
        }

        foreach ($customers as $customer) {
            if (!empty($customer->email)) {
                \Notification::route('mail', $customer->email)->notify(
                    new \App\Notifications\ProductReview($customer)
                );
            }
        }
    }
}
