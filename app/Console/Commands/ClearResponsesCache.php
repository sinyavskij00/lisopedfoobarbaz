<?php

namespace App\Console\Commands;

use App\Facades\ResponseCacher;
use Illuminate\Console\Command;

class ClearResponsesCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'responses-cache:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear cache for responses from server to client';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ResponseCacher::clearResponses();
    }
}
