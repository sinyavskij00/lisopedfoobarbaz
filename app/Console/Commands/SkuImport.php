<?php

namespace App\Console\Commands;

use App\Entities\Attribute;
use App\Entities\AttributeDescription;
use App\Entities\AttributesValues;
use App\Entities\AttributesValuesDescription;
use App\Entities\Category;
use App\Entities\CategoryDescription;
use App\Entities\Color;
use App\Entities\ColorDescription;
use App\Entities\Language;
use App\Entities\Manufacturer;
use App\Entities\ManufacturerDescription;
use App\Entities\Product;
use App\Entities\ProductDescription;
use App\Entities\Size;
use App\Entities\StockKeepingUnit;
use App\Entities\UnitImage;
use App\Entities\Vendor;
use App\Entities\Year;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SkuImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:sku';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import products from xml';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        if (!Storage::disk('local')->exists('xml_files' . DIRECTORY_SEPARATOR . 'import.xml')) {
            return;
        }
        $file = Storage::disk('local')->get('xml_files' . DIRECTORY_SEPARATOR . 'import.xml');
        $xml = new \SimpleXMLElement($file);
        $products = [];
        foreach ($xml->product as $product) {
            $productItem = [
                'name' => (string)$product->name,
                'description' => (string)$product->description,
                'image' => (string)$product->image,
                'vendor' => (string)$product->vendor,
                'manufacturer' => (string)$product->manufacturer,
                'category' => (string)$product->category
            ];

            $attributeValues = [];
            foreach ($product->attributes->attribute as $attribute) {
                $attributeValues[] = [
                    'attribute' => (string)$attribute->attributes(),
                    'value' => (string)$attribute
                ];
            }
            $productItem['attributeValues'] = $attributeValues;

            $productSkus = [];
            foreach ($product->skus->sku as $sku) {
                $skuItem = [
                    'price' => intval($sku->price),
                    'sku' => intval($sku->sku),
                    'vendor_sku' => intval($sku->vendor_sku),
                    'quantity' => intval($sku->quantity),
                    'model' => (string)$sku->model,
                    'color' => (string)$sku->color,
                    'size' => (string)$sku->size,
                    'year' => (string)$sku->year,
                ];

                $skuImages = [];
                foreach ($sku->images->image as $image) {
                    $skuImages[] = (string)$image;
                }
                $skuItem['images'] = $skuImages;
                $productSkus[] = $skuItem;
            }

            $productItem['skus'] = $productSkus;

            $products[] = $productItem;
        }

        $languages = Language::where('is_default', '=', 1)->get();
        $language = $languages->first();

        if (!isset($language)) {
            return;
        }
        config()->set('current_language_id', $language->id);
        $languageId = $language->id;
        $now = now();

        $bar = $this->output->createProgressBar(count($products));
        foreach ($products as $product) {
            $vendorName = $product['vendor'];
            if (!empty($vendorName)) {
                $vendor = Vendor::where('name', '=', $vendorName)->first();
                if (!isset($vendor)) {
                    $vendorId = Vendor::insertGetId([
                        'name' => $vendorName,
                        'telephone_1' => '',
                        'telephone_2' => '',
                        'email' => '',
                        'email2' => '',
                        'email3' => '',
                        'address' => '',
                        'comment' => '',
                        'site' => '',
                        'created_at' => $now,
                        'updated_at' => $now
                    ]);
                } else {
                    $vendorId = $vendor->id;
                }
            }

            $productAttributeValues = [];
            if (!empty($product['attributeValues'])) {
                foreach ($product['attributeValues'] as $prodAttrVal) {
                    $attributeName = $prodAttrVal['attribute'];
                    $attributeValueName = $prodAttrVal['value'];
                    $attributeDescription = AttributeDescription::where('language_id', '=', $languageId)
                        ->where('name', '=', $attributeName)->first();
                    if (!isset($attributeDescription)) {
                        $attributeId = Attribute::insertGetId([
                            'sort' => 0,
                            'is_display' => 1,
                            'slug' => $this->getUniqueValue('attributes', 'slug', str_slug($attributeValueName)),
                            'created_at' => $now,
                            'updated_at' => $now

                        ]);
                        foreach ($languages as $language) {
                            AttributeDescription::insert([
                                'name' => $attributeName,
                                'language_id' => $language->id,
                                'attribute_id' => $attributeId
                            ]);
                        }
                    } else {
                        $attributeId = $attributeDescription->attribute_id;
                    }

                    $attributeValueDescription = AttributesValues::leftJoin('attributes_values_descriptions', function ($join) use ($languageId) {
                        $join->on('attributes_values.id', '=', 'attributes_values_descriptions.attribute_value_id')
                            ->where('language_id', '=', $languageId);
                    })
                        ->where('attributes_values.attribute_id', '=', $attributeId)
                        ->where('attributes_values_descriptions.value', '=', $attributeValueName)
                        ->first();

                    if (!isset($attributeValueDescription)) {
                        $attributeValueId = AttributesValues::insertGetId([
                            'attribute_id' => $attributeId,
                            'slug' => $this->getUniqueValue('attributes_values', 'slug', str_slug($attributeValueName))
                        ]);
                        foreach ($languages as $language) {
                            AttributesValuesDescription::insert([
                                'attribute_value_id' => $attributeValueId,
                                'language_id' => $language->id,
                                'value' => $attributeValueName,
                                'filter_name' => $attributeValueName
                            ]);
                        }
                    } else {
                        $attributeValueId = $attributeValueDescription->attribute_value_id;
                    }
                    $productAttributeValues[] = $attributeValueId;
                }
            }

            $manufacturerName = $product['manufacturer'];
            $manufacturerId = null;
            if (!empty($manufacturerName)) {
                $manufacturerDescription = ManufacturerDescription::where('name', '=', $manufacturerName)
                    ->where('language_id', '=', $languageId)->first();
                if (!isset($manufacturerDescription)) {
                    $manufacturerId = Manufacturer::insertGetId([
                        'slug' => $this->getUniqueValue('manufacturers', 'slug', str_slug($manufacturerName)),
                        'created_at' => $now,
                        'updated_at' => $now
                    ]);
                    foreach ($languages as $language) {
                        ManufacturerDescription::insert([
                            'manufacturer_id' => $manufacturerId,
                            'language_id' => $language->id,
                            'name' => $manufacturerName,
                            'description' => $manufacturerName,
                            'meta_h1' => $manufacturerName,
                            'meta_description' => $manufacturerName,
                            'meta_keywords' => $manufacturerName,
                            'meta_title' => $manufacturerName
                        ]);
                    }
                } else {
                    $manufacturerId = $manufacturerDescription->manufacturer_id;
                }
            }

            $categoryName = $product['category'];
            $categoryId = null;
            if (!empty($categoryName)) {
                $categoryDescription = CategoryDescription::where('name', '=', $categoryName)
                    ->where('language_id', '=', $languageId)->first();
                if (!isset($categoryDescription)) {
                    $categoryId = Category::insertGetId([
                        'slug' => $this->getUniqueValue('categories', 'slug', str_slug($categoryName)),
                        'status' => 1,
                        'created_at' => $now,
                        'updated_at' => $now
                    ]);
                    foreach ($languages as $language) {
                        CategoryDescription::insert([
                            'category_id' => $categoryId,
                            'language_id' => $language->id,
                            'name' => $categoryName,
                            'description' => $categoryName,
                            'meta_h1' => $categoryName,
                            'meta_description' => $categoryName,
                            'meta_keywords' => $categoryName,
                            'meta_title' => $categoryName
                        ]);
                    }
                } else {
                    $categoryId = $categoryDescription->category_id;
                }
            }


            $productName = $product['name'];
            $productId = Product::insertGetId([
                'manufacturer_id' => $manufacturerId,
                'category_id' => $categoryId,
                'image' => !empty($product['image']) ? 'shop_images/new_import/' . $product['image'] : null,
                'points' => 0,
                'points_percent' => 0,
                'slug' => $this->getUniqueValue('products', 'slug', str_slug($productName)),
                'is_preorder' => 0,
                'is_new' => 0,
                'is_hit' => 0,
                'is_our_choice' => 0,
                'created_at' => $now,
                'updated_at' => $now
            ]);

            if (!empty($productAttributeValues)) {
                foreach ($productAttributeValues as $attrValueId) {
                    DB::table('attributes_values_to_product')->insert([
                        'product_id' => $productId,
                        'attribute_value_id' => $attrValueId
                    ]);
                }
            }

            foreach ($languages as $language) {
                ProductDescription::insert([
                    'product_id' => $productId,
                    'language_id' => $language->id,
                    'name' => $productName,
                    'description' => !empty($product['description']) ? $product['description'] : $productName,
                    'meta_description' => $productName,
                    'meta_keywords' => $productName,
                    'meta_title' => $productName
                ]);
            }

            foreach ($product['skus'] as $sku) {
                $colorName = $sku['color'];
                $color = ColorDescription::where('language_id', '=', $languageId)
                    ->where('name', '=', $colorName)
                    ->first();

                if (!isset($color)) {
                    $colorId = Color::insertGetId([
                        'image' => '',
                        'anchor' => $this->getUniqueValue('colors', 'anchor', str_slug($colorName)),
                        'created_at' => $now,
                        'updated_at' => $now
                    ]);
                    foreach ($languages as $language) {
                        ColorDescription::insert([
                            'name' => $colorName,
                            'language_id' => $language->id,
                            'color_id' => $colorId
                        ]);
                    }
                } else {
                    $colorId = $color->color_id;
                }

                $year = Year::where('value', '=', $sku['year'])->first();
                if (!isset($year)) {
                    $year = Year::create([
                        'value' => $sku['year'],
                        'created_at' => $now,
                        'updated_at' => $now
                    ]);
                }
                $yearId = $year->id;

                $size = Size::where('value', '=', $sku['size'])->first();
                if (!isset($size)) {
                    $size = Size::create([
                        'value' => $sku['value'],
                        'created_at' => $now,
                        'updated_at' => $now
                    ]);
                }
                $sizeId = $size->id;

                $stockKeepingUnit = StockKeepingUnit::create([
                    'price' => !empty($sku['price']) ? $sku['price'] : 1,
                    'vendor_price' => !empty($sku['price']) ? $sku['price'] : 1,
                    'vendor_id' => $vendorId,
                    'sku' => $this->getUniqueValue('stock_keeping_units', 'sku', random_int(100000, 999999)),
                    'vendor_sku' => null,
                    'model' => !empty($sku['model']) ? $sku['model'] : $productName,
                    'is_main' => 0,
                    'quantity' => !empty($sku['quantity']) ? $sku['quantity'] : 1000,
                    'product_id' => $productId,
                    'color_id' => $colorId,
                    'size_id' => $sizeId,
                    'year_id' => $yearId,
                    'created_at' => $now,
                    'updated_at' => $now
                ]);

                if (!empty($sku['images'])) {
                    foreach ($sku['images'] as $skuImage) {
                        UnitImage::create([
                            'unit_id' => $stockKeepingUnit->id,
                            'image' => 'shop_images/new_import/' . $skuImage
                        ]);
                    }
                }
            }
            $bar->advance();
        }
        $bar->finish();

        Product::whereDoesntHave('mainSku')->chunk(1000, function ($products) {
            $products->load('skus');
            $skuIds = [];
            foreach ($products as $product) {
                if($product->skus->count() > 0){
                    $skuIds[] = $product->skus->first()->id;
                }
            }
            StockKeepingUnit::whereIn('id', $skuIds)->update(['is_main' => 1]);
        });

        echo PHP_EOL;
    }


    private function getUniqueValue($table, $column, $slug, $i = null)
    {
        if (DB::table($table)->where($column, '=', $slug)->count() > 0) {
            $i = isset($i) ? $i + 1 : 1;
            return $this->getUniqueValue($table, $column, $slug . $i, $i);
        } else {
            return $slug;
        }
    }
}
