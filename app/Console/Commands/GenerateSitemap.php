<?php

namespace App\Console\Commands;

use App\Entities\Category;
use App\Entities\LandingPage;
use App\Entities\Language;
use App\Entities\Manufacturer;
use App\Entities\News;
use App\Entities\Product;
use App\Entities\StockKeepingUnit;
use App\Services\CatalogService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Spatie\ArrayToXml\ArrayToXml;

class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate-sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Sitemap.xml file';

    protected $urlPerSitemap = 10000;

    protected $sitemapsSubdirectory = 'sitemaps';

    protected $productSitemapName = 'product-sitemap';

    protected $pageSitemapName = 'page-sitemap';

    protected $imageSitemapName = 'image-sitemap';

    protected $productsLinks = [];

    protected $pagesLinks = [];

    protected $imagesLinks = [];

    protected $catalogService;

    /**
     * Create a new command instance.
     *
     * @param CatalogService $catalogService
     */
    public function __construct(CatalogService $catalogService)
    {
        parent::__construct();
        ini_set('max_execution_time', 300); //fix for shared
        $this->catalogService = $catalogService;
        $language = Language::where('is_default', '=', 1)->first();
        if (isset($language)) {
            config()->set('current_language_id', $language->id);
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->clearSubSitemaps();
        $currentDate = \Illuminate\Support\Carbon::parse(now())->format('Y-m-d');
        /*************** PAGES *********************/
        $this->addPageLink([
            'loc' => route('home'),
            'lastmod' => $currentDate,
            'changefreq' => 'daily',
            'priority' => 1
        ]);
        $this->addPageLink([
            'loc' => route('about_us'),
            'lastmod' => $currentDate,
            'changefreq' => 'monthly',
            'priority' => 0.5
        ]);
        $this->addPageLink([
            'loc' => route('delivery'),
            'lastmod' => $currentDate,
            'changefreq' => 'monthly',
            'priority' => 0.5
        ]);

        /*************** MANUFACTURERS *********************/
        $this->addPageLink([
            'loc' => route('manufacturer.list'),
            'lastmod' => $currentDate,
            'changefreq' => 'daily',
            'priority' => 0.8
        ]);

        Manufacturer::where('status', '=', 1)->chunk(1000, function ($manufacturers) use ($currentDate) {
            foreach ($manufacturers as $manufacturer) {
                $this->addPageLink([
                    'loc' => route('manufacturer.show', $manufacturer->slug),
                    'lastmod' => $currentDate,
                    'changefreq' => 'daily',
                    'priority' => 0.8
                ]);
            }
        });

        /*************** CATEGORIES *********************/

        Category::where('status', '=', 1)->chunk(1000, function ($categories) use ($currentDate) {
            foreach ($categories as $category) {
                $this->addPageLink([
                    'loc' => route('category_path', category_path($category->id)),
                    'lastmod' => $currentDate,
                    'changefreq' => 'daily',
                    'priority' => 0.9
                ]);
            }
        });

        /*************** LANDING PAGES *********************/
        LandingPage::chunk(1000, function ($landingPages) use ($currentDate) {
            foreach ($landingPages as $landingPage) {
                $this->addPageLink([
                    'loc' => url($landingPage->url),
                    'lastmod' => $currentDate,
                    'changefreq' => 'daily',
                    'priority' => 0.8
                ]);
            }
        });

        /*************** NEWSES *********************/
        $this->addPageLink([
            'loc' => route('news.list'),
            'lastmod' => $currentDate,
            'changefreq' => 'monthly',
            'priority' => 0.5
        ]);

        News::where('status', '=', 1)->chunk(1000, function ($newses) use ($currentDate) {
            foreach ($newses as $news) {
                $this->addPageLink([
                    'loc' => route('news.show', $news->slug),
                    'lastmod' => $currentDate,
                    'changefreq' => 'monthly',
                    'priority' => 0.5
                ]);
            }
        });

        $this->makeAdditionalSitemap($this->pagesLinks, $this->pageSitemapName);

        /*************** PRODUCTS *********************/
        Product::where('status', '=', 1)->chunk(5000, function ($products) use ($currentDate) {
            foreach ($products as $product) {
                $this->addProductLink([
                    'loc' => route('product_path', $product->slug),
                    'lastmod' => $currentDate,
                    'changefreq' => 'weekly',
                    'priority' => 0.7
                ]);
            }
        });

        $this->makeAdditionalSitemap($this->productsLinks, $this->productSitemapName);

        StockKeepingUnit::with('product')->chunk(500, function ($skus) {
            foreach ($skus as $sku) {
                if (isset($sku->product)
                    && isset($sku->product->slug)
                    && $sku->product->status === 1
                    && $sku->images->count() > 0
                ) {
                    $imageData = [
                        'loc' => route('product_path', $sku->product->slug),
                        'images' => []
                    ];
                    foreach ($sku->images as $image) {
                        $imageData['images'][] = [
                            'loc' => $image->image,
                            'title' => isset($sku->product->localDescription)
                                ? $sku->product->localDescription->name
                                : ''
                        ];
                    }
                    $this->addImageLink($imageData);
                }
            }
        });

        $this->makeImagesSitemap($this->imagesLinks, $this->imageSitemapName);
        $this->makeMainSitemap();
    }

    private function clearSubSitemaps()
    {
        $files = Storage::disk('public_directory')->files($this->sitemapsSubdirectory);
        Storage::disk('public_directory')->delete($files);
    }

    private function addProductLink($data)
    {
        $this->productsLinks[] = $data;
        if (count($this->productsLinks) >= $this->urlPerSitemap) {
            $this->makeAdditionalSitemap($this->productsLinks, $this->productSitemapName);
        }
    }

    private function addPageLink($data)
    {
        $this->pagesLinks[] = $data;
        if (count($this->pagesLinks) >= $this->urlPerSitemap) {
            $this->makeAdditionalSitemap($this->pagesLinks, $this->pageSitemapName);
        }
    }

    private function addImageLink($data)
    {
        $this->imagesLinks[] = $data;
        if (count($this->imagesLinks) >= $this->urlPerSitemap) {
            $this->makeImagesSitemap($this->imagesLinks, $this->imageSitemapName);
        }
    }

    private function makeAdditionalSitemap(&$stuff, $mapName)
    {
        if (!empty($stuff)) {
            $sitemapTempalte = '<?xml version="1.0" encoding="UTF-8"?>'
                . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'
                . '</urlset>';

            $xml = new \SimpleXMLElement($sitemapTempalte);
            for ($i = 0; $i < count($stuff); $i++) {
                $url = $xml->addChild('url');
                $url->addChild('loc', $stuff[$i]['loc']);
                $url->addChild('lastmod', $stuff[$i]['lastmod']);
                $url->addChild('changefreq', $stuff[$i]['changefreq']);
                $url->addChild('priority', $stuff[$i]['priority']);
            }

            for ($i = 1; true; $i++) {
                $fileName = $this->sitemapsSubdirectory . DIRECTORY_SEPARATOR . $mapName . $i . '.xml';
                if (!Storage::disk('public_directory')->exists($fileName)) {
                    Storage::disk('public_directory')->put($fileName, $xml->asXML());
                    break;
                }
            }
        }
        $stuff = [];
    }

    private function makeImagesSitemap(&$stuff, $mapName)
    {
        if (!empty($stuff)) {
            $sitemap_template = '<?xml version="1.0" encoding="UTF-8"?>'
                . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"'
                . ' xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">'
                . '</urlset>';

            $xml = new \SimpleXMLElement($sitemap_template);

            for ($i = 0; $i < count($stuff); $i++) {
                if (!empty($stuff[$i]['images'])) {
                    $url = $xml->addChild('url');
                    $url->addChild('loc', $stuff[$i]['loc']);
                    foreach ($stuff[$i]['images'] as $image) {
                        if (Storage::disk('public')->exists($image['loc'])) {
                            $size = getimagesize(Storage::disk('public')->path($image['loc']));

                            $resizedImage = $this->catalogService->resize($image['loc'], $size[0], $size[1]);
                            $namespace = 'http://www.google.com/schemas/sitemap-image/1.1';

                            $imageNode = $url->addChild('image:image', null, $namespace);
                            $imageNode->addChild("loc", $resizedImage, $namespace);
                            if (!empty($image['title'])) {
                                $imageNode->addChild('image:title', $image['title'], $namespace);
                            }
                        }
                    }
                }
            }
            for ($i = 1; true; $i++) {
                $fileName = $this->sitemapsSubdirectory . DIRECTORY_SEPARATOR . $mapName . $i . '.xml';
                if (!Storage::disk('public_directory')->exists($fileName)) {
                    Storage::disk('public_directory')->put($fileName, $xml->asXML());
                    break;
                }
            }
            $stuff = [];
        }
    }

    private function makeMainSitemap()
    {
        $currentDate = \Illuminate\Support\Carbon::parse(now())->format('Y-m-d');
        $mainSitemapTempalte = '<?xml version="1.0" encoding="UTF-8"?>'
            . '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'
            . '</sitemapindex>';

        $xml = new \SimpleXMLElement($mainSitemapTempalte);
        $files = Storage::disk('public_directory')->files($this->sitemapsSubdirectory);
        foreach ($files as $file) {
            $sitemapNode = $xml->addChild('siteamp');
            $sitemapNode->addChild('loc', url($file));
            $sitemapNode->addChild('lastmod', $currentDate);
        }
        Storage::disk('public_directory')->put('sitemap.xml', $xml->asXML());
    }
}
