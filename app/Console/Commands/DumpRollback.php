<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DumpRollback extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:rollback {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restore database backup';

    protected $folderName = 'db_dumps';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fileName = $this->argument('filename');

        if (\Storage::disk('local')->exists($this->folderName . '/' . $fileName)) {
            $filePath = \Storage::disk('local')->path($this->folderName . '/' . $fileName);

            $password = config('database.connections.mysql.password');
            $databaseName = config('database.connections.mysql.database');
            $userName = config('database.connections.mysql.username');
            $host = config('database.connections.mysql.host');

            $command = 'mysql -h' . $host . ' -u' . $userName . ' -p' . $password . ' ' . $databaseName . ' < ' . $filePath;
            $output = [];
            exec($command, $output, $worked);
            switch ($worked) {
                case 0:
                    echo 'Import file <b>' . $fileName . '</b> successfully imported to database <b>' . $databaseName . '</b>';
                    break;
                case 1:
                    echo 'There was an error during import. Please make sure the import file is saved in the same folder as this script and check your values:<br/><br/><table><tr><td>MySQL Database Name:</td><td><b>' . $databaseName . '</b></td></tr><tr><td>MySQL User Name:</td><td><b>' . $userName . '</b></td></tr><tr><td>MySQL Password:</td><td><b>NOTSHOWN</b></td></tr><tr><td>MySQL Host Name:</td><td><b>' . $host . '</b></td></tr><tr><td>MySQL Import Filename:</td><td><b>' . $fileName . '</b></td></tr></table>';
                    break;
            }
            echo PHP_EOL;
        }
    }
}
