<?php

namespace App\Console\Commands;

use App\Entities\Vendor;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use SimpleXMLElement;

class VendorExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:vendor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export vendors';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $vendors = Vendor::all();

        $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><vendors></vendors>");

        foreach ($vendors as $vendor) {
            $vendorNode = $xml->addChild('vendor');
            $vendor = $vendor->toArray();

            foreach ($vendor as $key => $value) {
                $vendorNode->addChild($key, $value);
            }
        }
        $xml = $xml->asXML();
        Storage::disk('1C')->put('vendors/lisoped_vendors_export.xml', $xml);
    }
}
