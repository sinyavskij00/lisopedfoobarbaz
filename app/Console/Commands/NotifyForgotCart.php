<?php

namespace App\Console\Commands;

use App\Entities\CartItem;
use App\Entities\Currency;
use App\Entities\Language;
use App\Entities\User;
use App\Notifications\ForgottenCart;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

class NotifyForgotCart extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:forgot-cart';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify customers about forgotten carts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $defaultCurrency = Currency::where('is_default', '=', 1)->first();
        config()->set('current_currency_id', $defaultCurrency->id);
        config()->set('current_currency', $defaultCurrency);

        $language = Language::where('is_default', '=', 1)->first();
        if (!isset($language)) {
            return;
        }
        config()->set('current_language_id', $language->id);
        $twoDaysAgo = Carbon::now()->subDay(2)->format('Y-m-d');
        $cartItems = CartItem::where('sku_id', '!=', null)
            ->whereDate('updated_at', '<=', $twoDaysAgo)
            ->get();

        if ($cartItems->count() === 0) {
            return;
        }

        $data = [];
        foreach ($cartItems as $cartItem) {
            $data[$cartItem->customer_id][] = $cartItem->sku;
        }

        foreach ($data as $customerId => $skus) {
            $customer = User::find($customerId);

            if (isset($customer) && !empty($customer->email)) {
                Notification::send($customer, new ForgottenCart($skus, $customer));
            }
        }
    }
}
