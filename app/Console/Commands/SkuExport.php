<?php

namespace App\Console\Commands;

use App\Entities\Category;
use App\Entities\CategoryDescription;
use App\Entities\Color;
use App\Entities\Language;
use App\Entities\Product;
use App\Entities\ProductDescription;
use App\Entities\Size;
use App\Entities\StockKeepingUnit;
use App\Entities\Vendor;
use App\Entities\Year;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use SimpleXMLElement;

class SkuExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:sku';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export sku';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $language = Language::where('is_default', '=', 1)->first();
        $languageId = isset($language) ? $language->id : 1;
        config()->set('current_language_id', $languageId);

        $sizes = Size::leftJoin('size_descriptions', function ($join) use ($languageId) {
            $join->on('sizes.id', '=', 'size_descriptions.size_id')
                ->where('size_descriptions.language_id', '=', $languageId);
        })->get();

        $colors = Color::leftJoin('color_descriptions', function ($join) use ($languageId) {
            $join->on('colors.id', '=', 'color_descriptions.color_id')->where('language_id', '=', $languageId);
        })->get();

        $years = Year::all();

        $vendors = Vendor::all();

        $categories = Category::leftJoin('category_descriptions', function ($join) use ($languageId) {
            $join->on('categories.id', '=', 'category_descriptions.category_id')
                ->where('category_descriptions.language_id', '=', $languageId);
        })->select([
            'categories.id',
            'category_descriptions.category_id'
        ])->get();

        $result = [];
        StockKeepingUnit::chunk(500, function ($skus) use ($sizes, $colors, $years, &$result, $languageId, $vendors) {
            foreach ($skus as $sku) {
                $productName = '';
                $categoryName = '';
                if (isset($sku->product_id)) {
                    $productDescription = ProductDescription::where('language_id', '=', $languageId)
                        ->where('product_id', '=', $sku->product_id)->first();
                    $productName = isset($productDescription) ? $productDescription->name : '';

                    $product = Product::where('id', '=', $sku->product_id)
                        ->select([
                            'category_id'
                        ])
                        ->first();
                    $categoryId = $product->category_id;
                    if (isset($categoryId)) {
                        $categoryDescription = CategoryDescription::where('category_id', '=', $categoryId)
                            ->where('language_id', '=', $languageId)
                            ->select([
                                'name'
                            ])
                            ->first();
                        $categoryName = isset($categoryDescription) ? $categoryDescription->name : '';
                    }
                }
                $sku->product_name = $productName;
                $sku->category = $categoryName;

                $sizeValue = '';
                if (isset($sku->size_id)) {
                    $size = $sizes->where('id', '=', $sku->size_id)->first();
                    $sizeValue = isset($size) ? $size->value . ' ( ' . $size->comment . ' )' : '';
                }
                $sku->size_value = $sizeValue;

                $colorValue = '';
                if (isset($sku->color_id)) {
                    $color = $colors->where('id', '=', $sku->color_id)->first();
                    $colorValue = isset($color) ? $color->name : '';
                }
                $sku->color_value = $colorValue;

                $yearValue = '';
                if (isset($sku->year_id)) {
                    $year = $years->where('id', '=', $sku->year_id)->first();
                    $yearValue = isset($year) ? $year->value : '';
                }
                $sku->year_value = $yearValue;

                $vendorName = '';
                if (isset($sku->vendor_id)) {
                    $vendor = $vendors->where('id', '=', $sku->vendor_id)->first();
                    $vendorName = isset($vendor) ? $vendor->name : '';
                }
                $sku->vendor_name = $vendorName;

                $result[] = $sku->toArray();
            }
        });

        $xmlSkuInfo = new SimpleXMLElement("<?xml version=\"1.0\"?><stock_keeping_units></stock_keeping_units>");
        $this->arrayToXml($result, $xmlSkuInfo);
        $xml = $xmlSkuInfo->asXML();
        Storage::disk('1C')->put('sku/lisoped_skus_export.xml', $xml);
    }

    private function arrayToXml($array, &$xml_user_info)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    $subnode = $xml_user_info->addChild("$key");
                    $this->arrayToXml($value, $subnode);
                } else {
                    $subnode = $xml_user_info->addChild("stock_keeping_unit");
                    $this->arrayToXml($value, $subnode);
                }
            } else {
                $xml_user_info->addChild("$key", htmlspecialchars("$value"));
            }
        }
    }
}
