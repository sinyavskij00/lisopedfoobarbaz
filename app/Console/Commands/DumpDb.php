<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DumpDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:dump';

    protected $folderName = 'db_dumps';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make database dump';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Spatie\DbDumper\Exceptions\CannotStartDump
     * @throws \Spatie\DbDumper\Exceptions\DumpFailed
     */
    public function handle()
    {
        $password = config('database.connections.mysql.password');
        $databaseName = config('database.connections.mysql.database');
        $userName = config('database.connections.mysql.username');

        $dumpFileName = date('Y_m_d') . '_' . intval(microtime(true)) . '_db_dump.sql';
        $dumpFilePath = \Storage::disk('local')->path($this->folderName . DIRECTORY_SEPARATOR . $dumpFileName);

        $check = \Storage::disk('local')->exists($this->folderName);
        if (!$check) {
            \Storage::disk('local')->createDir($this->folderName);
        }

        \Spatie\DbDumper\Databases\MySql::create()
            ->setDbName($databaseName)
            ->setUserName($userName)
            ->setPassword($password)
            ->dumpToFile($dumpFilePath);

        return $dumpFileName;
    }
}
