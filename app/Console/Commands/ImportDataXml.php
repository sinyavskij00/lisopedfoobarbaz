<?php

namespace App\Console\Commands;

use App\Entities\Attribute;
use App\Entities\Product;
use App\Entities\ProductDescription;
use App\Entities\ColorDescription;
use App\Entities\StockKeepingUnit;
use App\Entities\Year;
use App\Entities\SizeDescription;
use App\Entities\SkuSpecial;
use SimpleXMLElement;

use Illuminate\Console\Command;

class ImportDataXml extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:xml';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import a products, sku, attributes, manufactures and etc...';

    protected static $manufacturers;
    protected static $attributes;
    protected static $products;
    protected static $colors;
    protected static $years;
    protected static $sizes;
    protected static $skus;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fileContent = "https://velotrade.com.ua/shop-price-link.xml";
        try {
            $xml = new SimpleXMLElement($fileContent, 0, true);
        } catch (\Exception $e) {
            $xml = false;
        }
        $products = [];
        $categoryId = [];
        foreach ($xml->catalog->category as $category) {
            foreach ((array)$category as $one) {
                if (is_array($one) && !empty($one["parentId"]) && $one["parentId"] == '33') {
                    $categoryId[] = $one['id'];
                }
            }
        }

        foreach ($xml->items->item as $item) {
            $bool = true;
            $description = (string)$item->description;
            $product = (array)$item;
            unset($product['@attributes']);
            if (!in_array($product['categoryId'], $categoryId)) {
                continue;
            }
            $product['description'] = $description;
            $product['param'] = [];
            foreach ($item->param as $param) {
                if ((string)$param['name'] == 'Год' && (int)$param[0] < 2017) {
                    $bool = false;
                }
                $product['param'][] = [
                    'name' => (string)$param['name'],
                    'value' => (string)$param[0]
                ];
            }
            if (!$bool) {
                continue;
            }
            $products[] = $product;
        }

        // old_id => new_id
        $categries = [
            '33' => '1', //Велосипеды
            '100465' => '22', //Складные
            '100398' => '19', //Беговелы
            '100130' => '8', //Элеткровелосипеды
            '100129' => '20', //Детские
            '100128' => '21', //Подростковые
            '100127' => '9', //Городские
            '100126' => '12', //Туристические (гибрид)
            '100125' => '2', //Горные

//            '37' => '81', //Комплектующие
//            '2' => '50', //Аксессуары
//            '63' => '69', //Велокомпьютер
//
//            '66' => '64', //Детские велокресла
//            '70' => '135', //Инструмент
//            '71' => '82', //Колеса для детских велосипедов
//            '74' => '27', //Насосы
//            '78' => '32', //Свет
//            '79' => '42', //Тросы с замком
//            '81' => '48', //Фляги
//            '82' => '49', //Флягодержатели
//            '86' => '59', //Корзины
//            '40' => '84', //Камеры
//            '41' => '97', //Каретки
//            '42' => '95', //Кассеты
//            '43' => '93', //Манетки
//            '44' => '86', //Обода
//            '45' => '98', //Педали
//            '46' => '92', //Переключатели задние
//            '47' => '92', //Переключатели передние
//            '48' => '118', //Подседельные штыри
//            '49' => '83', //Покрышки
//            '54' => '116', //Седла
//            '58' => '94', //Шатуны
//            '59' => '101', //Тормоза
//            '90' => '122', //Вилки
//            '91' => '85', //Втулки
//            '92' => '110', //Выносы
//            '93' => '111', //Грипсы
        ];

        //copy images
        $publicDirPath = \Storage::disk('public')->path('shop_images' . DIRECTORY_SEPARATOR . 'import' . DIRECTORY_SEPARATOR);
        echo PHP_EOL . 'start copy images' . PHP_EOL;
        foreach ($products as $product) {
            //copy images
            if (!empty($product['image'])) {
                $imageUrl = $product['image'];
                $imagePathInfo = pathinfo($imageUrl);
                $imageBaseName = $imagePathInfo['basename'];
                $newImagePath = $publicDirPath . $imageBaseName;
                $newImagePath = str_replace('/', DIRECTORY_SEPARATOR, $newImagePath);
                $newImagePath = str_replace('\\', DIRECTORY_SEPARATOR, $newImagePath);
                if (!file_exists($newImagePath)) {
                    $image = file_get_contents($imageUrl);
                    file_put_contents($newImagePath, $image);
                }
            }
        }
        echo PHP_EOL . 'end copy images' . PHP_EOL;
        //create manufacturers
        $attributesDataOne = [];
        $attributesData = [];
        $newColors = [];
        $newSizes = [];
        $vendors = [];

        self::$attributes = Attribute::getAttributesByName()->toArray();
        self::$products = ProductDescription::getProductsByName();
        self::$colors = ColorDescription::getColorsByName();
        self::$skus = StockKeepingUnit::getSkuByVendorSku();
        self::$years = Year::get()->pluck('id', 'value');
        self::$sizes = SizeDescription::getSizesByName();

        foreach ($products as $product) {
            if (!empty($product['vendor'])) {
                $vendors[] = strtolower($product['vendor']);
            }
            if (!empty($product['param'])) {
                foreach ($product['param'] as $param) {
                    if ($param['name'] == 'Диаметр колес') {
                        $param['name'] = 'Диаметр колеса';
                    }
                    if ($param['name'] == 'Ручки переключения') {
                        $param['name'] = 'Ручки переключения (Манетки)';
                    }
                    if ($param['name'] == 'Кассета/трещетка') {
                        $param['name'] = 'Кассета';
                    }

                    if ($param['name'] == 'Размер рамы') {
                        $valueSize = $param['value'];
                    }

                    if ($param['name'] == 'Подходит на рост, см') {
                        $newSizes[$valueSize. 'рост ' . $param['value'] . ' см'] = array(
                            'value' => $valueSize,
                            'comment' => 'рост ' . $param['value'] . ' см');
                    }

                    $param['name'] = mb_strtolower($param['name']);

                    if (!isset($attributesData[$param['name']])) {
                        $attributesDataOne[$param['name']] = [];
                    }
                    $attributesDataOne[$param['name']] = $param['value'];
                }

                foreach ($attributesDataOne as $key => $attribute) {
                    if (isset(self::$attributes[$key])) {
                        $attributesData[$key] = array(
                            'id' => self::$attributes[$key],
                            'text' => $attribute
                        );
                    }
                    if ($key == 'цвет') {
                        if (!isset(self::$colors[$attribute])) {
                            $newColors[] = $attribute;
                        }
                    }
                }
            }
        }
        $vendors = array_values(array_unique($vendors));
        self::$manufacturers = \App\Entities\Manufacturer::get()->pluck('slug')->toArray();
        $languages = \App\Entities\Language::all();
        $now = now();

        foreach ($vendors as $vendor) {
            $manufacturerData = [
                'image' => '',
                'slug' => str_slug($vendor),
                'created_at' => $now,
                'updated_at' => $now,
            ];
            if (!in_array(str_slug($vendor), self::$manufacturers)) {
                echo 'create a manufacturer';

                $manufacturerId = \App\Entities\Manufacturer::insertGetId($manufacturerData);
                $manufacturerDescriptionData = [];
                foreach ($languages as $language) {
                    $manufacturerDescriptionData[] = [
                        'manufacturer_id' => $manufacturerId,
                        'language_id' => $language->id,
                        'name' => ucfirst($vendor),
                        'description' => $vendor,
                        'meta_h1' => $vendor,
                        'meta_description' => $vendor,
                        'meta_keywords' => $vendor,
                        'meta_title' => $vendor
                    ];
                }
                \App\Entities\ManufacturerDescription::insert($manufacturerDescriptionData);
            }
        }
        //create size
        foreach ($newSizes as $key => $size) {
            if (!isset(self::$sizes[$key])) {
                echo 'create a size';
                $sizeId = \App\Entities\Size::insertGetId([
                    'value' => $size['value'],
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
                $sizeDescriptionData = [];
                foreach ($languages as $language) {
                    $sizeDescriptionData[] = [
                        'size_id' => $sizeId,
                        'language_id' => $language->id,
                        'comment' => $size['comment']
                    ];
                }
                \App\Entities\SizeDescription::insert($sizeDescriptionData);
            }
        }
        //create colors
        if (!empty($newColors)) {
            $newColors = array_unique($newColors);
            foreach ($newColors as $color) {
                echo 'create a color';
                $colorId = \App\Entities\Color::insertGetId([
                    'anchor' => str_replace(' ', '-', $color),
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
                $colorDescriptionData = [];
                foreach ($languages as $language) {
                    $colorDescriptionData[] = [
                        'color_id' => $colorId,
                        'language_id' => $language->id,
                        'name' => $color
                    ];
                }
                \App\Entities\ColorDescription::insert($colorDescriptionData);
            }
        }

        //create/update products and sku
        $manufacturerDescriptions = \App\Entities\ManufacturerDescription::where('language_id', '=', 1)->get();
        foreach ($products as $product) {
            $praramProduct = 0;
            $praramColor = 0;
            $valueSize = '';
            foreach ($product['param'] as $param) {
                if ($param['name'] == 'Год') {
                    $praramProduct = $param['value'];
                }
                if ($param['name'] == 'Цвет') {
                    $praramColor = $param['value'];
                }
                if ($param['name'] == 'Размер рамы') {
                    $valueSize = $param['value'];
                }
                if ($param['name'] == 'Подходит на рост, см') {
                    $valueSize .= 'рост ' . $param['value'] . ' см';
                }
            }

            $full_price = isset($product['bnprice']) ? $product['bnprice'] : null;
            $price = isset($product['priceuah']) ? $product['priceuah'] : 0;
            $name = $product['name'];
            $description = $product['description'];
            $newCategoryId = isset($categries[$product['categoryId']]) ? $categries[$product['categoryId']] : 1;
            $product['vendor'] = strtolower($product['vendor']);
            $product['vendor'] = ucfirst($product['vendor']);
            $manufacturerId = !empty((string)$product['vendor'])
                ? $manufacturerDescriptions->where('name', '=', $product['vendor'])->first()->getAttributeValue('manufacturer_id') : null;
            $image = '';

            if (!empty($product['image'])) {
                $imageUrl = $product['image'];
                $imagePathInfo = pathinfo($imageUrl);
                $imageBaseName = $imagePathInfo['basename'];
                $image = 'shop_images/import/' . $imageBaseName;
            }

            $productName = mb_strtolower($name);

            if (isset(self::$products[$productName])) {
                $productId = self::$products[$productName];
            } else {
                $productId = null;
            }

            $year = self::$years[$praramProduct];
            $color = self::$colors[$praramColor];
            $statusProduct = $product['quantity'] == '0' ? 0 : 1;

            $sieId = isset(self::$sizes[$valueSize]) ? self::$sizes[$valueSize] : 1;
            if (!is_null($productId)) {
                echo "update a product $name\n";
                \App\Entities\Product::where('id', '=', $productId)
                    ->update(
                        [
                            'status' => $statusProduct,
                            'updated_at' => $now,
                        ]
                    );

                \App\Entities\StockKeepingUnit::where('product_id', '=', $productId)
                    ->where('vendor_sku', '=', $product['vendorCode'])
                    ->update(
                        [
                            'price' => isset($full_price) ? $full_price : $price,
                            'quantity' => isset($product['quantity']) ? $product['quantity'] : 0,
                        ]
                    );

                if (isset(self::$skus[$product['vendorCode']])) {
                $skusId = self::$skus[$product['vendorCode']];
                    if (isset($full_price)) {
                        \App\Entities\SkuSpecial::where('unit_id', '=', $skusId['id'])
                            ->update(
                                [
                                    'price' => $price,
                                    'date_end' => $now->addDays(1),
                                ]
                            );
                    } else {
                        \App\Entities\SkuSpecial::where('unit_id', '=', $skusId['id'])->delete();
                    }
                }
            } else {
                echo "create a product $name\n";
                $productId = \App\Entities\Product::insertGetId(
                    [
                        'category_id' => $newCategoryId,
                        'manufacturer_id' => $manufacturerId,
                        'points' => 0,
                        'points_percent' => 0,
                        'slug' => $this->getProductSlug($name),
                        'is_preorder' => 0,
                        'is_new' => random_int(0, 1),
                        'is_hit' => random_int(0, 1),
                        'is_our_choice' => random_int(0, 1),
                        'status' => $statusProduct,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ]
                );

                \DB::table('product_to_category')->insert([
                    'product_id' => $productId,
                    'category_id' => $newCategoryId
                ]);
                $productDescriptions = [];
                foreach ($languages as $language) {
                    $productDescriptions[] = [
                        'product_id' => $productId,
                        'language_id' => $language->id,
                        'name' => $name,
                        'description' => $description,
                        'meta_description' => $name,
                        'meta_keywords' => $name,
                        'meta_title' => $name
                    ];
                }
                \App\Entities\ProductDescription::insert($productDescriptions);

                // Velotrade have a vaendor_id = 23 in DB
                $skuId = StockKeepingUnit::insertGetId([
                    'price' => isset($full_price) ? $full_price : $price,
                    'vendor_id' => isset($product['vendorCode']) ? $product['vendorCode'] : null,
                    'sku' => $this->getUniqueSlug(),
                    'is_main' => 1,
                    'vendor_id' => 23,
                    'vendor_sku' => isset($product['vendorCode']) ? $product['vendorCode'] : 0,
                    'quantity' => isset($product['quantity']) ? $product['quantity'] : 0,
                    'product_id' => $productId,
                    'size_id' => $sieId,
                    'color_id' => isset($color) ? $color : 0,
                    'year_id' => isset($year) ? $year : 0,
                ]);

                if (isset($full_price)) {
                    \App\Entities\SkuSpecial::insert(
                        [
                            'customer_group_id' => 1,
                            'unit_id' => $skuId,
                            'priority' => 1,
                            'price' => $price,
                            'date_start' => $now,
                            'date_end' => $now,
                        ]
                    );
                }

                if (!empty($image)) {
                    \App\Entities\UnitImage::insert([
                        'unit_id' => $skuId,
                        'image' => $image
                    ]);
                }

                foreach ($attributesData as $attributeName => $attribute) {
                    \App\Entities\ProductAttribute::insert([
                        'product_id' => $productId,
                        'attribute_id' => $attribute['id'],
                        'language_id' => '1',
                        'text' => $attribute['text']
                    ]);
                }

            }
        }

    }

    public function getProductSlug($name, $i = 0)
    {
        $slug = ($i === 0) ? str_slug($name) : str_slug($name . $i);
        if (Product::where('slug', '=', $slug)->count() > 0) {
            return $this->getProductSlug($name, ($i + 1));
        } else {
            return $slug;
        }
    }

    protected function getUniqueSlug($slug = false)
    {
        $hash = uniqid();

        return $slug ? (str_slug($slug) . '-' . $hash) : $hash;
    }
}
