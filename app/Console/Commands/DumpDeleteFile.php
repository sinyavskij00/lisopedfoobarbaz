<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DumpDeleteFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:delete-dump {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete some dump file. Use "all" as filename option for deleting all dumps';

    protected $folderName = 'db_dumps';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fileName = $this->argument('filename');
        if (!empty($fileName)) {
            if ($fileName === 'all') {
                $files = \Storage::disk('local')->allFiles($this->folderName);
                foreach ($files as $file) {
                    \Storage::disk('local')->delete($file);
                }
            } else {
                $localFilePath = $this->folderName . DIRECTORY_SEPARATOR . $fileName;
                \Storage::disk('local')->delete($localFilePath);
            }
        }
    }
}
