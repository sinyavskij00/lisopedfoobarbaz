<?php

namespace App\Mail\Notification;

use App\Entities\StockKeepingUnit;
use App\Entities\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductBecomeCheaper extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public $sku;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param StockKeepingUnit $sku
     */
    public function __construct(User $user, StockKeepingUnit $sku)
    {
        $this->user = $user;
        $this->sku = $sku;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.notification.letter_product_become_cheaper');
    }
}
