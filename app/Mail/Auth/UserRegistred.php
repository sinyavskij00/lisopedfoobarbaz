<?php

namespace App\Mail\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRegistred extends Mailable
{
    use Queueable, SerializesModels;

    public $title;

    public $subTitle;

    public $login;

    public $password;

    public $name;

    /**
     * Create a new message instance.
     *
     * @param null $title
     * @param null $subTitle
     * @param null $login
     * @param null $password
     */
    public function __construct($title = null, $subTitle = null, $login = null, $password = null, $name = null)
    {
        $this->title = $title;
        $this->subTitle = $subTitle;
        $this->login = $login;
        $this->password = $password;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail/notification.user_registred');
    }
}
