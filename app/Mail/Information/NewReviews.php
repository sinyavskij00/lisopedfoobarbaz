<?php

namespace App\Mail\Information;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewReviews extends Mailable
{
    use Queueable, SerializesModels;

    public $id;
    public $text;

    /**
     * Create a new message instance.
     *
     * @param $text
     * @param null $link
     */
    public function __construct($id, $text)
    {
        $this->id = $id;
        $this->text = $text;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail/notification.new_reviews');
    }
}
