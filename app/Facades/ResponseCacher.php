<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;
use App\Services\ResponseCacher as ResponseCacherService;

class ResponseCacher extends Facade
{
    protected static function getFacadeAccessor()
    {
        return ResponseCacherService::class;
    }
}