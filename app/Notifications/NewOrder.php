<?php

namespace App\Notifications;

use App\Entities\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewOrder extends Notification
{
    use Queueable;

    protected $order;

    /**
     * Create a new notification instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \App\Mail\Orders\NewOrder
     */
    public function toMail($notifiable)
    {
        $mail = new \App\Mail\Orders\NewOrder($this->order);
        $adminEmail = config('app.admin_email');
        $mail->to($this->order->email);
        $mail->from($adminEmail, config('app.name'));
        $mail->replyTo($adminEmail);
        $mail->subject(__('mails.orders.new_order.subject', ['number' => $this->order->public_id]));
        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
