<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use NotificationChannels\SmscRu\SmscRuMessage;
use NotificationChannels\SmscRu\SmscRuChannel;

class OrderChangeStatusBySms extends Notification
{
    public function via($notifiable)
    {
        $channels = [];
        if ($notifiable->notify_sms === 1) {
            $channels[] = SmscRuChannel::class;
        }
        if ($notifiable->notify_viber === 1) {
            $channels[] = SmscRuViberChanel::class;
        }
        return $channels;
    }

    public function toSmscRu($notifiable)
    {
        $message = isset($notifiable->smsTemplate) ? $notifiable->smsTemplate->text : '';

        $publicId = isset($notifiable->order) && isset($notifiable->order->public_id)
            ? $notifiable->order->public_id
            : '';

        $message = str_replace('{public_id}', $publicId, $message);

        $orderStatus = isset($notifiable->status) && isset($notifiable->status->localDescription)
        && isset($notifiable->status->localDescription->name)
            ? $notifiable->status->localDescription->name
            : '';

        $message = str_replace('{order_status}', $orderStatus, $message);

        $publicComment = $notifiable->public_comment;

        $message = str_replace('{public_comment}', $publicComment, $message);

        $billingCart = $notifiable->billingCart;
        $billingCartName = isset($billingCart) ?  $billingCart->getAttributeValue('name') : '';
        $billingCartNumber = isset($billingCart) ? $billingCart->getAttributeValue('number') : '';

        $message = str_replace('{billing_cart_name}', $billingCartName, $message);
        $message = str_replace('{billing_cart_number}', $billingCartNumber, $message);

        $billingAccount = $notifiable->billingAccount;
        $billingAccountName = isset($billingAccount) ? $billingAccount->getAttributeValue('name') : '';
        $billingAccountEmployerNumber = isset($billingAccount)
            ? $billingAccount->getAttributeValue('employer_number')
            : '';
        $billingAccountBankNumber = isset($billingAccount) ? $billingAccount->getAttributeValue('bank_number') : '';
        $billingAccountCheckingAccount = isset($billingAccount)
            ? $billingAccount->getAttributeValue('checking_account')
            : '';

        $message = str_replace('{billing_account_name}', $billingAccountName, $message);
        $message = str_replace('{billing_account_employer_number}', $billingAccountEmployerNumber, $message);
        $message = str_replace('{billing_account_bank_number}', $billingAccountBankNumber, $message);
        $message = str_replace('{billing_account_checking_account}', $billingAccountCheckingAccount, $message);

        return SmscRuMessage::create($message);
    }
}
