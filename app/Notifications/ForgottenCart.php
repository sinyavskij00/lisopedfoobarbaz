<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ForgottenCart extends Notification
{
    use Queueable;

    protected $skus;

    protected $user;

    protected $adminEmail;

    /**
     * Create a new notification instance.
     *
     * @param $skus
     */
    public function __construct($skus, $user)
    {
        $this->skus = $skus;
        $this->user = $user;
        $this->adminEmail = config('app.admin_email');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \App\Mail\Notification\ForgottenCart
     */
    public function toMail($notifiable)
    {
        $mail = new \App\Mail\Notification\ForgottenCart($this->skus, $this->user);
        $mail->to($notifiable->email);
        $mail->subject(__('mails.notifications.forgotten_cart'));
        $mail->replyTo($this->adminEmail);
        $mail->from($this->adminEmail);
        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
