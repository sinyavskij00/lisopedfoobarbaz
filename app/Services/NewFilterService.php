<?php

namespace App\Services;

use App\Entities\Attribute;
use App\Entities\AttributeGroup;
use App\Entities\Category;
use App\Entities\Filter;
use App\Entities\Manufacturer;
use App\Entities\Product;
use App\Entities\ProductAttribute;
use App\Entities\Size;
use App\Entities\SkuSpecial;
use App\Entities\StockKeepingUnit;
use App\Entities\Year;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\DB;

class NewFilterService
{
	protected $filterVariable;

	protected $currentLanguageId;

	/**
	 * FilterService constructor.
	 */
	public function __construct()
	{
		$this->filterVariable = config('app.filter_data');
		$this->currentLanguageId = config('current_language_id');
	}


	/**
	 * @param Builder $products
	 * @return array
	 */

	public function getFilterByProducts(Builder $products, $categoryId = false)
	{
		$result = [];
		$allProducts = clone $products;
		$products = clone $products;

		$productsIds = $products->pluck('id');
		$allProductsIds = $allProducts->pluck('id');
		$filteredProducts = $this->getFilteredProducts($products);
		$filteredProductsIds = $filteredProducts->pluck('id');
		$activeSpecials = SkuSpecial::getActiveSpecials();

		$prices = StockKeepingUnit::leftJoinSub($activeSpecials, 'active_specials', function ($join) {
			$join->on('stock_keeping_units.id', '=', 'active_specials.unit_id');
		})->whereIn('stock_keeping_units.product_id', $productsIds)->select([
			DB::raw('min(stock_keeping_units.price) as min_price'),
			DB::raw('max(stock_keeping_units.price) as max_price'),
			DB::raw('min(active_specials.price) as min_special_price'),
			DB::raw('max(active_specials.price) as max_special_price'),
		])
			->where('stock_keeping_units.is_main', '=', 1)
			->first();

		$result['filteredProductsIds'] = $filteredProductsIds;

		$currentCategory = Category::find($categoryId);

		switch (explode('/', \Request::path())[0]) {
			case 'manufacturer' :
				$categoryId = Category::whereHas('products', function ($query) use ($categoryId) {
					$query->where('manufacturer_id', '=', $categoryId)
						->where('status', '=', 1);
				})->get()->pluck('id')->toArray();
				break;
			case 'search' :
				$currentProducts = $products->get();
				$categoryId = Category::whereHas('products', function ($query) use ($currentProducts) {
					$query->whereIn('id', $currentProducts->pluck('id')->toArray());
				})->get()->pluck('id')->toArray();
				break;
			case 'velosipedy' :
				$categoryTmp = Category::find($categoryId);
				$categoryId = [
					(int)$categoryId
				];
				while ($categoryTmp->parent_id !== null) {
					$tmpId = Category::where('id', $categoryTmp->parent()->first()->id)->first()->id;
					$categoryId[] = $tmpId;
					$categoryTmp = Category::find($tmpId);
				}
				break;
			default:
				$categoryId = [
					Category::find($categoryId)->id
				];
		}

		//filter by price
		$result['price']['min'] = isset($prices->min_special_price) && $prices->min_special_price < $prices->min_price
			? $prices->min_special_price
			: $prices->min_price;
		$result['price']['max'] = isset($prices->max_special_price) && $prices->max_special_price > $prices->max_price
			? $prices->max_special_price
			: $prices->max_price;

		//filter by attributes

		$filterProductCounts = DB::table('filter_value_to_product')
			->groupBy('filter_value_id')
			->select([
				DB::raw('COUNT(product_id) as product_count'),
				'filter_value_id'
			])
			->whereIn('product_id', $productsIds)
			->get();

		$filterValuesIds = $filterProductCounts->pluck('filter_value_id')->toArray();

		$result['filters'] = Filter::whereHas('values', function ($query) use ($filterValuesIds) {
			$query->whereIn('id', $filterValuesIds);
		})
			->with(['values' => function ($query) use ($filterValuesIds) {
				$query->whereIn('id', $filterValuesIds);
			},
				'values.localDescription',
				'localDescription',
			])->orderBy('sort')->get();

		foreach ($result['filters'] as $filter) {
			$filter->name = isset($filter->localDescription) && isset($filter->localDescription->name)
				? $filter->localDescription->name
				: '';
			foreach ($filter->values as $filterValue) {
				$filterProductCount = $filterProductCounts
					->where('filter_value_id', '=', $filterValue->id)
					->first();
				$filterValue->products_count = isset($filterProductCount) && isset($filterProductCount->product_count)
					? $filterProductCount->product_count
					: 0;
				$filterValue->name = isset($filterValue->localDescription)
				&& isset($filterValue->localDescription->name)
					? $filterValue->localDescription->name
					: '';
			}
		}

		if (isset($currentCategory->id) && isset(Category::find($currentCategory->id)->show_manufacture__filter) && explode('/', \Request::path())[0] !== 'manufacturer') {
			$result['show_manufacturers'] = Category::find($currentCategory->id)->show_manufacture__filter;
		} else {
			$result['show_manufacturers'] = 0;
		}

		if (explode('/', \Request::path())[0] === 'velosipedy') {
			$result['show_years'] = 1;
			$result['show_sizes'] = 1;
		} else {
			$result['show_years'] = 0;
			$result['show_sizes'] = 0;
		}

//filter by category group attributes
		$result['filters_cat'] = $this->getFilterByAttributes($categoryId, $filteredProductsIds, $allProductsIds);
//filter by category group attributes
		$result['manufacturers'] = $this->getFilterByManufacturers($categoryId, $filteredProductsIds, $allProductsIds);
//filter by category group attributes
		$result['sizes'] = $this->getFilterBySizes($categoryId, $filteredProductsIds, $allProductsIds);
//filter by category group attributes
		$result['years'] = $this->getFilterByYears($categoryId, $filteredProductsIds, $allProductsIds);
//dd($result);
		return array($result, $products);
	}

	public function getFilterByManufacturers($categoryId, $filteredProductsIds, $allProductsIds)
	{
		$manufacturersIds = Product::leftJoin('product_to_category', 'products.id', '=', 'product_to_category.product_id')
			->whereIn('product_to_category.category_id', $categoryId)
			->select('products.manufacturer_id')
			->distinct()
			->get()
			->pluck('manufacturer_id')
			->toArray();

		$result['manufacturers'] = Manufacturer::leftJoin('manufacturer_descriptions', function ($join) {
			$languageId = config('current_language_id');
			$join->on('manufacturers.id', '=', 'manufacturer_descriptions.manufacturer_id')
				->where('manufacturer_descriptions.language_id', '=', $languageId);
		})->select([
			'manufacturers.id',
			'manufacturers.slug',
			'manufacturer_descriptions.name',
		])
			->where('manufacturers.status', '=', 1)
			->whereIn('manufacturers.id', $manufacturersIds)
			->orderBy('manufacturer_descriptions.name')
			->get();


		if (!isset($this->filterVariable) || (count($this->filterVariable) == 1 && isset($this->filterVariable['manufacturers']))) {
			$allManufacturerProductCounts = DB::table('products')->groupBy('manufacturer_id')
				->select([
					DB::raw('COUNT(id) as product_count'),
					'manufacturer_id'
				])
				->whereIn('id', $allProductsIds)
				->get();

			foreach ($result['manufacturers'] as $k => $manufacturer) {
				$manufacturerProductCount = $allManufacturerProductCounts
					->where('manufacturer_id', '=', $manufacturer->id)
					->first();

				$manufacturer->products_count = isset($manufacturerProductCount)
				&& isset($manufacturerProductCount->product_count)
					? $manufacturerProductCount->product_count
					: 0;
			}
		} elseif (isset($this->filterVariable) && (count($this->filterVariable) > 1) || (!isset($this->filterVariable['manufacturers']))) {
			$manufacturerProductCounts = DB::table('products')->groupBy('manufacturer_id')
				->select([
					DB::raw('COUNT(id) as product_count'),
					'manufacturer_id'
				])
				->whereIn('id', $filteredProductsIds)
				->get();

			foreach ($result['manufacturers'] as $k => $manufacturer) {
				$manufacturerProductCount = $manufacturerProductCounts
					->where('manufacturer_id', '=', $manufacturer->id)
					->first();

				$manufacturer->products_count = isset($manufacturerProductCount)
				&& isset($manufacturerProductCount->product_count)
					? $manufacturerProductCount->product_count
					: 0;
			}
		}

		return $result['manufacturers'];
	}


	public function getFilterByAttributes($categoryId, $filteredProductsIds, $allProductsIds)
	{
		$filterProductCategoryCounts = DB::table('category_attributes')
			->join('product_attributes as pa', 'pa.attribute_id', '=', 'category_attributes.attribute_id')
			->whereIn('category_attributes.category_id', $categoryId)
			->groupBy('category_attributes.attribute_id')
			->select([
				'category_attributes.attribute_id'
			])
			->get();

		$filterAttrValuesIds = $filterProductCategoryCounts->pluck('attribute_id')->toArray();

		$filterCat = DB::table('attributes')
			->join('product_attributes', 'attributes.id', '=', 'product_attributes.attribute_id')
			->join('products', 'products.id', '=', 'product_attributes.product_id')
			->join('product_to_category', 'products.id', '=', 'product_to_category.product_id')
			->whereIn('product_attributes.attribute_id', $filterAttrValuesIds)
			->whereIn('product_to_category.category_id', $categoryId)
			->select('product_attributes.id')
			->get();

		$result['filters_cat'] = Attribute::select('attributes.*', 'attributes.slug')
			->whereHas('valuesWithoutCount', function ($query) use ($filterCat, $filterAttrValuesIds) {
				$query->whereIn('attribute_id', $filterAttrValuesIds);
			})
			->with(['valuesWithoutCount' => function ($query) use ($filterCat) {
				$query->whereIn('id', $filterCat->pluck('id'));
			}])
			->orderBy('sort_on_filter')->get();


		foreach ($result['filters_cat'] as $filter) {

			$filter->name = isset($filter->localDescription) && isset($filter->localDescription->name)
				? $filter->localDescription->name
				: '';

			foreach ($filter->valuesWithoutCount as $key_value => $value) {

				if (count(explode(';', $value->text)) > 1) {
					$tmpVal = explode(';', $value->text);

					foreach ($tmpVal as $tmpValKey => $tmpValItem) {
						$tmpVal[$tmpValKey] = trim($tmpValItem);
					}

					foreach ($tmpVal as $key => $tmpValItem) {
						$is_isset = $filter->valuesWithoutCount->where('text', '=', $tmpValItem)->first();

						if (!isset($is_isset)) {
							$tmpValObj = clone $value;
							$tmpValObj->text = $tmpValItem;
							$tmpValObj->is_multi = 1;
							$tmpValObj->products_count = 0;
							$tmpValObj->string_original = $value->slug;
							$filter->valuesWithoutCount->push($tmpValObj);
						}
						$filter->valuesWithoutCount->where('text', '=', $tmpValItem)->first()->is_multi = 1;
					}
					$filter->valuesWithoutCount->forget($key_value);
				}
			}


			if (!isset($this->filterVariable) ||
				isset($this->filterVariable['filter_attributes_grouped'][$filter->slug])
//					&& array_key_last($this->filterVariable['filter_attributes_grouped']) == $filter->slug
				&& (count($this->filterVariable['filter_attributes_grouped']) == 1)) {

				foreach ($filter->valuesWithoutCount->unique('slug') as $k => $value) {

					if (!isset($value->is_multi)) {

						$value->products_count = count(ProductAttribute::where('attribute_id', '=', $value->attribute_id)
							->where('slug', $value->slug)
							->whereIn('product_id', $allProductsIds)
							->groupBy('product_id')
							->get());

					} else {
						$cat = Product::where('id', $value->product_id)->pluck('category_id')[0];

						$value->products_count = count(DB::table('products')
							->join('product_to_category', 'products.id', '=', 'product_to_category.product_id')
							->join('product_attributes', 'products.id', '=', 'product_attributes.product_id')
							->where('product_attributes.attribute_id', $value->attribute_id)
							->where('products.category_id', $cat)
							->groupBy('products.id')
							->where(function ($q) use ($value) {
								$q->where('product_attributes.text','LIKE', '%; ' . $value->text . ';%')
									->orWhere('product_attributes.text', 'LIKE', '%; ' . $value->text)
									->orwhere('product_attributes.text', 'LIKE',  $value->text . ';%')
									->orwhere('product_attributes.text', '=',  $value->text );
							})
							->whereIn('product_attributes.product_id', $allProductsIds)
							->get());

//						$value->products_count = count(ProductAttribute::where('attribute_id', '=', $value->attribute_id)
//							->where(function ($q) use ($value) {
//								$q->where('product_attributes.text','LIKE', '%; ' . $value->text . ';%')
//									->orWhere('product_attributes.text', 'LIKE', '%; ' . $value->text)
//									->orwhere('product_attributes.text', 'LIKE',  $value->text . ';%')
//									->orwhere('product_attributes.text', '=',  $value->text );
//							})
//							->whereIn('product_attributes.product_id', $allProductsIds)
//							->get());

					}
				}

			} else {

				foreach ($filter->valuesWithoutCount->unique('slug') as $k => $value) {

					if (!isset($value->is_multi)) {
						$value->products_count = count(ProductAttribute::where('attribute_id', '=', $value->attribute_id)
							->where('slug', $value->slug)
							->whereIn('product_id', $filteredProductsIds)
							->groupBy('product_id')
							->get());

					} else {
						$cat = Product::where('id', $value->product_id)->pluck('category_id')[0];

						$value->products_count = count(DB::table('products')
							->join('product_to_category', 'products.id', '=', 'product_to_category.product_id')
							->join('product_attributes', 'products.id', '=', 'product_attributes.product_id')
							->where('product_attributes.attribute_id', $value->attribute_id)
							->where('products.category_id', $cat)
							->groupBy('products.id')
							->where(function ($q) use ($value) {
								$q->where('product_attributes.text','LIKE', '%; ' . $value->text . ';%')
									->orWhere('product_attributes.text', 'LIKE', '%; ' . $value->text)
									->orwhere('product_attributes.text', 'LIKE',  $value->text . ';%')
									->orwhere('product_attributes.text', '=',  $value->text );

							})
							->whereIn('product_attributes.product_id', $filteredProductsIds)
							->get());

//						$value->products_count = count(ProductAttribute::where('attribute_id', '=', $value->attribute_id)
//							->where(function ($q) use ($value) {
//								$q->where('product_attributes.text','LIKE', '%; ' . $value->text . ';%')
//									->orWhere('product_attributes.text', 'LIKE', '%; ' . $value->text)
//									->orwhere('product_attributes.text', 'LIKE',  $value->text . ';%')
//									->orwhere('product_attributes.text', '=',  $value->text );
//							})
//							->whereIn('product_attributes.product_id', $filteredProductsIds)
//							->get());
					}
				}
			}

		}

		return $result['filters_cat'];
	}


	public function getFilterByYears($categoryId, $filteredProductsIds, $allProductsIds)
	{
		$yearIds = StockKeepingUnit::whereHas('product', function ($productQuery) use ($categoryId) {

			$productQuery->leftJoin('product_to_category', function ($categoryJoin) use ($categoryId) {

				$categoryJoin->on('products.id', '=', 'product_to_category.product_id')
					->whereIn('product_to_category.category_id', $categoryId);
			});
		})
			->where('year_id', '!=', 0)
			->select('year_id')
			->distinct()
			->get()
			->pluck('year_id')
			->toArray();

		$result['years'] = Year::whereIn('id', $yearIds)
			->select([
				'id',
				'value',
				DB::raw('value as name'),
				DB::raw('value as slug')
			])->get();


		if (!isset($this->filterVariable) || ((count($this->filterVariable) == 1) && (isset($this->filterVariable['years'])))) {
			foreach ($result['years'] as $year) {
				$yearProductCount = Product::leftJoin('product_to_category', function ($categoryJoin) use ($categoryId) {
					$categoryJoin->on('products.id', '=', 'product_to_category.product_id')
						->whereIn('product_to_category.category_id', [$categoryId[0]]);
				})
					->whereHas('skus', function ($skusQuery) use ($year) {
						$skusQuery->where('year_id', $year->id);
					})
					->whereIn('products.id', $allProductsIds)
					->get();

				$year->products_count = count($yearProductCount);
			}
		} elseif (isset($this->filterVariable) && (count($this->filterVariable) > 1) || (!isset($this->filterVariable['years']))) {

			foreach ($result['years'] as $year) {

				$yearProductCount = Product::leftJoin('product_to_category', function ($categoryJoin) use ($categoryId) {
					$categoryJoin->on('products.id', '=', 'product_to_category.product_id')
						->whereIn('product_to_category.category_id', [$categoryId[0]]);
				})
					->whereHas('skus', function ($skusQuery) use ($year) {
						$skusQuery->where('year_id', $year->id);
					})
					->whereIn('products.id', $filteredProductsIds)
					->get();

				$year->products_count = count($yearProductCount);
			}
		}

		return $result['years'];
	}


	public function getFilterBySizes($categoryId, $filteredProductsIds, $allProductsIds)
	{
		$sizesIds = DB::table('products')
			->join('product_to_category', 'products.id', '=', 'product_to_category.product_id')
			->join('stock_keeping_units', 'stock_keeping_units.product_id', '=', 'products.id')
			->join('sizes', 'sizes.id', '=', 'stock_keeping_units.size_id')
			->whereIn('product_to_category.category_id', $categoryId)
			->groupBy('stock_keeping_units.size_id')
			->select('stock_keeping_units.size_id')
			->get()
			->pluck('size_id')
			->toArray();

		$sizesGroup = Size::whereIn('id', $sizesIds)
			->select([
				'id',
				'value'
			])
			->orderBy('id')
			->get();

		$groupSizesAttr = [];
		foreach ($sizesGroup as $group) {
			$groupSizesAttr[$group->value][$group->id] = $group->id;
		}

		$result['sizes'] = Size::whereIn('id', $sizesGroup->pluck('id')->unique())
			->select([
				'id',
				'value',
				DB::raw('value as name'),
				DB::raw('value as slug')
			])
			->groupBy('value')
			->orderBy('value')
			->get();

		if (!isset($this->filterVariable) || ((count($this->filterVariable) == 1) && (isset($this->filterVariable['size'])))) {
			foreach ($result['sizes'] as $size) {

				$sizeProductCount = Product::whereHas('skus', function ($skuQuery) use ($groupSizesAttr, $size) {
					$skuQuery->whereIn('size_id', $groupSizesAttr[$size->value]);
				})
					->whereIn('id', $allProductsIds)
					->get();

				$size->products_count = count($sizeProductCount);
				$size->slug = str_replace('"', "", $size->slug);
			}
		} elseif (isset($this->filterVariable) && count($this->filterVariable) > 1 || (!isset($this->filterVariable['size']))) {
			foreach ($result['sizes'] as $size) {
				$sizeProductCount = Product::whereHas('skus', function ($skuQuery) use ($groupSizesAttr, $size) {
					$skuQuery->whereIn('size_id', $groupSizesAttr[$size->value]);
				})
					->whereIn('id', $filteredProductsIds)
					->get();

				$size->products_count = count($sizeProductCount);
				$size->slug = str_replace('"', "", $size->slug);
			}
		}

		return $result['sizes'];
	}


	public function getFilteredProducts(Builder $products)
	{

		if (empty($this->filterVariable)) {
			return $products;
		}

		$filterVariable = $this->filterVariable;

		$products->where(function ($query) use ($filterVariable, $products) {
			if (isset($filterVariable['price'])) {
				if (isset($filterVariable['price']['min'])) {
					$priceMin = $filterVariable['price']['min'];
					$query->where(function ($query) use ($priceMin) {
						$query->where('sku.price', '>=', $priceMin)
							->orWhere('active_specials.price', '>=', $priceMin);
					});
				}

				if (isset($filterVariable['price']['max'])) {
					$priceMax = $filterVariable['price']['max'];
					$query->where(function ($query) use ($priceMax) {
						$query->where('sku.price', '<=', $priceMax)
							->orWhere('active_specials.price', '<=', $priceMax);
					});
				}
			}

			if (isset($filterVariable['filter_attributes'])) {
				$attrUrlParam = explode('/', \Request::path());
				$categoriesIDs = [];
				switch ($attrUrlParam[0]) {
					case 'manufacturer' :
						$categoriesIDs = DB::table('products')
							->join('product_to_category', 'products.id', '=', 'product_to_category.product_id')
							->join('categories', 'categories.id', '=', 'product_to_category.category_id')
							->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
							->where('manufacturers.slug', '=', $attrUrlParam[1])
							->select('categories.id')
							->get()
							->pluck('id')
							->unique()
							->toArray();
						break;
					case 'search' :
						/*$categoriesIDs = Category::whereHas('products', function($query) use ($products){
							$query->whereIn('id', $products->get()->pluck('id'));
						})->get()->pluck('id');*/
						$categoriesIDs = Category::all()->pluck('id');
						break;
					default:
						$categoriesIDs = Category::whereIn('slug', $attrUrlParam)->get()->pluck('id');
				}

				$filterProductAttribute = DB::table('category_attributes')
					->join('attributes', 'attributes.id', '=', 'category_attributes.attribute_id')
					->whereIn('category_attributes.category_id', $categoriesIDs)
					->select([
						'attributes.id',
						'attributes.slug'
					])
					->get();


				$filterProducts = ProductAttribute::where(function ($productAttributeQuery) use ($filterVariable, $filterProductAttribute) {

					foreach ($filterVariable['filter_attributes_grouped'] as $key => $filter_attributes) {

						foreach ($filter_attributes as $fil_attr_val) {

							$productAttributeQuery->orWhere(function ($productAttributeQueryInner) use ($filterVariable, $filterProductAttribute, $key, $filter_attributes, $fil_attr_val) {
								$productAttributeQueryInner->where('product_attributes.slug', 'LIKE', $fil_attr_val)
									->where('product_attributes.attribute_id', '=', $filterProductAttribute->where('slug', '=', $key)->first()->id);

							});
						}
					}
				})->get();



//                $filter_attributes_name = array_keys($filterVariable['filter_attributes_grouped']);
//
//                $attributes_filter =  Attribute::whereIn('slug', $filter_attributes_name)->get();
//
//                $separateProdItems = ProductAttribute::where('text', 'LIKE', '%' . ';'. '%')
//                    ->whereIn('attribute_id',$attributes_filter )
//                    ->get();
//
//                if ($separateProdItems)
//                {
//                    foreach ($separateProdItems as $item)
//                    {
//                        $pieceOfText = explode(';', $item->text);
//                        foreach ($pieceOfText as $v)
//                        {
//                            $a = $this->transliterate(trim($v));
//
//                            if (in_array($a, $filterVariable['filter_attributes']))
//                                $filterProducts->push($item);
//                        }
//                    }
//                }

				$filterProducts = $this->checkDoubleAttributes($filterProducts);

				$productAttrIDs = [];

				foreach ($filterProducts->unique('id')->groupBy('product_id') as $attrGroup) {
					if (count($attrGroup) === count($filterVariable['filter_attributes_grouped'])) {
						foreach ($attrGroup as $attrID) {
							$productAttrIDs[] = $attrID->id;
						}
					}
				}
				$query->whereHas('filterAttributes', function ($queryFilterAttributes) use ($productAttrIDs) {
					$queryFilterAttributes->whereIn('product_attributes.id', $productAttrIDs);
				});
			}
//dd($products->get());
			if (isset($filterVariable['filter_values'])) {
				$query->whereHas('filterValues', function ($query) use ($filterVariable) {
					$query->whereIn('filter_value_id', $filterVariable['filter_values']);
				});
			}

			if (isset($filterVariable['manufacturers'])) {
				$query->whereIn('manufacturer_id', $filterVariable['manufacturers']);
			}


			if (isset($filterVariable['year'])) {
				$query->whereHas('skus', function ($skuQuery) use ($filterVariable) {
					$skuQuery->whereIn('year_id', $filterVariable['year']);
				});
			}
			if (isset($filterVariable['size'])) {
				$query->whereHas('skus', function ($skuQuery) use ($filterVariable) {
					$skuQuery->whereIn('size_id', $filterVariable['size']);
				});
			}
		});

		return $products;
	}

	public function addLinksToFilter($filter)
	{
		$clearCurrentUrl = $this->getUrlWithoutFilter();
		$filterData = [
			'query_string' => request()->getQueryString(),
			'clear_current_url' => $clearCurrentUrl,
			'filter_parts' => config('app.filter_parts')
		];

		if (isset($filter['filters_cat'])) {
			foreach ($filter['filters_cat'] as $filterEntity) {
				foreach ($filterEntity->valuesWithoutCount as $filterValue) {
					$filterValue->filter_link = $this->makeFilterLink(array_merge(
						$filterData, ['current_key' => $filterEntity->slug, 'current_value' => $filterValue->slug]
					));
				}
			}
		}

		foreach ($filter['filters'] as $filterEntity) {
			foreach ($filterEntity->values as $filterValue) {
				$currentKey = $filterEntity->slug;
				$currentValue = $filterValue->slug;
				$filterValue->filter_link = $this->makeFilterLink(array_merge(
					$filterData,
					['current_key' => $currentKey, 'current_value' => $currentValue]
				));
			}
		}

		foreach ($filter['manufacturers'] as $manufacturer) {
			$currentKey = 'brand';
			$currentValue = $manufacturer->slug;
			$manufacturer->filter_link = $this->makeFilterLink(array_merge(
				$filterData,
				['current_key' => $currentKey, 'current_value' => $currentValue]
			));
		}

		foreach ($filter['years'] as $year) {
			$currentKey = 'year';
			$currentValue = $year->value;
			$year->filter_link = $this->makeFilterLink(array_merge(
				$filterData,
				['current_key' => $currentKey, 'current_value' => $currentValue]
			));
		}
		foreach ($filter['sizes'] as $size) {
			$currentKey = 'size';
			$currentValue = $size->slug;
			$size->filter_link = $this->makeFilterLink(array_merge(
				$filterData,
				['current_key' => $currentKey, 'current_value' => $currentValue]
			));
		}

		$filter['reset'] = $clearCurrentUrl;

		return $filter;
	}

	private function makeFilterLink($data)
	{
		$targetLink = $data['clear_current_url'];
		$currentKey = $data['current_key'];
		$currentValue = $data['current_value'];
		$queryString = $data['query_string'];
		$filterParts = $data['filter_parts'];

		if (isset($filterParts[$currentKey]) && in_array($currentValue, $filterParts[$currentKey])) {
			unset($filterParts[$currentKey][array_search($currentValue, $filterParts[$currentKey])]);
		} else {
			if (!isset($filterParts[$currentKey])) {
				$filterParts[$currentKey] = [];
			}
			$filterParts[$currentKey][] = $currentValue;
		}

		$linkParts = [];
		foreach ($filterParts as $valueTitle => $values) {
			if (count($values) > 0) {
				$linkParts[] = $valueTitle . ':' . implode(',', $values);
			}
		}

		if (count($linkParts) > 0) {
			$targetLink = rtrim($targetLink, '/');
			$targetLink .= '/filter=' . implode(';', $linkParts);
		}

		if (!empty($queryString)) {
			$targetLink .= '?' . $queryString;
		}
		return $targetLink;
	}

	public function getUrlWithoutFilter()
	{
		return str_before(url()->current(), '/filter=');
	}

	public function checkDoubleAttributes($filterProducts)
	{
		$filterVariable = $this->filterVariable;

		$filter_attributes_name = array_keys($filterVariable['filter_attributes_grouped']);

		$attributes_filter = Attribute::whereIn('slug', $filter_attributes_name)->pluck('id');

		$separateProdItems = ProductAttribute::where('text', 'LIKE', '%' . ';' . '%')
			->whereIn('attribute_id', $attributes_filter)
			->get();


		if ($separateProdItems) {
			foreach ($separateProdItems as $item) {
				$pieceOfText = explode(';', $item->text);
				foreach ($pieceOfText as $v) {
					$translit_item_text = $this->transliterate($v);

					if (in_array($translit_item_text, $filterVariable['filter_attributes']))
						$filterProducts->push($item);
				}
			}
		}


		return $filterProducts;
	}

	public function transliterate($str)
	{

		$str = trim($str);
		$str = mb_strtolower($str);

		$rus = [
			'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п',
			'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', ' ', ',', '/', '~', "\""
		];

		$lat = [
			'a', 'b', 'v', 'g', 'd', 'e', 'io', 'zhe', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
			'r', 's', 't', 'u', 'f', 'kh', 'ts', 'ch', 'sh', 'shch', 'a', 'y', '', 'e', 'yu', 'ya', '-', '', '', '', '',
		];

		$str_translit = str_replace($rus, $lat, $str);

		return $str_translit;
	}

	private function searchMainCategory($child_category)
	{
		$is_main = Category::find($child_category->parent_id);

		isset($is_main) ? $main_category = $this->searchMainCategory($is_main) : $main_category = $child_category->id;

		return $main_category;
	}

    public function getSortParamFromString($string, $start, $end){
	    if (strpos($string, $end) === false) {
             $string .= $end;
        }
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
}
