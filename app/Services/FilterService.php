<?php

namespace App\Services;

use App\Entities\Attribute;
use App\Entities\AttributeGroup;
use App\Entities\Category;
use App\Entities\Filter;
use App\Entities\Manufacturer;
use App\Entities\Product;
use App\Entities\ProductAttribute;
use App\Entities\Size;
use App\Entities\SkuSpecial;
use App\Entities\StockKeepingUnit;
use App\Entities\Year;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class FilterService
{
    protected $filterVariable;

    protected $currentLanguageId;

    /**
     * FilterService constructor.
     */
    public function __construct()
    {
        $this->filterVariable = config('app.filter_data');
        $this->currentLanguageId = config('current_language_id');
    }


    /**
     * @param Builder $products
     * @return array
     */

    public function getFilterByProducts(Builder $products, $categoryId = false)
    {
        $products = clone $products;
        $currentProducts = $products->get();
        $result = [];
        $productsIds = $products->pluck('id');
        $filteredProductsIds = $this->getFilteredProducts($products)->pluck('id');
        $activeSpecials = SkuSpecial::getActiveSpecials();
        $prices = StockKeepingUnit::leftJoinSub($activeSpecials, 'active_specials', function ($join) {
            $join->on('stock_keeping_units.id', '=', 'active_specials.unit_id');
        })->whereIn('stock_keeping_units.product_id', $productsIds)->select([
            DB::raw('min(stock_keeping_units.price) as min_price'),
            DB::raw('max(stock_keeping_units.price) as max_price'),
            DB::raw('min(active_specials.price) as min_special_price'),
            DB::raw('max(active_specials.price) as max_special_price'),
        ])->where('stock_keeping_units.is_main', '=', 1)->first();
        $result['filteredProductsIds'] = $filteredProductsIds;
        $currentCaregory = Category::find($categoryId);
        switch(explode('/', \Request::path())[0] ){
            case 'manufacturer' :
                $categoryId = Category::whereHas('products', function($query) use($categoryId){
                    $query->where('manufacturer_id', '=', $categoryId)
                        ->where('status', '=', 1);
                })->get()->pluck('id')->toArray();
                break;
            case 'search' :
                $categoryId = Category::whereHas('products', function($query) use ($currentProducts){
                    $query->whereIn('id', $currentProducts->pluck('id')->toArray());
                })->get()->pluck('id')->toArray();
                break;
            case 'velosipedy' :
                $categoryTmp = Category::find($categoryId);
                $categoryId = [
                    (int)$categoryId
                ];
                while($categoryTmp->parent_id !== null){
                    $tmpId =  Category::where('id', $categoryTmp->parent()->first()->id)->first()->id;
                    $categoryId[] = $tmpId;
                    $categoryTmp = Category::find($tmpId);
                }
                break;
            default:
                    $categoryId = [
                        Category::find($categoryId)->id
                    ];
        }
        //filter by price
        $result['price']['min'] = isset($prices->min_special_price) && $prices->min_special_price < $prices->min_price
            ? $prices->min_special_price
            : $prices->min_price;
        $result['price']['max'] = isset($prices->max_special_price) && $prices->max_special_price > $prices->max_price
            ? $prices->max_special_price
            : $prices->max_price;

        //filter by attributes
        $filterProductCounts = DB::table('filter_value_to_product')
            ->groupBy('filter_value_id')
            ->select([
                DB::raw('COUNT(product_id) as product_count'),
                'filter_value_id'
            ])
            ->whereIn('product_id', $productsIds)
            ->get();
        $filterValuesIds = $filterProductCounts->pluck('filter_value_id')->toArray();

        $result['filters'] = Filter::whereHas('values', function ($query) use ($filterValuesIds) {
            $query->whereIn('id', $filterValuesIds);
        })
            ->with(['values' => function ($query) use ($filterValuesIds) {
                $query->whereIn('id', $filterValuesIds);
            },
                'values.localDescription',
                'localDescription',
            ])->orderBy('sort')->get();

        foreach ($result['filters'] as $filter) {
            $filter->name = isset($filter->localDescription) && isset($filter->localDescription->name)
                ? $filter->localDescription->name
                : '';
            foreach ($filter->values as $filterValue) {
                $filterProductCount = $filterProductCounts
                    ->where('filter_value_id', '=', $filterValue->id)
                    ->first();
                $filterValue->products_count = isset($filterProductCount) && isset($filterProductCount->product_count)
                    ? $filterProductCount->product_count
                    : 0;
                $filterValue->name = isset($filterValue->localDescription)
                && isset($filterValue->localDescription->name)
                    ? $filterValue->localDescription->name
                    : '';
            }
        }

        //filter by category group attributes
        $filterProductCategoryCounts = DB::table('category_attributes')
            ->join('product_attributes as pa', 'pa.attribute_id', '=', 'category_attributes.attribute_id')
            ->whereIn('category_attributes.category_id',  $categoryId)
            ->groupBy('category_attributes.attribute_id')
            ->select([
                'category_attributes.attribute_id'
            ])
            ->get();

        $filterAttrValuesIds = $filterProductCategoryCounts->pluck('attribute_id')->toArray();
        $filterCat = DB::table('attributes')
            ->join('product_attributes', 'attributes.id', '=', 'product_attributes.attribute_id')
            ->join('products', 'products.id', '=', 'product_attributes.product_id')
            ->join('product_to_category', 'products.id', '=', 'product_to_category.product_id')
            ->whereIn('product_attributes.attribute_id', $filterAttrValuesIds)
            ->whereIn('product_to_category.category_id', $categoryId)
            ->select('product_attributes.id')
            ->get();

        $result['filters_cat'] = Attribute::select('attributes.*', 'attributes.slug')
            ->whereHas('valuesWithoutCount', function ($query) use ($filterCat, $filterAttrValuesIds) {
                $query->whereIn('attribute_id', $filterAttrValuesIds);
            })
            ->with(['valuesWithoutCount' => function ($query) use ($filterCat) {
                $query->whereIn('id', $filterCat->pluck('id'));
            }])
            ->orderBy('sort_on_filter')->get();
        //dd($result['filters_cat']->where('id', '=', 139)->first()->valuesWithoutCount->groupBy('slug'));
        //dd($result['filters_cat']->where('id', '=', 139)->first()->valuesWithoutCount->where('id', '=', 86791));
        foreach ($result['filters_cat'] as $filter) {

            $filter->name = isset($filter->localDescription) && isset($filter->localDescription->name)
                ? $filter->localDescription->name
                : '';
            foreach ($filter->valuesWithoutCount->unique('slug') as $key_value => $value){
                if(count(explode(';', $value->text)) > 1){

                    $tmpVal = explode(';', $value->text);
                    foreach ($tmpVal as $tmpValKey => $tmpValItem){
                        $tmpVal[$tmpValKey] = trim($tmpValItem);
                    }
                    foreach ($tmpVal as $key => $tmpValItem){
                        if(empty($filter->valuesWithoutCount->where('text', '=', $tmpValItem)->first())){
                            $tmpValObj = clone $value;
                            $tmpValObj->text = $tmpValItem;
                            $tmpValObj->is_multi = 1;
                            $tmpValObj->products_count = 0;
                            $tmpValObj->string_original = $value->slug;
                            $filter->valuesWithoutCount->push($tmpValObj);
                        }
                        $filter->valuesWithoutCount->where('text', '=', $tmpValItem)->first()->is_multi = 1;
                    }
                    $filter->valuesWithoutCount->forget($key_value);
                }
            }
            //$attrTmpProd = $products->get();
            foreach ($filter->valuesWithoutCount->unique('slug') as $key_value => $value){
                $value->products_count = count(ProductAttribute::where('attribute_id', '=', $value->attribute_id)
                    ->where('text', 'LIKE', '%' . $value->text . '%')
                    ->whereIn('product_id', $filteredProductsIds)
                    ->groupBy('product_id')
                    ->get());
                /*if($value->is_multi){
                    $separateAttrTmp = ProductAttribute::where('attribute_id', '=', $value->attribute_id)
                        ->where('slug', 'LIKE', '%' . $value->slug . '%')
                        ->whereIn('product_id', $filteredProductsIds)
                        ->groupBy('product_id')
                        ->get();

                }*/

                if(explode('/', \Request::path())[0] === 'manufacturer' && $value->products_count === 0){
                    $filter->valuesWithoutCount->forget($key_value);
                }
            }
        }
        //filter by manufacturers
        $manufacturersIds = Product::leftJoin('product_to_category', 'products.id', '=', 'product_to_category.product_id')
            ->whereIn('product_to_category.category_id', $categoryId)
            ->select('products.manufacturer_id')
            ->distinct()
            ->get()
            ->pluck('manufacturer_id')
            ->toArray();
        $result['manufacturers'] = Manufacturer::leftJoin('manufacturer_descriptions', function ($join) {
            $languageId = config('current_language_id');
            $join->on('manufacturers.id', '=', 'manufacturer_descriptions.manufacturer_id')
                ->where('manufacturer_descriptions.language_id', '=', $languageId);
        })->select([
            'manufacturers.id',
            'manufacturers.slug',
            'manufacturer_descriptions.name',
        ])
            ->where('manufacturers.status', '=', 1)
            ->whereIn('manufacturers.id', $manufacturersIds)
            ->orderBy('manufacturer_descriptions.name')
            ->get();

        $manufacturerProductCounts = DB::table('products')->groupBy('manufacturer_id')
            ->select([
                DB::raw('COUNT(id) as product_count'),
                'manufacturer_id'
            ])
            ->whereIn('id', $filteredProductsIds)
            ->get();
        foreach ($result['manufacturers'] as $manufacturer) {
            $manufacturerProductCount = $manufacturerProductCounts
                ->where('manufacturer_id', '=', $manufacturer->id)
                ->first();
            $manufacturer->products_count = isset($manufacturerProductCount)
            && isset($manufacturerProductCount->product_count)
                ? $manufacturerProductCount->product_count
                : 0;
        }
        if(isset($currentCaregory->id) && isset(Category::find($currentCaregory->id)->show_manufacture__filter) && explode('/', \Request::path())[0] !== 'manufacturer'){
            $result['show_manufacturers'] = Category::find($currentCaregory->id)->show_manufacture__filter;
        }else{
            $result['show_manufacturers'] = 0;
        }

        if(explode('/', \Request::path())[0] === 'velosipedy'){
            $result['show_years'] = 1;
            $result['show_sizes'] = 1;
        }else{
            $result['show_years'] = 0;
            $result['show_sizes'] = 0;
        }

        $yearIds = StockKeepingUnit::whereHas('product', function($productQuery) use($categoryId){
            $productQuery->leftJoin('product_to_category', function($categoryJoin) use ($categoryId){
                $categoryJoin->on('products.id', '=', 'product_to_category.product_id')
                    ->whereIn('product_to_category.category_id',$categoryId);
            });
        })
            ->where('year_id', '!=', 0)
            ->select('year_id')
            ->distinct()
            ->get()
            ->pluck('year_id')
            ->toArray();
        $result['years'] = Year::whereIn('id', $yearIds)
            ->select([
                'id',
                'value',
                DB::raw('value as name'),
                DB::raw('value as slug')
            ])->get();
        foreach ($result['years'] as $year) {
            $yearProductCount = Product::leftJoin('product_to_category', function($categoryJoin) use ($categoryId){
                $categoryJoin->on('products.id', '=', 'product_to_category.product_id')
                    ->whereIn('product_to_category.category_id', $categoryId);
            })
                ->whereHas('skus', function ($skusQuery) use ($year){
                    $skusQuery->where('year_id', $year->id);
                })
                ->whereIn('products.id', $filteredProductsIds)
                ->get();
            $year->products_count = count($yearProductCount);
        }
        $sizesIds = DB::table('products')
            ->join('product_to_category', 'products.id', '=', 'product_to_category.product_id')
            ->join('stock_keeping_units', 'stock_keeping_units.product_id', '=', 'products.id')
            ->join('sizes', 'sizes.id', '=', 'stock_keeping_units.size_id')
            ->whereIn('product_to_category.category_id', $categoryId)
            ->groupBy('stock_keeping_units.size_id')
            ->select('stock_keeping_units.size_id')
            ->get()
            ->pluck('size_id')
            ->toArray();
        $sizesGroup = Size::whereIn('id', $sizesIds)
            ->select([
                'id',
                'value'
            ])
            ->orderBy('value')
            ->get();
        $groupSizesAttr = [];
        foreach ($sizesGroup as $group){
            $groupSizesAttr[$group->value][$group->id] = $group->id;
        }
        $result['sizes'] = Size::whereIn('id', $sizesGroup->pluck('id')->unique())
            ->select([
                'id',
                'value',
                DB::raw('value as name'),
                DB::raw('value as slug')
            ])
            ->groupBy('value')
            ->orderBy('value')
            ->get();

        foreach ($result['sizes'] as $size) {
            $sizeProductCount = Product::whereHas('skus', function ($skuQuery) use ($groupSizesAttr, $size){
                $skuQuery->whereIn('size_id', $groupSizesAttr[$size->value]);
            })
                ->whereIn('id', $filteredProductsIds)
                ->get();
            $size->products_count = count($sizeProductCount);
            $size->slug = str_replace('"', "", $size->slug);
        }
        return $result;
    }

    public function getFilteredProducts(Builder $products)
    {
        if (empty($this->filterVariable)) {
            return $products;
        }
        $filterVariable = $this->filterVariable;
        $products->where(function($query) use ($filterVariable, $products) {
            if (isset($filterVariable['price'])) {
                if (isset($filterVariable['price']['min'])) {
                    $priceMin = $filterVariable['price']['min'];
                    $query->where(function ($query) use ($priceMin) {
                        $query->where('sku.price', '>=', $priceMin)
                            ->orWhere('active_specials.price', '>=', $priceMin);
                    });
                }

                if (isset($filterVariable['price']['max'])) {
                    $priceMax = $filterVariable['price']['max'];
                    $query->where(function ($query) use ($priceMax) {
                        $query->where('sku.price', '<=', $priceMax)
                            ->orWhere('active_specials.price', '<=', $priceMax);
                    });
                }
            }
            if (isset($filterVariable['filter_attributes'])) {
                $attrUrlParam = explode('/', \Request::path());
                $categoriesIDs = [];
                switch ($attrUrlParam[0]){
                    case 'manufacturer' :
                        $categoriesIDs = DB::table('products')
                            ->join('product_to_category', 'products.id', '=', 'product_to_category.product_id')
                            ->join('categories', 'categories.id', '=', 'product_to_category.category_id')
                            ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
                            ->where('manufacturers.slug', '=', $attrUrlParam[1])
                            ->select('categories.id')
                            ->get()
                            ->pluck('id')
                            ->unique()
                            ->toArray();
                        break;
                    case 'search' :
                        /*$categoriesIDs = Category::whereHas('products', function($query) use ($products){
                            $query->whereIn('id', $products->get()->pluck('id'));
                        })->get()->pluck('id');*/
                        $categoriesIDs = Category::all()->pluck('id');
                        break;
                    default:
                        $categoriesIDs = Category::whereIn('slug', $attrUrlParam)->get()->pluck('id');
                }
                $filterProductAttribute = DB::table('category_attributes')
                    ->join('attributes', 'attributes.id', '=', 'category_attributes.attribute_id')
                    ->whereIn('category_attributes.category_id', $categoriesIDs)
                    ->select([
                        'attributes.id',
                        'attributes.slug'
                    ])
                    ->get();
                $filterProducts = ProductAttribute::where(function($productAttributeQuery) use ($filterVariable, $filterProductAttribute){
                    foreach ($filterVariable['filter_attributes_grouped'] as $key => $filter_attributes){
                        foreach ($filter_attributes as $fil_attr_val) {
                            $productAttributeQuery->orWhere(function($productAttributeQueryInner) use ($filterVariable, $filterProductAttribute, $key, $filter_attributes, $fil_attr_val){
                                $productAttributeQueryInner->where('product_attributes.slug', 'LIKE', $fil_attr_val)
                                    ->where('product_attributes.attribute_id', '=', $filterProductAttribute->where('slug', '=', $key)->first()->id);

                            });
                        }
                    }
                })->get();
                $filterProductsSeparate = null;
                foreach ($filterProducts as $filterProductItem){
                    $separateProdItems = ProductAttribute::where('slug', 'LIKE', '%' . $filterProductItem->slug . '%')->where('attribute_id', '=', $filterProductItem->attribute_id)->get();
                    if($separateProdItems){
                        foreach ($separateProdItems as $separateProdItem){
                            $separateProdItemObj = explode(';', $separateProdItem->text);
                            if(count($separateProdItemObj) > 1){
                               // dd($separateProdItemObj);
                                $filterProducts->push($separateProdItem);
                            }
                        }
                    }
                }
                $productAttrIDs = [];
                foreach ($filterProducts->unique('id')->groupBy('product_id') as $attrGroup){
                    if(count($attrGroup) === count($filterVariable['filter_attributes_grouped'])){
                        foreach ($attrGroup as $attrID){
                            $productAttrIDs[] = $attrID->id;
                        }
                    }
                }
                $query->whereHas('filterAttributes', function ($queryFilterAttributes) use ($productAttrIDs) {
                    $queryFilterAttributes->whereIn('product_attributes.id', $productAttrIDs);
                });
            }
            if (isset($filterVariable['filter_values'])) {
                $query->whereHas('filterValues', function ($query) use ($filterVariable) {
                    $query->whereIn('filter_value_id', $filterVariable['filter_values']);
                });
            }

            if (isset($filterVariable['manufacturers'])) {
                $query->whereIn('manufacturer_id', $filterVariable['manufacturers']);
            }


            if (isset($filterVariable['year'])) {
                $query->whereHas('skus', function ($skuQuery) use ($filterVariable){
                    $skuQuery->whereIn('year_id', $filterVariable['year']);
                });
            }
            if (isset($filterVariable['size'])) {
                $query->whereHas('skus', function ($skuQuery) use ($filterVariable){
                    $skuQuery->whereIn('size_id', $filterVariable['size']);
                });
            }
        });
        return $products;
    }

    public function addLinksToFilter($filter)
    {
        $clearCurrentUrl = $this->getUrlWithoutFilter();
        $filterData = [
            'query_string' => request()->getQueryString(),
            'clear_current_url' => $clearCurrentUrl,
            'filter_parts' => config('app.filter_parts')
        ];

        if (isset($filter['filters_cat'])) {
            foreach ($filter['filters_cat'] as $filterEntity) {
                foreach ($filterEntity->valuesWithoutCount as $filterValue) {
                    $filterValue->filter_link = $this->makeFilterLink(array_merge(
                        $filterData,
                        ['current_key' => $filterEntity->slug, 'current_value' => $filterValue->slug]
                    ), true);
                }
            }
        }

        foreach ($filter['filters'] as $filterEntity) {
            foreach ($filterEntity->values as $filterValue) {
                $currentKey = $filterEntity->slug;
                $currentValue = $filterValue->slug;
                $filterValue->filter_link = $this->makeFilterLink(array_merge(
                    $filterData,
                    ['current_key' => $currentKey, 'current_value' => $currentValue]
                ));
            }
        }

        foreach ($filter['manufacturers'] as $manufacturer) {
            $currentKey = 'brand';
            $currentValue = $manufacturer->slug;
            $manufacturer->filter_link = $this->makeFilterLink(array_merge(
                $filterData,
                ['current_key' => $currentKey, 'current_value' => $currentValue]
            ));
        }

        foreach ($filter['years'] as $year) {
            $currentKey = 'year';
            $currentValue = $year->value;
            $year->filter_link = $this->makeFilterLink(array_merge(
                $filterData,
                ['current_key' => $currentKey, 'current_value' => $currentValue]
            ));
        }
        foreach ($filter['sizes'] as $size) {
            $currentKey = 'size';
            $currentValue = $size->slug;
            $size->filter_link = $this->makeFilterLink(array_merge(
                $filterData,
                ['current_key' => $currentKey, 'current_value' => $currentValue]
            ));
        }

        $filter['reset'] = $clearCurrentUrl;

        return $filter;
    }

    private function makeFilterLink($data)
    {
        $targetLink = $data['clear_current_url'];
        $currentKey = $data['current_key'];
        $currentValue = $data['current_value'];
        $queryString = $data['query_string'];
        $filterParts = $data['filter_parts'];

        if (isset($filterParts[$currentKey]) && in_array($currentValue, $filterParts[$currentKey])) {
            unset($filterParts[$currentKey][array_search($currentValue, $filterParts[$currentKey])]);
        } else {
            if (!isset($filterParts[$currentKey])) {
                $filterParts[$currentKey] = [];
            }
            $filterParts[$currentKey][] = $currentValue;
        }

        $linkParts = [];
        foreach ($filterParts as $valueTitle => $values) {
            if (count($values) > 0) {
                $linkParts[] = $valueTitle . ':' . implode(',', $values);
            }
        }

        if (count($linkParts) > 0) {
            $targetLink = rtrim($targetLink, '/');
            $targetLink .= '/filter=' . implode(';', $linkParts);
        }

        if (!empty($queryString)) {
            $targetLink .= '?' . $queryString;
        }
        return $targetLink;
    }

    public function getUrlWithoutFilter()
    {
        return str_before(url()->current(), '/filter=');
    }
}
