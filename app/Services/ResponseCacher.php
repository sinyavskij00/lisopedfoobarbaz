<?php

namespace App\Services;

use App\Entities\Product;
use App\Traits\CacheTrait;
use App\Jobs\RecacheJob;

/**
 * Class ResponseCacher
 *
 * [id: [url:response_id]]
 * ( response_id - uuid `responses` )
 */
class ResponseCacher
{
    use CacheTrait;

    private const KEY = 'cached_responses';

    private const TIME = 60;

    protected static $cacheableRoutes;

    public static function regiserCacheableRoute(string $method, string $route, array $params)
    {
        /*
         * WARNING: only for routes with 1 / 0 dynamic parameters (for simplicity)
        ex.:
        method: GET
        route: /foo/{bar}
        params: [
          bar: table.column
        ]
        */
        self::$cacheableRoutes[] = [
            'method' => $method,
            'route' => $route,
            'params' => $params,
        ];
    }

    public static function getCacheableRoutes()
    {
        if (!is_null(self::$cacheableRoutes)) {
         $routes = [];
         foreach (self::$cacheableRoutes as $route) {
             $routes[] = $route['route'];
         }
         return $routes;
        } else {
         return [];
        }
    }

    public static function cacheResponse($response)
    {
        $cache = \Cache::get(self::KEY);
        $currentActualURL = request()->method() . ' ' . \Request::fullUrl();
        $user_id = \Auth::id();

        // [ 197 => [ '/foo/bar' => '.......' ] ]
        $uuid = \Illuminate\Support\Str::uuid()->toString();
        \DB::table('responses')->insert([ 'uuid' => $uuid, 'response' => $response ]);
        $cache[$user_id] = [$currentActualURL => $uuid];

        self::store($cache);
    }

    public static function getResponseIfExists()
    {
        $key = self::KEY;
        // [ [ user_id => [ 'GET /foo/bar' => '..........' ] ]  , ...]

        $responses = [];

        // get authenticated user or null
        $userid = \Auth::id();

        // get cached responses: [ 'user_id' => [ url => response ] ]
        $cached = \Cache::get($key);
        $all_responses = $cached ? $cached : [];


        if ($userid) {
            // if user authenticated -> get all responses for this user from cache
            foreach ($all_responses as $user => $resp) {
                if ($user == $userid) {
                    $responses[$user] = $resp;
                }
            }
        } else {
            // if it is guest -> get all responses for guests
            foreach($all_responses as $user => $resp) {
                if (!$user) {
                    $responses[] = $resp;
                }
            }
        }
        // if no responses found -> return nothing (this response would be cached)
        if ((null == $responses) || (count($responses) == 0)) {
            return null;
        }
        $actualResponse = null;
        $currentActualURL = request()->method() . ' ' . \Request::fullUrl();

        if ($userid) {
            // [ user => [ url => response ] ]
            foreach ($responses as $usr => $r) {

                // [ url -> response ]
                if ( (key($r) == $currentActualURL) && ($usr == \Auth::id()) ) {
                    $actualResponse = \DB::table('responses')->select('response')->where('uuid', $r[key($r)])->get()->toArray()[0];
                    break;
                }
            }
        } else {

            foreach ($responses as $r) {
                if (  (key($r) == $currentActualURL) ) {
                    $actualResponse = \DB::table('responses')->select('response')->where('uuid', $r[key($r)])->get()->toArray()[0];
                    break;
                }
            }

        }
        if (!$actualResponse) {
            return null;
        }
        if (strpos($actualResponse->response, '<!DOCTYPE html>') !== false) {
            return strstr($actualResponse->response, '<!DOCTYPE html>');
        }
        return $actualResponse;
    }

    public static function recacheAllResponses()
    {
        // TODO: http method
        self::clearResponses();
        \DB::table('responses')->truncate();





        /*$cacheableRoutes = self::$cacheableRoutes;

        if (0 == count($cacheableRoutes)) {
            return;
        }
        $routeSlugs = [];

        // get all params and build with routes

        foreach ($cacheableRoutes as $route) {
            if (count($route['params']) == 1) {
                foreach ($route['params'] as $param => $v) {
                    $data = explode('.', $v);
                    $table = $data[0];
                    $column = $data[1];
                    // ex.: [ '/foo/{bar}' => [ bar1, bar2, bar3 ] ], ...
                    $routeSlugs[$param] =  [$route['route'], \DB::table($table)->select($column)->get()->toArray()];
                }
            } else if (count($route['params']) == 0) {
                $routeSlugs[] = [$route['route']];
            }
        }

        $readyToCall = [];
        foreach ($routeSlugs as $alias => $urlAndVals) {
            # bar => ['/url/url/url', [ 'fof', 'sos' ] ]


            if (count($urlAndVals) == 0) {
                continue;
            }

            if (count($urlAndVals) == 1) {
               $readyToCall[] = $urlAndVals[0];
            }

            if (count($urlAndVals) == 2) {
               foreach($urlAndVals[1] as $val) {
                    // here you can bind method in braces ....
                    $readyToCall[] = preg_replace("/\{.*\}/", reset($val), $urlAndVals[0]);
                }
            }


        }*/

// calls ... (no need to cache here, it will cache automaticly as a guest)

    }

    /**
     * @param $data
     *
     *  [ 197 => [ '/foo/bar' => '.......' ] ]
     */
    public static function store($data)
    {
        if (is_array($data)) {
            if (null != \Cache::get(self::KEY)) {
                $data = array_merge(\Cache::get(self::KEY), $data);
            }
        }
        \Cache::put(self::KEY, $data, self::TIME);
    }

    public static function clearResponses()
    {
        \Cache::forget(self::KEY);
        \DB::table('responses')->truncate();
    }
}

