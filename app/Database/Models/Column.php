<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 25.09.19
 * Time: 11:51
 */

namespace App\Database\Models;

/**
 * Class Column
 */
class Column
{
    /**
     * @var string
     */
    private $tableName;

    /**
     * @var string
     */
    private $columnName;

    /**
     * @var string
     */
    private $columnType;

    /**
     * @var bool
     */
    private $isUnique;

    /**
     * @var bool
     */
    private $isFillable;

    /**
     * @var mixed
     */
    private $defaultValue;

    /**
     * @var bool
     */
    public $isDefaultValueSetted;

    /**
     * @param string $name
     */
    public function setTableName(string $name): void
    {
        $this->tableName = $name;
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * @param $name
     */
    public function setColumnName($name): void
    {
        $this->columnName = $name;
    }

    /**
     * @return string
     */
    public function getColumnName(): string
    {
        return $this->columnName;
    }

    /**
     * @param string $type
     */
    public function setColumnType(string $type): void
    {
        $this->columnType = $type;
    }

    /**
     * @return string
     */
    public function getColumnType()
    {
        return $this->columnType;
    }

    /**
     * @param bool $unique
     */
    public function setUnique(bool $unique): void
    {
       $this->isUnique = $unique;
    }

    /**
     * @return bool
     */
    public function getUnique(): bool
    {
        return $this->isUnique;
    }

    /**
     * @param bool $fillable
     */
    public function setFillable(bool $fillable): void
    {
       $this->isFillable = $fillable;
    }

    /**
     * @return bool
     */
    public function getFillable(): bool
    {
        return $this->isFillable;
    }

    /**
     * @param mixed $value
     */
    public function setDefaultValue($value)
    {
        $this->defaultValue = $value;
        $this->isDefaultValueSetted = true;
    }

    /**
     * @return mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }
}