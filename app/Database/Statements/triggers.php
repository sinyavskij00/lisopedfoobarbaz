<?php

/**
 *  DEPRECATED
 */

// These triggers needed to clear responses cache on each change for specific table,
// to keep data up to date if something changes, for example - by admin in admin panel.

/*
products
product_to_category
category_attributes
attributes
product_descriptions
share_take_products
categories
filter_values
filter_value_to_product
product_attributes
manufacturers
product_related
stock_keeping_units
product_reviews
product_rewards
product_videos
manufacturer_descriptions
sku_specials
landing_pages
filters
filter_value_descriptions
filter_values
filter_value_to_product
 */
return "
use lspd_db;
set global sql_mode='';
DROP TRIGGER IF EXISTS `insert_products_table`;
DROP TRIGGER IF EXISTS `update_products_table`;
DROP TRIGGER IF EXISTS `delete_products_table`;
DROP TRIGGER IF EXISTS `insert_product_to_category_table`;
DROP TRIGGER IF EXISTS `update_product_to_category_table`;
DROP TRIGGER IF EXISTS `delete_product_to_category_table`;
DROP TRIGGER IF EXISTS `insert_category_attributes_table`;
DROP TRIGGER IF EXISTS `update_category_attributes_table`;
DROP TRIGGER IF EXISTS `delete_category_attributes_table`;
DROP TRIGGER IF EXISTS `insert_attributes_table`;
DROP TRIGGER IF EXISTS `update_attributes_table`;
DROP TRIGGER IF EXISTS `delete_attributes_table`;
DROP TRIGGER IF EXISTS `insert_product_descriptions_table`;
DROP TRIGGER IF EXISTS `update_product_descriptions_table`;
DROP TRIGGER IF EXISTS `delete_product_descriptions_table`;
DROP TRIGGER IF EXISTS `insert_share_take_products_table`;
DROP TRIGGER IF EXISTS `update_share_take_products_table`;
DROP TRIGGER IF EXISTS `delete_share_take_products_table`;
DROP TRIGGER IF EXISTS `insert_categories_table`;
DROP TRIGGER IF EXISTS `update_categories_table`;
DROP TRIGGER IF EXISTS `delete_categories_table`;
DROP TRIGGER IF EXISTS `insert_filter_values_table`;
DROP TRIGGER IF EXISTS `update_filter_values_table`;
DROP TRIGGER IF EXISTS `delete_filter_values_table`;
DROP TRIGGER IF EXISTS `insert_filter_value_to_product_table`;
DROP TRIGGER IF EXISTS `update_filter_value_to_product_table`;
DROP TRIGGER IF EXISTS `delete_filter_value_to_product_table`;
DROP TRIGGER IF EXISTS `insert_product_attributes_table`;
DROP TRIGGER IF EXISTS `update_product_attributes_table`;
DROP TRIGGER IF EXISTS `delete_product_attributes_table`;
DROP TRIGGER IF EXISTS `insert_manufacturers_table`;
DROP TRIGGER IF EXISTS `update_manufacturers_table`;
DROP TRIGGER IF EXISTS `delete_manufacturers_table`;
DROP TRIGGER IF EXISTS `insert_product_related_table`;
DROP TRIGGER IF EXISTS `update_product_related_table`;
DROP TRIGGER IF EXISTS `delete_product_related_table`;
DROP TRIGGER IF EXISTS `insert_stock_keeping_units_table`;
DROP TRIGGER IF EXISTS `update_stock_keeping_units_table`;
DROP TRIGGER IF EXISTS `delete_stock_keeping_units_table`;
DROP TRIGGER IF EXISTS `insert_product_reviews_table`;
DROP TRIGGER IF EXISTS `update_product_reviews_table`;
DROP TRIGGER IF EXISTS `delete_product_reviews_table`;
DROP TRIGGER IF EXISTS `insert_product_rewards_table`;
DROP TRIGGER IF EXISTS `update_product_rewards_table`;
DROP TRIGGER IF EXISTS `delete_product_rewards_table`;
DROP TRIGGER IF EXISTS `insert_product_videos_table`;
DROP TRIGGER IF EXISTS `update_product_videos_table`;
DROP TRIGGER IF EXISTS `delete_product_videos_table`;
DROP TRIGGER IF EXISTS `insert_manufacturer_descriptions_table`;
DROP TRIGGER IF EXISTS `update_manufacturer_descriptions_table`;
DROP TRIGGER IF EXISTS `delete_manufacturer_descriptions_table`;
DROP TRIGGER IF EXISTS `insert_sku_specials_table`;
DROP TRIGGER IF EXISTS `update_sku_specials_table`;
DROP TRIGGER IF EXISTS `delete_sku_specials_table`;
DROP TRIGGER IF EXISTS `insert_landing_pages_table`;
DROP TRIGGER IF EXISTS `update_landing_pages_table`;
DROP TRIGGER IF EXISTS `delete_landing_pages_table`;
DROP TRIGGER IF EXISTS `insert_filters_table`;
DROP TRIGGER IF EXISTS `update_filters_table`;
DROP TRIGGER IF EXISTS `delete_filters_table`;
DROP TRIGGER IF EXISTS `insert_filter_value_descriptions_table`;
DROP TRIGGER IF EXISTS `update_filter_value_descriptions_table`;
DROP TRIGGER IF EXISTS `delete_filter_value_descriptions_table`;
DROP TRIGGER IF EXISTS `insert_filter_values_table`;
DROP TRIGGER IF EXISTS `update_filter_values_table`;
DROP TRIGGER IF EXISTS `delete_filter_values_table`;
DROP TRIGGER IF EXISTS `insert_filter_value_to_product_table`;
DROP TRIGGER IF EXISTS `update_filter_value_to_product_table`;
DROP TRIGGER IF EXISTS `delete_filter_value_to_product_table`;
delimiter //
create trigger insert_products_table after insert on products
for each row
  begin
    system touch foobarbaztest.txt
  end//
delimiter ;
delimiter //
create trigger update_products_table after update on products
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_products_table after delete on products
for each row
  begin
    create table FooBarBaz;
  end//
delimiter ;
delimiter //
create trigger insert_product_to_category_table after insert on product_to_category
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_product_to_category_table after update on product_to_category
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_product_to_category_table after delete on product_to_category
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_category_attributes_table after insert on category_attributes
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_category_attributes_table after update on category_attributes
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_category_attributes_table after delete on category_attributes
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_attributes_table after insert on attributes
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_attributes_table after update on attributes
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_attributes_table after delete on attributes
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_product_descriptions_table after insert on product_descriptions
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_product_descriptions_table after update on product_descriptions
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_product_descriptions_table after delete on product_descriptions
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_share_take_products_table after insert on share_take_products
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_share_take_products_table after update on share_take_products
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_share_take_products_table after delete on share_take_products
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_categories_table after insert on categories
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_categories_table after update on categories
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_categories_table after delete on categories
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_filter_values_table after insert on filter_values
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_filter_values_table after update on filter_values
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_filter_values_table after delete on filter_values
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_filter_value_to_product_table after insert on filter_value_to_product
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_filter_value_to_product_table after update on filter_value_to_product
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_filter_value_to_product_table after delete on filter_value_to_product
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_product_attributes_table after insert on product_attributes
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_product_attributes_table after update on product_attributes
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_product_attributes_table after delete on product_attributes
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_manufacturers_table after insert on manufacturers
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_manufacturers_table after update on manufacturers
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_manufacturers_table after delete on manufacturers
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_product_related_table after insert on product_related
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_product_related_table after update on product_related
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_product_related_table after delete on product_related
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_stock_keeping_units_table after insert on stock_keeping_units
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_stock_keeping_units_table after update on stock_keeping_units
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_stock_keeping_units_table after delete on stock_keeping_units
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_product_reviews_table after insert on product_reviews
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_product_reviews_table after update on product_reviews
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_product_reviews_table after delete on product_reviews
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_product_rewards_table after insert on product_rewards
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_product_rewards_table after update on product_rewards
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_product_rewards_table after delete on product_rewards
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_product_videos_table after insert on product_videos
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_product_videos_table after update on product_videos
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_product_videos_table after delete on product_videos
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_manufacturer_descriptions_table after insert on manufacturer_descriptions
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_manufacturer_descriptions_table after update on manufacturer_descriptions
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_manufacturer_descriptions_table after delete on manufacturer_descriptions
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_sku_specials_table after insert on sku_specials
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_sku_specials_table after update on sku_specials
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_sku_specials_table after delete on sku_specials
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_landing_pages_table after insert on landing_pages
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_landing_pages_table after update on landing_pages
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_landing_pages_table after delete on landing_pages
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_filters_table after insert on filters
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_filters_table after update on filters
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_filters_table after delete on filters
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_filter_value_descriptions_table after insert on filter_value_descriptions
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_filter_value_descriptions_table after update on filter_value_descriptions
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_filter_value_descriptions_table after delete on filter_value_descriptions
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_filter_values_table after insert on filter_values
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_filter_values_table after update on filter_values
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_filter_values_table after delete on filter_values
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger insert_filter_value_to_product_table after insert on filter_value_to_product
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger update_filter_value_to_product_table after update on filter_value_to_product
for each row
  begin
    show triggers;
  end//
delimiter ;
delimiter //
create trigger delete_filter_value_to_product_table after delete on filter_value_to_product
for each row
  begin
    show triggers;
  end//
delimiter ;
";