<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 25.09.19
 * Time: 11:48
 */

namespace App\Database;

use App\Database\Models\Column;
use Illuminate\Support\Facades\Schema;

class CustomMigrate
{
    public function addColumnIfNotExist(Column $info, $model)
    {
        $table_name = $info->getTableName();
        $column_name = $info->getColumnName();
        $type = $info->getColumnType();
        if (!Schema::hasColumn($table_name, $column_name)) {
            Schema::table($table_name, function ($table) use ($info, $type, $column_name) {
                if ($info->getUnique()) {
                    $table->$type($column_name)->unique();
                }
                if ($info->isDefaultValueSetted) {
                    $table->$type($column_name)->default($info->getDefaultValue());
                }
            });
            if (($info->getFillable()) && (method_exists($model, 'setFillable')) && (method_exists($model, 'getFillable'))) {
                $fillables = $model->getFillable();
                $model->setFillable(
                    array_push($fillables, $column_name)
                );
            }
        }
    }
}