<?php

namespace App\Entities;

/**
 * App\Entities\BillingAccount
 *
 * @property int $id
 * @property string $name
 * @property string $employer_number
 * @property string $bank_number
 * @property string $checking_account
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingAccount whereBankNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingAccount whereCheckingAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingAccount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingAccount whereEmployerNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingAccount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingAccount whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingAccount whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingAccount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingAccount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingAccount query()
 */
class BillingAccount extends BaseModel
{
    protected $fillable = ['name', 'employer_number', 'bank_number', 'checking_account'];
}
