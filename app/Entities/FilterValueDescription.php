<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class FilterValueDescription extends Model
{
    protected $fillable = ['filter_value_id', 'language_id', 'name', 'filter_name'];

    public $timestamps = false;

    public function filterValue()
    {
        return $this->belongsTo(FilterValue::class, 'filter_value_id', 'id');
    }
}
