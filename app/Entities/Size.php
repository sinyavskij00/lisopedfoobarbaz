<?php

namespace App\Entities;

use Illuminate\Support\Facades\Config;

/**
 * App\Entities\Size
 *
 * @property int $id
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\SizeDescription[] $descriptions
 * @property-read mixed $comment
 * @property-read \App\Entities\SizeDescription $localDescription
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Size whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Size whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Size whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Size whereValue($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Size newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Size newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Size query()
 */
class Size extends BaseModel
{
    protected $fillable = ['value'];

    public function descriptions()
    {
        return $this->hasMany(SizeDescription::class, 'size_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(SizeDescription::class, 'size_id', 'id')
            ->where('language_id', '=', Config::get('current_language_id'))
            ->withDefault(['comment' => '']);
    }

    public function getCommentAttribute()
    {
        return $this->localDescription->comment;
    }
}
