<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\ProductReward
 *
 * @property int $id
 * @property int $customer_group_id
 * @property int $product_id
 * @property int $rewards
 * @property-read \App\Entities\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReward whereCustomerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReward whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReward whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReward whereRewards($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReward newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReward newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReward query()
 */
class ProductReward extends Model
{
    public $timestamps = false;

    protected $fillable = ['customer_group_id', 'product_id', 'rewards'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
