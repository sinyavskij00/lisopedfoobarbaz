<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class HeaderBannerDescription extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'banner_id',
        'language_id',
        'text'
    ];
}
