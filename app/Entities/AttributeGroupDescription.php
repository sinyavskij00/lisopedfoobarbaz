<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\AttributeGroupDescription
 *
 * @property int $id
 * @property int $language_id
 * @property int $attribute_group_id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeGroupDescription whereAttributeGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeGroupDescription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeGroupDescription whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeGroupDescription whereName($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeGroupDescription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeGroupDescription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeGroupDescription query()
 */
class AttributeGroupDescription extends Model
{
    protected $fillable = ['language_id', 'attribute_group_id', 'name'];

    public $timestamps = false;
}
