<?php

namespace App\Entities;

class ProductToCategory extends BaseModel
{
    public $timestamps = false;

    protected $table = 'product_to_category';

    protected $fillable = [
        'id',
        'product_id',
        'category_id'
    ];

    public static function getCategoriesByProductId()
    {
        $existCategories = [];
        $productCategories = ProductToCategory::select('product_id', 'category_id')->get();
        foreach ($productCategories as $cat) {
            $existCategories[$cat['product_id']][] = $cat['category_id'];
        }

        return $existCategories;
    }
}
