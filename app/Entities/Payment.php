<?php

namespace App\Entities;

/**
 * App\Entities\Payment
 *
 * @property int $id
 * @property string $code
 * @property int $confirm_status_id
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Payment whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Payment whereConfirmStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Payment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Payment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Payment whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Payment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Payment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Payment query()
 */
class Payment extends BaseModel
{
    protected $fillable = ['status', 'confirm_status_id', 'code'];
}
