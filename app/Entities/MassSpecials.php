<?php

namespace App\Entities;

/**
 * App\Entities\MassSpecials
 *
 * @property int $id
 * @property mixed|null $manufacturers
 * @property mixed|null $categories
 * @property int $customer_group_id
 * @property int $priority
 * @property float $percent
 * @property string $date_start
 * @property string $date_end
 * @property-read mixed $created_at
 * @property-read mixed $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\MassSpecials newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\MassSpecials newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\MassSpecials query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\MassSpecials whereCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\MassSpecials whereCustomerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\MassSpecials whereDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\MassSpecials whereDateStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\MassSpecials whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\MassSpecials whereManufacturers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\MassSpecials wherePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\MassSpecials wherePriority($value)
 * @mixin \Eloquent
 */
class MassSpecials extends BaseModel
{

    public $timestamps = false;
    protected $fillable = ['id', 'manufactures', 'categories', 'customer_group_id', 'priority', 'percent'];

    public function getDateStartAttribute($value)
    {
        return $this->getDate($value);
    }

    public function getDateEndAttribute($value)
    {
        return $this->getDate($value);
    }
}
