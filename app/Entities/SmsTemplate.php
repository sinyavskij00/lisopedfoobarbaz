<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SmsTemplate extends Model
{
    protected $fillable = [
        'title',
        'text'
    ];
}
