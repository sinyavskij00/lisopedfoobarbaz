<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\OrderSku
 *
 * @property int $id
 * @property int $order_id
 * @property \App\Entities\StockKeepingUnit $sku
 * @property int|null $sku_id
 * @property string $name
 * @property string $model
 * @property string $size
 * @property string $color
 * @property string $year
 * @property int $quantity
 * @property float $price
 * @property float $vendor_price
 * @property float $total
 * @property float $vendor_total
 * @property float $reward
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Entities\Order $order
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereReward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereSkuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereVendorPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereVendorTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereYear($value)
 * @mixin \Eloquent
 * @property string|null $vendor_sku
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereVendorSku($value)
 * @property int $shipped_status
 * @property string $ttn
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereShippedStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderSku whereTtn($value)
 */
class OrderSku extends Model
{
    protected $fillable = [
        'sku_id',
        'sku',
        'vendor_sku',
        'order_id',
        'color',
        'size',
        'year',
        'name',
        'model',
        'quantity',
        'price',
        'vendor_price',
        'total',
        'vendor_total',
        'shipped_status',
        'status',
        'reward',
        'ttn',
        'prepayment',
    ];

    public function sku()
    {
        return $this->hasOne(StockKeepingUnit::class, 'id', 'sku_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function orderSkuStatus()
    {
        return $this->hasOne(OrderSkuStatus::class, 'id', 'status');
    }

    public function keepingUnit()
    {
        return $this->hasOne(StockKeepingUnit::class, 'id', 'sku_id');
    }
}
