<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\TestimonialImage
 *
 * @property int $id
 * @property string $image
 * @property int $testimonial_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\TestimonialImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\TestimonialImage whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\TestimonialImage whereTestimonialId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\TestimonialImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\TestimonialImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\TestimonialImage query()
 */
class TestimonialImage extends Model
{
    public $timestamps = false;

    protected $fillable = ['image', 'testimonial_id'];
}
