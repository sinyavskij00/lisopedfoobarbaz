<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\SkuDiscount
 *
 * @property int $id
 * @property int $customer_group_id
 * @property int $unit_id
 * @property int $quantity
 * @property int $priority
 * @property float $price
 * @property string $date_start
 * @property string $date_end
 * @property-read \App\Entities\StockKeepingUnit $sku
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuDiscount whereCustomerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuDiscount whereDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuDiscount whereDateStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuDiscount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuDiscount wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuDiscount wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuDiscount whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuDiscount whereUnitId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuDiscount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuDiscount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuDiscount query()
 */
class SkuDiscount extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'quantity',
        'customer_group_id',
        'unit_id',
        'priority',
        'price',
        'date_start',
        'date_end'
    ];

    public function sku()
    {
        return $this->belongsTo(StockKeepingUnit::class, 'unit_id', 'id');
    }
}
