<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class CategoryGroupAttribute extends Model
{
    protected $fillable = [
        'category_id',
        'attribute_group_id'
    ];
}
