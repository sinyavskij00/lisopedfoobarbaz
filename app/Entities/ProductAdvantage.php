<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductAdvantage extends Model
{
    protected $fillable = [
        'link',
        'icon',
        'status',
    ];

    public function descriptions()
    {
        return $this->hasMany(ProductAdvantageDescription::class, 'product_advantage_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(ProductAdvantageDescription::class, 'product_advantage_id', 'id')
            ->where('language_id', '=', config('current_language_id'))
            ->withDefault(['title' => '', 'text' => '']);
    }
}
