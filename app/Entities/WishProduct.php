<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\WishProduct
 *
 * @property int $id
 * @property int $customer_id
 * @property int $product_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Entities\User $customer
 * @property-read \App\Entities\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\WishProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\WishProduct whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\WishProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\WishProduct whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\WishProduct whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\WishProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\WishProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\WishProduct query()
 */
class WishProduct extends Model
{
    protected $fillable = ['product_id', 'customer_id'];

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id', 'id');
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}
