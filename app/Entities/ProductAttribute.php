<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'attribute_id',
        'product_id',
        'language_id',
        'text',
        'slug'
    ];

    public function setTextAttribute($text)
    {
        $this->attributes['text'] = $text;
        $this->attributes['slug'] = str_slug($text);
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function attribute()
    {
        return $this->belongsTo(Attribute::class, 'attribute_id', 'id');
    }
}
