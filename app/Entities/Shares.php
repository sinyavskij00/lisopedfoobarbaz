<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Shares extends Model
{
    public function special()
    {
        return $this->belongsTo(Special::class, 'special_id', 'id');
    }

    public function shareProducts()
    {
        return $this->hasMany(ShareProducts::class, 'share_id', 'id');
    }

    public function shareTakeProducts()
    {
        return $this->hasMany(ShareTakeProducts::class, 'share_id', 'id');
    }
}
