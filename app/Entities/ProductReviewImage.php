<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\ProductReviewImage
 *
 * @property int $id
 * @property int $product_review_id
 * @property string $image
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReviewImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReviewImage whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReviewImage whereProductReviewId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReviewImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReviewImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReviewImage query()
 */
class ProductReviewImage extends Model
{
    public $timestamps = false;

    protected $fillable = ['image', 'product_review_id'];
}
