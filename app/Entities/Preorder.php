<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Preorder extends Model
{
    protected $fillable = [
        'name',
        'email',
        'telephone',
        'notify',
        'sku_id',
        'comment'
    ];

    public static function checkProductAvailability(){
        $preorders = Preorder::where('notify', 0)->get();
        if(count($preorders) > 0){
            foreach ($preorders as $preorder){
                $sku = StockKeepingUnit::where('id', $preorder->sku_id)->first();
                $productName = ProductDescription::where('product_id', $sku->product_id)->first()->name;
                $productSlug = Product::where('id', $sku->product_id)->first()->slug;
                if($sku && $sku->quantity > 0){
                    Mail::send('mail.notification.product_available', ['link' => '/product/' . $productSlug, 'productName' => $productName], function ($message) use ($preorder) {
                        $message->to($preorder->email)->subject('Product Available');
                    });
                    $preorder->notify = 1;
                    $preorder->save();
                }
            }
        }
    }
}
