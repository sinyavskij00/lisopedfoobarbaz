<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ShareTakeProducts extends Model
{
    public function shares()
    {
        return $this->belongsTo(Shares::class, 'share_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function getSharedTakeProductsById($id){
        $shareProducts = ShareTakeProducts::where('share_id', $id)->get();
        $result = [];
        foreach ($shareProducts as $shareProduct){
            $product = Product::where('id', '=', $shareProduct->product_id)->with(['localDescription'])->first();
            $result[]= $product->id;
        }
        return $result;
    }

}
