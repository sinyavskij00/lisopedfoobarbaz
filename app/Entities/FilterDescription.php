<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class FilterDescription extends Model
{
    protected $fillable = ['name', 'language_id', 'filter_id'];

    public $timestamps = false;

    public function filter()
    {
        return $this->belongsTo(Filter::class, 'filter_id', 'id');
    }
}
