<?php

namespace App\Entities;

use Illuminate\Support\Facades\Config;

/**
 * App\Entities\QuestionGroup
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\QuestionGroupDescription[] $descriptions
 * @property-read \App\Entities\QuestionGroupDescription $localDescription
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Question[] $questions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionGroup query()
 */
class QuestionGroup extends BaseModel
{
    protected $fillable = [];

    public function descriptions()
    {
        return $this->hasMany(QuestionGroupDescription::class, 'question_group_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(QuestionGroupDescription::class, 'question_group_id', 'id')
            ->where('language_id', '=', Config::get('current_language_id'))
            ->withDefault(['name' => '']);
    }

    public function questions()
    {
        return $this->hasMany(Question::class, 'question_group_id', 'id');
    }
}
