<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\CategoryDescription
 *
 * @property int $id
 * @property int $category_id
 * @property int $language_id
 * @property string $name
 * @property string|null $description
 * @property string|null $meta_h1
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $meta_keywords
 * @property-read \App\Entities\Category $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CategoryDescription whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CategoryDescription whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CategoryDescription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CategoryDescription whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CategoryDescription whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CategoryDescription whereMetaH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CategoryDescription whereMetaKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CategoryDescription whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CategoryDescription whereName($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CategoryDescription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CategoryDescription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CategoryDescription query()
 */
class CategoryDescription extends Model
{
    protected $fillable = [
        'category_id',
        'language_id',
        'name',
        'description',
        'meta_h1',
        'meta_description',
        'meta_keywords',
        'meta_title'
    ];

    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public static function getCategoriesByName()
    {
        return CategoryDescription::select(
            'category_id',
            \DB::raw('LOWER(name) as name')
        )
            ->where(
                'language_id',
                config()->get('current_language_id')
            )
            ->get()
            ->pluck('category_id', 'name');
    }
}
