<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\BannerDescription
 *
 * @property int $id
 * @property int $banner_id
 * @property int $language_id
 * @property string $text
 * @property string $link
 * @property string $link_title
 * @property string $link_subtitle
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BannerDescription whereBannerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BannerDescription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BannerDescription whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BannerDescription whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BannerDescription whereLinkSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BannerDescription whereLinkTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BannerDescription whereText($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BannerDescription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BannerDescription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BannerDescription query()
 */
class BannerDescription extends Model
{
    public $timestamps = false;

    protected $fillable = ['text', 'link', 'link_title', 'link_subtitle', 'banner_id', 'language_id'];
}
