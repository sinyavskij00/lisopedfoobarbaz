<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    protected $fillable = ['sort', 'is_display', 'slug', 'filter_url'];


    /**
     * @param array $columns
     */
    public function setFillable(array $columns)
    {
        $this->fillable = $columns;
    }

    /**
     * @return array
     */
    public function getFillable()
    {
        return $this->fillable;
    }

    public function descriptions()
    {
        return $this->hasMany(FilterDescription::class, 'filter_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(FilterDescription::class, 'filter_id', 'id')
            ->where('language_id', '=', config('current_language_id'))
            ->withDefault(['name' => '']);
    }
    public function description()
    {
        return $this->hasOne(FilterDescription::class, 'filter_id', 'id')
            ->where('language_id', '=', config('current_language_id'));
    }

    public function values()
    {
        return $this->hasMany(FilterValue::class, 'filter_id', 'id');
    }
}
