<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\OrderStatusDescription
 *
 * @property int $id
 * @property int $order_status_id
 * @property int $language_id
 * @property string $name
 * @property-read \App\Entities\OrderStatus $orderStatus
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderStatusDescription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderStatusDescription whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderStatusDescription whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderStatusDescription whereOrderStatusId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderStatusDescription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderStatusDescription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderStatusDescription query()
 */
class OrderStatusDescription extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'order_status_id', 'language_id'];

    public function orderStatus()
    {
        return $this->belongsTo(OrderStatus::class, 'order_status_id', 'id');
    }
}
