<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\UnitImage
 *
 * @property int $id
 * @property int $unit_id
 * @property string $image
 * @property-read \App\Entities\StockKeepingUnit $unit
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\UnitImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\UnitImage whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\UnitImage whereUnitId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\UnitImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\UnitImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\UnitImage query()
 */
class UnitImage extends Model
{
    public $timestamps = false;

    protected $fillable = ['unit_id', 'image'];

    public function unit()
    {
        return $this->belongsTo(StockKeepingUnit::class, 'unit_id', 'id');
    }
}
