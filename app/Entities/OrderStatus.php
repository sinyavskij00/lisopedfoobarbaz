<?php

namespace App\Entities;

use Illuminate\Support\Facades\Config;

/**
 * App\Entities\OrderStatus
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\OrderStatusDescription[] $descriptions
 * @property-read \App\Entities\OrderStatusDescription $localDescription
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderStatus whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $color
 * @property int $is_new
 * @property int $is_finished
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderStatus whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderStatus whereIsFinished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderStatus whereIsNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderStatus query()
 */
class OrderStatus extends BaseModel
{
    protected $fillable = [
        'color',
        'is_new',
        'is_finished',
        'export_1c'
    ];

    public function descriptions()
    {
        return $this->hasMany(OrderStatusDescription::class, 'order_status_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(OrderStatusDescription::class, 'order_status_id', 'id')
            ->where('language_id', '=', Config::get('current_language_id'))
            ->withDefault(['name' => '']);
    }

    public static function getClosedStatusId()
    {
        return OrderStatus::where('is_finished', '=', 1)->first()->id;
    }
}
