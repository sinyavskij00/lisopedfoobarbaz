<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\AttributeDescription
 *
 * @property int $id
 * @property int $attribute_id
 * @property int $language_id
 * @property string $name
 * @property-read \App\Entities\Attribute $attribute
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeDescription whereAttributeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeDescription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeDescription whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeDescription whereName($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeDescription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeDescription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeDescription query()
 */
class AttributeDescription extends Model
{
    protected $fillable = [
        'name',
        'language_id',
        'attribute_id'
    ];

    public $timestamps = false;

    public function attribute()
    {
        return $this->belongsTo(Attribute::class, 'attribute_id', 'id');
    }
}
