<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\SpecialDescription
 *
 * @property int $id
 * @property int $special_id
 * @property int $language_id
 * @property string $title
 * @property string|null $text
 * @property-read \App\Entities\Special $special
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SpecialDescription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SpecialDescription whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SpecialDescription whereSpecialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SpecialDescription whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SpecialDescription whereTitle($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SpecialDescription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SpecialDescription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SpecialDescription query()
 */
class SpecialDescription extends Model
{
    public $timestamps = false;

    protected $fillable = ['language_id', 'special_id', 'title', 'text'];

    public function special()
    {
        return $this->belongsTo(Special::class, 'special_id', 'id');
    }
}
