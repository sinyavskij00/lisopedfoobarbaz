<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\Currency
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property float $value
 * @property string $symbol_left
 * @property string $symbol_right
 * @property int $decimal_place
 * @property int $is_default
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Currency whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Currency whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Currency whereDecimalPlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Currency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Currency whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Currency whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Currency whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Currency whereSymbolLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Currency whereSymbolRight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Currency whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Currency whereValue($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Currency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Currency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Currency query()
 */
class Currency extends Model
{
    protected $fillable = [
        'name',
        'code',
        'value',
        'symbol_left',
        'symbol_right',
        'decimal_place',
        'is_default',
        'status'
    ];
}
