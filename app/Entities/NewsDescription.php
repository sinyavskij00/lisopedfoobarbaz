<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\NewsDescription
 *
 * @property int $id
 * @property int $news_id
 * @property int $language_id
 * @property string $title
 * @property string|null $text
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $meta_keywords
 * @property-read \App\Entities\News $news
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsDescription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsDescription whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsDescription whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsDescription whereMetaKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsDescription whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsDescription whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsDescription whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsDescription whereTitle($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsDescription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsDescription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsDescription query()
 */
class NewsDescription extends Model
{
    protected $fillable = [ 'language_id', 'news_id', 'title', 'text', 'meta_title', 'meta_description', 'meta_keywords'];

    public $timestamps = false;

    public function news()
    {
        return $this->belongsTo(News::class, 'news_id', 'id');
    }
}
