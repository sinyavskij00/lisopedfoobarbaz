<?php

namespace App\Entities;

/**
 * App\Entities\Year
 *
 * @property int $id
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Year whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Year whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Year whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Year whereValue($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Year newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Year newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Year query()
 */
class Year extends BaseModel
{
    protected $fillable = ['value'];
}
