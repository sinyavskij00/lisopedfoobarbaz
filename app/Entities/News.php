<?php

namespace App\Entities;

/**
 * App\Entities\News
 *
 * @property int $id
 * @property string $slug
 * @property string|null $image
 * @property int $views
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Kalnoy\Nestedset\Collection|\App\Entities\NewsReview[] $activeReviews
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\NewsDescription[] $descriptions
 * @property-read \App\Entities\NewsDescription $localDescription
 * @property-read \Kalnoy\Nestedset\Collection|\App\Entities\NewsReview[] $reviews
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\News whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\News whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\News whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\News whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\News whereViews($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\News newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\News newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\News query()
 */
class News extends BaseModel
{
    protected $table = 'newses';

    protected $fillable = ['slug', 'image', 'parent_id'];

    public function descriptions()
    {
        return $this->hasMany(NewsDescription::class, 'news_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(NewsDescription::class, 'news_id', 'id')
            ->withDefault([
                'title' => '',
                'text' => '',
                'meta_title' => '',
                'meta_description' => '',
                'meta_keywords' => ''
            ]);
    }

    public function reviews()
    {
        return $this->hasMany(NewsReview::class, 'news_id', 'id');
    }

    public function activeReviews()
    {
        return $this->hasMany(NewsReview::class, 'news_id', 'id')
            ->where('status', '=', 1);
    }
}
