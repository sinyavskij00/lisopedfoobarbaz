<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductAdvantageDescription extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'product_advantage_id',
        'language_id',
        'title',
        'text',
    ];
}
