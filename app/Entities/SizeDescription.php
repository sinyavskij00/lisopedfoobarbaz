<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\SizeDescription
 *
 * @property int $id
 * @property int $size_id
 * @property int $language_id
 * @property string $comment
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeDescription whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeDescription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeDescription whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeDescription whereSizeId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeDescription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeDescription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeDescription query()
 */
class SizeDescription extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'comment',
        'size_id',
        'language_id'
    ];

    public static function getSizesByName()
    {
        return Size::select(
            'sizes.id',
            \DB::raw('LOWER(CONCAT(sizes.value, sd.comment)) as value_comment')
        )->leftJoin('size_descriptions as sd', function($join) {
            $join->on('sd.size_id', '=', 'sizes.id')
                ->where(
                    'sd.language_id',
                    config()->get('current_language_id')
                );
        })
            ->get()
            ->pluck('id', 'value_comment');
    }
}
