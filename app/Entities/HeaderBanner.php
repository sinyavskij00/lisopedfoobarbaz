<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class HeaderBanner extends Model
{
    protected $fillable = [
        'background_color',
        'text_color',
        'status'
    ];

    public function descriptions()
    {
        return $this->hasMany(HeaderBannerDescription::class, 'banner_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(HeaderBannerDescription::class, 'banner_id', 'id')
            ->where('language_id', '=', config('current_language_id'))
            ->withDefault(['text' => '']);
    }

    public function getTextAttribute()
    {
        return $this->localDescription->text;
    }
}
