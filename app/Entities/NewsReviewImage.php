<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\NewsReviewImage
 *
 * @property int $id
 * @property int $news_review_id
 * @property string $image
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReviewImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReviewImage whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReviewImage whereNewsReviewId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReviewImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReviewImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReviewImage query()
 */
class NewsReviewImage extends Model
{
    public $timestamps = false;

    protected $fillable = ['image', 'news_review_id'];
}
