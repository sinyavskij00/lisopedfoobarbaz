<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\QuestionGroupDescription
 *
 * @property int $id
 * @property int $question_group_id
 * @property int $language_id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionGroupDescription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionGroupDescription whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionGroupDescription whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionGroupDescription whereQuestionGroupId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionGroupDescription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionGroupDescription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionGroupDescription query()
 */
class QuestionGroupDescription extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'question_group_id', 'language_id'];
}
