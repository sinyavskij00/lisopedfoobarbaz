<?php

namespace App\Traits;

use Carbon\Carbon;

trait DumpTrait {
 
    public function uploadLargeDump($paramdriver, $paramhost, $paramport, $paramsocket, $paramusername, $parampassword, $paramdatabase)
    {
     function importSqlFile($pdo, $sqlFile, $tablePrefix = null, $InFilePath = null, $paramdriver, $paramhost, $paramport, $paramsocket, $paramusername, $parampassword, $paramdatabase)
{
	try {
		
		// Enable LOAD LOCAL INFILE
		$pdo->setAttribute(\PDO::MYSQL_ATTR_LOCAL_INFILE, true);
		
		$errorDetect = false;
		
		// Temporary variable, used to store current query
		$tmpLine = '';
		
		// Read in entire file
		$lines = file($sqlFile);
		
		// Loop through each line
		foreach ($lines as $line) {
			// Skip it if it's a comment
			if (substr($line, 0, 2) == '--' || trim($line) == '') {
				continue;
			}
			
			// Read & replace prefix
			$line = str_replace(['<<prefix>>', '<<InFilePath>>'], [$tablePrefix, $InFilePath], $line);
			
			// Add this line to the current segment
			$tmpLine .= $line;
			
			// If it has a semicolon at the end, it's the end of the query
			if (substr(trim($line), -1, 1) == ';') {
				try {
					// Perform the Query
					$pdo->exec($tmpLine);
				} catch (\PDOException $e) {
					echo "<br><pre>Error performing Query: '<strong>" . $tmpLine . "</strong>': " . $e->getMessage() . "</pre>\n";
					$errorDetect = true;
				}
				
				// Reset temp variable to empty
				$tmpLine = '';
			}
		}
		
		// Check if error is detected
		if ($errorDetect) {
			return false;
		}
		
	} catch (\Exception $e) {
		echo "<br><pre>Exception => " . $e->getMessage() . "</pre>\n";
		return false;
	}
	
	return true;
}

$driver = $paramdriver;
$host = $paramhost;
$port = $paramport;
$socket = $paramsocket; // Optional
$username = $paramusername;
$password = $parampassword;
$database = $paramdatabase;
$options = [
	\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_OBJ,
	\PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
	\PDO::ATTR_EMULATE_PREPARES   => true,
	\PDO::ATTR_CURSOR             => \PDO::CURSOR_FWDONLY,
];
try {
	// Get the Connexion's DSN
	if (empty($socket)) {
		$dsn = $driver . ':host=' . $host . ';port=' . $port . ';dbname=' . $database . ';charset=utf8';
	} else {
		$dsn = $driver . ':unix_socket=' . $socket . ';dbname=' . $database . ';charset=utf8';
	}
	// Connect to the Database Server
	$pdo = new \PDO($dsn, $username, $password, $options);
	
} catch (\PDOException $e) {
	die("Can't connect to the database server. ERROR: " . $e->getMessage());
} catch (\Exception $e) {
	die("The database connection failed. ERROR: " . $e->getMessage());
}
$filePath = app_path() . '/dumplisoped03.09.sql';
// Import the SQL file
$res = importSqlFile($pdo, $filePath, null, null, $paramdriver, $paramhost, $paramport, $paramsocket, $paramusername, $parampassword, $paramdatabase);
if ($res === false) {
        die('ERROR');
}

      
    }
    
}
