<?php


namespace App\Traits;

trait CacheTrait
{

    public static function renderIfNeeded($response)
    {
        if (method_exists($response, 'render')) {
            $response = $response->render();
        }
        return $response;
    }

}