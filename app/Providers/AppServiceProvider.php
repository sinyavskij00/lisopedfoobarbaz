<?php

namespace App\Providers;

use App\Entities\StockKeepingUnit;
use App\Library\Checkout\Cart;
use App\Library\Checkout\Payment\BankCartPayment;
use App\Library\Checkout\Payment\LiqPayPayment;
use App\Library\Checkout\Payment\ReceiptPayment;
use App\Library\Checkout\Shipping\NewPostCourierShipping;
use App\Library\Checkout\Shipping\NewPostOfficeShipping;
use App\Library\Compare\CompareDriver;
use App\Library\Compare\CompareSessionDriver;
use App\Library\Settings;
use App\Library\Wishlist\WishlistDbDriver;
use App\Library\Wishlist\WishlistDriver;
use App\Library\Wishlist\WishlistSessionDriver;
use App\Observers\StockKeepingUnitObserver;
use App\Services\CatalogService;
use App\Services\WishListService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Database\CustomMigrate;
use App\Services\ResponseCacher;
use App\Traits\DumpTrait;

class AppServiceProvider extends ServiceProvider
{

    use DumpTrait;

    /**
     * Bootstrap any application services.
     *
     * @param CatalogService $catalogService
     * @param Settings $settings
     * @return void
     */
    public function boot(CatalogService $catalogService, Settings $settings)
    {
        // IMPORT LARGE DUMP IF NEEDED
//        $paramdriver = env('DB_CONNECTION');
//        $paramhost = env('DB_HOST');
//        $paramport = env('DB_PORT');
//        $paramsocket = '';
//        $paramusername = env('DB_USERNAME');
//        $parampassword = env('DB_PASSWORD');
//        $paramdatabase = env('lspd_db');
//        $this->uploadLargeDump($paramdriver, $paramhost, $paramport, $paramsocket, $paramusername, $parampassword, $paramdatabase);
//        \DB::unprepared('create table responses( uuid int, response longtext );');
//        \DB::unprepared('alter table filters add column filter_url varchar(191) unique');
//        \DB::unprepared('alter table products add column is_render int default 1');
//        \DB::unprepared('alter table filters add column h varchar(191)');
//        \DB::unprepared('alter table filters add column title varchar(191)');
//        \DB::unprepared('alter table filters add column name varchar(191)');
//        \DB::unprepared('alter table filters add column links_cloud varchar(191)');
//        \DB::unprepared('alter table filters add column markup_description text null');

        app('view')->prependNamespace('admin', resource_path('views/admin'));

        Schema::defaultStringLength(191);
        StockKeepingUnit::observe(StockKeepingUnitObserver::class);
        $siteSettings = $settings->getByCode('site_settings');
        if (!empty($siteSettings['admin_email'])) {
            config()->set('app.admin_email', $siteSettings['admin_email']);
        }
        $this->app->singleton(Settings::class, function ($app) use ($settings) {
            return $settings;
        });

        $this->app->singleton(Cart::class, function ($app) use ($catalogService) {
            return new Cart($catalogService);
        });

        $this->app->tag([
            NewPostOfficeShipping::class,
            NewPostCourierShipping::class
        ], ['shipping_methods']);

        $this->app->tag([
            LiqPayPayment::class,
            BankCartPayment::class,
            ReceiptPayment::class,
        ], ['payment_methods']);

        //wishlist
        $this->app->bind(WishlistDriver::class, function () {
            return app()->make(Auth::check() ? WishlistDbDriver::class : WishlistSessionDriver::class);
        });

        $this->app->singleton(WishListService::class, function ($app) {
            $driver = $app->make(WishlistDriver::class);
            return new WishListService($driver);
        });

        $this->app->bind(CompareDriver::class, function () {
            return app()->make(CompareSessionDriver::class);
        });

        $this->app->when(\LiqPay::class)->needs('$public_key')->give(config('app.liqpay.public_key'));
        $this->app->when(\LiqPay::class)->needs('$private_key')->give(config('app.liqpay.private_key'));

        $this->app->bind(CustomMigrate::class, function ($app) {
            return new CustomMigrate();
        });

        $this->app->bind(ResponseCacher::class, function ($app) {
            return new ResponseCacher();
        });

        // Eloquent ORM Observers ...
        \App\Entities\StockKeepingUnit::observe(\App\Observers\StockKeepingUnitObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
