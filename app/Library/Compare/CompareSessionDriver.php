<?php

namespace App\Library\Compare;

use App\Entities\Product;

class CompareSessionDriver implements CompareDriver
{

    private $sessionKey = 'compare';

    protected $compareList;

    public function add(Product $product)
    {
        $compareList = session($this->sessionKey, []);
        $compareList[] = $product->id;
        $compareList = array_values(array_unique($compareList));
        session([$this->sessionKey => $compareList]);
        return true;
    }

    public function delete($productId)
    {
        $compareList = session($this->sessionKey, []);
        if (($key = array_search($productId, $compareList)) !== false) {
            unset($compareList[$key]);
        }
        $compareList = array_values(array_unique($compareList));
        session([$this->sessionKey => $compareList]);
        return true;
    }

    public function getProducts()
    {
        return isset($this->compareList) ? $this->compareList : collect(session($this->sessionKey, []));
    }

    public function isCompared(Product $product)
    {
        return $this->getProducts()->contains($product->id);
    }

    public function getQuantity()
    {
        return $this->getProducts()->count();
    }
}
