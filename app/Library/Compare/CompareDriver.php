<?php

namespace App\Library\Compare;

use App\Entities\Product;

interface CompareDriver
{
    public function add(Product $product);

    public function delete($productId);

    public function getProducts();

    public function isCompared(Product $product);

    public function getQuantity();
}
