<?php

namespace App\Library\Checkout;

use App\Entities\CartItem;
use App\Services\CatalogService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Cart
{
    protected $items;
    protected $customerId = null;
    protected $sessionId;
    protected $catalogService;
    protected $customerGroup;

    public function __construct(CatalogService $catalogService)
    {
        $this->sessionId = session()->getId();
        $this->customerId = Auth::id();
        $this->customerGroup = Auth::check() ? Auth::user()->customerGroup : config('current_customer_group');
        $this->catalogService = $catalogService;
        $this->updateItems($this->sessionId, $this->sessionId, $this->customerId);
    }

    public function add($skuId, $quantity = 1)
    {
        $cartItem = CartItem::where('sku_id', '=', $skuId)
            ->where('session_id', '=', $this->sessionId)
            ->first();
        if ($cartItem) {
            $this->changeItem($cartItem, $quantity);
        } else {
            CartItem::create([
                'customer_id' => $this->customerId,
                'session_id' => $this->sessionId,
                'sku_id' => $skuId,
                'quantity' => $quantity,
            ]);
        }
        $this->updateItems($this->sessionId, $this->sessionId, $this->customerId);
        return true;
    }

    public function delete($cartItemId)
    {
        try {
            CartItem::find($cartItemId)->delete();
        } catch (\Exception $e) {
            Log::alert('cart item was not deleted');
        }
        $this->updateItems($this->sessionId, $this->sessionId, $this->customerId);
    }

    public function changeItem(CartItem $cartItem, $quantity)
    {
        $resultQuantity = intval($quantity) + $cartItem->quantity;
        if ($resultQuantity > 0) {
            $cartItem->quantity = $resultQuantity;
            $cartItem->save();
            $this->updateItems($this->sessionId, $this->sessionId, $this->customerId);
        }
        return true;
    }

    public function items()
    {
        return $this->items;
    }

    public function subTotal()
    {
        $subTotal = 0;
        foreach ($this->items as $item) {
            $subTotal += $this->catalogService->getSkuPrice($item->sku, $item->quantity, $this->customerGroup, true);
        }

        return $subTotal;
    }

    public function updateItems($oldSessionId, $newSessionId, $customerId = null)
    {
        $query = CartItem::where('session_id', '=', $oldSessionId);

        if ($customerId) {
            $query->orWhere('customer_id', '=', $this->customerId);
        }

        $query->with([
            'sku.size',
            'sku.year',
            'sku.color.localDescription',
            'sku.activeSpecials',
            'sku.product.localDescription',
            'sku.images',
            'sku.activeDiscounts'
        ]);

        $items = $query->get();
        $this->items = $items;
        CartItem::whereIn('id', $this->items->pluck('id'))->update([
            'session_id' => $newSessionId,
            'customer_id' => $customerId
        ]);
    }

    public function deleteAll()
    {
        try {
            CartItem::whereIn('id', $this->items()->pluck('id'))->delete();
        } catch (\Exception $e) {
            Log::alert('cart delete all items failed');
        }
        $this->updateItems($this->sessionId, $this->sessionId, $this->customerId);
    }

    public function getWeight()
    {
        $weight = 0;
        foreach ($this->items() as $item) {
            if (isset($item->sku) && isset($item->sku->product) && is_numeric($item->sku->product->weight)) {
                $weight += $item->sku->product->weight * $item->quantity;
            }
        }
        return $weight;
    }
}
