<?php

namespace App\Library\Checkout\Shipping;

use App\Entities\Shipping;
use App\Http\Controllers\Front\Checkout\Shipping\ShippingMethod;
use App\Library\Checkout\Cart;
use App\Library\Checkout\NewPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewPostOfficeShipping implements ShippingMethod
{
    protected $cityName;

    protected $cityRef;

    protected $storeName;

    protected $storeRef;

    protected $currentCityStores;

    protected $code;

    protected $name;

    protected $cost;

    protected $status;

    protected $cartSubTotal;

    public function __construct(Request $request, NewPost $newPost, Cart $cart)
    {
        $this->cartSubTotal = $cart->subTotal();

        $customer = Auth::user();
        $this->cityRef = $request->input(
            'city_ref',
            isset($customer) && isset($customer->city_ref) ? $customer->city_ref : ''
        );

        $city = $newPost->getCityByRef($this->cityRef);
        $this->cityName = isset($city) ? $city->DescriptionRu : null;

        if (isset($this->cityRef)) {
            $this->currentCityStores = $newPost->getStores($this->cityRef);
        }

        $this->storeRef = $request->input(
            'store_ref',
            isset($customer) && isset($customer->store_ref) ? $customer->store_ref : ''
        );

        $store = $newPost->getStoreByRef($this->storeRef);
        $this->storeName = isset($store) ? $store->DescriptionRu : null;

        $this->code = 'office';
        $this->name = __('checkout.library.shipping.office.name');

        $shipping = Shipping::where('code', '=', $this->code)->first();

        $this->status = isset($shipping) && isset($shipping->status) ? intval($shipping->status) : 0;
        $this->cost = isset($shipping) && isset($shipping->price) ? floatval($shipping->price) : 0;
    }

    public function show()
    {
        return view('front.checkout.shipping.new_post_office', [
            'cityRef' => $this->cityRef,
            'storeRef' => $this->storeRef,
            'cityName' => $this->cityName,
            'storeName' => $this->storeName,
            'currentCityStores' => $this->currentCityStores
        ]);
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAddress()
    {
        return $this->cityName . ', ' . $this->storeName;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function getCostTitle()
    {
        return $this->cartSubTotal >= 2000
            ? __('shipping.new_post_office.free')
            : __('shipping.new_post_office.according_to_tariffs');
    }

    public function getStatus()
    {
        return $this->status;
    }
}
