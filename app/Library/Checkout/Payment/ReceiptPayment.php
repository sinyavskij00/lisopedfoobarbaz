<?php

namespace App\Library\Checkout\Payment;

use App\Entities\Order;
use App\Entities\Payment;
use App\Library\Settings;
use App\Services\OrderService;

class ReceiptPayment implements PaymentMethod
{
    protected $code;
    protected $name;

    public $confirmStatus;
    public $status;

    public $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;

        $this->code = 'receipt';
        $this->name = __('checkout.library.payment.receipt.name');

        $payment = Payment::where('code', '=', $this->code)->first();

        $this->confirmStatus = isset($payment) && isset($payment->confirm_status_id)
            ? intval($payment->confirm_status_id)
            : 0;
        $this->status = isset($payment) && isset($payment->status) ? intval($payment->status) : 0;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getName()
    {
        return $this->name;
    }

    public function confirm(Order $order)
    {
        $this->orderService->addOrderHistory($order, $this->confirmStatus, 0);
        return view('front.checkout.payment.receipt')->render();
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getConfirmStatus()
    {
        return $this->confirmStatus;
    }
}
