<?php

namespace App\Library\Wishlist;

use App\Entities\Product;
use Illuminate\Support\Collection;


interface WishlistDriver {
    public function add(Product $product) : bool;

    public function delete(Product $product) : bool;

    public function getWishlist() : Collection;
}