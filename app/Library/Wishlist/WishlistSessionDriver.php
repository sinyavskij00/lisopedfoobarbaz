<?php

namespace App\Library\Wishlist;

use App\Entities\Product;
use Illuminate\Support\Collection;

class WishlistSessionDriver implements WishlistDriver
{
    private $sessionKey = 'wishList';

    public function add(Product $product) : bool
    {
        $wishList = session($this->sessionKey, []);
        $wishList[] = $product->id;
        $wishList = array_values(array_unique($wishList));
        session([$this->sessionKey => $wishList]);
        return true;
    }

    public function delete(Product $product) : bool
    {
        $wishList = session($this->sessionKey, []);
        if (($key = array_search($product->id, $wishList)) !== false) {
            unset($wishList[$key]);
        }
        $wishList = array_values(array_unique($wishList));
        session([$this->sessionKey => $wishList]);
        return true;
    }

    public function getWishlist() : Collection
    {
        return collect(session($this->sessionKey, []));
    }
}