<?php

namespace App\Library\Modules\Front;

use App\Entities\Product;
use App\Entities\StockKeepingUnit;
use Illuminate\Http\Request;

class PickupBicycleModule
{
    public function show()
    {
        if (Product::count() === 0 || StockKeepingUnit::count() === 0) {
            return '';
        }

      /*  $priceMin = intval(StockKeepingUnit::min('price'));
        $priceMax = intval(StockKeepingUnit::max('price'));

        if ($priceMin === $priceMax) {
            return '';
        }

        $step = ($priceMax - $priceMin) / 10;
        $step = ($step < 3000) ? $step : 3000;

        $priceRange = [];
        foreach (range(0, $priceMax, $step) as $item) {
            $priceRange[] = intval($item);
        }

        $priceOptions = [];
        for ($i = 0; $i < count($priceRange); $i++) {
            if (isset($priceRange[$i + 1])) {
                $priceOptions[] = $priceRange[$i] . ' - ' . $priceRange[$i + 1];
            }
        }*/

        $priceOptions = [
            '0-3000',
            '3000-6000',
            '6000-9000',
            '9000-12000',
            '12000-16000',
            '16000-22000',
            '22000-30000',
            '30000-48000',
            '48000-70000',
            '70000-200000',
        ];

        $genderOptions = [
            1 => __('modules.pickup_bicycle.gender_options.man'),
            2 => __('modules.pickup_bicycle.gender_options.woman'),
            3 => __('modules.pickup_bicycle.gender_options.child'),
        ];

        $traceTypeOptions = [
            1 => __('modules.pickup_bicycle.trace_type.asphalt'),
            2 => __('modules.pickup_bicycle.trace_type.asphalt_and_flat_soil'),
            3 => __('modules.pickup_bicycle.trace_type.bumpy_ground_descents'),
            4 => __('modules.pickup_bicycle.trace_type.anywhere'),
        ];

        return view('front.modules.pickup_bicycle', [
            'traceTypeOptions' => $traceTypeOptions,
            'genderOptions' => $genderOptions,
            'priceOptions' => $priceOptions
        ])->render();
    }

    public function filter(Request $request)
    {
        $priceRange = $request->input('price_range');
        $priceRange = explode('-', $priceRange);

        $priceMin = $priceRange[0];
        $priceMax = $priceRange[1];

        $traceType = $request->input('trace_type');
        $weight = $request->input('weight');
        $height = $request->input('height');
        $gender = $request->input('gender');
        $query = Product::where('min_height', '<=', $height)
            ->where('max_height', '>=', $height);
        $query->where('min_weight', '<=', $weight)->where('max_weight', '>=', $weight);
        $query->where('gender', 'LIKE', '%' . $gender . '%');
        $query->where('trace_type', 'LIKE', '%' . $traceType . '%');
        $query->where('status', '=', 1);

        $query->whereHas('mainSku', function ($subQuery) use ($priceMin, $priceMax) {
            $subQuery->where('price', '>=', $priceMin)
                ->where('price', '<=', $priceMax)
                ->where('quantity', '>', 0);
        });
        return $query;
    }
}
