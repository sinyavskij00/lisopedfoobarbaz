<?php

namespace App\Library\Modules\Front;

use App\Entities\Category;

class CategoryModule
{
    public function show()
    {
        $categories = Category::where('parent_id', '=', null)
            ->take(5)
            ->with(['localDescription'])
            ->get();

        if ($categories->count() == 0) {
            return '';
        }

        $firstCategory = $categories->shift();

        return view('front.modules.categories', [
            'categories' => $categories,
            'firstCategory' => $firstCategory
        ])->render();
    }
}