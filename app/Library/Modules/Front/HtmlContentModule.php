<?php

namespace App\Library\Modules\Front;

use App\Library\Settings;

class HtmlContentModule
{
    protected $moduleSettings;

    public function __construct(Settings $settings)
    {
        $this->moduleSettings = $settings->getByCode('home_text');
    }

    public function show()
    {
        $languageId = config('current_language_id');
        $text = isset($this->moduleSettings)
        && isset($this->moduleSettings['descriptions'])
        && isset($this->moduleSettings['descriptions'][$languageId])
        && isset($this->moduleSettings['descriptions'][$languageId]['text'])
            ? $this->moduleSettings['descriptions'][$languageId]['text']
            : null;

        $status = isset($this->moduleSettings) && isset($this->moduleSettings['status'])
            ? $this->moduleSettings['status'] : 0;


        if ($status != 1) {
            return '';
        }

        return view('front.modules.html_content', [
            'text' => $text
        ])->render();
    }
}