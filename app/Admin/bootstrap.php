<?php

/**
 * Laravel-admin - admin builder based on Laravel.
 * @author z-song <https://github.com/z-song>
 *
 * Bootstraper for Admin.
 *
 * Here you can remove builtin form field:
 * Encore\Admin\Form::forget(['map', 'editor']);
 *
 * Or extend custom form field:
 * Encore\Admin\Form::extend('php', PHPEditor::class);
 *
 * Or require js and css assets:
 * Admin::css('/packages/prettydocs/css/styles.css');
 * Admin::js('/packages/prettydocs/js/main.js');
 *
 */

use Encore\Admin\Form;
use App\Admin\Extensions\Form\CKEditor;
use App\Admin\Extensions\Form\Elfinder;
use App\Admin\Extensions\Column\ExpandRow;
use Encore\Admin\Grid\Column;

Encore\Admin\Form::forget(['map', 'editor']);

\Encore\Admin\Facades\Admin::css(['admin_assets/custom-style.css']);

Form::extend('ckeditor', CKEditor::class);
Form::extend('elfinder', Elfinder::class);
Column::extend('expand', ExpandRow::class);
