<?php

namespace App\Admin\Controllers;

use App\Entities\Language;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class LanguageController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Языки')
            ->description('Все языки')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр языка')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование')
            ->description('Редактирование языка')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание')
            ->description('Создание языка')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Language);

        $grid->id('Id')->sortable();
        $grid->title('Название')->sortable();
        $grid->column('image', 'Изображение')->image(null, 70)->sortable();
        $grid->slug('Языковая приставка')->sortable();
        $grid->column('is_default', 'По умолчанию')->display(function ($isDefault) {
            return isset($isDefault) && $isDefault === 1 ? 'Да' : 'Нет';
        })->sortable();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Language::findOrFail($id));

        $show->id('Id');
        $show->title('Название');
        $show->image('Изображение')->image();
        $show->slug('Языковая приставка');
        $show->is_default('По умолчанию')->as(function ($isDefault) {
            return isset($isDefault) && $isDefault === 1 ? 'Да' : 'Нет';
        });
        $show->created_at('Создано');
        $show->updated_at('Обновлено');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Language);

        $form->text('title', 'Название');
        $form->elfinder('image', 'Изображение');
        $form->text('slug', 'Языковая приставка')->help('Используется для формирования URL сайта');
        $form->switch('is_default', 'Группа поумолчанию')->states([
            'on'  => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
        ])->help('Если язык является главным, то его приставка опускается при формировании URL сайта.');

        $form->saved(function (Form $form) {
            $language = $form->model();
            if ($language->is_default === 1) {
                Language::where('id', '!=', $language->id)->update(['is_default' => 0]);
            }
        });

        return $form;
    }
}
