<?php

namespace App\Admin\Controllers;

use App\Entities\Language;
use App\Entities\Special;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class SpecialController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Акции')
            ->description('Все акции')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр акции')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактировать акцию')
            ->description('Редактировать акцию')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создать акцию')
            ->description('Создать акцию')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Special);

        $grid->id('Id')->sortable();
        $grid->column('image', 'Изображение')->image(null, 100)->sortable();
        $grid->column('status', 'Статус')->display(function ($status) {
            return isset($status) && $status === 1 ? 'Включено' : 'Отключено';
        })->sortable();

        $grid->date_start('Дата начала')->sortable();
        $grid->date_end('Дата конца')->sortable();
        $grid->created_at('Создано')->sortable();
        $grid->updated_at('Обновлено')->sortable();

        $grid->filter(function (Grid\Filter $filter) {
            $filter->equal('status', 'Статус')->select([
                0 => 'Отключено',
                1 => 'Включено',
            ]);
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Special::findOrFail($id));

        $show->id('Id');
        $show->image('Изображение')->image();
        $show->status('Статус')->as(function ($status) {
            return isset($status) && $status === 1 ? 'Включено' : 'Отключено';
        });
        $show->slug('SEO-урл');
        $show->date_start('Дата начала');
        $show->date_end('Дата конца');
        $show->created_at('Создано');
        $show->updated_at('Обновлено');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Special);

        $form->tab('Основное', function ($form) {
            $form->elfinder('image', 'Изображение');
            $form->switch('status', 'Статус')->states([
                'on'  => ['value' => 1, 'text' => 'Включено', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => 'Выключено', 'color' => 'danger'],
            ]);
            $form->text('slug', 'SEO-урл');
            $form->date('date_start', 'Дата начала акции');
            $form->date('date_end', 'Дата конца акции');
        })->tab('Описание', function ($form) {
            $form->hasMany('descriptions', 'Описание', function (Form\NestedForm $form) {
                $form->select('language_id', 'Язык')->options(Language::all()->pluck('title', 'id'));
                $form->text('title', 'Название');
                $form->ckeditor('text', 'Текст');
            });
        });

        return $form;
    }
}
