<?php

namespace App\Admin\Controllers;

use App\Entities\Category;
use App\Entities\CustomerGroup;
use App\Entities\Manufacturer;
use App\Entities\Product;
use App\Entities\SiteSetting;
use App\Entities\SkuSpecial;
use App\Entities\StockKeepingUnit;
use App\Http\Controllers\Controller;
use App\Library\Settings;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Form;
use Illuminate\Http\Request;


class CarouselModuleController extends Controller
{
    public function index(Content $content, Settings $settings)
    {
        $moduleSettings = $settings->getByCode('home_carousel');
        return $content
            ->header('Модуль карусели на главной')
            ->description('Модуль карусели на главной')
            ->row(function (Row $row) use ($moduleSettings) {

                $form = new Form();

                $specialProducts = isset($moduleSettings['special_products']) ? $moduleSettings['special_products'] : [];
                $hitProducts = isset($moduleSettings['hit_products']) ? $moduleSettings['hit_products'] : [];
                $newProducts = isset($moduleSettings['new_products']) ? $moduleSettings['new_products'] : [];
                $form->listbox('special_products', 'Акционные товары')->options(Product::with('localDescription')->whereHas('skus', function ($skuQuery){
                    $skuQuery->whereHas('specials', function ($specialQuery){

                    });
                })->get()->pluck('localDescription.name', 'id'))->value($specialProducts);
                $form->listbox('hit_products', 'Хиты')->options(Product::with('localDescription')->get()->where('is_hit', '1')->pluck('localDescription.name', 'id'))->value($hitProducts);
                $form->listbox('new_products', 'Новые')->options(Product::with('localDescription')->get()->where('is_new', '1')->pluck('localDescription.name', 'id'))->value($newProducts);

                $form->action('carousel-module');
                $form->disablePjax();
                $row->column(12, $form);
            });
    }

    public function update(Request $request)
    {
        SiteSetting::updateByCode('home_carousel', $request->only(['special_products', 'hit_products', 'new_products']));
        return redirect()->back();
    }
}
