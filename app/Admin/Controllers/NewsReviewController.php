<?php

namespace App\Admin\Controllers;

use App\Entities\News;
use App\Entities\NewsReview;
use App\Http\Controllers\Controller;
use App\Mail\Information\ReviewAccepted;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Mail;

class NewsReviewController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Отзывы о новостях')
            ->description('Отзывы о новостях')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр отзыва о новости')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактировать отзыв')
            ->description('Редактировать отзыв о новости')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создать отзыв')
            ->description('Создать отзыв о новости')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new NewsReview);

        $grid->id('Id')->sortable();
        $grid->column('news_id', 'Новость')->display(function ($newsId) {
            if (isset($newsId)) {
                $news = News::find($newsId);
                if (isset($news) && isset($news->localDescrition)) {
                    return $news->localDescrition->title;
                }
            }
            return '---';
        })->sortable();
        $grid->name('Имя')->sortable();
        $grid->email('Email')->sortable();
        $grid->column('text', 'Текст')->display(function ($text) {
            if (isset($text)) {
                return str_limit($text, 100);
            }
            return '';
        })->sortable();
        $grid->column('status', 'Статус')->display(function ($default) {
            return isset($default) && $default === 1 ? 'Одобрен' : 'Неодобрен';
        })->sortable();
        $grid->created_at('Создано')->sortable();
        $grid->updated_at('Обновлено')->sortable();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(NewsReview::findOrFail($id));

        $show->id('Id');
        $show->news_id('Новость')->as(function ($newsId) {
            if (isset($newsId)) {
                $news = News::find($newsId);
                if (isset($news) && isset($news->localDescrition)) {
                    return $news->localDescrition->title;
                }
            }
            return '---';
        });
        $show->name('Имя');
        $show->email('Email');
        $show->text('Текст');
        $show->status('Статус')->as(function ($default) {
            return isset($default) && $default === 1 ? 'Одобрен' : 'Неодобрен';
        });
        $show->created_at('Создано');
        $show->updated_at('Обновлено');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new NewsReview);

        $form->tab('Основное', function ($form) {
            $newses = News::with('localDescription')->get();
            $newsesOptions = $newses->pluck('localDescription.title', 'id')->toArray();

            $form->select('news_id', 'Новость')
                ->options($newsesOptions)
                ->value(request('news_id'));

            $newsReviewTree = NewsReview::withDepth()->defaultOrder()->get()->toTree();
            $newsReviewOptions = [];

            $traverse = function ($reviews, $prefix = '') use (&$traverse, &$newsReviewOptions, $newsesOptions) {
                foreach ($reviews as $review) {
                    $prefix = !empty($prefix) ? $prefix
                        : (isset($newsesOptions[$review->news_id]) ? $newsesOptions[$review->news_id] : '') . ' | ';
                    $newsReviewOptions[$review->id] = $prefix
                        . ' '
                        . $review->name
                        . ' '
                        . '( ' . $review->created_at . ' )';

                    $prefix .= ' ' . $review->name . ' ( ' . $review->created_at . ' ) > ';
                    $traverse($review->children, $prefix);
                }
            };

            $traverse($newsReviewTree);

            $form->select('parent_id', 'Родительский Комментарий')
                ->options($newsReviewOptions)
                ->value(request('news_id'));

            $form->text('name', 'Имя');
            $form->email('email', 'Email');
            $form->textarea('text', 'Текст ');
            $form->switch('status', 'Статус')->states([
                'on' => ['value' => 1, 'text' => 'Одобрен', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => 'Неодобрен', 'color' => 'danger'],
            ]);
        })->tab('Изображения', function ($form) {
            $form->hasMany('images', 'Изображения', function (Form\NestedForm $form) {
                $form->image('image', 'Изображение');
            });
        });

        $form->saved(function (Form $form) {
            $newsReview = $form->model();
            if (isset($newsReview->status) && $newsReview->status === 1 && !empty($newsReview->email)) {
                $news = News::where('id', '=', $newsReview->news_id)->first();
                $link = isset($news) ? route('news.show', $news->slug) : null;
                $mail = new ReviewAccepted($newsReview->text, $link);
                $mail->subject(__('mails.information.review_accepted.subject'));
                Mail::to($newsReview->email)->send($mail);
            }
        });

        return $form;
    }
}
