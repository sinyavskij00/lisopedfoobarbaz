<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Library\Checkout\NewPost;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Table;


class NewPostController extends Controller
{
    protected $newPost;

    public function __construct(NewPost $newPost)
    {
        $this->newPost = $newPost;
    }

    public function index(Content $content)
    {
        return $content
            ->header('Модуль управления новой почтой')
            ->description('Модуль управления новой почтой')
            ->row(function (Row $row) {

                $citiesCount = $this->newPost->getCitiesCount();
                $warehousesCount = $this->newPost->getWarehousesCount();

                // table 1
                $headers = ['Название списка', 'Количество', 'Обновить'];
                $rows = [
                    ['Города', $citiesCount, '<a href="' . route('module.newPost.updateCities') . '"><i class="fa fa-cog"></i></a>'],
                    ['Отделения', $warehousesCount, '<a href="' . route('module.newPost.updateWarehouses') . '"><i class="fa fa-cog"></i></a>'],
                ];

                $table = new Table($headers, $rows);

                $row->column(12, $table);
            });
    }

    public function updateCities()
    {
        $this->newPost->updateCities();
        return redirect()->route('module.newPost');
    }

    public function updateWarehouses()
    {
        $this->newPost->updateWarehouses();
        return redirect()->route('module.newPost');
    }
}
