<?php

namespace App\Admin\Controllers;

use App\Entities\Attribute;
use App\Entities\AttributeGroup;
use App\Entities\Category;
use App\Entities\CategoryDescription;
use App\Entities\Language;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class CategoryController extends Controller
{
    use HasResourceActions;

    protected $categories;


    public function __construct()
    {
        $this->categories = Category::with('localDescription')->get();
    }
    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Категории')
            ->description('Категории')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('категории')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактировать')
            ->description('категорию')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создать')
            ->description('категорию')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Category);

        $grid->model()->with([
            'localDescription'
        ]);

        $grid->id('Id')->sortable();
        $grid->slug('SEO-урл')->sortable();
        $grid->column('description.name', 'Название')->sortable();

        $categories = $this->categories;
        $grid->column('parent_id', 'Родительская категория')->display(function ($categoryId) use ($categories) {
            $category = $categories->firstWhere('id', '=', $categoryId);
            if (isset($category) && isset($category->localDescription)) {
                return $category->localdescription->name;
            }
            return '-';
        })->sortable();
        $grid->column('status', 'Статус')->display(function ($default) {
            return isset($default) && $default === 1 ? 'Активна' : 'Неактивна';
        });
        $grid->created_at('Создан')->sortable();
        $grid->updated_at('Обновлен')->sortable();

        $grid->filter(function (Grid\Filter $filter) {
            $filter->equal('parent_id', 'Родительская категория')
                ->select(
                    Category::with('localDescription')->get()
                        ->pluck('localDescription.name', 'id')
                );
            $filter->where(function ($query) {
                $query->whereHas('localDescription', function ($query) {
                    $query->where('name', 'like', '%' . $this->input . '%');
                });
            }, 'Название');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Category::findOrFail($id));

        $show->id('Id');
        $show->slug('Seo-урл');
        $show->image('Изображение');
        $show->field('status', 'Статус')->as(function ($default) {
            return isset($default) && $default === 1 ? 'Активна' : 'Неактивна';
        });
        $show->created_at('Создан');
        $show->updated_at('Обновлен');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Category);

        $form->tab('Основное', function ($form) {
            $form->text('slug', 'SEO-урл');

            $form->listbox('categoryattributes', 'Атрибуты')->options(
                Attribute::with('localDescription')->get()->pluck('localDescription.name', 'id')
            )->help('Фильтры');

            $form->elfinder('image', 'Изображение');
            $form->elfinder('icon', 'Иконка')->help('Используется в пиьме');
            $form->select('parent_id', 'Родительская категория')
                ->options($this->categories->pluck('localDescription.name', 'id'));

            $form->radio('layout_type', 'Тип отображения')->options([
                0 => 'Листинг товаров',
                1 => 'Подкатегории'
            ]);
            $form->switch('status', 'Статус')->states([
                'on' => ['value' => 1, 'text' => 'Активна', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => 'Неактивна', 'color' => 'danger'],
            ]);
            $form->switch('show_manufacture__filter', 'Показывать параметр "Производители" в фильтре')->states([
                'on' => ['value' => 1, 'text' => 'Включить', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => 'Выключиь', 'color' => 'danger'],
            ]);
        })->tab('Описание', function ($form) {
            $form->hasMany('descriptions', ' ', function (Form\NestedForm $form) {
                $form->select('language_id', 'Язык')->options(Language::all()->pluck('title', 'id'));
                $form->text('name', 'Название');
                $form->ckeditor('description', 'Описание');
                $form->textarea('meta_h1');
                $form->textarea('meta_description');
                $form->textarea('meta_keywords');
                $form->textarea('meta_title');
            });
        });


        return $form;
    }

    public function updateMetaSEO($obj, $titleText, $descriptionText, $parentCategory = null){
        $categoryDescription = CategoryDescription::where('category_id', '=', $obj->id)->first();
        $parseText = str_replace('(НАЗВАНИЕ)', $categoryDescription->name, $titleText);
        $parseText = str_replace('(название)', $categoryDescription->name, $parseText);
        $parseText = str_replace('(Название)', $categoryDescription->name, $parseText);
        $parseDescription = str_replace('(НАЗВАНИЕ)', $categoryDescription->name, $descriptionText);
        $parseDescription = str_replace('(название)', $categoryDescription->name, $parseDescription);
        $parseDescription = str_replace('(Название)', $categoryDescription->name, $parseDescription);
        if($parentCategory !== null){
            $parentCategory =  CategoryDescription::where('category_id', '=', $parentCategory)->first();
            $parseText = str_replace('(РОДИТЕЛЬСКАЯ КАТЕГОРИЯ)', $parentCategory->name, $parseText);
            $parseDescription = str_replace('(родительская категория)', $parentCategory->name, $parseDescription);
        }
        $categoryDescription->meta_title = $parseText;
        $categoryDescription->meta_description = $parseDescription;
        $categoryDescription->save();
    }

    public function productsCategoryMetaMake(){
        foreach (get_category_tree() as $category_level_1){
            if($category_level_1->id === 1){
                foreach ($category_level_1->children as $category_level_2){
                    $this->updateMetaSEO($category_level_2, '(НАЗВАНИЕ) Купить в Украине ᐈ Лучшая цена на (название) в интернет-магазине Lisoped, Харьков, Киев Кредит, рассрочка, оплата частями', 'Купить (название) в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине ⏩от2000грн ❗Акции ✅Гарантия');
                    foreach ($category_level_2->children as $category_level_3){
                        if($category_level_2->id === 2){
                            $this->updateMetaSEO($category_level_3, 'Купить (НАЗВАНИЕ) ВЕЛОСИПЕДЫ горные в Украине ᐈ Лучшая цена на велосипеды (название) в интернет-магазине Lisoped, Харьков, Киев Кредит, рассрочка, оплата частями', 'Купить (название) велосипеды в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине ⏩от2000грн ❗Акции ✅Гарантия');
                        }
                        if($category_level_2->id === 9){
                            $this->updateMetaSEO($category_level_3, 'Купить (НАЗВАНИЕ) ВЕЛОСИПЕДЫ в Украине ᐈ Лучшая цена на велосипеды (название) в интернет-магазине Lisoped, Харьков, Киев Кредит, рассрочка, оплата частями', 'Купить (название) велосипеды в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине ⏩от2000грн ❗Акции ✅Гарантия');
                        }
                        if($category_level_2->id === 15){
                            $this->updateMetaSEO($category_level_3, '(НАЗВАНИЕ) (РОДИТЕЛЬСКАЯ КАТЕГОРИЯ) купить в Украине ᐈ Лучшая цена на (название) велосипеды для девушек и женщин в интернет-магазине Lisoped, Харьков, Киев Кредит, рассрочка, оплата частями', 'Купить (название) (родительская категория) велосипеды в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине ⏩от2000грн ❗Акции ✅Гарантия', $category_level_2->id);
                        }
                        if($category_level_2->id === 18){
                            $this->updateMetaSEO($category_level_3, 'Купить (НАЗВАНИЕ) ВЕЛОСИПЕДЫ в Украине ᐈ Лучшая цена на велосипеды (название) для детей в интернет-магазине Lisoped, Харьков, Киев Кредит, рассрочка, оплата частями', 'Купить (название) велосипеды в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине ⏩от2000грн ❗Акции ✅Гарантия');
                        }
                        if($category_level_2->id === 22){
                            $this->updateMetaSEO($category_level_3, 'Купить (НАЗВАНИЕ) в Украине ᐈ Лучшая цена на (название) в интернет-магазине Lisoped, Харьков, Киев Кредит, рассрочка, оплата частями', 'Купить (название) в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине ⏩от2000грн ❗Акции ✅Гарантия');
                        }
                    }

                }
            }

            if($category_level_1->id === 26){
                foreach ($category_level_1->children as $category_level_2){
                    $this->updateMetaSEO($category_level_2, '(НАЗВАНИЕ) для велосипеда купить в Украине ᐈ Лучшая цена на (название) в интернет-магазине Lisoped, Харьков, Киев', '(Название) - купить в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине ⏩от2000грн ❗Акции ✅Гарантия');
                    foreach ($category_level_2->children as $category_level_3){
                        $this->updateMetaSEO($category_level_3, '(НАЗВАНИЕ) для велосипеда купить в Украине ᐈ Лучшая цена на (название) в интернет-магазине Lisoped, Харьков, Киев', '(Название) - купить в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине ⏩от2000грн ❗Акции ✅Гарантия');
                    }
                }
            }

            if($category_level_1->id === 81){
                foreach ($category_level_1->children as $category_level_2){
                    $this->updateMetaSEO($category_level_2, '(НАЗВАНИЕ) для велосипеда купить в Украине ᐈ Лучшая цена на велосипедные (название) в интернет-магазине Lisoped, Харьков, Киев', '(Название) - купить в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине ⏩от2000грн ❗Акции ✅Гарантия');
                    foreach ($category_level_2->children as $category_level_3){
                        $this->updateMetaSEO($category_level_3, '(НАЗВАНИЕ) для велосипеда купить в Украине ᐈ Лучшая цена на велосипедные (название) в интернет-магазине Lisoped, Харьков, Киев', '(Название) - купить в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине ⏩от2000грн ❗Акции ✅Гарантия');
                    }
                }
            }

            if($category_level_1->id === 129){
                foreach ($category_level_1->children as $category_level_2){
                    $this->updateMetaSEO($category_level_2, '(НАЗВАНИЕ) для велосипеда купить в Украине ᐈ Лучшая цена на (название) для велосипедов в интернет-магазине Lisoped, Харьков, Киев', '(Название) для велосипедов - купить в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине ⏩от2000грн ❗Акции ✅Гарантия');
                    foreach ($category_level_2->children as $category_level_3){
                        $this->updateMetaSEO($category_level_3, '(НАЗВАНИЕ) для велосипеда купить в Украине ᐈ Лучшая цена на (название) для велосипедов в интернет-магазине Lisoped, Харьков, Киев', '(Название) для велосипедов - купить в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине ⏩от2000грн ❗Акции ✅Гарантия');
                    }
                }
            }

            if($category_level_1->id === 163){
                foreach ($category_level_1->children as $category_level_2){
                    $this->updateMetaSEO($category_level_2, '(НАЗВАНИЕ) купить в Украине ᐈ Лучшая цена на (название) для велосипедистов в интернет магазине Lisoped', 'Купить (название) для велосипедистов в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине ⏩от2000грн ❗Акции ✅Гарантия');
                    foreach ($category_level_2->children as $category_level_3){
                        $this->updateMetaSEO($category_level_3, '(НАЗВАНИЕ) купить в Украине ᐈ Лучшая цена на (название) для велосипедистов в интернет магазине Lisoped', 'Купить (название) для велосипедистов в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине ⏩от2000грн ❗Акции ✅Гарантия');
                    }
                }
            }

            if($category_level_1->id === 144){
                foreach ($category_level_1->children as $category_level_2){
                    $this->updateMetaSEO($category_level_2, '(НАЗВАНИЕ) для велосипеда купить в Украине ᐈ Лучшая цена на (название) для велосипедов в интернет-магазине Lisoped, Харьков, Киев', '(Название) для велосипедистов - купить в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине ⏩от2000грн ❗Акции ✅Гарантия');
                    foreach ($category_level_2->children as $category_level_3){
                        $this->updateMetaSEO($category_level_3, '(НАЗВАНИЕ) для велосипеда купить в Украине ᐈ Лучшая цена на (название) для велосипедов в интернет-магазине Lisoped, Харьков, Киев', '(Название) для велосипедистов - купить в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине ⏩от2000грн ❗Акции ✅Гарантия');
                    }
                }
            }


















            /*switch ($category_level_1->id){
                case 26:
                    foreach ($category_level_1->children as $category_level_2){
                        $categoryDescription = CategoryDescription::where('category_id', '=', $category_level_2->id)->first();
                        $categoryDescription->meta_title = $categoryDescription->name .' для велосипеда купить в Украине ᐈ Лучшая цена на '. $categoryDescription->name . ' в интернет-магазине Lisoped';
                        $categoryDescription->meta_description = $categoryDescription->name .' - купить в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине от2000грн ❗Рассрочка ✅Гарантия';
                        $categoryDescription->save();

                        foreach ($category_level_2->children as $category_level_3){
                            $categoryDescription = CategoryDescription::where('category_id', '=', $category_level_3->id)->first();
                            $categoryDescription->meta_title = $categoryDescription->name .' для велосипеда купить в Украине ᐈ Лучшая цена на '. $categoryDescription->name . ' в интернет-магазине Lisoped';
                            $categoryDescription->meta_description = $categoryDescription->name .' - купить в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине от2000грн ❗Рассрочка ✅Гарантия';
                            $categoryDescription->save();
                        }
                    }
                    break;
                case 81:
                    foreach ($category_level_1->children as $category_level_2){
                        $categoryDescription = CategoryDescription::where('category_id', '=', $category_level_2->id)->first();
                        $categoryDescription->meta_title = $categoryDescription->name .' для велосипеда купить в Украине ᐈ Лучшая цена на велосипедные '. $categoryDescription->name . ' в интернет-магазине Lisoped';
                        $categoryDescription->meta_description = $categoryDescription->name .' - купить в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине от2000грн ❗Рассрочка ✅Гарантия';
                        $categoryDescription->save();

                        foreach ($category_level_2->children as $category_level_3){
                            $categoryDescription = CategoryDescription::where('category_id', '=', $category_level_3->id)->first();
                            $categoryDescription->meta_title = $categoryDescription->name .' для велосипеда купить в Украине ᐈ Лучшая цена на велосипедные '. $categoryDescription->name . ' в интернет-магазине Lisoped';
                            $categoryDescription->meta_description = $categoryDescription->name .' - купить в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине от2000грн ❗Рассрочка ✅Гарантия';
                            $categoryDescription->save();
                        }
                    }
                    break;
                case 129:
                    foreach ($category_level_1->children as $category_level_2){
                        $categoryDescription = CategoryDescription::where('category_id', '=', $category_level_2->id)->first();
                        $categoryDescription->meta_title = $categoryDescription->name .' для велосипеда купить в Украине ᐈ Лучшая цена на '. $categoryDescription->name . ' для велосипедов в интернет-магазине Lisoped';
                        $categoryDescription->meta_description = $categoryDescription->name .' для велосипедов - купить в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине от2000грн ❗Рассрочка ✅Гарантия';
                        $categoryDescription->save();

                        foreach ($category_level_2->children as $category_level_3){
                            $categoryDescription = CategoryDescription::where('category_id', '=', $category_level_3->id)->first();
                            $categoryDescription->meta_title = $categoryDescription->name .' для велосипеда купить в Украине ᐈ Лучшая цена на '. $categoryDescription->name . ' для велосипедов в интернет-магазине Lisoped';
                            $categoryDescription->meta_description = $categoryDescription->name .' для велосипедов - купить в Украине в магазине Lisoped ✅Лучшие цены, ✈Бесплатная доставка по Украине от2000грн ❗Рассрочка ✅Гарантия';
                            $categoryDescription->save();
                        }
                    }
                    break;
            }*/
        }
    }

    public function veloCategory(){
        $categoryAttrs = \DB::table('category_attributes')->where('category_id', '=', 1)->get();
        foreach (get_category_tree()->where('id', '=', 1)->first()->children as $category_level_2){
            \DB::table('category_attributes')->where('category_id', '=', $category_level_2->id)->delete();
            foreach ($categoryAttrs as $categoryAttr){
                $categoryAttrNew = \DB::table('category_attributes')->insert([
                    'category_id' => $category_level_2->id,
                    'attribute_id' => $categoryAttr->attribute_id
                ]);
            }
            foreach ($category_level_2->children as $category_level_3){
                \DB::table('category_attributes')->where('category_id', '=', $category_level_3->id)->delete();
                foreach ($categoryAttrs as $categoryAttr){
                    $categoryAttrNew = \DB::table('category_attributes')->insert([
                        'category_id' => $category_level_3->id,
                        'attribute_id' => $categoryAttr->attribute_id
                    ]);
                }
            }
        }
    }


}
