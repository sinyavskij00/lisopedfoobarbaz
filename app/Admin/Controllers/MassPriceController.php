<?php

namespace App\Admin\Controllers;

use App\Entities\Category;
use App\Entities\Manufacturer;
use App\Entities\Product;
use App\Entities\StockKeepingUnit;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Form;
use Illuminate\Http\Request;


class MassPriceController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->header('Модуль массового управления ценами')
            ->description('Модуль массового управления ценами')
            ->row(function (Row $row) {

                $form = new Form();

                $manufacturers = Manufacturer::with('localDescription')->get();
                $form->listbox('manufacturers', 'Производители')->options($manufacturers->pluck('localDescription.name', 'id'));

                $categories = Category::with('localDescription')->get();
                $form->listbox('categories', 'Категории')->options($categories->pluck('localDescription.name', 'id'));

                $form->number('percent', 'Процент')->min(-100)->max(100);

                $form->action('mass-price');

                $row->column(12, $form);
            });
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'percent' => 'required',
        ]);

        $percent = intval($request->input('percent'));

        if ($percent > 0) {
            $categoriesIds = $request->input('categories', []);
            $manufacturersIds = $request->input('manufacturers', []);
            $productsIds = Product::whereIn('category_id', $categoriesIds)->orWhereIn('manufacturer_id', $manufacturersIds)->get(['id'])->pluck(['id']);
            $skus = StockKeepingUnit::whereIn('product_id', $productsIds);

            $skus->chunk(1000, function ($skus) use ($percent) {
                foreach ($skus as $sku) {
                    $sku->price = $sku->price * (1 + ($percent / 100));
                    $sku->save();
                }

                //todo this method better
//                $manufacturers = $request->get('manufacturers', []);
//                $categories = $request->get('categories', []);
//                $changeValue = floatval($request->get('change_value', 0));
//                $changeType = intval($request->get('change_type', 1));
//
//                $query = Product::whereIn('category_id', $categories)
//                    ->orWhereIn('manufacturer_id', $manufacturers);
//
//                $newPrice = $changeType === 1 ? 'price + ' . $changeValue : 'price * ' . (1 + ($changeValue / 100));
//
//                $query->update([
//                    'price' => DB::raw($newPrice)
//                ]);
            });
        }
    }
}
