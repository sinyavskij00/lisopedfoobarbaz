<?php

namespace App\Admin\Controllers;

use App\Entities\Category;
use App\Entities\CustomerGroup;
use App\Entities\Order;
use App\Entities\OrderSku;
use App\Entities\OrderStatus;
use App\Entities\Product;
use App\Entities\StockKeepingUnit;
use App\Entities\User;
use App\Entities\Vendor;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Box;
use Encore\Admin\Widgets\Form;
use Encore\Admin\Widgets\Table;
use Illuminate\Http\Request;

class OrderReportController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->header('Модуль отчета по покупателю')
            ->description('Модуль отчета по покупателю')
            ->row(function (Row $row) {

                $form = new Form();

                $form->datetime('date_start', 'Дата начала')->rules('required');
                $form->datetime('date_end', 'Дата конца')->rules('required');

                $orderStatuses = OrderStatus::with('localDescription')->get();
                $form->listbox('order_statuses', 'Статусы заказа')->options($orderStatuses->pluck('localDescription.name', 'id'));

                $users = User::all();
                $userOptions = [];
                foreach ($users as $user) {
                    $userOptions[$user->id] = $user->first_name . ' ' . $user->last_name;
                }
                $form->select('customer_id', 'Покупатели')->options($userOptions);

                $categories = Category::with('localDescription')->get();
                $form->select('category_id', 'Категория')->options($categories->pluck('localDescription.name', 'id'));

                $customerGroups = CustomerGroup::with('localDescription')->get();
                $form->select('customer_group_id', 'Группа покупателей')->options($customerGroups->pluck('localDescription.name', 'id'));

                $vendors = Vendor::all();
                $form->select('vendor_id', 'Поставщик')->options($vendors->pluck('name', 'id'));

                $form->action('order-report');

                $form->disablePjax();

                $row->column(12, $form);
            });
    }

    public function getReport(Request $request, Content $content)
    {
        $this->validate($request, [
            'date_start' => 'required',
            'date_end' => 'required'
        ]);

        $dateStart = $request->input('date_start');
        $dateEnd = $request->input('date_end');

        $orderQuery = Order::where('created_at', '>=', $dateStart)
            ->where('created_at', '<=', $dateEnd);

        $customerId = $request->input('customer_id');
        if (isset($customerId)) {
            $orderQuery->where('orders.customer_id', '=', $customerId);
        }

        $customerGroupId = $request->input('customer_group_id');
        if (isset($customerGroupId)) {
            $orderQuery->where('orders.customer_group_id', '=', $customerGroupId);
        }

        $orderStatusIds = array_filter($request->input('order_statuses', []));
        if (!empty($orderStatusIds)) {
            $orderQuery->whereIn('orders.order_status_id', $orderStatusIds);
        }

        $orderIds = $orderQuery->get(['id'])->pluck('id')->toArray();

        $skuIds = [];
        $categoryId = $request->input('category_id');
        if (isset($categoryId)) {
            $productIds = Product::where('category_id', '=', $categoryId)
                ->get(['id'])
                ->pluck('id')
                ->toArray();

            $idsByCategory = StockKeepingUnit::whereIn('product_id', $productIds)
                ->get(['id'])
                ->pluck('id')
                ->toArray();

            $skuIds = array_merge($skuIds, $idsByCategory);
        }

        $vendorId = $request->input('vendor_id');
        if (isset($vendorId)) {
            $idsByVendor = StockKeepingUnit::where('vendor_id', '=', $vendorId)
                ->get(['id'])
                ->pluck('id')
                ->toArray();

            $skuIds = array_merge($skuIds, $idsByVendor);
        }

        $orderSkuQuery = OrderSku::whereIn('order_id', $orderIds);
        if (!empty($skuIds)) {
            $orderSkuQuery->whereIn('sku_id', $skuIds);
        }
        $orderSkus = $orderSkuQuery->with([
            'order.status.localDescription'
        ])->get();

        $orders = [];
        foreach ($orderSkus as $orderSku) {
            if (!isset($orders[$orderSku->order_id])) {
                $status = '';
                if (isset($orderSku->order) && isset($orderSku->order->status)
                    && isset($orderSku->order->status->localDescription)
                    && isset($orderSku->order->status->localDescription->name)
                ) {
                    $status = $orderSku->order->status->localDescription->name;
                }

                $orders[$orderSku->order_id] = [
                    'status' => $status,
                    'products' => []
                ];
            }

            $orders[$orderSku->order_id]['products'][] = [
                'name' => isset($orderSku->name) ? $orderSku->name : '',
                'model' => isset($orderSku->model) ? $orderSku->model : '',
                'sku' => isset($orderSku->sku) ? $orderSku->sku : '',
                'price' => isset($orderSku->price) ? $orderSku->price : '',
                'quantity' => isset($orderSku->quantity) ? $orderSku->quantity : '',
                'total' => isset($orderSku->total) ? $orderSku->total : '',
            ];
        }

        $boxes = [];
        $headers = ['Товар', 'Модель', 'Артикул', 'Цена', 'Количество', 'Всего'];
        foreach ($orders as $orderId => $order) {
            $rows = [];
            foreach ($order['products'] as $product) {
                $rows[] = [
                    $product['name'],
                    $product['model'],
                    $product['sku'],
                    $product['price'],
                    $product['quantity'],
                    $product['total'],
                ];
            }
            $table = new Table($headers, $rows);

            $box = new Box('Заказа №' . $orderId . '( ' . $order['status'] . ' )', $table);
            $box->style('info');
            $boxes[] = $box;
        }

        return $content
            ->header('Отчет о заказах')
            ->description('Отчет в период с ' . $dateStart . ' по ' . $dateEnd)
            ->row(function (Row $row) use ($boxes) {
                foreach ($boxes as $box) {
                    $row->column(12, $box);
                }
            });
    }
}
