<?php

namespace App\Admin\Controllers;

use App\Entities\SmsTemplate;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class SmsTemplateController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Шаблоны SMS')
            ->description('Все шаблоны SMS')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Шаблоны SMS')
            ->description('Просмотр')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Шаблоны SMS')
            ->description('Редактирование')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Шаблоны SMS')
            ->description('Создание')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new SmsTemplate);

        $grid->id('Id');
        $grid->title('Название шаблона');
        $grid->created_at('Создан');
        $grid->updated_at('Обновлен');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(SmsTemplate::findOrFail($id));

        $show->id('Id');
        $show->title('Название шаблона');
        $show->text('Текст');
        $show->created_at('Создано');
        $show->updated_at('Обновлено');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new SmsTemplate);

        $form->text('title', 'Название шаблона');

        $help = [
            '{public_id} - публичный id заказа',
            '{order_status} - статус заказа',
            '{public_comment} - публичный комментарий',
            '{billing_cart_name} - имя с платежной карты',
            '{billing_cart_number} - номер с платежной карты',
            '{billing_account_name} - имя с платежного счета',
            '{billing_account_employer_number} - ЕГРПОУ с платежного счета',
            '{billing_account_bank_number} - МФО с платежного счета',
            '{billing_account_checking_account} - P/C с платежного счета',
        ];

        $form->textarea('text', 'Текст')->help(implode('<br/>', $help));

        return $form;
    }
}
