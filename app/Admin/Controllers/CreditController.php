<?php

namespace App\Admin\Controllers;

use App\Entities\Language;
use App\Entities\SiteSetting;
use App\Http\Controllers\Controller;
use App\Library\Settings;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Form;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class CreditController extends Controller
{
    protected $languages;

    protected $settings;

    public function __construct(Settings $settings)
    {
        $this->languages = Language::all();
        $this->settings = $settings;
    }

    public function index(Content $content)
    {
        return $content
            ->header('Страница "Рассрочка"')
            ->description('Страница "Рассрочка"')
            ->row(function (Row $row) {
                $form = new Form();

                $textes = $this->settings->getByKey('credit');
                foreach ($this->languages as $language) {
                    $localText = isset($textes[$language->id]) ? $textes[$language->id] : '';
                    $form->ckeditor(
                        'credit[' . $language->id . ']',
                        'Текст на странице "Рассрочка" ' . $language->slug
                    )->default($localText);
                }

                $form->disablePjax();
                $form->action(route('pages.credit'));

                $row->column(12, $form);
            });
    }

    public function update(Request $request)
    {
        SiteSetting::updateByCode('credit', $request->except(['_token']));
        $success = new MessageBag([
            'title' => 'Изменения были применены',
        ]);
        return back()->with(compact('success'));
    }
}
