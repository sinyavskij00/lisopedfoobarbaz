<?php

namespace App\Admin\Controllers;

use App\Entities\CustomerGroup;
use App\Entities\CustomerGroupDescription;
use App\Entities\User;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Покупатели')
            ->description('Все покупатели')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр покупателя')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование покупателя')
            ->description('Редактирование покупателя')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание покупателя')
            ->description('Создание покупателя')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User);

        $grid->id('Id')->setAttributes(['width' => ' 60px'])->sortable();
        $grid->first_name('Имя')->sortable();
        $grid->last_name('Фамилия')->sortable();
        $grid->patronymic('Отчество')->sortable();
        $grid->telephone('Телефон')->setAttributes(['width' => ' 130px'])->sortable();
        $grid->address('Адрес')->setAttributes(['width' => ' 220px'])->sortable();
        $grid->column('customer_group_id', 'Группа покупателей')->display(function ($customerGroupId) {
            if (isset($customerGroupId)) {
                $customerGroup = CustomerGroup::find($customerGroupId);
                if (isset($customerGroup) && isset($customerGroup->localDescription)) {
                    return $customerGroup->localDescription->name;
                }
            }
            return '-';
        })->setAttributes(['width' => ' 180px'])->sortable();
        $grid->email('Email')->setAttributes(['width' => ' 110px'])->sortable();
        $grid->column('subscribe', 'Подписан')->display(function ($subscribe) {
            return isset($subscribe) && $subscribe === 1 ? 'Да' : 'Нет';
        })->setAttributes(['width' => ' 110px'])->sortable();
        $grid->created_at('Создан')->setAttributes(['width' => '100px'])->sortable();
        $grid->updated_at('Обновлен')->setAttributes(['width' => '110px'])->sortable();

        $grid->filter(function (Grid\Filter $filter) {
            $filter->disableIdFilter();

            $filter->like('first_name', 'Имя');

            $filter->like('last_name', 'Фамимлия');

            $filter->like('telephone', 'Телефон');

            $customerGroups = CustomerGroup::with(['localDescription'])->get();
            $filter->equal('customer_group_id', 'Группа покупателей')
                ->select($customerGroups->pluck('localDescription.name', 'id'));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->id('Id');
        $show->first_name('Имя');
        $show->last_name('Фамилия');
        $show->patronymic('Отчество');
        $show->telephone('Телефон');
        $show->address('Адрес');
        $show->city_ref('Город')->as(function ($ref) {
            $city = DB::table('new_post_cities')->where('Ref', $ref)->first();
            return isset($city) ? $city->DescriptionRu : '';
        });
        $show->store_ref('Отделение')->as(function ($ref) {
            $store = DB::table('new_post_warehouses')->where('Ref', $ref)->first();
            return isset($store) ? $store->DescriptionRu . ' | ' . $store->CityDescriptionRu : '';
        });

        $show->customer_group_id('Группа покупатеелй')->as(function ($customerGroupId) {
            if (isset($customerGroupId)) {
                $languageId = config('current_language_id');
                $description = CustomerGroupDescription::where('customer_group_id', '=', $customerGroupId)
                    ->where('language_id', '=', $languageId)->first();
                if (isset($description)) {
                    return $description->name;
                }
            }
            return '---';
        });
        $show->email('E-mail');
        $show->column('Подписан', 'subscribe')->as(function ($subscribe) {
            return isset($subscribe) && $subscribe === 1 ? 'Да' : 'Нет';
        });

        $show->created_at('Создан');
        $show->updated_at('Обновлен');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User);

        $form->text('first_name', 'Имя');
        $form->text('last_name', 'Фамилия');
        $form->text('patronymic', 'отчество');
        $form->text('telephone', 'Телефон');
        $form->text('address', 'Адрес');
        $form->date('birthday', 'День рожденья');

        $form->select('city_ref', 'Город')
            ->options(
                DB::table('new_post_cities')->get(['Ref', 'DescriptionRu'])->pluck('DescriptionRu', 'Ref')
            );

        $stores = Db::table('new_post_warehouses')->get(['Ref', 'DescriptionRu', 'CityDescriptionRu']);
        $storeOptions = [];
        foreach ($stores as $store) {
            $storeOptions[$store->Ref] = $store->DescriptionRu . ' | ' . $store->CityDescriptionRu;
        }
        $form->select('store_ref', 'Отделение')->options($storeOptions);

        $form->select('customer_group_id', 'Группа покупателей')
            ->options(CustomerGroup::all()->pluck('localDescription.name', 'id'));
        $form->email('email', 'Email');
        $form->switch('subscribe', 'Подписка')->states([
            'on' => ['value' => 1, 'text' => 'Включена', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => 'Выключена', 'color' => 'danger'],
        ]);

        return $form;
    }

    public function getUser(Request $request)
    {
        $userId = $request->get('user_id');
        return User::where('id', '=', $userId)->first();
    }
}
