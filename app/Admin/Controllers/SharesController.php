<?php

namespace App\Admin\Controllers;

use App\Entities\Language;
use App\Entities\Manufacturer;
use App\Entities\Product;
use App\Entities\Shares;
use App\Entities\Special;
use App\Entities\Year;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Tab;
use Illuminate\Http\Request;
use App\Entities\ShareProducts;
use App\Entities\ShareTakeProducts;
use Illuminate\Support\MessageBag;

class SharesController extends Controller
{
    use HasResourceActions;
    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Расширенная акция')
            ->description('Все расширенные акции')
            ->body($this->grid());
    }

    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр расширенной акции')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактировать расширенную акцию')
            ->description('Редактировать расширенную акцию')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создать расширенную акцию')
            ->description('Создать расширенную акцию')
            ->body($this->form());
    }

    protected function grid()
    {
        $grid = new Grid(new Shares);

        $grid->id('id')->sortable();
        $grid->column('special_id',  'Название основной акции')->display(function ($special_id){
            return Special::find($special_id)->localDescription->title;
        });
        $grid->title('Название расширенной')->sortable();
        $grid->column('status', 'Статус')->display(function ($status) {
            return isset($status) && $status == 1 ? 'Действует' : 'Не действует';
        })->sortable();
        $grid->date_start('Дата начала')->sortable();
        $grid->date_end('Дата конца')->sortable();

        return $grid;
    }



    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Shares::findOrFail($id));

        $show->id('id');
        $show->special_id('Название основной акции')->as(function ($special_id){
            return Special::find($special_id)->localDescription->title;
        });
        $show->slug('SEO-урл');
        $show->banner_image('Изображение')->banner_image();
        $show->status('Статус')->as(function ($status) {
            return isset($status) && $status == 1 ? 'Действует' : 'Не действует';
        });
        $show->views('Просмотры');
        $show->date_start('Дата начала');
        $show->date_end('Дата конца');
        $show->title('Название');
        $show->description('Текст');
        $tab = new Tab();
        $tab->add('Основное', $show->render());

        $form = new \Encore\Admin\Widgets\Form();
        $sharesTakeProducts = new ShareTakeProducts();
        $products = Product::with(['localDescription'])->get();
        $form->multipleSelect('action', 'Акционные продукты')
            ->options($products->pluck('localDescription.name', 'id'))->default($sharesTakeProducts->getSharedTakeProductsById($id));
        $form->hidden('share_id')->default($id);
        $form->disablePjax();
        $form->action(route('share.shares-take-product'));
        $tab->add('Товари по акции', $form);

        $form = new \Encore\Admin\Widgets\Form();
        $form->hidden('share_id')->default($id);
        $form->disablePjax();
        $form->action(route('share.shares-take-product-reset'));

        $tab->add('Очистить', $form);


        $defaultManufacturers = \DB::table('share_take_products')
            ->join('products', 'share_take_products.product_id', '=', 'products.id')
            ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
            ->groupBy('products.manufacturer_id')
            ->get()
            ->pluck('manufacturer_id')
            ->toArray();
        $defaultYears = \DB::table('share_take_products')
            ->join('products', 'share_take_products.product_id', '=', 'products.id')
            ->join('stock_keeping_units', 'products.id', '=', 'stock_keeping_units.product_id')
            ->groupBy('stock_keeping_units.year_id')
            ->get()
            ->pluck('year_id')
            ->toArray();
        $form = new \Encore\Admin\Widgets\Form();
        /*$form->multipleSelect('action', 'Тип массового присвоения')
            ->options([
                'addByManufacturer' => 'По бренду',
                'addByYear' => 'По году'
            ])
            ->default('on_year');*/
        $form->hidden('share_id')->default($id);
        $years = Year::orderBy('value');
        $manufactures = Manufacturer::orderBy('slug')->with(['localDescription' => function($query){
            return $query->groupBy('manufacturer_id');
        }])->get();
        $form->multipleSelect('years', 'Год')->options($years->pluck('value', 'id'))->default($defaultYears);
        $form->multipleSelect('manufactures', 'Производитель')->options($manufactures->pluck('localDescription.name', 'id'))->default($defaultManufacturers);

        $form->disablePjax();
        $form->action(route('share.mass-add'));

        $tab->add('Массовое добавлние акции', $form);
        $form = new \Encore\Admin\Widgets\Form();
        $sharesProducts = new ShareProducts();
        $products = Product::with(['localDescription'])->get();
        $form->multipleSelect('action', 'Продукты участвующие в акции')
            ->options($products->pluck('localDescription.name', 'id'))->default($sharesProducts->getSharedProductsById($id));
        $form->hidden('share_id')->default($id);
        $form->disablePjax();
        $form->action(route('share.shares-product'));

        $tab->add('Продукты участвующие в акции', $form);
        return $tab;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Shares());
        //dd(Special::with(['localDescription'])->get());
        $form->tab('Основное', function ($form) {
            $form->select('special_id', 'Акция')->options(Special::with(['localDescription'])->get()->pluck('localDescription.title', 'id'));
            $form->text('slug', 'SEO-урл');
            $form->elfinder('banner_image', 'Изображение');
            $form->switch('status', 'Статус')->states([
                'on'  => ['value' => 1, 'text' => 'Действует', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => 'Не действует', 'color' => 'danger'],
            ]);
            $form->date('date_start', 'Дата начала акции');
            $form->date('date_end', 'Дата конца акции');
        })->tab('Описание', function ($form) {
            $form->text('title', 'Название');
            $form->ckeditor('description', 'Текст');
        });

        return $form;
    }

    public function massAdd(Request $request){
        //$method = $request->action;
        ShareTakeProducts::where('share_id', '=', $request->share_id)->delete();
        $this->addMassSharesProduct($request);
        return back()->with([
            'success' => new MessageBag([
                'title' => 'Товары доабвлены в ацкию',
                'message' => ''
            ])
        ]);
    }

    public function addMassSharesProduct($request){
        $yearsIDs = array_filter($request->years, function($item){return !is_null($item);} ) ;
        $manufactiresIDs = array_filter($request->manufactures, function($item){return !is_null($item);} ) ;
        $products = \DB::table('products')
            ->join('stock_keeping_units', 'products.id', '=', 'stock_keeping_units.product_id')
            ->join('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
            ->where('products.status', '=', 1)
            ->where('stock_keeping_units.price', '>', 0)
            ->where('stock_keeping_units.quantity', '>', 0)
            ->groupBy('products.id')
            ->select('products.id');
        if(count($yearsIDs) > 0){
            $products = $products->whereIn('stock_keeping_units.year_id',  $yearsIDs);
        }
        if(count($manufactiresIDs) > 0){
            $products = $products->whereIn('products.manufacturer_id',  $manufactiresIDs);
        }
        if(count($products->get()) > 0){
            foreach ($products->get() as $product){
                $shareTakeProduct = new ShareTakeProducts();
                $shareTakeProduct->share_id = $request->share_id;
                $shareTakeProduct->product_id = $product->id;
                $shareTakeProduct->save();
            }
        }

    }

    public function addByManufacturer($request){
        $products = Product::where('manufacturer_id', '=', $request->manufactures)
            ->select('id')
            ->get();
        foreach ($products as $product){
            $shareTakeProduct = new ShareTakeProducts();
            $shareTakeProduct->share_id = $request->share_id;
            $shareTakeProduct->product_id = $product->id;
            $shareTakeProduct->save();
        }
    }

    public function addByYear($request){
        $products = \DB::table('products')
            ->join('stock_keeping_units', 'products.id', '=', 'stock_keeping_units.product_id')
            ->where('stock_keeping_units.year_id', '=', $request->years)
            ->groupBy('stock_keeping_units.product_id')
            ->select('products.id')
            ->get();
        foreach ($products as $product){
            $shareTakeProduct = new ShareTakeProducts();
            $shareTakeProduct->share_id = $request->share_id;
            $shareTakeProduct->product_id = $product->id;
            $shareTakeProduct->save();
        }
    }

    public function addSharesProducts(Request $request){
        foreach ($request->action as $product){
            if($product !== null){
                $shareProduct = new ShareProducts();
                $shareProduct->share_id = $request->share_id;
                $shareProduct->product_id = $product;
                $shareProduct->save();
            }
        }
        return back()->with([
            'success' => new MessageBag([
                'title' => 'Подарки добавлены в ацкию',
                'message' => ''
            ])
        ]);
    }

    public function addSharesTakeProducts(Request $request){
        ShareTakeProducts::where('share_id', '=', $request->share_id)->delete();
        foreach ($request->action as $product){
            if($product !== null){
                $shareProduct = new ShareTakeProducts();
                $shareProduct->share_id = $request->share_id;
                $shareProduct->product_id = $product;
                $shareProduct->save();
            }
        }
        return back()->with([
            'success' => new MessageBag([
                'title' => 'Товары добавлены в ацкию',
                'message' => ''
            ])
        ]);
    }

    public function resetSharesTakeProducts(Request $request){
        ShareTakeProducts::where('share_id', '=', $request)->delete();
        return back()->with([
            'success' => new MessageBag([
                'title' => 'Товары удалены из ацкии',
                'message' => ''
            ])
        ]);
    }

}
