<?php

namespace App\Admin\Controllers;

use App\Entities\Category;
use App\Entities\CustomerGroup;
use App\Entities\Manufacturer;
use App\Entities\Order;
use App\Entities\Product;
use App\Entities\SkuDiscount;
use App\Entities\StockKeepingUnit;
use App\Http\Controllers\Controller;
use App\Library\Checkout\Payment\LiqPayPayment;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Box;
use Encore\Admin\Widgets\Form;
use Encore\Admin\Widgets\Table;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;


class LiqpayCheckController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->header('Модуль проверки оплаты LiqPay')
            ->description('Модуль проверки оплаты LiqPay')
            ->row(function (Row $row) {

                $form = new Form();

                $orders = Order::all();

                $orderOptions = [];
                foreach ($orders as $order) {
                    $orderOptions[$order->id] = '№ ' . $order->id . ' ' . $order->first_name . ' '
                        . $order->last_name . ' ' . $order->telephone;
                }

                $form->select('order_id', 'Номер заказа')->options($orderOptions);
                $form->action('liqpay-check');

                $row->column(12, $form);
            });
    }

    public function update(Request $request, LiqPayPayment $liqPayPayment, Content $content)
    {
        $this->validate($request, [
            'order_id' => 'required',
        ]);

        $orderId = $request->input('order_id');
        $result = $liqPayPayment->checkPaymentStatus($orderId);

        $headers = ['Ключ', 'Значение'];
        $rows = [];

        foreach ($result as $key => $value) {
            $rows[] = [$key, $value];
        }

        $table = new Table($headers, $rows);

        $box = new Box('Информация об оплате', $table);
        $box->style('info');

        return $content
            ->header('Информация об оплате заказа №' . $orderId)
            ->description('Информация о статусе оплаты через liqpay')
            ->row(function (Row $row) use ($box) {
                $row->column(12, $box);
            });
    }
}
