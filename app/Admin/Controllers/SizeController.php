<?php

namespace App\Admin\Controllers;

use App\Entities\Language;
use App\Entities\Size;
use App\Entities\SizeDescription;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class SizeController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Размеры')
            ->description('Все размеры')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр размера')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактировать размер')
            ->description('Редактировать размер')
            ->row($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создать размер')
            ->description('Создать размер')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Size);


        $grid->column('id', 'Значение')->display(function ($id) {
            $size = Size::with('localDescription')->where('id', '=', $id)->first();
            if(isset($size)){
                $result = $size->value;
                if(isset($size->localDescription)){
                    $result .= ' ( ' . $size->localDescription->comment . ' )';
                }
                return $result;
            }
            return '---';
        })->sortable();
        $grid->created_at('Создано')->sortable();
        $grid->updated_at('Обновлено')->sortable();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Size::findOrFail($id));

        $show->id('Id');
        $show->value('Значение')->as(function ($value) use($id) {
            $size = Size::with('localDescription')->where('id', '=', $id)->first();
            if(isset($size)){
                $result = $size->value;
                if(isset($size->localDescription)){
                    $result .= ' ( ' . $size->localDescription->comment . ' )';
                }
                return $result;
            }
            return '---';
        });
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Size);

        $form->tab('Основная информация', function ($form) {
            $form->text('value', 'Значение');
        })->tab('Описание', function ($form) {
            $form->hasMany('descriptions', 'Описания', function (Form\NestedForm $form) {
                $form->select('language_id', 'Язык')->options(Language::all()->pluck('title', 'id'));
                $form->text('comment', 'Комментарий');
            });
        });

        return $form;
    }
}
