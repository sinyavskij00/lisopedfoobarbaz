<?php

namespace App\Admin\Controllers;

use App\Entities\CustomerGroup;
use App\Entities\Language;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class CustomerGroupController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Группы покупателей')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактировать группу покупателей')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CustomerGroup);

        $grid->id('Id')->sortable();
        $grid->column('description.name', 'Название')->sortable();
        $grid->column('is_default', 'Поумолчанию')->display(function ($default) {
            return isset($default) && $default === 1 ? 'Да' : 'Нет';
        })->sortable();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CustomerGroup::findOrFail($id));

        $show->id('Id');
        $show->field('is_default', 'Поумолчанию')->as(function ($default) {
            return isset($default) && $default === 1 ? 'Да' : 'Нет';
        });
        $show->created_at('Дата создания');
        $show->updated_at('Дата последнего обновления');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new CustomerGroup);

        $form->switch('is_default', 'Группа поумолчанию')->states([
            'on'  => ['value' => 1, 'text' => 'Включено', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => 'Выключено', 'color' => 'danger'],
        ]);

        $form->hasMany('descriptions', 'Описание', function (Form\NestedForm $form) {
            $form->select('language_id', 'Язык')->options(Language::all()->pluck('title', 'id'));
            $form->text('name', 'Название');
        });

        return $form;
    }
}
