<?php

namespace App\Admin\Controllers;

use App\Entities\OrderSku;
use App\Entities\Vendor;
use App\Entities\OrderSkuStatus;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Box;
use App\Admin\Redefined\CustomWidgetForm;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use App\Library\Settings;

class InvoiceController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->header('Модуль сбора накладной')
            ->description(' ')
            ->row(function (Row $row) {
                $box = new Box();
                $box->title('Заполните форму для того что бы проверить наличее заказов.');
                $box->content(
                    'Если заказы для поставщика, присутствуют. '
                    . 'Вы сможете сохранить или отправить заказы на email поставщика в виде xlsx файла.'
                );

                $row->column(12, $box);
            })
            ->row(function (Row $row) {
                $form = new CustomWidgetForm('admin.invoice_module_form');

                $form->datetime('date_start', 'Дата начала')->rules('required');
                $form->datetime('date_end', 'Дата конца')->rules('required');

                $form->select('vendor_id', 'Поставщик')
                    ->options(Vendor::all()->pluck('name', 'id'))
                    ->rules('required');

                $form->action('invoice-module');

                $form->disablePjax();

                $row->column(12, $form);
            });
    }

    public function getInvoice(Request $request)
    {
        $this->validate($request, [
            'vendor_id' => 'required',
            'date_start' => 'required',
            'date_end' => 'required'
        ]);

        $action = $request->input('action');
        $request = $request->all();
        $request[$action] = $this->$action($request, Vendor::findOrFail($request['vendor_id']));

        return back()->withInput($request);
    }

    protected function check($request)
    {
        $orderIds = array_values(array_unique($this->sku($request)->get(['order_id'])->pluck('order_id')->toArray()));
        return count($orderIds);
    }

    protected function sku($request)
    {
        return OrderSku::whereHas('order', function ($query) use ($request) {
            $query->where('created_at', '>=', $request['date_start'])->where('created_at', '<=', $request['date_end']);
        })->whereHas('sku', function ($query) use ($request) {
            $query->where('vendor_id', '=', $request['vendor_id']);
        })->where('order_skus.shipped_status', '=', 0)->with(['sku', 'order']);
    }

    protected function download($request, $vendor)
    {
        //redirect output to client browser
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="export.xlsx"');
        header('Cache-Control: max-age=0');

        $xlsx = $this->generateXlsx($request, $vendor);
        $xlsx->save('php://output');
    }

    protected function sendToEmail($request, $vendor)
    {
        $mailPath = storage_path('emails/' . time() . '.xlsx');
        $xlsx = $this->generateXlsx($request, $vendor, true);

        $xlsx->save($mailPath);

        \Mail::send('mail.admin_invoice_module_excel', [], function ($message) use ($mailPath, $vendor) {
            $appName = config()->get('app.name');

            $message->to($vendor->email)
                ->subject('Накладная от ' . $appName)
                ->attach($mailPath, [
                    'as' => $appName . '.xlsx',
                    'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                ]);
        });

        unlink($mailPath);

        return 'email_send';
    }

    protected function generateXlsx($request, $vendor, $updateData = false)
    {
        $orderSkus = $this->sku($request)->get();

        $skusIds = [];
        $ordersIds = [];
        $orders = [];
        foreach ($orderSkus as $orderSku) {
            $ordersIds[] = $orderSku->order->id;
            $skusIds[] = $orderSku->id;

            if (!isset($orders[$orderSku->order->id])) {
                $orders[$orderSku->order->id]['client'] = [
                    'fio' => $orderSku->order->first_name . ' ' . $orderSku->order->last_name,
                    'phone' => $orderSku->order->telephone,
                    'address' => $orderSku->order->address,
                ];
            }

            $statusDelivery = empty($orderSku->status) ? 1 : $orderSku->status;
            $orderStatus = new OrderSkuStatus;
            $statusDeliveryName = $orderStatus::where('id','=', $statusDelivery)->pluck('name');
            $orders[$orderSku->order->id]['products'][] = [
                'sku' => isset($orderSku->vendor_sku) ? $orderSku->vendor_sku : '',
                'name' => $orderSku->name,
                'quantity' => $orderSku->quantity,
                'total' => $orderSku->total - (!empty($orderSku->prepayment) ? $orderSku->prepayment : 0) . ' грн',
                'payment_method' => $statusDeliveryName[0],
                'shipping_method' => $orderSku->order->shipping_method,
                'comment_color' => isset($orderSku->orderSkuStatus) && isset($orderSku->orderSkuStatus->color)
                    ? $orderSku->orderSkuStatus->color
                    : ''
            ];
        }

        $settings = new Settings;

        if ($updateData) {
            // Update getting skus
            \DB::table('order_skus')->whereIn('id', $skusIds)->update(['shipped_status' => 1]);
            // Update order statuses
            \DB::table('orders')
                ->whereIn('id', $ordersIds)
                ->update(['order_status_id' => $settings->getByKey('status_sent_to_vendor')]);
        }

        // Build xlsx file
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // Set title with vendor and current date
        $sheet->mergeCells('A1:I1');
        $sheet->getStyle('A1:I1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->setCellValueByColumnAndRow(
            1,
            1,
            $vendor->getAttributeValue('name') . ' заказ ' . date(config('app.date_format'))
        );

        // Set titles for product and client
        $sheet->mergeCells('A2:F2');
        $sheet->setCellValueByColumnAndRow(1, 2, 'Товар');
        $sheet->mergeCells('G2:I2');
        $sheet->setCellValueByColumnAndRow(7, 2, 'Клиент');
        $sheet->getStyle('A2:I2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $sheet->fromArray(
            ['Артикул', 'Название', 'Количество', 'Цена', 'Доставку оплачивает', 'Метод доставки', 'ФИО', 'Телефон', 'Адрес'],
            null,
            'A3'
        );

        $startOrderCell = 4;
        foreach ($orders as $order) {
            $nextCell = $startOrderCell + count($order['products']);
            $endCell = $nextCell - 1;

            $currentCell = $startOrderCell;
            foreach ($order['products'] as $product) {
                $rgb = !empty($product['comment_color'])
                    ? str_replace('#', '', $product['comment_color'])
                    : null;

                $sheet->fromArray(array_values($product), null, 'A' . $currentCell);
                if (!empty($rgb)) {
                    $sheet->getStyle('F' . $currentCell)
                        ->getFont()
                        ->getColor()
                        ->setRGB($rgb);
                }
                $currentCell++;
            }

            $sheet->mergeCells('G' . $startOrderCell . ':G' . $endCell);
            $sheet->mergeCells('H' . $startOrderCell . ':H' . $endCell);
            $sheet->mergeCells('I' . $startOrderCell . ':I' . $endCell);

            $sheet->fromArray(array_values($order['client']), null, 'G' . $startOrderCell);

            $sheet->getStyle('A' . $startOrderCell . ':I' . $endCell)
                ->getBorders()
                ->getBottom()
                ->setBorderStyle(Border::BORDER_MEDIUM);

            $startOrderCell = $nextCell;
        }

        // Set default with for columns
        $sheet->getColumnDimension('B')->setWidth(40);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(30);
        $sheet->getColumnDimension('F')->setWidth(30);
        $sheet->getColumnDimension('G')->setWidth(25);
        $sheet->getColumnDimension('H')->setWidth(20);
        $sheet->getColumnDimension('I')->setWidth(70);

        // Set outline border
        $productsEndRow = $sheet->getHighestRow();
        $sheet->getStyle('A2:I' . ($productsEndRow))
            ->getBorders()
            ->getOutline()
            ->setBorderStyle(Border::BORDER_MEDIUM);

        //set text format for sku
        $sheet->getStyle('A2:' . 'A' . $productsEndRow)
            ->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_TEXT);

        // Set header borders
        $sheet->getStyle('A2:I3')
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_MEDIUM);

        return new Xlsx($spreadsheet);
    }
}
