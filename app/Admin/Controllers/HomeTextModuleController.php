<?php

namespace App\Admin\Controllers;

use App\Entities\Category;
use App\Entities\CustomerGroup;
use App\Entities\Language;
use App\Entities\Manufacturer;
use App\Entities\Product;
use App\Entities\SiteSetting;
use App\Entities\SkuSpecial;
use App\Entities\StockKeepingUnit;
use App\Http\Controllers\Controller;
use App\Library\Settings;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Form;
use Illuminate\Http\Request;


class HomeTextModuleController extends Controller
{
    protected $settingCode = 'home_text';

    public function index(Content $content, Settings $settings)
    {
        $moduleSettings = $settings->getByCode($this->settingCode);
        return $content
            ->header('Модуль текста на главной')
            ->description('Модуль текста на главной')
            ->row(function (Row $row) use ($moduleSettings) {

                $form = new Form();
                $languages = Language::all();
                foreach ($languages as $language) {
                    $text = isset($moduleSettings) && isset($moduleSettings['descriptions'])
                    && isset($moduleSettings['descriptions'][$language->id])
                        ? $moduleSettings['descriptions'][$language->id]['text'] : '';

                    $form->hidden('descriptions[' . $language->id . '][language_id]')->default($language->id);
                    $form->ckeditor('descriptions[' . $language->id . '][text]', 'Текст ( ' . $language->title . ' )')->default($text);
                }
//                dd($moduleSettings);
                $status = isset($moduleSettings) && isset($moduleSettings['status']) ? $moduleSettings['status'] : 0;
                $form->radio('status', 'Статус')->options([
                    0 => 'Отключено',
                    1 => 'Включено'
                ])->default($status);

                $form->action('home-text-module');
                $form->disablePjax();
                $row->column(12, $form);
            });
    }

    public function update(Request $request)
    {

        SiteSetting::updateByCode($this->settingCode, $request->only(['descriptions', 'status']));
        return redirect()->back();
    }
}
