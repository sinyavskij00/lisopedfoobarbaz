<?php

namespace App\Admin\Controllers;

use App\Entities\Product;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Form;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;

class GenerateRelatedProducts extends Controller
{
    protected $relatedProductsTable = 'product_related';

    public function index(Content $content)
    {
        return $content
            ->header('Модуль массового добавления похожих товаров')
            ->description('Модуль массового добавления похожих товаров')
            ->row(function (Row $row) {
                $form = new Form();

                $form->radio('rewrite', 'Перезаписывать')->options([
                    0 => 'Нет',
                    1 => 'Да',
                ]);

                $form->number('quantity', 'Количество')->default(5)->min(0);

                $form->disablePjax();
                $form->action('mass-related');

                $row->column(12, $form);
            });
    }

    public function update(Request $request)
    {
        $rewrite = intval($request->input('rewrite', 0));
        $quantity = intval($request->input('quantity', 0));


        if ($rewrite === 1) {
            DB::table($this->relatedProductsTable)->delete();
        }

        $excludeProductIds = DB::table($this->relatedProductsTable)
            ->get(['product_id'])
            ->pluck('product_id')
            ->toArray();



        $allProducts = DB::table('products')->leftJoin('stock_keeping_units', function ($join) {
            $join->on('products.id', '=', 'stock_keeping_units.product_id')->limit(1);
        })->select([
            'products.id as product_id',
            'products.category_id',
            'stock_keeping_units.price'
        ])
            ->where('stock_keeping_units.is_main', '=', 1)
            ->whereNotIn('products.id', $excludeProductIds)
            ->get();

        $allProducts = $allProducts->unique('product_id');

        $allProducts->chunk(100)->each(function ($products) use ($allProducts, $quantity) {
            $relatedData = [];
            foreach ($products as $product) {
                $categoryId = $product->category_id;

                $price = $product->price;
                $maxPrice = $price + 4000;
                $minPrice = $price - 1000;

                if (isset($categoryId)) {
                    $appropriateRelated = $products->where('category_id', '=', $categoryId)
                        ->where('product_id', '!=', $product->product_id)
                        ->where('price', '>=', $minPrice)
                        ->where('price', '<=', $maxPrice)
                        ->where('price', '<=', $maxPrice);
                    $relatedProducts = collect();
                    if ($appropriateRelated->count() <= $quantity) {
                        $relatedProducts = $appropriateRelated;
                    } else {
                        $relatedProducts = $appropriateRelated->random($quantity);
                    }
                    foreach ($relatedProducts as $relatedProduct) {
                        $relatedData[] = [
                            'product_id' => $product->product_id,
                            'related_id' => $relatedProduct->product_id
                        ];
                    }
                }
            }
            DB::table($this->relatedProductsTable)->insert($relatedData);
        });


        $success = new MessageBag([
            'title' => 'Похожие товары добавлены',
        ]);
        return back()->with(compact('success'));
    }
}
