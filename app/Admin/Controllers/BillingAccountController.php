<?php

namespace App\Admin\Controllers;

use App\Entities\BillingAccount;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class BillingAccountController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Платежные счета')
            ->description(' ')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('платежного счета')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование')
            ->description('платежного счета')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание')
            ->description('платежного счета')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new BillingAccount);

        $grid->id('Id')->sortable();
        $grid->name('ФЛП')->sortable();
        $grid->employer_number('ЕГРПОУ')->sortable();
        $grid->bank_number('МФО')->sortable();
        $grid->checking_account('Р/С')->sortable();
        $grid->created_at('Создан')->sortable();
        $grid->updated_at('Обновлен')->sortable();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(BillingAccount::findOrFail($id));

        $show->id('Id');
        $show->name('ФЛП');
        $show->employer_number('ЕГРПОУ');
        $show->bank_number('МФО');
        $show->checking_account('Р/С');
        $show->created_at('Создан');
        $show->updated_at('Обновлен');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new BillingAccount);

        $form->text('name', 'ФЛП');
        $form->text('employer_number', 'ЕГРПОУ');
        $form->text('bank_number', 'МФО');
        $form->text('checking_account', 'P/C');

        return $form;
    }
}
