<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class RecacheJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $cacheableRoutes;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($cacheableRoutes)
    {
        $this->cacheableRoutes = $cacheableRoutes;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $routeSlugs = [];
        // get all params and build with routes
        foreach ($this->cacheableRoutes as $route) {
            if (count($route['params']) == 1) {
                foreach ($route['params'] as $param => $v) {
                    $data = explode('.', $v);
                    $table = $data[0];
                    $column = $data[1];
                    // ex.: [ '/foo/{bar}' => [ bar1, bar2, bar3 ] ], ...
                    $routeSlugs[$route['route']] = \DB::table($table)->select($column)->get()->toArray();
                }
            } else if (count($route['params']) == 0) {
                $routeSlugs[$route['route']] = [];
            }
        }

        $readyToCall = [];
        foreach ($routeSlugs as $route => $vals) {
            if (count($vals) > 0) {
                foreach ($vals as $val) {
                    // here you can bind method in braces ....
                    $readyToCall[] = preg_replace('/\{.*\}/', (string)$val->name, (string)$route);
                }
            } else if (0 == count($vals)) {
                $readyToCall[] = $route;
            }
        }

        $results = [];
        $client = \App::make('asyncHTTPClient');
        foreach ($readyToCall as $link) {
            $client->get($link)->then(function (\Psr\Http\Message\ResponseInterface $response) use (&$results, $link) {
                $results[$link] = $response->getBody();
            });
        }
        \App::make('eventLoop')->run();

        \ResponseCacher::store($results);
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        dump($exception->getMessage());
    }
}
