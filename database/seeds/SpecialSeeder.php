<?php

use Illuminate\Database\Seeder;
use App\Entities\Special;
use Faker\Generator as Faker;

class SpecialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countSpecials = 15;
        $images = collect(\Illuminate\Support\Facades\Storage::files('shop_images/specials'));
        $now = now();

        for ($i = 0; $i < $countSpecials; $i++) {
            Special::create([
                'image' => $images->random(1)->first(),
                'status' => 1,
                'slug' => 'special-' . ($i + 1),
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }

        $faker = app()->make(Faker::class);
        $languages = \App\Entities\Language::all();
        Special::chunk(1000, function ($specials) use ($languages, $faker) {
            $descriptions = [];
            foreach ($specials as $special) {
                foreach ($languages as $language) {
                    $descriptions[] = [
                        'special_id' => $special->id,
                        'language_id' => $language->id,
                        'title' => $faker->title,
                        'text' => $faker->realText(3000, 2)
                    ];
                }
            }
            \App\Entities\SpecialDescription::insert($descriptions);
        });
    }
}
