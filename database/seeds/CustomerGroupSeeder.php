<?php

use Illuminate\Database\Seeder;
use App\Entities\CustomerGroup;
use App\Entities\CustomerGroupDescription;
use App\Entities\Language;

class CustomerGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = Language::all();
        $group1 = CustomerGroup::create();
        $group2 = CustomerGroup::create();
        $descriptions1 = [];
        $descriptions2 = [];
        foreach ($languages as $language){
            $descriptions1[] = CustomerGroupDescription::make([
                'language_id' => $language->id,
                'name' => 'Розничный'
            ]);
            $descriptions2[] = CustomerGroupDescription::make([
                'language_id' => $language->id,
                'name' => 'Оптовый'
            ]);
        }
        $group1->descriptions()->saveMany($descriptions1);
        $group2->descriptions()->saveMany($descriptions2);

        //set default group
        $group1->is_default = 1;
        $group1->save();
    }
}
