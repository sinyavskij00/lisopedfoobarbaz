<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use App\Entities\StockKeepingUnit;

class SkuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $skuCount = 3000;

        $sizes = \App\Entities\Size::all();
        $colors = \App\Entities\Color::all();
        $years = \App\Entities\Year::all();

        $vendors = \App\Entities\Vendor::all();

        for ($b = 0; $b < ceil($skuCount / 1000); $b++) {
            $skuData = [];
            for ($i = 0; $i < 1000; $i++) {
                $price = random_int(100, 100000);
                $skuData[] = [
                    'sku' => str_random(5) . $i,
                    'quantity' => random_int(0, 100),
                    'price' => $price,
                    'vendor_price' => intval($price * 0.8),
                    'vendor_id' => $vendors->random(1)->first()->id,
                    'model' => 'model-' . $i,
                    'size_id' => $sizes->random(1)->first()->id,
                    'color_id' => $colors->random(1)->first()->id,
                    'year_id' => $years->random(1)->first()->id,
                ];
            }
            DB::table('stock_keeping_units')->insert($skuData);
        }


        $productImages = collect(Storage::files('shop_images/products'));

        StockKeepingUnit::chunk(3000, function ($units) use ($productImages) {
            $imageData = [];
            foreach ($units as $unit) {
                $images = $productImages->random(5);
                foreach ($images as $image) {
                    $imageData[] = [
                        'unit_id' => $unit->id,
                        'image' => $image
                    ];
                }
            }
            DB::table('unit_images')->insert($imageData);
        });
    }
}
