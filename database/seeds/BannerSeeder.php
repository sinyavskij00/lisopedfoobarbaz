<?php

use Illuminate\Database\Seeder;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = \App\Entities\Category::all();

        $banner1 = \App\Entities\Banner::create([
            'background_image' => 'shop_images/banners/banner_1_bg.jpg',
            'main_image' => 'shop_images/banners/bicycle_1.png',
            'status' => 1
        ]);

        $languages = \App\Entities\Language::all();

        $descriptions = [];
        foreach ($languages as $language){
            $link = 'some/link';
            if($categories->count() > 0){
                $link = route('category_path', category_path($categories->random(1)->first()->id));
            }

            $descriptions[] = [
                'language_id' => $language->id,
                'banner_id' => $banner1->id,
                'text' => 'Новая коллекция',
                'link' => $link,
                'link_title' => 'Горные велосипеды',
                'link_subtitle' => 'Merida Reacto 7000E'
            ];
        }
        $banner1->descriptions()->createMany($descriptions);

        $banner2 = \App\Entities\Banner::create([
            'background_image' => 'shop_images/banners/banner_4_bg.jpg',
            'status' => 1
        ]);

        $descriptions = [];
        foreach ($languages as $language){
            $link = 'some/link';
            if($categories->count() > 0){
                $link = route('category_path', category_path($categories->random(1)->first()->id));
            }
            $descriptions[] = [
                'language_id' => $language->id,
                'banner_id' => $banner1->id,
                'text' => 'На наших великах гоняют чемпионы! Истории из жизни, проблемы и советы',
                'link' => $link,
                'link_title' => 'Горные велосипеды',
            ];
        }
        $banner2->descriptions()->createMany($descriptions);
    }
}
