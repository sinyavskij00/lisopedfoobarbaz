<?php

use Illuminate\Database\Seeder;

class AdminMenuDeliverySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('admin_menu')->where('uri', '=', 'pages/delivery')->delete();
        \DB::table('admin_menu')->insert(array (
            'parent_id' => 76,
            'order' => 4,
            'title' => 'Доставка и оплата',
            'icon' => 'fa-globe',
            'uri' => 'pages/delivery',
            'created_at' => NULL,
            'updated_at' => NULL,
        ));
    }
}
