<?php

use Illuminate\Database\Seeder;

class OrderProfitabilityTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('order_profitability_types')->delete();
        
        \DB::table('order_profitability_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Расход 1',
                'created_at' => '2018-10-10 15:43:45',
                'updated_at' => '2018-10-10 15:43:45',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Расход 2',
                'created_at' => '2018-10-10 15:43:49',
                'updated_at' => '2018-10-10 15:43:49',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Доход 1',
                'created_at' => '2018-10-10 15:43:53',
                'updated_at' => '2018-10-10 15:43:53',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Доход 2',
                'created_at' => '2018-10-10 15:43:57',
                'updated_at' => '2018-10-10 15:43:57',
            ),
        ));
        
        
    }
}