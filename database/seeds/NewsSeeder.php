<?php

use Illuminate\Database\Seeder;
use App\Entities\News;
use Faker\Generator as Faker;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newsCount = 20;
        $images = collect(Storage::files('shop_images/newses'));
        $now = now();
        $data = [];
        for ($i = 0; $i < $newsCount; $i++) {
            $data[] = [
                'slug' => 'news-' . ($i + 1),
                'views' => random_int(0, 10000),
                'status' => 1,
                'image' => $images->random(1)->first(),
                'created_at' => $now,
                'updated_at' => $now
            ];
        }
        News::insert($data);

        $languages = \App\Entities\Language::all();
        $faker = app()->make(Faker::class);
        News::chunk(1000, function ($newses) use ($languages, $faker, $now) {
            $descriptions = [];
            $reviews = [];
            foreach ($newses as $news) {
                foreach ($languages as $language) {
                    $descriptions[] = [
                        'news_id' => $news->id,
                        'language_id' => $language->id,
                        'title' => 'news' . $news->id . ' ' . $language->slug,
                        'text' => $faker->paragraphs(60, true),
                        'meta_title' => 'meta title news ' . $news->id,
                        'meta_description' => 'meta description news ' . $news->id,
                        'meta_keywords' => 'meta tags news ' . $news->id,
                    ];
                }

                for ($i = 0; $i < 2; $i++) {
                    $reviews[] = [
                        'news_id' => $news->id,
                        'text' => "Разнообразный и богатый опыт начало повседневной работы по формированию позиции играет важную роль в формировании позиций, занимаемых участниками в отношении поставленных задач. Равным образом постоянное информационно-пропагандистское обеспечение нашей деятельности требуют от нас анализа форм развития",
                        'name' => $faker->firstName . ' ' . $faker->lastName,
                        'email' => $faker->email,
                        'status' => 1,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ];
                }
            }

            \App\Entities\NewsDescription::insert($descriptions);
            \App\Entities\NewsReview::insert($reviews);
        });

        \App\Entities\NewsReview::chunk(1000, function ($reviews) use ($faker, $now) {
            $data = [];
            foreach ($reviews as $review) {
                for ($i = 0; $i < 2; $i++) {
                    $data[] = [
                        'news_id' => $review->news_id,
                        'parent_id' => $review->id,
                        'text' => "Разнообразный и богатый опыт начало повседневной работы по формированию позиции играет важную роль в формировании позиций, занимаемых участниками в отношении поставленных задач. Равным образом постоянное информационно-пропагандистское обеспечение нашей деятельности требуют от нас анализа форм развития",
                        'name' => $faker->firstName . ' ' . $faker->lastName,
                        'email' => $faker->email,
                        'status' => 1,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ];
                }
            }
            \App\Entities\NewsReview::insert($data);
        });
        \App\Entities\NewsReview::fixTree();

        $images = collect(\Illuminate\Support\Facades\Storage::files('shop_images/reviews'));
        \App\Entities\NewsReview::chunk(1000, function ($reviews) use ($images) {
            $data = [];
            foreach ($reviews as $review){
                $data[] = [
                    'image' => $images->random(1)->first(),
                    'news_review_id' => $review->id
                ];
            }
            \App\Entities\NewsReviewImage::insert($data);
        });
    }
}
