<?php

use Illuminate\Database\Seeder;

class SkuDiscountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date_yesterday = \Illuminate\Support\Carbon::yesterday();
        $date_tomorrow = \Illuminate\Support\Carbon::tomorrow();
        $customerGroups = \App\Entities\CustomerGroup::all();


        \App\Entities\StockKeepingUnit::chunk(1000, function ($skus) use ($date_yesterday, $date_tomorrow, $customerGroups) {
            $discounts = [];
            foreach ($skus as $sku) {
                $discounts[] = [
                    'customer_group_id' => $customerGroups->random(1)->first()->id,
                    'priority' => random_int(-10, 10),
                    'unit_id' => $sku->id,
                    'quantity' => 3,
                    'price' => intval($sku->price * 0.9),
                    'date_start' => $date_yesterday,
                    'date_end' => $date_tomorrow,
                ];
            }
            DB::table('sku_discounts')->insert($discounts);
        });
    }
}
