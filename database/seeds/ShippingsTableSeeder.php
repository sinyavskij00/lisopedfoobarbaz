<?php

use Illuminate\Database\Seeder;

class ShippingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('shippings')->delete();
        
        \DB::table('shippings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'office',
                'price' => '0.00',
                'status' => 1,
                'created_at' => '2018-10-08 13:38:06',
                'updated_at' => '2018-10-08 13:38:06',
            ),
            1 => 
            array (
                'id' => 3,
                'code' => 'courier',
                'price' => '100.00',
                'status' => 1,
                'created_at' => '2018-10-08 13:39:09',
                'updated_at' => '2018-10-08 13:39:09',
            ),
        ));
        
        
    }
}