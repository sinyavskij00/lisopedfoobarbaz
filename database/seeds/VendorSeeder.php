<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class VendorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vendorCount = 10;

        $data = [];
        $now = now();
        $faker = app()->make(Faker::class);
        for($i = 0; $i < $vendorCount; $i++){
            $data[] = [
                'name' => 'Поставщик ' . ($i + 1),
                'telephone_1' => $faker->phoneNumber(),
                'telephone_2' => $faker->phoneNumber(),
                'email' => $faker->email(),
                'email2' => $faker->email(),
                'email3' => $faker->email(),
                'address' => $faker->address(),
                'comment' => $faker->realText(100),
                'site' => $faker->url(),
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        \App\Entities\Vendor::insert($data);
    }
}
