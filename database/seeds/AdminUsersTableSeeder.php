<?php

use Illuminate\Database\Seeder;

class AdminUsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_users')->delete();
        
        \DB::table('admin_users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'username' => 'admin',
                'password' => '$2y$10$D/xiaQH/FM8PQlRC.jjpyu3T3u7W1QDNAStsmmrOtVwrUcAOKtecq',
                'name' => 'Administrator',
                'avatar' => NULL,
                'remember_token' => NULL,
                'created_at' => '2018-10-04 10:33:25',
                'updated_at' => '2018-10-04 10:33:25',
            ),
        ));
        
        
    }
}