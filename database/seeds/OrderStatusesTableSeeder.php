<?php

use Illuminate\Database\Seeder;

class OrderStatusesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('order_statuses')->delete();
        
        \DB::table('order_statuses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'color' => '#849fa5',
                'is_new' => 1,
                'is_finished' => 0,
                'created_at' => '2018-11-02 16:00:24',
                'updated_at' => '2018-11-02 16:15:09',
            ),
            1 => 
            array (
                'id' => 2,
                'color' => '#849fa5',
                'is_new' => 0,
                'is_finished' => 0,
                'created_at' => '2018-11-02 16:00:24',
                'updated_at' => '2018-11-02 16:15:09',
            ),
            2 => 
            array (
                'id' => 3,
                'color' => '#849fa5',
                'is_new' => 0,
                'is_finished' => 0,
                'created_at' => '2018-11-02 16:00:24',
                'updated_at' => '2018-11-02 16:15:09',
            ),
            3 => 
            array (
                'id' => 4,
                'color' => '#849fa5',
                'is_new' => 0,
                'is_finished' => 0,
                'created_at' => '2018-11-02 16:00:24',
                'updated_at' => '2018-11-02 16:15:09',
            ),
            4 => 
            array (
                'id' => 5,
                'color' => '#849fa5',
                'is_new' => 0,
                'is_finished' => 0,
                'created_at' => '2018-11-02 16:00:24',
                'updated_at' => '2018-11-02 16:15:09',
            ),
            5 => 
            array (
                'id' => 6,
                'color' => '#849fa5',
                'is_new' => 0,
                'is_finished' => 0,
                'created_at' => '2018-11-02 16:00:25',
                'updated_at' => '2018-11-02 16:15:09',
            ),
            6 => 
            array (
                'id' => 7,
                'color' => '#849fa5',
                'is_new' => 0,
                'is_finished' => 0,
                'created_at' => '2018-11-02 16:00:25',
                'updated_at' => '2018-11-02 16:15:09',
            ),
            7 => 
            array (
                'id' => 8,
                'color' => '#849fa5',
                'is_new' => 0,
                'is_finished' => 0,
                'created_at' => '2018-11-02 16:00:25',
                'updated_at' => '2018-11-02 16:15:09',
            ),
            8 => 
            array (
                'id' => 9,
                'color' => '#849fa5',
                'is_new' => 0,
                'is_finished' => 0,
                'created_at' => '2018-11-02 16:00:25',
                'updated_at' => '2018-11-02 16:15:09',
            ),
            9 => 
            array (
                'id' => 10,
                'color' => '#849fa5',
                'is_new' => 0,
                'is_finished' => 0,
                'created_at' => '2018-11-02 16:00:25',
                'updated_at' => '2018-11-02 16:15:09',
            ),
            10 => 
            array (
                'id' => 11,
                'color' => '#849fa5',
                'is_new' => 0,
                'is_finished' => 1,
                'created_at' => '2018-11-02 16:00:25',
                'updated_at' => '2018-11-02 16:15:09',
            ),
        ));
        
        
    }
}