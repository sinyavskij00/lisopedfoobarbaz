<?php

use Illuminate\Database\Seeder;

class AdminMenuSharesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('admin_menu')->where('uri', '=', 'shares')->delete();
        \DB::table('admin_menu')->insert(array (
            'parent_id' => 76,
            'order' => 28,
            'title' => 'Расширенная акция',
            'icon' => 'fa-adn',
            'uri' => 'shares',
            'created_at' => NULL,
            'updated_at' => NULL,
        ));
    }
}
