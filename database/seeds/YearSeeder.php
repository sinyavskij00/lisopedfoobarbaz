<?php

use Illuminate\Database\Seeder;

class YearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $years = [2016, 2017, 2018];
        $now = now();

        $data = [];
        foreach ($years as $year) {
            $data[] = [
                'value' => $year,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        \App\Entities\Year::insert($data);
    }
}
