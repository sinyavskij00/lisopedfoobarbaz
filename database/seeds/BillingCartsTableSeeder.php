<?php

use Illuminate\Database\Seeder;

class BillingCartsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('billing_carts')->delete();
        
        \DB::table('billing_carts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Карта 1',
                'name' => 'Иванов Иван Иванович',
                'number' => '1234 1234 1234 1234',
                'created_at' => '2018-10-18 12:09:04',
                'updated_at' => '2018-10-18 12:09:04',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Карта 2',
                'name' => 'Петров Петр Петрович',
                'number' => '1478 8963 6541 1236',
                'created_at' => '2018-10-18 12:09:36',
                'updated_at' => '2018-10-18 12:09:36',
            ),
        ));
        
        
    }
}