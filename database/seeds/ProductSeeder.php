<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use App\Entities\Product;
use App\Entities\Category;
use App\Entities\Manufacturer;
use App\Entities\StockKeepingUnit;
use Faker\Generator as Faker;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = \App\Entities\Language::all();
        $categories = Category::all();
        $manufacturers = Manufacturer::all();

        //products creating
        $timestamp = \Illuminate\Support\Carbon::now();
        $productImages = collect(Storage::files('shop_images/products'));

        $productsCount = 500;

        for ($b = 0; $b < ceil($productsCount / 1000); $b++) {
            $data = [];
            for ($i = 0; $i < 1000; $i++) {
                $category = $categories->random(1)->first();
                $manufacturer = $manufacturers->random(1)->first();

                $data[] = [
                    'category_id' => $category->id,
                    'slug' => 'product-' . ($i + ($b * 1000) + 1),
                    'manufacturer_id' => $manufacturer->id,
                    'image' => $productImages->random(1)->first(),
                    'is_new' => random_int(0, 1),
                    'is_hit' => random_int(0, 1),
                    'is_our_choice' => random_int(0, 1),
                    'status' => 1,
                    'created_at' => $timestamp,
                    'updated_at' => $timestamp,
                    'min_weight' => random_int(20, 40),
                    'max_weight' => random_int(80, 100),
                    'min_height' => random_int(100, 120),
                    'max_height' => random_int(160, 190),
                    'gender' => random_int(1, 3), //1-man, 2-woman, 3 - child
                    'trace_type' => random_int(1, 4)
                ];
            }
            Product::insert($data);
        }

        $faker = app()->make(Faker::class);
        Product::chunk(2000, function ($products) use ($languages, $faker) {
            //fill product description
            $data = [];

            foreach ($products as $product) {
                foreach ($languages as $language) {
                    $info = 'товар ' . ($product->id);
                    $data[] = [
                        'product_id' => ($product->id),
                        'language_id' => $language->id,
                        'name' => $info . ' ' . $language->slug,
                        'description' => $faker->paragraphs(6, true),
                        'meta_title' => $info,
                        'meta_description' => $info,
                        'meta_keywords' => $info,
                    ];
                }
            }

            DB::table('product_descriptions')->insert($data);
        });

        //related
        Product::chunk(2000, function ($products) {
            $data = [];
            foreach ($products as $product) {
                $relatedProducts = $products->random(10);
                foreach ($relatedProducts as $relatedProduct) {
                    $data[] = [
                        'product_id' => $product->id,
                        'related_id' => $relatedProduct->id
                    ];
                }
            }
            DB::table('product_related')->insert($data);
        });

        $productIds = Product::get(['id']);
        $unitsIds = StockKeepingUnit::get(['id'])->pluck('id');
        foreach ($unitsIds->chunk(5) as $skus) {
            $productId = $productIds->random(1)->first()->id;
            StockKeepingUnit::whereIn('id', $skus->toArray())->update(['product_id' => $productId]);
        }

        Product::whereDoesntHave('skus')->update(['status' => 0]);

        Product::chunk(1000, function ($products) {
            $videoLink = 'https://www.youtube.com/embed/Nz3RxvztB_0?rel=0&amp;showinfo=0';
            $data = [];
            foreach ($products as $product) {
                $videoCount = random_int(0, 5);
                for ($i = 0; $i < $videoCount; $i++) {
                    $data[] = [
                        'video' => $videoLink,
                        'product_id' => $product->id
                    ];
                }
            }
            \App\Entities\ProductVideo::insert($data);
        });

        Product::chunk(500, function ($products) {
            $products->load('skus');
            foreach ($products as $product) {
                if($product->skus->count() > 0){
                    $mainSku = $product->skus->random(1)->first();
                    if (isset($mainSku)) {
                        $mainSku->is_main = 1;
                        $mainSku->save();
                    }
                }
            }
        });
    }
}
