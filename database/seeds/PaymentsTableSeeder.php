<?php

use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payments')->delete();
        
        \DB::table('payments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'bankCart',
                'confirm_status_id' => 1,
                'status' => 1,
                'created_at' => '2018-10-08 13:37:25',
                'updated_at' => '2018-10-08 13:37:25',
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'receipt',
                'confirm_status_id' => 1,
                'status' => 1,
                'created_at' => '2018-10-08 13:37:38',
                'updated_at' => '2018-10-08 13:37:38',
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'liqpay',
                'confirm_status_id' => 1,
                'status' => 1,
                'created_at' => '2018-10-08 13:37:48',
                'updated_at' => '2018-10-08 13:37:48',
            ),
        ));
        
        
    }
}