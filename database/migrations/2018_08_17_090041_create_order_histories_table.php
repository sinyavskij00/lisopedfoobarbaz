<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('admin_id')->nullable();
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('order_status_id');
            $table->unsignedInteger('billing_cart_id')->nullable();
            $table->unsignedInteger('billing_account_id')->nullable();
            $table->tinyInteger('notify')->default(0);
            $table->text('public_comment')->nullable();
            $table->text('private_comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_histories');
    }
}
