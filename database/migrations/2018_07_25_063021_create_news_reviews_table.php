<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('news_id')->index();
            $table->string('name');
            $table->string('email')->nullable();
            $table->text('text');
            $table->tinyInteger('status')->default(0);
            $table->nestedSet();
            $table->timestamps();
        });

        Schema::create('news_review_images', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('news_review_id');
            $table->string('image');

            $table->foreign('news_review_id')
                ->references('id')->on('news_reviews')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_reviews');
        Schema::dropIfExists('news_review_images');
    }
}
