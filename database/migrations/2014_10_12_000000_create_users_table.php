<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name')->default('');
            $table->string('patronymic')->default('');
            $table->string('telephone')->default('');
            $table->string('address')->default('');
            $table->date('birthday')->nullable();
            $table->string('city_ref')->default('')->nullable();
            $table->string('store_ref')->default('')->nullable();
            $table->integer('customer_group_id')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->tinyInteger('subscribe')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('customer_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('is_default')->default(0);
            $table->timestamps();
        });

        Schema::create('customer_group_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('customer_group_id');
            $table->unsignedInteger('language_id');
            $table->string('name');
            $table->unique(['customer_group_id', 'language_id'], 'group_id_lang_id');

            $table->foreign('customer_group_id')
                ->references('id')->on('customer_groups')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('customer_groups');
        Schema::dropIfExists('customer_group_descriptions');
    }
}
