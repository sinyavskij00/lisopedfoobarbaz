<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewPostTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_post_cities', function (Blueprint $table) {
            $table->increments('id');
            $table->text('Description')->nullable();
            $table->text('DescriptionRu')->nullable();
            $table->text('Ref')->nullable();
            $table->text('Area')->nullable();
            $table->text('SettlementType')->nullable();
            $table->text('SettlementTypeDescription')->nullable();
            $table->text('SettlementTypeDescriptionRu')->nullable();
            $table->integer('CityID');
        });

        Schema::create('new_post_warehouses', function (Blueprint $table) {
            $table->increments('id');
            $table->text('Description')->nullable();
            $table->text('DescriptionRu')->nullable();
            $table->text('ShortAddress')->nullable();
            $table->text('ShortAddressRu')->nullable();
            $table->text('TypeOfWarehouse')->nullable();
            $table->text('Ref')->nullable();
            $table->text('CityRef')->nullable();
            $table->text('CityDescription')->nullable();
            $table->text('CityDescriptionRu')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_post_cities');
        Schema::dropIfExists('new_post_warehouses');
    }
}
