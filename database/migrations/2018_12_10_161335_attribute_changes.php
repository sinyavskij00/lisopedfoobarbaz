<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AttributeChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('attributes_values_descriptions');
        Schema::dropIfExists('attributes_values_to_product');
        Schema::dropIfExists('attributes_values');

        Schema::table('attributes', function (Blueprint $table) {
            $table->dropColumn('is_display');
//            $table->dropColumn('slug');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('attribute_value_1');
            $table->dropColumn('attribute_value_2');
            $table->dropColumn('attribute_value_3');
        });

        Schema::create('product_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('attribute_id')->index();
            $table->unsignedInteger('product_id')->index();
            $table->unsignedInteger('language_id')->index();
            $table->string('text')->nullable();
            $table->integer('sort')->default(0); //first three for product item
            $table->string('slug', 100)->default('');

            $table->foreign('attribute_id')
                ->references('id')->on('attributes')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')->on('languages')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('attributes_values', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('attribute_id')->index();
            $table->string('slug')->unique();

            $table->foreign('attribute_id')
                ->references('id')->on('attributes')
                ->onDelete('cascade');
        });

        Schema::create('attributes_values_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('attribute_value_id')->index();
            $table->unsignedInteger('language_id');
            $table->string('value');
            $table->string('filter_name')->default('')->nullable();

            $table->unique(['attribute_value_id', 'language_id'], 'attr_val_lang_id');

            $table->foreign('attribute_value_id')
                ->references('id')->on('attributes_values')
                ->onDelete('cascade');
        });

        Schema::create('attributes_values_to_product', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->index();
            $table->unsignedInteger('attribute_value_id')->index();
            $table->unique(['product_id', 'attribute_value_id'], 'prod_attr_value');

            $table->foreign('attribute_value_id')
                ->references('id')->on('attributes_values')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('cascade');
        });

        Schema::table('attributes', function (Blueprint $table) {
            $table->tinyInteger('is_display')->default(0)->after('attribute_group_id');
            $table->string('slug')->unique()->after('sort');
        });

        Schema::dropIfExists('product_attributes');

        Schema::table('products', function (Blueprint $table) {
            $table->unsignedInteger('attribute_value_1')->nullable()->after('status');
            $table->unsignedInteger('attribute_value_2')->nullable()->after('attribute_value_1');
            $table->unsignedInteger('attribute_value_3')->nullable()->after('attribute_value_2');
        });
    }
}
