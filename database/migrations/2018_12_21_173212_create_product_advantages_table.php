<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductAdvantagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_advantages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('link')->nullable();
            $table->string('icon')->nullable();
            $table->boolean('status')->default(0);
            $table->timestamps();
        });

        Schema::create('product_advantage_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_advantage_id');
            $table->unsignedInteger('language_id');
            $table->string('title')->nullable();
            $table->text('text')->nullable();

            $table->foreign('product_advantage_id')
                ->references('id')
                ->on('product_advantages')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_advantages');
        Schema::dropIfExists('product_advantage_descriptions');
    }
}
