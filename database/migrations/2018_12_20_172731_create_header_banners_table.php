<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeaderBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('header_banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('background_color');
            $table->string('text_color');
            $table->boolean('status');
            $table->timestamps();
        });

        Schema::create('header_banner_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('banner_id');
            $table->unsignedInteger('language_id');
            $table->text('text')->nullable();

            $table->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('header_banners');
        Schema::dropIfExists('header_banner_descriptions');
    }
}
