<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('question_group_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('question_group_id');
            $table->unsignedInteger('language_id');
            $table->string('name');
            $table->unique(['question_group_id', 'language_id']);

            $table->foreign('question_group_id')
                ->references('id')->on('question_groups')
                ->onDelete('cascade');
        });

        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('question_group_id')->nullable();
            $table->timestamps();
        });

        Schema::create('question_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('question_id')->index();
            $table->unsignedInteger('language_id');
            $table->text('answer');
            $table->string('question_text');
            $table->unique(['question_id', 'language_id']);

            $table->foreign('question_id')
                ->references('id')->on('questions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
        Schema::dropIfExists('question_descriptions');
    }
}
