<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderSkusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_skus', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id')->index();
            $table->string('sku')->default('')->nullable();
            $table->string('vendor_sku')->default('')->nullable();
            $table->unsignedInteger('sku_id')->nullable();
            $table->string('name')->default('');
            $table->string('model')->default('');
            $table->string('size')->default('');
            $table->string('color')->default('');
            $table->string('year')->default('');
            $table->integer('quantity')->default(1);
            $table->decimal('price')->default(0);
            $table->decimal('vendor_price')->default(0);
            $table->decimal('total')->default(0);
            $table->decimal('vendor_total')->default(0);
            $table->decimal('reward')->default(0);
            $table->timestamps();

            $table->foreign('order_id')
                ->references('id')->on('orders')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_skus');
    }
}
