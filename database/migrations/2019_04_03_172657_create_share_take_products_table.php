<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShareTakeProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('share_take_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('share_id')->unsigned()->default(0);
            $table->integer('product_id')->unsigned()->default(0);
            $table->foreign('share_id')->references('id')->on('shares');
            $table->foreign('product_id')->references('id')->on('products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('share_take_products');
    }
}
