<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shares', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('special_id')->unsigned()->default(0);
            $table->foreign('special_id')->references('id')->on('specials');
            $table->string('slug')->unique();
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->text('banner_image')->nullable();
            $table->string('status')->default('1');
            $table->bigInteger('views')->default('0');
            $table->dateTime('date_start')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('date_end')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shares');
    }
}
