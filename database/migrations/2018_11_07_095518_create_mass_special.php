<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMassSpecial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mass_specials', function (Blueprint $table) {
            $table->increments('id');
            $table->json('manufacturers')->nullable();
            $table->json('categories')->nullable();
            $table->unsignedInteger('customer_group_id');
            $table->integer('priority');
            $table->decimal('percent');
            $table->date('date_start');
            $table->date('date_end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mass_specials');
    }
}
