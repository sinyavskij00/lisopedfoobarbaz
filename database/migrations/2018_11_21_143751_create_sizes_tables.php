<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSizesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('size_tables', function (Blueprint $table) {
            $table->increments('id');
        });

        Schema::create('size_table_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('table_id');
            $table->unsignedInteger('language_id');
            $table->string('title')->default('')->nullable();
            $table->text('text')->nullable();

            $table->foreign('table_id')
                ->references('id')->on('size_tables')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')->on('languages')
                ->onDelete('cascade');
        });

        Schema::create('size_table_to_product', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('table_id');

            $table->foreign('table_id')
                ->references('id')->on('size_tables')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('size_tables');
        Schema::dropIfExists('size_table_descriptions');
        Schema::dropIfExists('size_table_to_product');
    }
}
