<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->tinyInteger('status')->default(0);
            $table->string('slug')->unique();
            $table->timestamps();
        });

        Schema::create('special_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('special_id')->index();
            $table->unsignedInteger('language_id');
            $table->string('title')->default('');
            $table->text('text')->nullable();
            $table->unique(['special_id', 'language_id'], 'special_id_to_lang_id');

            $table->foreign('special_id')
                ->references('id')->on('specials')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specials');
        Schema::dropIfExists('special_descriptions');
    }
}
