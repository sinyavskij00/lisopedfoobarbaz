<?php

use Faker\Generator as Faker;
use App\Entities\Category;
use App\Entities\Manufacturer;

$factory->define(App\Entities\Product::class, function (Faker $faker) {
    $category = Category::all()->random(1)->first();
    $manufacturer = Manufacturer::all()->random(1)->first();

    return [
        'category_id' => $category->id,
        'price' => rand(1, 50000),
        'manufacturer_id' => $manufacturer->id,
    ];
});
