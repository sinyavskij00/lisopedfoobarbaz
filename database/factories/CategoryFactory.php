<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Category::class, function (Faker $faker) {
    return [
        'slug' => $faker->unique()->slug(2)
    ];
});
